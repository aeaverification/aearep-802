# JEP-2019-1128 Internal mobility in the US: The important role of the foreign-born population Validation and Replication results

SUMMARY
-------
The replication was reattempted using the latest replication package, plus several files provided by authors subsequently. No attempt to run the data preparation programs was made. All Tables and Figures were reproduced. 

We will waive the requirement to reproduce the analysis files from raw data sources, it is quite complex, and predates the AEA Code and Availability Policy of 2019. This makes this replication package only partially reproducible.

The replication package is accepted.

- [x] partial replication (data creation not fully functional, still issues in manuscript tables)

### Previously

> [We REQUESTED] Please update the reference to Grelewicz' data to account for the newly available openICPSR deposit with his data
    
- The README references citation now gives a doi link to Grelewicz's openICPSR deposit.

>  [NOTE] We do not penalize the authors for the unavailability of data subject to the actions of others (Foote). However, we encourage the authors to provide an update to their data deposit that ensures complete reproducibility.

- The README now elaborates on Foote's data, stating that it's an intermediate data file produced using Foote's cleaning dofiles and taking National Archives data as input. The cleaning dofiles are provided.

> [We REQUESTED] Please ensure that all code is provided, and that code and README are consistent.
    
- No mention of the code to produce ```license_graph.dta``` from Grelewicz's four data files was mentioned in the latest response from the authors, and the replicator could still not identify such code in the replication package

> [We REQUESTED] The setup program that installs all packages should specify all necessary commands. 

- Done. The only missing package encountered during the previous replication attempt, `distinct`, has been added to the setup program, and no other missing packages were encountered.

> [We REQUESTED] Please provide debugged code, addressing the issues identified in this report.

- Mostly done.

Much detail below, including how previous issues were addressed.

General 
-------

We suggest following the structure of our  [template README](https://aeadataeditor.github.io/aea-de-guidance/template-README.html) to ensure that all required information is provided.

Data description
----------------

### Data Sources

Notes:

- Insufficient directions for downloading IPUMS census data from the IPUMS website, although these data seem to be included in the replication package
- Import_irs.do automatically downloads the IRS data
- No codebook or summary statistics are provided for most data, although the primary data sources, the IRS and Census data, are publicly available.
    - for the Census .dta files given in the replication package, variable labels are present


####  Census data from IPUMS:

- Included in replication package
- No codebook or summary statistics are provided for this data. However there is some description of this data in the section "1. Measures of total mobility: Updating the trends" of the introduction.
- There is a data citation for these data under Ruggles et. al. in the references section

> [We REQUESTED] Please make it clear in the README that the data have been provided as part of the archive. 

- done, readme clarifies that the raw IPUMS data are in the package



#### IRS migration data from the Statistics on Income website (1990-2009)

- According to the readme, these data are not present in the replication package and are intended to be downloaded by the import_irs.do and import_irs_state.do. For access information, README directs replicator to URLs coded into the do-files.
- No codebook or summary statistics are provided for this data. However there is some description of this data in the section "1. Measures of total mobility: Updating the trends" of the introduction.
- cited in README

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).

- Data is now cited in the README; no updated manuscript provided

> [We REQUESTED] Please provide a clear description of access modality and location for this dataset. This includes corrected URLs, visible in the README, and functional in the Stata programs that are meant to download these data. It may be convenient to provide these data files - in their raw form - as part of the replication package.
    
- README states raw data are not included in the replication package and points to the URLs coded into import_irs.do and import_irs_state.do but does not include those URLs in the README

- README clarifies that the analysis files output by import_irs.do, import_irs_state.do, and IRS_cty_to_cz.do are in fact included in the replication package

#### Additional IRS migration data from Andrew Foote covering the period 1983-1989

- In the latest Readme, authors elaborate that this is an intermediate data set produced by Foote's cleaning dofiles taking national archives data as input
- No codebook or summary statistics are provided for these data.
- cited in README

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).
    
- data is now cited in the README; no updated manuscript provided

> [We REQUESTED] Please provide this dataset. You should ensure that you have permission to redistribute the data.
    
- Dataset is not, URL to National Archives provided. 

#### Data from Joshua Grelewicz on licensing and foreign-born used to produce figure 8

- No codebook or summary statistics are provided for these data.
- cited in README

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references).

- data is now cited in the README; no updated manuscript provided

> [We REQUESTED] Please provide this dataset. You should ensure that you have permission to redistribute the data.
    
- README points to DOI archive of input files.
    
- However, the data file used to produce the single table using Grelewicz's data in the manuscript is present in the replication package


### Analysis Data Files

- [x] No analysis data file mentioned
- [ ] Analysis data files mentioned, not provided (explain reasons below)
- [ ] Analysis data files mentioned, provided. File names listed below.

- While not mentioned, it's apparent that license_graph.dta is an analysis data file generated from the data attributed to Joshua Grelewicz in the README

### ICPSR data deposit

#### Requirements 

- [X] README is in TXT, MD, PDF format
- [X] openICPSR deposit has no ZIP files
- [X] Title conforms to guidance (starts with "Data and Code for:", is properly capitalized)
- [x] Authors (with affiliations) are listed in the same order as on the paper

> [We REQUESTED] Please ensure that a ASCII (txt), Markdown (md), or PDF version of the README are available in the data and code deposit.
    
- Done, README in PDF format is now in the openICPSR deposit

> [We REQUESTED] openICPSR should not have ZIP files visible. ZIP files should be uploaded to openICPSR via "Import from ZIP" instead of "Upload Files". Please delete the ZIP files, and re-upload using the "Import from ZIP" function.

- Done, ZIP files have been removed

#### Deposit Metadata

- [x] JEL Classification (required)
- [x] Manuscript Number (required)
- [x] Subject Terms (highly recommended)
- [x] Geographic coverage (highly recommended)
- [x] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [ ] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [ ] Units of Observation (suggested)

- [NOTE] openICPSR metadata is sufficient.

- Manuscript number is given as "1128" and not "JEP-2019-1128"

- [SUGGESTED] We suggest you update the fields marked as (suggested) above, in order to improve findability of your data and code supplement. 

For additional guidance, see [https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html](https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html).

Data checks
-----------

- All provided data is readable and in either .dta or .xlsx format
- PII_stata_scan.do identified 12 variables. However, all of these variables are from datasets whose observations are geographic areas.


Code description
----------------

- There are many do files, all of which are necessary to run the replication, called by masterdo.do
    - several of these are data preparation programs
        - clean_census.do processes the raw IPUMS census data in the replication package into analysis data files
        - import_irs.do and import_irs_state.do directly download the IRS data from the IRS tax migration data website mentioned in the README and save them as analysis data files
        - IRS_cty_to_cz.do also produced analysis data files
        - The README states that the output of import_irs.do, import_irs_state.do, and IRS_cty_to_cz.do are all included in the replication package
    - the following do-files produce the manuscript's tables and figures:
        - summstats_demogr_decomp.do


        - summstats_census_irs_19802017_baselinegraphs.do


        - summstats_census_19802017_baselinegraphs.do


        - directed_census_19802017.do


        - tableA2.do

        - directed_census_fromabroad_20002017.do

        - directed_cz_census_fromabroad_20052017.do


        - graph_licensing_fborn.do

- The README claims graph_licensing_fborn.do reproduces figure A2, which uses Joshua Grelewicz's data. However, the only figure in the manuscript (first draft) available to us that mentions use of Grelewicz's data is figure 8, which is the only figure produced by the do-file.
- No code is given to generate license_graph.dta from the data attributed to Joshua Grelewicz in the README.

> [We REQUESTED] Please ensure that all code is provided, and that code and README are consistent.

- Code to generate `license_graph.dta` from Grelewicz's four data files is not provided.

Stated Requirements
---------------------

- [x] No requirements specified
- [ ] Software Requirements specified as follows:
- [ ] Computational Requirements specified as follows:
- [ ] Time Requirements specified as follows:

Replication steps
-----------------

Note: replication was re-run

1. Added `include config.do` statement in header of masterdo.do
2. Commented out data creation commands (since all analysis data are provided)
3. Ran masterdo.do and tableA2.do

> [We REQUESTED] Please ensure that replication package as downloaded from openICPSR can be run without re-arrangement of files

- Done

> [We REQUESTED] The setup program that installs all packages should specify all necessary commands. 

- Done. The setup program now installs `distinct`, the only missing package noted in the first revision, as well. 

> [We REQUESTED] Please provide debugged code, addressing the issues identified in this report.

We encountered several errors in the data creation steps.
- clean_census.do and summstats_demogr_decomp.do: While the authors state that the unzip statements in these programs do not result in errors, these statements produce errors because the zip files they reference are still not in the replication package.
- import_irs_state.do: It's still the case no references to District of Columbia in this program have spaces, despite the fact that the downloaded DC data file is still named ```District of Columbia91in.xls```. While during this replication attempt an error prevented this point in the code from being run, the replicator finds that an error would have occurred by attempting the command that would have been executed.
- The authors do not mention in their response attempts to address the other sources of error recorded in the previous report

**We note that reproducing the analysis files may require significant effort by a replicator.**

Computing Environment of the Replicator
---------------------

- CISER Shared Windows Server 2016, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (10 logical processors)

Software used:
- Stata/MP 16.0

Findings
--------

### Data Preparation Code

Not run.

### Tables 

- Table 1: Replicated
- Table A2: Replicated

### Figures 

- Figures 1-8: Replicated

### In-Text Numbers

[x] There are no in-text numbers, or all in-text numbers stem from tables and figures.

[ ] There are in-text numbers, but they are not identified in the code

Classification
--------------

- [ ] full replication
- [ ] full replication with minor issues
- [x] partial replication (data creation not fully functional)
- [ ] not able to replicate most or all of the results (reasons see above)

clear all
set more off
set scheme s1color


*******************************************************
* 1. Append state inmigration 1980-1990-2000/2017
* 2. Table A2a - 1980-2000 (logfile: regressions_1980_2000.log)
* 3. Table A2b - 2000-2017 (logfile: regressions_2000_2017.log)
* 4. Clean folder re: 1980-2000-2017
*******************************************************

*******************************************************
*** 1. Append state inmigration 1980-1990-2000/2017
*******************************************************

use ${data}/state_directed_2000ACS.dta, clear
foreach j of numlist 2001/2017 { 
	append using ${data}/state_directed_`j'.dta,
}
* To distinguish ACS 2000 from Census 2000
replace year = 200 if year == 2000

foreach j of numlist 1980 2000 { 
	append using ${data}/state_directed_`j'.dta,
}

rename inctot inc

tempfile alldata
save `alldata'


********************************************************************************
*** 2. Table A2a - 1980-2000
********************************************************************************
use `alldata', clear
keep if year>=1980&year<=2000
preserve
	collapse (rawsum) totpop = pop totempl = empl (mean) totinc=inc [aw=pop], by(statefip year)	
	gen logtotepop = log(totempl/totpop)
	tempfile statedata19802000
	save `statedata19802000'
restore
	
collapse (rawsum) pop empl mig5yrstate mig5yrabroad (mean) inc [aw=pop], by(fborn statefip year)

merge m:1 statefip year using `statedata19802000'

gen mig1yrstate = mig5yrstate/5 if year<2001
gen mig1yrabroad = mig5yrabroad/5 if year<2001

preserve
	use ${data}/state_collapsed_2000_vACS, clear
	rename pop pop_ACS 
	rename mig1yrstate mig1yrstate_ACS
	rename mig1yrabroad mig1yrabroad_ACS
	collapse (sum) pop_ACS mig1yrstate_ACS mig1yrabroad_ACS, by(year state fborn)
	tempfile dACS
	save `dACS'
restore
merge 1:1 state year fborn using `dACS', nogen update

preserve
	collapse (sum) mig1yrstate mig1yrstate_ACS, by(year)
	gen ratio = mig1yrstate/mig1yrstate_ACS
	summ ratio
restore

bys statefip fborn: egen correction = max(mig1yrstate_ACS/mig1yrstate)
replace mig1yrstate = mig1yrstate*correction if year<=2000
summ correction 
local correction `r(mean)'

bys statefip fborn: egen correctionaborad = max(mig1yrabroad_ACS/mig1yrabroad)
replace mig1yrabroad = mig1yrabroad*correctionaborad if year<=2000
qui summ correctionaborad 
local correctionaborad `r(mean)'

gen logtotinc = log(totinc)
gen logtotempl = log(totempl)
gen logpop = log(pop)
gen logepop = log(empl/pop)

egen id = group(fborn statefip)
xtset id year, delta(10)

foreach var of varlist logtotinc totpop logtotempl totinc totempl {
	gen x = `var' if yea==1980
	bys state: egen `var'1980 = max(x)
	drop x
}

xtset
gen ownpopgrowth = (d.pop/l.pop)/(year-1980) if year>1980
gen popgrowth = (d.pop/l.totpop)/(year-1980) if year>1980
gen totincgrowth = (d.totinc/l.totinc)/(year-1980) if year>1980
gen totemplgrowth = (d.totempl/l.totempl)/(year-1980) if year>1980
gen totpopgrowth = (d.totpop/l.totpop)/(year-1980) if year>1980
gen x=pop if fborn == 1
bys state year: egen fbornpop = max(x)
gen fbornshare = fbornpop/totpop

preserve
	keep if fborn==1
	keep state year fbornshare
	rename fbornshare fbornshare1980
	keep if year==1980
	drop year
	tempfile fbshare1980
	save `fbshare1980'
restore

cap log close
log using ${tables}/regressions_1980_2000_v080320.log, replace

putexcel set $tables/tableA2.xlsx, sheet(1980_2000) replace
putexcel A2 = "Natives"
putexcel A3 = "Foreign born"
putexcel A4 = "Total"

preserve
	keep if year==2000|year==1980
	drop *growth
	xtset, clear
	xtset id year, delta(20)
	gen popgrowth = (d.pop/l.totpop)/(year-1980) if year>1980
	gen ownpopgrowth = (d.pop/l.pop)/(year-1980) if year>1980
	gen totemplgrowth = (d.totempl/l.totempl)/(year-1980) if year>1980
	gen totpopgrowth = (d.totpop/l.totpop)/(year-1980) if year>1980

	estimates clear
	eststo NatTot: qui reg popgrowth totemplgrowth i.year [aw=l.totpop] if fborn==0, vce(cluster state)
	scalar nativebeta = _b[totemplgrowth]
	eststo FboTot: qui reg popgrowth totemplgrowth i.year [aw=l.totpop] if fborn==1, vce(cluster state)
	scalar fbornbeta = _b[totemplgrowth]
	eststo AllTot: qui reg totpopgrowth totemplgrowth i.year [aw=l.totpop] if fborn==0, vce(cluster state)
	scalar overallbeta = _b[totemplgrowth]
	esttab, b(3) se(3) keep(totemplgrowth) mtit

	putexcel D1 = "Mobility in response to employment changes"
	putexcel D2 = nativebeta
	putexcel D3 = fbornbeta
	putexcel D4 = overallbeta
	
restore

collapse (sum) pop totpop mig1yrstate, by(fborn)
gen popshare = 100*(pop/totpop)
gen internalmigrationshare = 100*(mig1yrstate/totpop)

summ popshare if fborn==0 
putexcel B1 = "Population share"
putexcel B2 = `r(mean)'

summ internalmigrationshare if fborn==0
putexcel C1 = "Total mobility"
scalar intmignat=`r(mean)'
putexcel C2 = intmignat

scalar relmobility= 100*nativebeta/overallbeta
putexcel E1 = "Share of mobility in response to employment changes"
putexcel E2 = relmobility

summ popshare if fborn==1
putexcel B3 = `r(mean)'

summ internalmigrationshare if fborn==1
scalar intmigfb=`r(mean)'
putexcel C3 = intmigfb

scalar relmobilityfb= 100*fbornbeta/overallbeta
putexcel E3 =relmobilityfb

summ internalmigrationshare
putexcel C4 = intmignat+intmigfb

putexcel B4 = 100.0
putexcel E4 = 100.0

log close 


********************************************************************************
* 3. Table A2b - 2000-2017 (logfile: regressions_2000_2017.log)
********************************************************************************
use `alldata', clear
keep if year>=2000 
preserve
	collapse (rawsum) totpop = pop totempl = empl (mean) totinc=inc [aw=pop], by(statefip year)	
	gen logtotepop = log(totempl/totpop)
	tempfile statedata20002017
	save `statedata20002017'
restore
	
collapse (rawsum) pop empl mig1yrstate mig1yrabroad (mean) inc [aw=pop], by(fborn statefip year)

merge m:1 statefip year using `statedata20002017'

gen logtotinc = log(totinc)
gen logtotempl = log(totempl)
gen logpop = log(pop)
gen logepop = log(empl/pop)

egen id = group(fborn statefip)
xtset id year, delta(1)

foreach var of varlist logtotinc totpop logtotempl totinc totempl {
	gen x = `var' if yea==2000
	bys state: egen `var'2000 = max(x)
	drop x
}

gen popshare = (pop/totpop)
gen internalmigrationshare = (mig1yrstate/totpop)
gen abroadmigrationshare = (mig1yrabroad/totpop)

xtset
gen popgrowth = (d.pop/l.totpop)/(year-2000) if year>2000
gen ownpopgrowth = (d.pop/l.pop)/(year-2000) if year>2000
gen totincgrowth = (d.totinc/l.totinc)/(year-2000) if year>2000
gen totemplgrowth = (d.totempl/l.totempl)/(year-2000) if year>2000
gen totpopgrowth = (d.totpop/l.totpop)/(year-2000) if year>2000
gen x=pop if fborn == 1
bys state year: egen fbornpop = max(x)
gen fbornshare = fbornpop/totpop


cap log close
log using ${tables}/regressions_2000_2017_v080320.log, replace

putexcel set $tables/tableA2.xlsx, sheet(2000_2017) modify
putexcel A2 = "Natives"
putexcel A3 = "Foreign born"
putexcel A4 = "Total"

preserve
	keep if year==2000|year==2017
	drop *growth
	xtset, clear
	xtset id year, delta(17)
	gen popgrowth = (d.pop/l.totpop)/(year-2000) if year>2000
	gen ownpopgrowth = (d.pop/l.pop)/(year-2000) if year>2000
	gen totemplgrowth = (d.totempl/l.totempl)/(year-2000) if year>2000
	gen totpopgrowth = (d.totpop/l.totpop)/(year-2000) if year>2000

	estimates clear
	eststo NatTot: qui reg popgrowth totemplgrowth i.year [aw=l.totpop] if fborn==0, vce(cluster state)
	scalar nativebeta = _b[totemplgrowth]
	eststo FboTot: qui reg popgrowth totemplgrowth i.year [aw=l.totpop] if fborn==1, vce(cluster state)
	scalar fbornbeta = _b[totemplgrowth]
	eststo AllTot: qui reg totpopgrowth totemplgrowth i.year [aw=l.totpop] if fborn==0, vce(cluster state)
	scalar overallbeta = _b[totemplgrowth]
	esttab, b(3) se(3) keep(totemplgrowth) mtit

	putexcel D1 = "Mobility in response to employment changes"
	putexcel D2 = nativebeta
	putexcel D3 = fbornbeta
	putexcel D4 = overallbeta
	
restore

collapse (sum) pop totpop mig1yrstate, by(fborn)
gen popshare = 100*(pop/totpop)
gen internalmigrationshare = 100*(mig1yrstate/totpop)

summ popshare if fborn==0 
putexcel B1 = "Population share"
putexcel B2 = `r(mean)'

summ internalmigrationshare if fborn==0
putexcel C1 = "Total mobility"
scalar intmignat=`r(mean)'
putexcel C2 = intmignat

scalar relmobility= 100*nativebeta/overallbeta
putexcel E1 = "Share of mobility in response to employment changes"
putexcel E2 = relmobility

summ popshare if fborn==1
putexcel B3 = `r(mean)'

summ internalmigrationshare if fborn==1
scalar intmigfb=`r(mean)'
putexcel C3 = intmigfb

scalar relmobilityfb= 100*fbornbeta/overallbeta
putexcel E3 =relmobilityfb

summ internalmigrationshare
putexcel C4 = intmignat+intmigfb

putexcel B4 = 100.0
putexcel E4 = 100.0

log close 

clear
set more off

cd ${data}/data/rawdata

foreach j of numlist 152/159 166/167 171/179 181 183 {
	cap !gunzip usa_00`j'.dat.gz // Not available in the replication package (see README documentation)
	run ${dofiles}/censusraw/import_00`j'.do
	cd ${data}/data/rawdata
	!rm usa_00`j'.dat
}



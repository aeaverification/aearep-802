clear 
set more off
set scheme s1color


************************************************
* 1. Construct data - Census State level
* 2. Graphs - state level (Figure 3a)
* 3. Construct data - CZs level
* 4. Graphs - CZ level (Figure 3b)
************************************************


************************************************
*** 1. Construct data - Census State level
************************************************
use ${data}/state_collapsed_1980.dta, clear
cap rename mig5yrstate mig1yrstate
collapse (sum) pop mig1yrstate, by(year state fborn)
	
foreach j of numlist 1990(10)2000 2001/2017 {
	preserve
	use ${data}/state_collapsed_`j'.dta, clear
	cap rename mig5yrstate mig1yrstate
	collapse (sum) pop mig1yrstate, by(year state fborn)
	tempfile d`j'
	save `d`j''
	restore
	append using `d`j''
}

replace mig1yrstate = mig1yrstate/5 if year<2001

preserve
	use ${data}/state_collapsed_2000_vACS, clear
	rename pop pop_ACS 
	rename mig1yrstate mig1yrstate_ACS 
	collapse (sum) pop_ACS mig1yrstate_ACS, by(year state fborn)
	tempfile dACS
	save `dACS'
restore
merge 1:1 state year fborn using `dACS', nogen update

bys statefip fborn: egen correction = max(mig1yrstate_ACS/mig1yrstate)
replace mig1yrstate = mig1yrstate*correction if year<=2000
qui summ correction 
local correction `r(mean)'

************************************************
*** 2. Graphs - state level (Figure 3a)
************************************************
collapse (sum) mig1yrstate pop, by(year fborn)
gen time = 1 if year == 1980
replace time = 2 if year == 1983
replace time = year-1981 if year>1983

replace mig1yrstate=. if mig1yrstate==0
gen stateinmigrate = 100*mig1yrstate/pop  

tw scatter stateinmigrate year if fborn==0&year<2000, mcol(black) || ///
	line stateinmigrate year if fborn==0&year<=2000, lw(medthick) lcol(black) lp(dot) || ///
	line stateinmigrate year if fborn==0&year>=2000, lw(medthick) lcol(black) lp(solid) || ///
	scatter stateinmigrate year if fborn==1&year<2000, mcol(gs9) || ///
	line stateinmigrate year if fborn==1&year<=2000, lw(medthick) lcol(gs9) lp(dot) || ///
	line stateinmigrate year if fborn==1&year>=2000, lw(medthick) lcol(gs9) lp(dash) ///
	legend(label(3 "Natives") label(6 "Foreign born") order(3 6)) ///
	ylabel(1.5(.5)3.5, angle(0)) xtit("") ytit("Inter-state migration rate (p.p.)") xlabel(1980(10)2000 2001(2)2017, angle(45))
graph export $graphs/state_census_fborn_1980_2017.png, as(png) replace

sort fborn year
export excel stateinmigrate fborn year using $tables/descriptives_migration_natives_fborn_trends.xls, first(var) replace sheet("states")

************************************************
*** 3. Construct data - CZs level
************************************************
use ${data}/czone_collapsed_1980.dta, clear
	cap rename mig5yrpuma mig1yrpuma
	collapse (sum) pop mig1yrpuma, by(year cz fborn)
	
foreach j of numlist 1990(10)2000 2005/2017 {
	preserve
	use ${data}/czone_collapsed_`j'.dta, clear
	cap rename mig5yrpuma mig1yrpuma
	collapse (sum) pop mig1yrpuma, by(year cz fborn)
	tempfile d`j'
	save `d`j''
	restore
	append using `d`j''
}

replace mig1yrpuma = mig1yrpuma/5 if year<2001

************************************************
*** 4. Graphs - CZ level (Figure 3b)
************************************************

collapse (sum) mig1yrpuma pop, by(year fborn)
xtset, clear
gen time = 1 if year == 1980
replace time = 2 if year == 1983
replace time = year-1981 if year>1983

replace mig1yrpuma=. if mig1yrpuma==0

gen czinmigrate = 100*mig1yrpuma/pop 

gen czinmigrate_ACS = czinmigrate
replace czinmigrate_ACS = czinmigrate_ACS*`correction' if year<=2000

keep if year>=2005

tw line czinmigrate year if fborn==0, lw(medthick) lcol(black) lp(solid) || ///
	line czinmigrate year if fborn==1, lw(medthick) lcol(gs9) lp(dash) ///
	legend(label(1 "Natives") label(2 "Foreign born") order(1 2)) ylabel(3(.5)6, angle(0)) ///
	ytit("Inter-CZ migration rate (p.p.)") xtit("") xlabel(2005(1)2017, angle(45))
graph export $graphs/cz_census_fborn_2005_2017.png, as(png) replace

sort fborn year
export excel czinmigrate fborn year using $tables/descriptives_migration_natives_fborn_trends.xls, first(var) sheetreplace sheet("CZs")


clear 
set more off

cd ${data}/ancillary

/*************************************************************************
 Crosswalk between PUMA 2000 and MIGPUMA 2000 (with 2012 PUMA population)
*************************************************************************/
import excel using ipums_usa_puma_migpuma_2000.xlsx, sheet(raw) clear first
destring _all, replace

rename puma puma2000
replace puma2000 = statefip*10000+puma2000
replace migpuma = statefip*10000+migpuma

rename migpuma aggregatedpuma2000

drop if statefip>56
drop statefip 

compress
saveold ${data}/ancillary/puma2000_migpuma1.dta, replace

cap unzipfile census_micro_2000
use ${data}/census_micro_2000.dta, clear
keep perwt puma statefip
rename puma puma2000
replace puma2000 = statefip*10000+puma2000
gen pop=perwt
collapse (sum) pop, by(puma2000)
compress
saveold ${data}/population_puma2000.dta, replace

use ${data}/ancillary/puma2000_migpuma1.dta, clear
merge 1:1 puma2000 using ${data}/population_puma2000.dta, 
drop _m
rename pop poppuma2000
compress
saveold ${data}/ancillary/puma2000_migpuma1.dta, replace







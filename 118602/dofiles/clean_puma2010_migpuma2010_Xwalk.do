clear 
set more off

cd ${data}/ancillary

/*************************************************************************
 Crosswalk between PUMA 2010 and MIGPUMA 2010 (with 2012 PUMA population)
*************************************************************************/
import excel using puma_migpuma1_pwpuma00.xls, sheet(raw) clear first
destring _all, replace

replace puma2010 = statefip*100000+puma2010
replace migpuma = statefip*100000+migpuma

rename migpuma aggregatedpuma2010

drop if statefip>56
drop statefip 

compress
saveold ${data}/ancillary/puma2010_migpuma1.dta, replace

cd ${data}
cap unzipfile census_micro_2012
use ${data}/census_micro_2012.dta, clear
keep perwt puma statefip
rename puma puma2010
replace puma2010 = statefip*100000+puma2010
gen pop=perwt
collapse (sum) pop, by(puma2010)
compress
saveold ${data}/population_puma2010.dta, replace

use ${data}/ancillary/puma2010_migpuma1.dta, clear
merge 1:1 puma2010 using ${data}/population_puma2010.dta, 
drop _m
rename pop poppuma2010
compress
saveold ${data}/ancillary/puma2010_migpuma1.dta, replace


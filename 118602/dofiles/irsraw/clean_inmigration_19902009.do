
		rename state statefipdestination
		rename county countyfipdestination
		rename c statefiporigin 
		rename d countyfiporigin
		foreach v of varlist retu exemp {
                capture confirm string variable `v'
                if !_rc {
					gen v_`v' = regexs(2) if regexm(`v', "^([^0-9]*)([0-9]+)([^0-9]*)$")
				}
		}
		foreach v of varlist retu exemp {
                capture confirm numeric variable `v'
                if !_rc {
					gen v_`v'=`v'
				}
		}
		destring _all, replace
		drop retu
		drop exem
		rename v_re taxreturns
		rename v_ex taxexemptions

		*replace taxreturns = . if taxreturns<20&year<2013 & (statefipor!=57|statefipor!=58|statefipor!=59|statefipor!=96|statefipor!=97|statefipor!=98)
		*replace taxexemptions = . if taxreturns==.
		
		drop if countyfipdes==0
		
		gen x1 = taxreturns if statefipor==96
		gen x2 = taxexemptions if statefipor==96
		bys countyfipdes statefipdes: egen tottaxreturns = total(x1) 
		bys countyfipdes statefipdes: egen tottaxexemptions = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipor==97&countyfipor==0
		gen x2 = taxexemptions if statefipor==97&countyfipor==0
		bys countyfipdes statefipdes: egen tottaxreturns_US = total(x1) 
		bys countyfipdes statefipdes: egen tottaxexemptions_US = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipor==97&countyfipor==1
		gen x2 = taxexemptions if statefipor==97&countyfipor==1
		bys countyfipdes statefipdes: egen tottaxreturns_samestate = total(x1) 
		bys countyfipdes statefipdes: egen tottaxexemptions_samestate = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipor==97&countyfipor==3
		gen x2 = taxexemptions if statefipor==97&countyfipor==3
		bys countyfipdes statefipdes: egen tottaxreturns_diffstate = total(x1) 
		bys countyfipdes statefipdes: egen tottaxexemptions_diffstate = total(x2)
		drop x1 x2		
		
		gen x1 = taxreturns if statefipor==98
		gen x2 = taxexemptions if statefipor==98
		bys countyfipdes statefipdes: egen tottaxreturns_allforeign = total(x1) 
		bys countyfipdes statefipdes: egen tottaxexemptions_allforeign = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipor==57&countyfipor==1
		gen x2 = taxexemptions if statefipor==57&countyfipor==1
		bys countyfipdes statefipdes: egen tottaxreturns_trueforeign = total(x1) 
		bys countyfipdes statefipdes: egen tottaxexemptions_trueforeign = total(x2)
		drop x1 x2
		
		gen x = taxreturns if countyfipor==countyfipdes&statefipor==statefipdes
		gen y = taxexemptions if countyfipor==countyfipdes&statefipor==statefipdes
		bys countyfipdes statefipdes: egen taxreturnsstayers=max(x)
		bys countyfipdes statefipdes: egen taxexemptionsstayers=max(y)
		drop x y
		drop if countyfipor==countyfipdes&statefipor==statefipdes
		drop if statefipor==57|statefipor==58|statefipor==59|statefipor==96|statefipor==97|statefipor==98	
		
		/*
		bys countyfipdes statefipdes: egen x1 = total(taxreturns) if statefipor==58
		bys countyfipdes statefipdes: egen y1 = total(taxexemptions) if statefipor==58
		bys countyfipdes statefipdes: egen x2 = total(taxreturns) if statefipor==59
		bys countyfipdes statefipdes: egen y2 = total(taxexemptions) if statefipor==59
		
		bys countyfipdes statefipdes: egen othreturnssamestate = total(x1)
		bys countyfipdes statefipdes: egen othexempsamestate = total(y1)
		bys countyfipdes statefipdes: egen othreturnsdiffstate  = total(x2)
		bys countyfipdes statefipdes: egen othexempdiffstate = total(y2)
		drop *1 *2
		drop if statefipor==58|statefipor==59 
	
		gen xx = taxreturns+(taxreturns/tottaxreturns_samestate)*othreturnssamestate if statefipor==statefipdes
		gen yy = taxreturns+(taxreturns/tottaxreturns_diffstate)*othreturnsdiffstate if statefipor!=statefipdes
		replace xx = 0 if xx==.
		replace yy = 0 if yy==.
		replace taxreturns = xx+yy
		drop xx yy
		
		gen xx = taxexemptions+(taxexemptions/tottaxexemptions_samestate)*othexempsamestate if statefipor==statefipdes
		gen yy = taxexemptions+(taxexemptions/tottaxexemptions_diffstate)*othexempdiffstate if statefipor!=statefipdes
		replace xx = 0 if xx==.
		replace yy = 0 if yy==.
		replace taxexemptions = xx+yy
		drop xx yy oth*
		*/

		

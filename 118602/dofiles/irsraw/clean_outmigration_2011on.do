
		rename y2_statefips statefipdestination
		rename y2_countyfips countyfipdestination
		rename y1_statefips statefiporigin
		rename y1_countyfips countyfiporigin
		drop y2_state 
		drop y2_countyname
		foreach v of varlist n1 n2 {
                capture confirm string variable `v'
                if !_rc {
					gen v_`v' = regexs(2) if regexm(`v', "^([^0-9]*)([0-9]+)([^0-9]*)$")
				}
		}
		foreach v of varlist n1 n2 {
                capture confirm numeric variable `v'
                if !_rc {
					gen v_`v'=`v'
				}
		}
		drop agi
		destring _all, replace
		drop n1
		drop n2
		rename v_n1 taxreturns
		rename v_n2 taxexemptions

		drop if countyfipor==0
		
		*replace taxreturns = . if taxreturns<20&year<2013 & (statefipdes!=57|statefipdes!=58|statefipdes!=59|statefipdes!=96|statefipdes!=97|statefipdes!=98)
		*replace taxreturns = . if taxreturns<10 & (statefipdes!=57|statefipdes!=58|statefipdes!=59|statefipdes!=96|statefipdes!=97|statefipdes!=98)
		*replace taxexemptions = . if taxreturns==.
		
		gen x1 = taxreturns if statefipdes==96
		gen x2 = taxexemptions if statefipdes==96
		bys countyfipor statefipor: egen tottaxreturns = total(x1) 
		bys countyfipor statefipor: egen tottaxexemptions = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipdes==97&countyfipdes==0
		gen x2 = taxexemptions if statefipdes==97&countyfipdes==0
		bys countyfipor statefipor: egen tottaxreturns_US = total(x1) 
		bys countyfipor statefipor: egen tottaxexemptions_US = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipdes==97&countyfipdes==1
		gen x2 = taxexemptions if statefipdes==97&countyfipdes==1
		bys countyfipor statefipor: egen tottaxreturns_samestate = total(x1) 
		bys countyfipor statefipor: egen tottaxexemptions_samestate = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipdes==97&countyfipdes==3
		gen x2 = taxexemptions if statefipdes==97&countyfipdes==3
		bys countyfipor statefipor: egen tottaxreturns_diffstate = total(x1) 
		bys countyfipor statefipor: egen tottaxexemptions_diffstate = total(x2)
		drop x1 x2		
		
		gen x1 = taxreturns if statefipdes==98
		gen x2 = taxexemptions if statefipdes==98
		bys countyfipor statefipor: egen tottaxreturns_allforeign = total(x1) 
		bys countyfipor statefipor: egen tottaxexemptions_allforeign = total(x2)
		drop x1 x2
		
		gen x1 = taxreturns if statefipdes==57&countyfipdes==1
		gen x2 = taxexemptions if statefipdes==57&countyfipdes==1
		bys countyfipor statefipor: egen tottaxreturns_trueforeign = total(x1) 
		bys countyfipor statefipor: egen tottaxexemptions_trueforeign = total(x2)
		drop x1 x2
		
		gen x = taxreturns if countyfipdes==countyfipor&statefipdes==statefipor
		gen y = taxexemptions if countyfipdes==countyfipor&statefipdes==statefipor
		bys countyfipor statefipor: egen taxreturnsstayers=max(x)
		bys countyfipor statefipor: egen taxexemptionsstayers=max(y)
		drop x y
		drop if countyfipdes==countyfipor&statefipdes==statefipor
		drop if statefipdes==57|statefipdes==58|statefipdes==59|statefipdes==96|statefipdes==97|statefipdes==98	
		
		/*
		bys countyfipor statefipor: egen x1 = total(taxreturns) if statefipdes==58
		bys countyfipor statefipor: egen y1 = total(taxexemptions) if statefipdes==58
		bys countyfipor statefipor: egen x2 = total(taxreturns) if statefipdes==59
		bys countyfipor statefipor: egen y2 = total(taxexemptions) if statefipdes==59
		
		bys countyfipor statefipor: egen othreturnssamestate = total(x1)
		bys countyfipor statefipor: egen othexempsamestate = total(y1)
		bys countyfipor statefipor: egen othreturnsdiffstate  = total(x2)
		bys countyfipor statefipor: egen othexempdiffstate = total(y2)
		drop *1 *2
		drop if statefipdes==58|statefipdes==59 
	
		gen xx = taxreturns+(taxreturns/tottaxreturns_samestate)*othreturnssamestate if statefipor==statefipdes
		gen yy = taxreturns+(taxreturns/tottaxreturns_diffstate)*othreturnsdiffstate if statefipor!=statefipdes
		replace xx = 0 if xx==.
		replace yy = 0 if yy==.
		replace taxreturns = xx+yy
		drop xx yy
		
		gen xx = taxexemptions+(taxexemptions/tottaxexemptions_samestate)*othexempsamestate if statefipor==statefipdes
		gen yy = taxexemptions+(taxexemptions/tottaxexemptions_diffstate)*othexempdiffstate if statefipor!=statefipdes
		replace xx = 0 if xx==.
		replace yy = 0 if yy==.
		replace taxexemptions = xx+yy
		drop xx yy oth*
		*/
		


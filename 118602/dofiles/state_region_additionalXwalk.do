clear
set more off
set scheme s1color

use ${data}/ancillary/eq_opportunity_health_ineq_online_table_10.dta, clear
keep cz* state* fips
rename fips statefip
compress
saveold ${data}/ancillary/CZs_state_Xwalk.dta, replace

use ${data}/census_micro_2017.dta, clear
collapse region, by(statefip)
rename region division
gen region = floor(division/10)
merge 1:m statefip using ${data}/ancillary/CZs_state_Xwalk.dta, nogen
compress
saveold ${data}/ancillary/CZs_state_region_Xwalk.dta, replace

use ${data}/ancillary/CZs_state_region_Xwalk.dta, clear
rename division divisionbirth
rename statefip bpld
collapse *birth, by(bpld)
gen regionbirth = floor(division/10)
compress
saveold ${data}/ancillary/state_region_birth_Xwalk.dta, replace

use ${data}/ancillary/CZs_state_region_Xwalk.dta, clear
rename division divisionorigin
rename statefip migplac1
collapse *origin, by(migplac1)
gen regionorigin = floor(division/10)
compress
saveold ${data}/ancillary/state_region_migration_Xwalk.dta, replace


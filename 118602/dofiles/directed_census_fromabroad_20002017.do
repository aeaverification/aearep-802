clear
set more off
set scheme s1color


*******************************************************
* 1. Graph (Figure 6a)
*******************************************************

use ${data}/state_directed_2000.dta, clear
foreach j of numlist 2001/2017 { 
	append using ${data}/state_directed_`j'.dta,
}

rename inctot inc

preserve
	collapse (rawsum) totpop = pop totempl = empl (mean) totinc=inc [aw=pop], by(statefip year)
	tempfile statedata20002017
	save `statedata20002017'
restore
	
collapse (rawsum) pop empl mig1yrabroad (mean) inc [aw=pop], by(statefip year)

merge m:1 statefip year using `statedata20002017'

gen logtotempl = log(totempl)
gen logpop = log(pop)

xtset statefip year
	
foreach var of varlist totpop logtotempl totempl {
	gen x = `var' if yea==2000
	bys state: egen `var'2000 = max(x)
	drop x
}

gen totemplgrowth = ((totempl-totempl2000)/totempl2000)
gen emplgrowth = ((empl-totempl2000)/totempl2000)

xtset 
bys statefip (year): gen cumimmigration = sum(mig1yrabroad)
replace cumimmig = cumimmig/totpop2000

gen logtotempl_z_state = .
gen totemplgrowth_z_state = .
qui levelsof statefip, local(st)


keep if yea==2017

reg cumimmigration totemplgrowth [aw=totpop2000], vce(cluster state)
summ cumimmigration totemplgrowth, d
xtile bin_totemplgrowth = totemplgrowth, nq(10)
xtset
collapse (mean) cumimmigration totemplgrowth (sum) totpop2000, by(bin_totemplgrowth)
tw scatter cumimmigration totemplgrowth [aw=totpop2000], m(Th) mcol(gs9) || ///
	lfit cumimmigration totemplgrowth [aw=totpop2000], lcol(gs9) ///
	ylabel(,angle(0)) xtit("Employment growth 2000-2017") ///
	ytit("Foreign immigration from abroad 2000-2017") legend(off)
graph export ${graphs}/state_cumimmigration_emplgrowth_binned_2000_2017.png, as(png) replace


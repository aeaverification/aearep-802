clear
set more off

*-------------------------------------------------------------------------------
* Description: Clean and compile the IRS county migration data
*
* Authors: Justin Wiltshire and Gaetano Basso
* Updated: October 10, 2019
*-------------------------------------------------------------------------------

cd ${data}/irsraw

************************************** 
*** Import and save IRS data
************************************** 
*** 1990-1992
* Set the primary locals
forval j = 90/91 {
	local k = `j'+1
	local m = `j' + 1900
	local n = `k' + 1900
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'countymigration.zip `m'to`n'countymigration.zip, replace
	unzipfile `m'to`n'countymigration.zip, replace
		
	* Set the secondary locals
	foreach p in in out {
		if "`p'" == "in" {
			local q "In"
			local r "i"
		}
		else if "`p'" == "out" {
			local q "Out"
			local r "o"
		}
		local s "`m'to`n'CountyMigration/`m'to`n'CountyMigration`q'flow"
			
		* Loop over each state of interest
		foreach st in /* ak hi */ al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			
			* Import the data
			import delimited using `s'/C`j'`k'`st'`r'.txt, clear
			erase ${data}/irsraw/`s'/C`j'`k'`st'`r'.txt			
			
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			if "`st'" == "dc" {
				qui rename wash v1
				qui tab v1
				local obnum = r(N) + 1
				set obs `obnum'
				local v2: variable label dctotal
				qui replace v1 = "`v2'" if mi(v1)
				qui rename dc v2
				qui tostring v2, replace
			}
			if `j' == 90 | (`j' == 91 & "`p'" == "in" & "`st'" != "nh") | (`j' == 91 & "`p'" == "out") {
				qui cap replace v1 = v2 if substr(v2, 1, 12) == " Diff. State" | substr(v2, 1, 4) == "D.C."
				qui cap drop v2
			}
			split v1, limit(11)
			qui gen stateor = 11 if v11 == "D.C." & v12 == "DC"
			qui replace stateor = 571 if v11 == "Foreign"
			qui gen f_ret = v12 if stateor == 571
			qui replace stateor = 58 if v11 == "Same" & v12 == "State"
			qui replace stateor = 591 if (v11 == "Diff." & v12 == "State") | (v11 == "Different" & v12 == "Region")
			qui replace stateor = 59 if v13 == "Northeast" | v13 == "Midwest" | v13 == "South" | v13 == "West"
			qui replace stateor = 96 if substr(v14, 1, 2) == "To" | substr(v15, 1, 2) == "To" | substr(v16, 1, 2) == "To" | substr(v17, 1, 2) == "To"
			qui replace stateor = 961 if v11 == "All" & v12 == "Migration"
			qui replace stateor = 962 if v11 == "D.C." & substr(v12, 1, 3) == "Tot"
			qui destring v11 v12, replace force
			qui replace v11 = stateor if inlist(stateor, 11, 571, 58, 59, 591, 961, 962)
			qui replace v12 = 1 if stateor == 11
			qui replace v12 = 0 if inlist(stateor, 571, 58, 59, 591, 961, 962)
			drop stateor
			
			* Distribute state/county of destination
			qui gen state = v11 if v13 == "County"
			qui gen county = v12 if v13 == "County"
			forval x = 1/1000 { 
				qui replace state = state[_n+1] if mi(state)
				qui replace county = county[_n+1] if mi(county)
			}
			if "`st'" == "dc" {
				qui replace state = 11 if mi(state)
				qui replace county = 1 if (county)
			}
			
			* Confirm state/county of destination have been properly distributed
			if "`st'" != "dc" {
				bysort state county: assert v11 == state & v12 == county if _n == 1
			}
			
			* Recode total migration as from state code 96
			bysort state county: replace v11 = 96 if substr(v14, 1, 2) == "To" | substr(v15, 1, 2) == "To" | substr(v16, 1, 2) == "To" | substr(v17, 1, 2) == "To"
			qui replace v12 = 0 if v11 == 96
			
			* Observe returns and exemptions
			qui gen returns = v13 if inlist(v11, 11, 58, 591)
			qui gen exemptions = v15 if inlist(v11, 11, 58, 591)
			qui replace returns = v14 if inlist(v11, 59, 961)
			qui replace returns = v15 if v13 == "County" | v11 == 962
			qui replace exemptions = v16 if v13 == "County" | inlist(v11, 59, 961, 962)
			qui replace returns = v15 if v13 != "County" & !inlist(v11, 11, 57, 571, 58, 59, 591, 961, 962)
			qui replace exemptions = v17 if v13 != "County" & !inlist(v11, 11, 57, 571, 58, 59, 591, 961, 962)
			qui replace returns = v17 if v15 == "Overseas"
			qui replace exemptions = v19 if v15 == "Overseas"
			qui tab v15 if v15 == "Virgin" | v15 == "Puerto"
			if r(N) != 0 {
				qui replace returns = v18 if v15 == "Puerto" | v15 == "Virgin"
				qui replace exemptions = v110 if v15 == "Puerto" | v15 == "Virgin"
			}
			qui tab v15 if v15 == "FPO"
			if r(N) != 0 {
				qui replace returns = v19 if v15 == "FPO" 
				qui replace exemptions = v111 if v15 == "FPO"
			}
			qui replace returns = f_ret if v11 == 571
			qui replace exemptions = v14 if v11 == 571
			
			* Replace empty/wrong obs based on number of words in county name
			destring returns exemptions, replace force
			tostring returns exemptions, replace
			forval v = 16/19 {
				qui distinct returns if returns == "."
				if r(ndistinct) != 0 {
					local w = `v' + 2
					if "`p'" == "in" & `j' == 90 {
						if `v' == 18 {
							local w = 110
						}
						else if `v' == 19 {
							if "`st'" != "wi" {
								local w = 111
							}
							else if "`st'" == "wi" {
								local w = 110
							}
						}
					}
					else if "`p'" == "out" & `j' == 90 {
						if `v' == 18 {
							local w = 110
						}
						else if `v' == 19 {
							if "`st'" != "mo" {
								local w = 111
							}
							else if "`st'" == "mo" {
								local w = 110
							}
						}
					}
					else {
						if `v' == 18 {
							local w = 110
						}
						else if `v' == 19 {
							local w = 111
						}
					}
					qui replace exemptions = v`w' if returns == "."
					qui replace returns = v`v' if returns == "."
					destring returns exemptions, replace force
					tostring returns exemptions, replace
				}
			}
			
			* Replace empty/wrong total obs based on number of words in county name
			qui replace returns = "." if v11 == 96
			qui replace exemptions = "." if v11 == 96
			forval ret = 17/19 {
				local e = `ret' + 1
				if `ret' == 19 {
					local e = 110
				}
				qui replace exemptions = v`e' if v11 == 96 & returns == "."
				qui replace returns = v`ret' if v11 == 96 & returns == "."
				if "`st'" == "dc" & `ret' == 17 {
					qui replace exemptions = v16 if v11 == 96
					qui replace returns = v15 if v11 == 96
				}
				destring returns exemptions, replace force
				if `ret' < 19 {
					tostring returns exemptions, replace
				}
			}
			
			* Finish cleaning up
			qui replace v11 = 57 if v11 == 571
			qui replace v11 = 59 if v11 == 591
			qui replace v11 = 96 if v11 == 962
			qui rename v11 c
			qui rename v12 d
			qui drop v* f_ret
			qui order state county c d returns exemptions
			qui sort state county c d
			collapse (sum) returns exemptions, by(state county c d)
			
			
			* Sanity checks
			assert !mi(returns)
			assert !mi(exemptions)
			assert returns == round(returns)
			assert exemptions == round(exemptions)
			qui replace returns=0 if exemptions<returns & exemptions==0
			assert returns <= exemptions
			bysort state county: egen sum_ret = total(returns) if c != 96 & (1000*state + county != 1000*c + d)
			bysort state county: egen rets = total(returns) if c == 96
			bysort state county: egen tot_ret = max(rets)
			bysort state county: egen sum_ex = total(exemptions) if c != 96 & (1000*state + county != 1000*c + d)
			bysort state county: egen exs = total(exemptions) if c == 96
			bysort state county: egen tot_ex = max(exs)
			qui drop if c == 961
			assert tot_ret == sum_ret if !mi(sum_ret)
			assert tot_ex == sum_ex if !mi(sum_ex)
			qui drop sum* tot* rets exs
						
			* Add summary observations as needed
			qui gen cty_fips = 1000*state + county
			levelsof cty_fips, local(cty)
			foreach c of local cty {
				qui sum state
				local nums = r(N)
				local state = r(mean)
				forval i = 1/4 {
					local nums`i' = `nums' + `i'
				}
				qui set obs `nums4'
				qui replace d = 0 if mi(county)
				qui replace c = 97 if inlist(_n, `nums1', `nums2', `nums3')
				qui replace d = 1 if _n == `nums2'
				qui replace d = 3 if _n == `nums3'
				qui replace c = 98 if _n == `nums4'
				qui replace county = `c' - 1000*r(mean) if mi(county)
				qui replace state = `state' if mi(state)
			}
			qui drop cty_fips
			sort state county c d
			bysort state county: egen ztotret = total(returns) if c == 96
			bysort state county: egen ztotex = total(exemptions) if c == 96
			bysort state county: egen ztotret2 = max(ztotret)
			bysort state county: egen ztotex2 = max(ztotex)
			bysort state county: egen zstret = total(returns) if (c == state & d != county) | c == 58
			bysort state county: egen zstex = total(exemptions) if (c == state & d != county) | c == 58
			bysort state county: egen zstret2 = max(zstret)
			bysort state county: egen zstex2 = max(zstex)
			bysort state county: egen zfret = total(returns) if c == 57
			bysort state county: egen zfex = total(exemptions) if c == 57
			bysort state county: egen zfret2 = max(zfret)
			bysort state county: egen zfex2 = max(zfex)
			qui replace returns = ztotret2 - zfret2 if c == 97 & d == 0
			qui replace exemptions = ztotex2 - zfex2 if c == 97 & d == 0
			qui replace returns = zstret2 if c == 97 & d == 1
			qui replace exemptions = zstex2 if c == 97 & d == 1
			qui replace returns = ztotret2 - zstret2 - zfret2 if c == 97 & d == 3
			qui replace exemptions = ztotex2 - zstex2 - zfex2 if c == 97 & d == 3
			qui replace returns = zfret2 if c == 98
			qui replace exemptions = zfex2 if c == 98
			qui drop z*
			
			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			run ${dofiles}/irsraw/clean_`p'migration_19902009.do
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
			
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_al.dta, clear
		foreach st in /* ak hi */ az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_al.dta
	}

	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'countymigration.zip
}

*** 1992-1993
* Set the primary locals
local j = 92
local k = `j'+1
local m = `j' + 1900
local n = `k' + 1900
	
* Download and unzip the data
copy https://www.irs.gov/pub/irs-soi/`m'to`n'countymigration.zip `m'to`n'countymigration.zip, replace
unzipfile `m'to`n'countymigration.zip, replace
	
* Set the secondary locals
foreach p in in out {
	if "`p'" == "in" {
		local q "In"
		local r "i"
	}
	else if "`p'" == "out" {
		local q "Out"
		local r "o"
	}
	local s "`m'to`n'CountyMigration/`m'to`n'CountyMigration`q'flow"
		
	* Loop over each state of interest
	foreach st in /* Ak Hi */ Al Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
		
		* Import the data
		import excel using `s'/C`j'`k'`st'`r'.xls, firstrow cellrange(A7) case(lower) clear
		erase ${data}/irsraw/`s'/C`j'`k'`st'`r'.xls	
		
		* Restrict to variables of interest
		qui drop in 1
		qui keep state county c d returns exemptions
		
		* Destring the variables to recode them
		qui destring _all, replace force
		
		* Add summary observations as needed
		qui replace c = 96 if c == 0
		qui replace d = 0 if c == 96
		qui gen cty_fips = 1000*state + county
		levelsof cty_fips, local(cty)
		foreach c of local cty {
			qui sum state
			local nums = r(N)
			local state = r(mean)
			forval i = 1/5 {
				local nums`i' = `nums' + `i'
			}
			qui set obs `nums4'
			qui replace d = 0 if mi(county)
			qui replace c = 97 if inlist(_n, `nums1', `nums2', `nums3')
			qui replace d = 1 if _n == `nums2'
			qui replace d = 3 if _n == `nums3'
			qui replace c = 98 if _n == `nums4'
			qui replace county = `c' - 1000*r(mean) if mi(county)
			qui replace state = `state' if mi(state)
		}
		qui drop cty_fips
		sort state county c d
		
		* Fill in the aggregate obs
		qui gen z_fips = 1000*c + d
		bysort state county: egen ztotret = total(returns) if c == 96
		bysort state county: egen ztotex = total(exemptions) if c == 96
		bysort state county: egen ztotret2 = max(ztotret)
		bysort state county: egen ztotex2 = max(ztotex)
		bysort state county: egen zstret = total(returns) if (c == state & d != county) | z_fips == 63010
		bysort state county: egen zstex = total(exemptions) if (c == state & d != county) | z_fips == 63010
		bysort state county: egen zstret2 = max(zstret)
		bysort state county: egen zstex2 = max(zstex)
		bysort state county: egen zfret = total(returns) if c == 57 | z_fips == 63015
		bysort state county: egen zfex = total(exemptions) if c == 57 | z_fips == 63015
		bysort state county: egen zfret2 = max(zfret)
		bysort state county: egen zfex2 = max(zfex)
		qui replace returns = ztotret2 - zfret2 if c == 97 & d == 0
		qui replace exemptions = ztotex2 - zfex2 if c == 97 & d == 0
		qui replace returns = zstret2 if c == 97 & d == 1
		qui replace exemptions = zstex2 if c == 97 & d == 1
		qui replace returns = ztotret2 - zstret2 - zfret2 if c == 97 & d == 3
		qui replace exemptions = ztotex2 - zstex2 - zfex2 if c == 97 & d == 3
		qui replace returns = zfret2 if c == 98
		qui replace exemptions = zfex2 if c == 98
		qui replace c = state if z_fips == 63050
		qui replace d = county if z_fips == 63050
		qui drop z*
		qui drop if c == 63
		
		* Record the year, run the clean file, and save
		qui gen year = 19`j'
		run ${dofiles}/irsraw/clean_`p'migration_19902009.do
		compress
		save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
	}
		
	* Combine the state-year files and erase unwanted .dta files
	use ${data}/irsraw/`p'migration`j'`k'_Al.dta, clear
	foreach st in /* Ak Hi */ Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
		append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
	}
	saveold ${data}/irsraw/`p'migration`j'`k'.dta, replace
	erase ${data}/irsraw/`p'migration`j'`k'_Al.dta
}

* Erase the unwanted files
cd ${data}/irsraw
erase `m'to`n'countymigration.zip

*** 1993-1996
* Set the primary locals
forval j = 93/95 { 
	* Set the primary locals
	local k = `j'+1
	local l = `k' - 90
	local m = `j' + 1900
	local n = `k' + 1900
	local z = 7
	if `j' != 93 {
		local z = 6
	}
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'countymigration.zip `m'to`n'countymigration.zip, replace
	unzipfile `m'to`n'countymigration.zip, replace
		
	* Set the secondary locals
	foreach p in in out { 
		if "`p'" == "in" {
			local q "In"
			local r "i"
			if `j' == 95 {
				local r "ir"
			}
		}
		else if "`p'" == "out" {
			local q "Out"
			local r "o"
			if `j' == 95 {
				local r "or"
			}
		}
		local s "`m'to`n'CountyMigration/`m'to`n'CountyMigration`q'flow"
			
		* Loop over each state of interest
		foreach st in /* ak hi */ al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			
			* Import the data
			if `j' == 95 & "`p'" == "in" & "`st'" == "co" {
				import excel using `s'/co`j'`l'`st'`r'.xls, firstrow cellrange(A6:H2698) case(lower) clear
				erase ${data}/irsraw/`s'/co`j'`l'`st'`r'.xls
			}
			else {
				import excel using `s'/co`j'`l'`st'`r'.xls, firstrow cellrange(A`z') case(lower) clear
				erase ${data}/irsraw/`s'/co`j'`l'`st'`r'.xls
			}
		
			* Restrict to variables of interest
			qui drop in 1
			qui keep state county c d returns exemptions
			
			* Observe aggregate values from 1993-1995
			if inlist(`j', 93, 94) {
				* Destring the variables to recode them
				qui destring _all, replace force
				
				* Add summary observations as needed
				qui replace c = 96 if c == 0
				qui replace d = 0 if c == 96
				if "`st'" == "ar" & "`p'" == "out" {
					qui drop if mi(state) & mi(county)
				}
				if "`st'" == "wy" {
					replace state = 56 if state == 1|state == 7|state == 21|state == 25
				}
				qui gen cty_fips = 1000*state + county
				levelsof cty_fips, local(cty)
				foreach c of local cty {
					qui sum state
					local nums = r(N)
					local state = r(mean)
					forval i = 1/5 {
						local nums`i' = `nums' + `i'
					}
					qui set obs `nums4'
					qui replace d = 0 if mi(county)
					qui replace c = 97 if inlist(_n, `nums1', `nums2', `nums3')
					qui replace d = 1 if _n == `nums2'
					qui replace d = 3 if _n == `nums3'
					qui replace c = 98 if _n == `nums4'
					qui replace county = `c' - 1000*r(mean) if mi(county)
					qui replace state = `state' if mi(state)
				}
				qui drop cty_fips
				sort state county c d
				
				* Fill in the aggregate obs
				qui gen z_fips = 1000*c + d
				bysort state county: egen ztotret = total(returns) if c == 96
				bysort state county: egen ztotex = total(exemptions) if c == 96
				bysort state county: egen ztotret2 = max(ztotret)
				bysort state county: egen ztotex2 = max(ztotex)
				bysort state county: egen zstret = total(returns) if (c == state & d != county) | z_fips == 63010
				bysort state county: egen zstex = total(exemptions) if (c == state & d != county) | z_fips == 63010
				bysort state county: egen zstret2 = max(zstret)
				bysort state county: egen zstex2 = max(zstex)
				bysort state county: egen zfret = total(returns) if c == 57 | z_fips == 63015
				bysort state county: egen zfex = total(exemptions) if c == 57 | z_fips == 63015
				bysort state county: egen zfret2 = max(zfret)
				bysort state county: egen zfex2 = max(zfex)
				qui replace returns = ztotret2 - zfret2 if c == 97 & d == 0
				qui replace exemptions = ztotex2 - zfex2 if c == 97 & d == 0
				qui replace returns = zstret2 if c == 97 & d == 1
				qui replace exemptions = zstex2 if c == 97 & d == 1
				qui replace returns = ztotret2 - zstret2 - zfret2 if c == 97 & d == 3
				qui replace exemptions = ztotex2 - zstex2 - zfex2 if c == 97 & d == 3
				qui replace returns = zfret2 if c == 98
				qui replace exemptions = zfex2 if c == 98
				qui replace c = state if z_fips == 63050
				qui replace d = county if z_fips == 63050
				qui drop z*
				qui drop if c == 63
			}
			
			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			run ${dofiles}/irsraw/clean_`p'migration_19902009.do
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
			
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_al.dta, clear
		foreach st in /* ak hi */ az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_al.dta
	}

	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'countymigration.zip
}

*** 1996-1997
* Set the primary locals
local j = 96
local k = `j'+1
local l = `k' - 90
local m = `j' + 1900
local n = `k' + 1900
		
* Download and unzip the data
copy https://www.irs.gov/pub/irs-soi/`m'to`n'countymigration.zip `m'to`n'countymigration.zip, replace
unzipfile `m'to`n'countymigration.zip, replace
		
* Set the secondary locals
foreach p in in out {
	if "`p'" == "in" {
		local q "In"
		local r "i"
	}
	else if "`p'" == "out" {
		local q "Out"
		local r "o"
	}
	local s "`m'to`n'CountyMigration/`m'to`n'CountyMigration`q'flow"
		
	* Loop over each state of interest
	if "`p'" == "in" {
		foreach st in /* ak hi */ al az ar ca co ct de Dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			
			* Import the data
			import excel using `s'/co`j'`l'`st'`r'.xls, firstrow cellrange(A6) case(lower) clear
			erase ${data}/irsraw/`s'/co`j'`l'`st'`r'.xls
							
			* Record the year, run the clean file, and save
			drop in 1
			keep state county c d returns exemptions
			qui gen year = 19`j'
			run ${dofiles}/irsraw/clean_`p'migration_19902009.do
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
			
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_al.dta, clear
		foreach st in /* ak hi */ az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_al.dta
	}
	else if "`p'" == "out" {
		foreach st in /* ak hi */ al az ar ca co ct DE dc FL GA ID IL IN IA KS KY LA ME MD MA MI MN MO MS MT NE NH NV NJ NM ny NC ND OH OK OR pa ri sc sd tn tx ut vt va wa wv wi wy {
		
			* Deal with data indiosyncracies
			if upper("`st'") == "`st'" {
				local r = "O"
			}
			else {
				local r = "o"
			}
			
			* Import the data
			import excel using `s'/co`j'`l'`st'`r'.xls, firstrow cellrange(A6) case(lower) clear
			erase ${data}/irsraw/`s'/co`j'`l'`st'`r'.xls
				
			* Record the year, run the clean file, and save
			drop in 1
			keep state county c d returns exemptions
			qui gen year = 19`j'
			run ${dofiles}/irsraw/clean_`p'migration_19902009.do
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
		
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_al.dta, clear
		foreach st in /* ak hi */ az ar ca co ct DE dc FL GA ID IL IN IA KS KY LA ME MD MA MI MN MO MS MT NE NH NV NJ NM ny NC ND OH OK OR pa ri sc sd tn tx ut vt va wa wv wi wy {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_al.dta
	}
}

* Erase the unwanted files
cd ${data}/irsraw
erase `m'to`n'countymigration.zip

*** 1997-2000
* Set the primary locals
forval j = 97/99 {
	local k = `j'+1
	local l = `k' - 90
	local m = `j' + 1900
	local n = `k' + 1900
	if `k' == 100 {
		local l = 0
		local k "00"
	}
	local z = 7
	if `j' == 97 {
		local z = 6
	}
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'countymigration.zip `m'to`n'countymigration.zip, replace
	unzipfile `m'to`n'countymigration.zip, replace
		
	* Set the secondary locals
	foreach p in in out {
		if "`p'" == "in" {
			local q "In"
			local r "i"
		}
		else if "`p'" == "out" {
			local q "Out"
			local r "o"
		}
		local s "`m'to`n'CountyMigration/`m'to`n'CountyMigration`q'flow"
			
		* Loop over each state of interest
		foreach st in /* ak hi */ al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			
			* Import the data
			import excel using `s'/co`j'`l'`st'`r'.xls, firstrow cellrange(A`z') case(lower) clear
			erase ${data}/irsraw/`s'/co`j'`l'`st'`r'.xls
			
			* Record the year, run the clean file, and save
			drop in 1
			keep state county c d returns exemptions
			qui gen year = 19`j'
			run ${dofiles}/irsraw/clean_`p'migration_19902009.do
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
			
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_al.dta, clear
		foreach st in /* ak hi */ az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_al.dta
	}

	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'countymigration.zip
}

*** 2000-2002

foreach j of numlist 0(1)2 {

	* Set the primary locals
	local k = `j'+1
	local m = `j' + 2000
	local n = `k' + 2000
	
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'countymigration.zip `m'to`n'countymigration.zip, replace
	unzipfile `m'to`n'countymigration.zip, replace
	
	* Set the secondary locals
	foreach p in in out {
		if "`p'" == "in" {
			local q "In"
			local r "i"
		}
		else if "`p'" == "out" {
			local q "Out"
			local r "o"
		}
		local s "`m'to`n'CountyMigration/`m'to`n'CountyMigration`q'flow"
		
		* Loop over each state of interest
		foreach st in /* ak hi */ al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
		
			* Deal with raw data inconsistencies and save to main folder
			if `j' == 0 {
				if "`st'" != "vt" | "`p'" == "out" {
					import excel using `s'/co`j'0`k'`st'`r'r.xls, firstrow cellrange(A7) case(lower) clear
					erase ${data}/irsraw/`s'/co`j'0`k'`st'`r'r.xls			
				}
				else if "`st'" == "vt" & "`p'" == "in" {
					import excel using `s'/co`j'0`k'`st'`r'.xls, first cellrange(A7) case(lower) clear
					erase ${data}/irsraw/`s'/co`j'0`k'`st'`r'.xls
				}
			}
			else if `j' == 1 {
				if "`st'" != "wy" | "`p'" == "out" {
					import excel using `s'/co`j'0`k'`st'`r'.xls, firstrow cellrange(A7) case(lower) clear
					erase ${data}/irsraw/`s'/co`j'0`k'`st'`r'.xls			
				}
				else if "`st'" == "wy" & "`p'" == "in" {
					import excel using `s'/co`j'0`k'Wy`r'.xls, first cellrange(A7) case(lower) clear
					erase ${data}/irsraw/`s'/co`j'0`k'Wy`r'.xls
				}
			}
			else if `j' == 2 {
				import excel using `s'/co`j'0`k'`st'`r'.xls, firstrow cellrange(A6) case(lower) clear
				erase ${data}/irsraw/`s'/co`j'0`k'`st'`r'.xls			
			}
			
			* Record the year, run the clean file, and save
			drop in 1
			keep state county c d returns exemptions
			qui gen year = 200`j'
			run ${dofiles}/irsraw/clean_`p'migration_19902009.do
			compress
			save "${data}/irsraw/`p'migration0`j'0`k'_`st'.dta", replace
		}
		
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration0`j'0`k'_al.dta, clear
		foreach st in /* ak hi */ az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
			append using ${data}/irsraw/`p'migration0`j'0`k'_`st'.dta
			erase ${data}/irsraw/`p'migration0`j'0`k'_`st'.dta
		}
		saveold ${data}/irsraw/`p'migration0`j'0`k'.dta, replace
		erase ${data}/irsraw/`p'migration0`j'0`k'_al.dta
	}
	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'countymigration.zip
}

*** 2003
* Set the primary locals
local j = 3
local k = `j'+1
local m = `j' + 2000
local n = `k' + 2000

* Download and unzip the data
copy https://www.irs.gov/pub/irs-soi/`m'to`n'countymigration.zip `m'to`n'countymigration.zip, replace
unzipfile `m'to`n'countymigration.zip, replace
	
* Set the secondary locals
foreach p in in out {
	if "`p'" == "in" {
		local q "In"
		local r "i"
	}
	else if "`p'" == "out" {
		local q "Out"
		local r "o"
	}
	local s "`m'to`n'CountyMigration/`m'to`n'CountyMigration`q'flow"
		
	* Loop over each state of interest
	foreach st in /* Ak Hi */ AL AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {

		* Deal with raw data inconsistencies and save to main folder
		di "p: `p'; s: `s'; st: `st'"
		import excel using `s'/co0`j'0`k'`st'`r'.xls, firstrow cellrange(A7) case(lower) clear
		erase ${data}/irsraw/`s'/co0`j'0`k'`st'`r'.xls	
	
		* Record the year, run the clean file, and save
		drop in 1
		keep state county c d returns exemptions
		qui gen year = 200`j'
		run ${dofiles}/irsraw/clean_`p'migration_19902009.do
		compress
		save "${data}/irsraw/`p'migration0`j'0`k'_`st'.dta", replace
	}
	
	* Combine the state-year files and erase unwanted .dta files
	use ${data}/irsraw/`p'migration0`j'0`k'_AL.dta, clear
	foreach st in /* AK HI */ AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
		append using ${data}/irsraw/`p'migration0`j'0`k'_`st'.dta
		erase ${data}/irsraw/`p'migration0`j'0`k'_`st'.dta
	}
	saveold "${data}/irsraw/`p'migration0`j'0`k'.dta", replace
	erase ${data}/irsraw/`p'migration0`j'0`k'_AL.dta
}
* Erase the ZIP file
cd ${data}/irsraw
erase `m'to`n'countymigration.zip

*** 2004-2006
foreach j of numlist 4(1)5 {
	local k = `j'+1
	copy https://www.irs.gov/pub/irs-soi/county0`j'0`k'.zip county0`j'0`k'.zip, replace
	unzipfile county0`j'0`k'.zip
	
	foreach st in /* AK HI */ AL AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
		import excel using co0`j'0`k'`st'i.xls, first cellrange(A7) case(lower) clear
		drop in 1
		keep state county c d returns exemptions
		qui gen year = 200`j'
		run ${dofiles}/irsraw/clean_inmigration_19902009.do
		compress
		save "${data}/irsraw/inmigration0`j'0`k'_`st'.dta", replace
	
		import excel using co0`j'0`k'`st'o.xls, first cellrange(A7) case(lower) clear
		drop in 1
		keep state county c d returns exemptions
		qui gen year = 200`j'
		run ${dofiles}/irsraw/clean_outmigration_19902009.do
		compress
		save "${data}/irsraw/outmigration0`j'0`k'_`st'.dta", replace
		
	}
	use ${data}/irsraw/inmigration0`j'0`k'_AL.dta, clear
	foreach st in /* AK HI */ AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
		append using ${data}/irsraw/inmigration0`j'0`k'_`st'.dta
	}
	saveold "${data}/irsraw/inmigration0`j'0`k'.dta", replace

	use ${data}/irsraw/outmigration0`j'0`k'_AL.dta, clear
	foreach st in /* AK HI */ AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
		append using ${data}/irsraw/outmigration0`j'0`k'_`st'.dta
	}
	saveold "${data}/irsraw/outmigration0`j'0`k'.dta", replace
	
	foreach st in /* AK HI */ AL AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
		cd ${data}/irsraw
		!rm /s /q inmigration0`j'0`k'_`st'.dta 
		!rm /s /q outmigration0`j'0`k'_`st'.dta
	}
	cd ${data}/irsraw
	!rm /s /q co*i.xls 
	!rm /s /q co*o.xls 
	!rm /s /q county*.zip 
	!rm /s /q *.dat 
}

*** 2006-2007
copy https://www.irs.gov/pub/irs-soi/county0607.zip county0607.zip, replace
unzipfile county0607.zip
	
foreach st in /* Ak Hi */ Al Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
	import excel using co0607`st'i.xls, first cellrange(A7) case(lower) clear
	drop in 1
	keep state county c d returns exemptions
	qui gen year = 2006
	run ${dofiles}/irsraw/clean_inmigration_19902009.do
	compress
	saveold "${data}/irsraw/inmigration0607_`st'.dta", replace

	import excel using co0607`st'o.xls, first cellrange(A7) case(lower) clear
	drop in 1
	keep state county c d returns exemptions
	qui gen year = 2006
	run ${dofiles}/irsraw/clean_outmigration_19902009.do
	compress
	saveold "${data}/irsraw/outmigration0607_`st'.dta", replace			
}

use ${data}/irsraw/inmigration0607_Al.dta, clear
foreach st in /* Ak Hi */ Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
	append using ${data}/irsraw/inmigration0607_`st'.dta
}
saveold "${data}/irsraw/inmigration0607.dta", replace

use ${data}/irsraw/outmigration0607_Al.dta, clear
foreach st in /* Ak Hi */ Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
	append using ${data}/irsraw/outmigration0607_`st'.dta
}
saveold "${data}/irsraw/outmigration0607.dta", replace

foreach st in /* Ak Hi */ Al Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
	cd ${data}/irsraw
	!rm /s /q inmigration0607_`st'.dta 
	!rm /s /q outmigration0607_`st'.dta 
}
cd ${data}/irsraw
!rm /s /q co*i.xls 
!rm /s /q co*o.xls 
!rm /s /q county*.zip 
!rm /s /q *.dat 

*** 2007-2009
foreach j of numlist 7(1)8 {
	local k = `j'+1
	copy https://www.irs.gov/pub/irs-soi/county0`j'0`k'.zip county0`j'0`k'.zip, replace
	unzipfile county0`j'0`k'.zip
	
	foreach st in /* Ak Hi */ Al Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
		import excel using co0`j'0`k'i`st'.xls, first cellrange(A7) case(lower) clear
		drop in 1
		keep state county c d returns exemptions
		qui gen year = 200`j'
		run ${dofiles}/irsraw/clean_inmigration_19902009.do
		compress
		saveold "${data}/irsraw/inmigration0`j'0`k'_`st'.dta", replace
	
		import excel using co0`j'0`k'o`st'.xls, first cellrange(A7) case(lower) clear
		drop in 1
		keep state county c d returns exemptions
		qui gen year = 200`j'
		run ${dofiles}/irsraw/clean_outmigration_19902009.do
		compress
		saveold "${data}/irsraw/outmigration0`j'0`k'_`st'.dta", replace			
	}
	use ${data}/irsraw/inmigration0`j'0`k'_Al.dta, clear
	foreach st in /* Ak Hi */ Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
		append using ${data}/irsraw/inmigration0`j'0`k'_`st'.dta
	}
	saveold "${data}/irsraw/inmigration0`j'0`k'.dta", replace

	use ${data}/irsraw/outmigration0`j'0`k'_Al.dta, clear
	foreach st in /* Ak Hi */ Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
		append using ${data}/irsraw/outmigration0`j'0`k'_`st'.dta
	}
	saveold "${data}/irsraw/outmigration0`j'0`k'.dta", replace
	
	foreach st in /* Ak Hi */ Al Az Ar Ca Co Ct De Dc Fl Ga Id Il In Ia Ks Ky La Me Md Ma Mi Mn Ms Mo Mt Ne Nv Nh Nj Nm Ny Nc Nd Oh Ok Or Pa Ri Sc Sd Tn Tx Ut Vt Va Wa Wv Wi Wy {
		cd ${data}/irsraw
		!rm /s /q inmigration0`j'0`k'_`st'.dta 
		!rm /s /q outmigration0`j'0`k'_`st'.dta 
	}
	cd ${data}/irsraw
	!rm /s /q co*i.xls 
	!rm /s /q co*o.xls 
	!rm /s /q county*.zip 
	!rm /s /q *.dat 
	!rm /s /q *.csv 
}

*** 2009-2011
copy https://www.irs.gov/pub/irs-soi/county0910.zip county0910.zip, replace
unzipfile county0910.zip
foreach st in /* AK HI */ AL AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
	import excel using co0910i`st'.xls, first clear cellrange(A5)
	qui gen year = 2009
	run ${dofiles}/irsraw/clean_inmigration_20092011.do
	compress
	saveold "${data}/irsraw/inmigration0910_`st'.dta", replace

	import excel using co0910o`st'.xls, first clear cellrange(A5)
	qui gen year = 2009
	run ${dofiles}/irsraw/clean_outmigration_20092011.do
	compress
	saveold "${data}/irsraw/outmigration0910_`st'.dta", replace
			
}
use ${data}/irsraw/inmigration0910_AL.dta, clear
foreach st in /* AK HI */ AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
	append using ${data}/irsraw/inmigration0910_`st'.dta
}
saveold "${data}/irsraw/inmigration0910.dta", replace

use ${data}/irsraw/outmigration0910_AL.dta, clear
foreach st in /* AK HI */ AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
	append using ${data}/irsraw/outmigration0910_`st'.dta
}
saveold "${data}/irsraw/outmigration0910.dta", replace

foreach st in /* AK HI */ AL AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY {
	cd ${data}/irsraw
	!rm /s /q inmigration0910_`st'.dta 
	!rm /s /q outmigration0910_`st'.dta 
}
cd ${data}/irsraw
!rm /s /q co*i.xls 
!rm /s /q co*o.xls 
!rm /s /q county*.zip 
!rm /s /q *.dat 
!rm /s /q *.csv 

copy https://www.irs.gov/pub/irs-soi/county1011.zip county1011.zip, replace
unzipfile county1011.zip
foreach st in /* ak hi */ al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
	import excel using co1011i`st'.xls, first clear cellrange(A5)
	qui gen year = 2010
	run ${dofiles}/irsraw/clean_inmigration_20092011.do
	compress
	saveold "${data}/irsraw/inmigration1011_`st'.dta", replace

	import excel using co1011o`st'.xls, first clear cellrange(A5)
	qui gen year = 2010
	run ${dofiles}/irsraw/clean_outmigration_20092011.do
	compress
	saveold "${data}/irsraw/outmigration1011_`st'.dta", replace		
}
use ${data}/irsraw/inmigration1011_al.dta, clear
foreach st in /* ak hi */ az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
	append using ${data}/irsraw/inmigration1011_`st'.dta
}
saveold "${data}/irsraw/inmigration1011.dta", replace

use ${data}/irsraw/outmigration1011_al.dta, clear
foreach st in /* ak hi */ az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
	append using ${data}/irsraw/outmigration1011_`st'.dta
}
saveold "${data}/irsraw/outmigration1011.dta", replace

foreach st in /* ak hi */ al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy {
	cd ${data}/irsraw
	!rm /s /q inmigration1011_`st'.dta 
	!rm /s /q outmigration1011_`st'.dta 
}
cd ${data}/irsraw
!rm /s /q co*i.xls 
!rm /s /q co*o.xls 
!rm /s /q county*.zip 
!rm /s /q *.dat 
!rm /s /q *.csv 

*** 2011-2016
foreach j of numlist 11(1)15 {
	local k = `j'+1
	copy https://www.irs.gov/pub/irs-soi/countyinflow`j'`k'.csv countyinflow`j'`k'.csv, replace
	insheet using countyinflow`j'`k'.csv, clear names
	qui gen year = 20`j'
	run ${dofiles}/irsraw/clean_inmigration_2011on.do
	compress
	saveold "${data}/irsraw/inmigration`j'`k'.dta", replace
		
	copy https://www.irs.gov/pub/irs-soi/countyoutflow`j'`k'.csv countyoutflow`j'`k'.csv, replace
	insheet using countyoutflow`j'`k'.csv, clear names
	qui gen year = 20`j'
	run ${dofiles}/irsraw/clean_outmigration_2011on.do
	compress
	saveold "${data}/irsraw/outmigration`j'`k'.dta", replace
	
	cd ${data}/irsraw
	!rm /s /q co*i.xls 
	!rm /s /q co*o.xls 
	!rm /s /q county*.zip 
	!rm /s /q *.dat 
	!rm /s /q *.csv
	capture erase countyinflow`j'`k'.csv
	capture erase countyoutflow`j'`k'.csv
}	

* Create panels for each data series
foreach p in in out {
qui use "${data}/irsraw/`p'migration9091.dta", clear
	forval j = 1(1)25 {
		local k = `j' + 1
		if `k' < 10 {
			append using "${data}/irsraw/`p'migration9`j'9`k'.dta"
		}
		else if `k' == 10 {
			append using "${data}/irsraw/`p'migration9`j'00.dta"
		}
		if inrange(`k', 11, 19) {
			local j = `j' - 10
			local k = `k' - 10
			append using "${data}/irsraw/`p'migration0`j'0`k'.dta"
		}
		else if `k' == 20 {
			local j = `j' - 10
			local k = `k' - 10
			append using "${data}/irsraw/`p'migration0`j'`k'.dta"
		}
		else if `k' >= 20  & `j' >= 20 {
			local j = `j' - 10
			local k = `k' - 10
			append using "${data}/irsraw/`p'migration`j'`k'.dta"
		}
	}
	capture drop j k J K
	save "${data}/`p'migration_9016.dta", replace
}


************************************** 
*** Clean folder
************************************** 

cd ${data}/irsraw
!rm /s /q *.xls 
!rm /s /q *.doc 
!rm /s /q *.zip 
!rm /s /q *.dat 

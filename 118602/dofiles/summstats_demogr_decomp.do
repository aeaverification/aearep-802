clear all
set more off


*******************************************************
* 1. Clean data 2000, 2005/2017
* 2. Summary statistics
* 3. Year of arrivals for foreign born (Figure 4)
* 4. State-level Oaxaca decomposition
*******************************************************

*******************************************************
*** 1. Clean data 2000, 2005 to 2017
*******************************************************
cd ${data}
*unzipfile census_micro_2000_vACS // to be run in case the data have been zipped before

use ${data}/census_micro_2000_vACS.dta, clear

drop if gq==0 | gq==3 | gq==4 
*drop if age<25 | age>64 
*drop if school==2 

gen lowskilled = educd<=64
replace lowskilled = . if educd<10|educd==.

gen educ4cat = .
replace educ4cat = 1*(educd>=10&educd<=62)+2*(educd>=63&educd<=81)+3*(educd==101)+4*(educd>=114&educd<=116)

gen age10cat = .
replace age10cat = 0*(age<25)+1*(age>=25&age<=29)+2*(age>=30&age<=34)+3*(age>=35&age<=39)+4*(age>=40&age<=44) ///
	+5*(age>=45&age<=49)+6*(age>=50&age<=54)+7*(age>=55&age<=59)+8*(age>=60&age<=64)+9*(age>64)
gen age4cat = .
replace age4cat = 0*(age<25)+1*(age>=25&age<=44)+2*(age>=45&age<=64)+3*(age>64)

*drop if statefip==2|statefip==15

run ${dofiles}/BPLaggregation.do

replace bpld = bpld/100 if bpld<15000
merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen

gen stateborn = (statefip==bpld)	
gen miglifestate = 1-stateborn

gen byte fborn=(bpld>=15000)
replace fborn=0 if bpld==90011 | bpld==90021

cap gen yrsusa2 = 1*(yrsusa1<6)+2*(yrsusa1>=6&yrsusa1<=10)+3*(yrsusa1>=11&yrsusa1<=15)+4*(yrsusa1>15)
replace yrsusa2 = 4 if yrsusa2>4&yrsusa2!=.
rename yrsusa2 yrsusa4cat
replace yrsusa4cat = . if fborn==0

gen female = sex==2

drop raced
gen race3cat = race 
replace race3cat = 3 if race>3&race!=.
gen white = (race==1)
replace white = . if race==.

gen pop = 1

rename region division
gen region = floor(division/10)

recast double migplac1 statefip bpld
replace migplac1 = statefip if migplac1==0
gen mig1yrstate = (migplac1!=statefip)

gen mig1yrabroad = mig1yrstate if migplac1>56
replace mig1yrabroad = 0 if mig1yrabroad==.
replace mig1yrstate = . if mig1yrabroad==1

keep statefip mig1yrstate mig1yrabroad race3cat white female fborn yrsusa4cat division age10cat age4cat educ4cat lowskilled perwt bplgroup year school

tempfile state2000
save `state2000'


*** 2001/2017
foreach y of numlist 2001(1)2017 {
	
	cd ${data}
	*unzipfile census_micro_`y'.zip // to be run in case the data have been zipped before
	
	use census_micro_`y'.dta, clear

	drop if gq==0 | gq==3 | gq==4 
	*drop if age<25 | age>64 
	*drop if school==2 

	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.

	gen educ4cat = .
	replace educ4cat = 1*(educd>=10&educd<=62)+2*(educd>=63&educd<=81)+3*(educd==101)+4*(educd>=114&educd<=116)

	gen age10cat = .
	replace age10cat = 0*(age<25)+1*(age>=25&age<=29)+2*(age>=30&age<=34)+3*(age>=35&age<=39)+4*(age>=40&age<=44) ///
		+5*(age>=45&age<=49)+6*(age>=50&age<=54)+7*(age>=55&age<=59)+8*(age>=60&age<=64)+9*(age>64)
	gen age4cat = .
	replace age4cat = 0*(age<25)+1*(age>=25&age<=44)+2*(age>=45&age<=64)+3*(age>64)

	*drop if statefip==2|statefip==15

	run ${dofiles}/BPLaggregation.do

	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen

	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn

	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021

	cap gen yrsusa2 = 1*(yrsusa1<6)+2*(yrsusa1>=6&yrsusa1<=10)+3*(yrsusa1>=11&yrsusa1<=15)+4*(yrsusa1>15)
	replace yrsusa2 = 4 if yrsusa2>4&yrsusa2!=.
	rename yrsusa2 yrsusa4cat
	replace yrsusa4cat = . if fborn==0

	gen female = sex==2

	drop raced
	gen race3cat = race 
	replace race3cat = 3 if race>3&race!=.
	gen white = (race==1)
	replace white = . if race==.

	gen pop = 1

	rename region division
	gen region = floor(division/10)

	recast double migplac1 statefip bpld
	replace migplac1 = statefip if migplac1==0
	gen mig1yrstate = (migplac1!=statefip)

	gen mig1yrabroad = mig1yrstate if migplac1>56
	replace mig1yrabroad = 0 if mig1yrabroad==.
	replace mig1yrstate = . if mig1yrabroad==1

	keep statefip mig1yrstate mig1yrabroad race3cat white female fborn yrsusa4cat division age10cat age4cat educ4cat lowskilled perwt bplgroup year school

	tempfile state`y'
	save `state`y''
	
	*cd ${data} // to be run in case the data have been zipped before
	*!rm census_micro_`y'.dta // to be run in case the data have been zipped before
}

*******************************************************
*** 2. State-level summary statistics table (Table 1)
*******************************************************
cd ${tables}
*!rm summstats_tab1.xls

use `state2000', clear
forvalues j = 2001/2017 {
	append using `state`j''
}
drop if year==.

gen period1 = (year==2000)
gen period2 = (year>=2000)
gen period3 = (year>=2000&year<=2007)
gen period4 = (year==2017)
gen period5 = (year>=2008)


*** Table summary statistics 	
foreach p of numlist 3 5 { // to produce other period statistics type "1(1)5" instead of "3 5"
preserve	
	keep if period`p'==1
	if `p'==1 {
		local per "2000"
	}
	if `p'==2 {
		local per "2000-2017"
	}
	if `p'==3 {
		local per "2000-2007"
	}
	if `p'==4 {
		local per "2017"
	}
	if `p'==5 {
		local per "2008-2017"
	}
	mat stats = J(16,3,.)
	summ mig1yrstate if fborn==0 & female==0 [aw=perwt]
	mat stats[1,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==0 & female==1 [aw=perwt]
	mat stats[2,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & female==0 [aw=perwt]
	mat stats[1,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & female==1 [aw=perwt]
	mat stats[2,3] = round(r(mean),.0001)
	
	summ mig1yrstate if fborn==0 & age4cat==0 [aw=perwt]
	mat stats[3,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==0 & age4cat==1 [aw=perwt]
	mat stats[4,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==0 & age4cat==2 [aw=perwt]
	mat stats[5,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==0 & age4cat==3 [aw=perwt]
	mat stats[6,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & age4cat==0 [aw=perwt]
	mat stats[3,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & age4cat==1 [aw=perwt]
	mat stats[4,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & age4cat==2 [aw=perwt]
	mat stats[5,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & age4cat==3 [aw=perwt]
	mat stats[6,3] = round(r(mean),.0001)

	summ mig1yrstate if fborn==0 & white==0 [aw=perwt]
	mat stats[7,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==0 & white==1 [aw=perwt]
	mat stats[8,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & white==0 [aw=perwt]
	mat stats[7,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & white==1 [aw=perwt]
	mat stats[8,3] = round(r(mean),.0001)

	summ mig1yrstate if fborn==0 & lowskill==1 [aw=perwt]
	mat stats[9,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==0 & lowskill==0 [aw=perwt]
	mat stats[10,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & lowskill==1 [aw=perwt]
	mat stats[9,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & lowskill==0 [aw=perwt]
	mat stats[10,3] = round(r(mean),.0001)

	summ mig1yrstate if fborn==1 & yrsusa4cat==1 [aw=perwt]
	mat stats[11,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & yrsusa4cat==2 [aw=perwt]
	mat stats[12,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & yrsusa4cat==3 [aw=perwt]
	mat stats[13,3] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 & yrsusa4cat==4 [aw=perwt]
	mat stats[14,3] = round(r(mean),.0001)

	summ mig1yrabroad if fborn==0 [aw=perwt]
	mat stats[15,2] = round(r(mean),.0001)
	summ mig1yrabroad if fborn==1 [aw=perwt]
	mat stats[15,3] = round(r(mean),.0001)
	
	summ mig1yrstate if fborn==0 [aw=perwt]
	mat stats[16,2] = round(r(mean),.0001)
	summ mig1yrstate if fborn==1 [aw=perwt]
	mat stats[16,3] = round(r(mean),.0001)
	
	mat colnames stats = "Group" "USnatives" "Foreignborn" 

	clear
	svmat stats, names(col) 
	local i = 1
	tostring Group, replace
	foreach j in Male Female <25 25-44 45-64 65+ Other White HS College 0-5 6-10 11-15 16+ Abroad1yr {
		replace Group = "`j'" if _n==`i' 
		local i = `i'+1
	}
	export excel using ${tables}/summstats_tab1.xls, first(varlabels) cell(A1) keepcellfmt sheet("`per'", modify) 
restore
}


*******************************************************
*** 3. Year of arrivals for foreign born (Figure 4)
*******************************************************
preserve
	keep if year == 2000 | year == 2017 | year == 2007
	collapse (sum) fborn [pw=perwt], by(year yrsusa4cat)
	bys year: egen totalfborn = total(fborn)
	gen shfborn = 100*fborn/totalfborn
	la drop _all
	la define mig 1 "0-5" 2 "6-10" 3 "11-15" 4 "16+"
	la values yrsusa mig 
	graph bar shfborn, over(yrsusa4cat) over(year) showyvars ///
		asyvar legend(off) nofill ytitle("Percentage of foreign born population") ///
		ylabel(,angle(0)) scheme(s1mono)
	graph export ${graphs}/years_stay_2000_2017.png, as(png) replace
	list year yrsusa shfborn 
restore


*******************************************************
*** 4. State-level Oaxaca decomposition
*******************************************************

*** Oaxaca decomposition: generate time dummies such that difference is t-(t-1)
keep if year == 2000 | year == 2017 | year == 2007
gen year_oaxaca = 2*(year==2000)+3*(year==2007)+4*(year==2017)
sum year_oaxaca, det
forvalues i=`r(min)'(1)`r(max)' {
	cap gen y`i'=(year_oaxaca==`i') if year_oaxaca==`i'|year_oaxaca==(`i'-1)
	cap replace y`i'=1-y`i'
}

*** Oaxaca routine
* Overall migration equation (choose specification, can freely add other variables. This includes demographics only)
cd ${tables}
!rm oaxaca_2000_2017.xls
!rm oaxaca_fborn_2000_2017.xls
!rm oaxaca_natives_2000_2017.xls
!rm oaxaca_fborn_yearsUS_2000_2017.xls

* Years since migration
tab yrsusa4cat, gen(fborn_yearsUS)

forvalues i=3(1)4 {

	preserve
	
	keep if school==1&(age4cat==1|age4cat==2)
	keep if mig1yrstate!=.
	
/*		xi: oaxaca mig1yrstate i.educ4cat i.female i.age10cat i.fborn [pw=perwt], by(y`i') detail
		mat A`i'=e(b)
		mat2txt, matrix(A`i') saving(${tables}/oaxaca_2000_2017.xls) format(%12.4f) append
		
		xi: oaxaca mig1yrstate i.educ4cat i.female i.age10cat [pw=perwt] if fborn==0, by(y`i') detail
		mat A`i'=e(b)
		mat2txt, matrix(A`i') saving(${tables}/oaxaca_natives_2000_2017.xls) format(%12.4f) append
		
		xi: oaxaca mig1yrstate i.educ4cat i.female i.age10cat [pw=perwt] if fborn==1, by(y`i') detail
		mat A`i'=e(b)
		mat2txt, matrix(A`i') saving(${tables}/oaxaca_fborn_2000_2017.xls) format(%12.4f) append
*/		
		xi: oaxaca mig1yrstate fborn_yearsUS1 fborn_yearsUS2 fborn_yearsUS3 [pw=perwt] if fborn==1, by(y`i') detail
		mat A`i'=e(b)
		mat2txt, matrix(A`i') saving(${tables}/oaxaca_fborn_yearsUS_2000_2017.xls) format(%12.4f) append

		xi: oaxaca mig1yrstate i.educ4cat i.female i.age10cat fborn_yearsUS1 fborn_yearsUS2 fborn_yearsUS3 [pw=perwt] if fborn==1, by(y`i') detail
		mat A`i'=e(b)
		mat2txt, matrix(A`i') saving(${tables}/oaxaca_fborn_yearsUS_2000_2017.xls) format(%12.4f) append

	restore

}


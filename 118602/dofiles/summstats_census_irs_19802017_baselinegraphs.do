clear all
set more off
set scheme s1color


************************************************
* 1. Construct data - IRS State level
* 2. Construct data - Census State level
* 3. Graphs - state level (Figure 1)
* 4. Construct data - IRS CZs level
* 5. Construct data - Census CZ level
* 6. Graphs - CZ level (Figure 2)
* 7. Lifetime state-level migration (Figure A1)
************************************************


************************************************
*** 1. Construct data - IRS State level
************************************************
use ${data}/foote_state_inflows.dta, clear
keep if state==1
keep year totInexemptions totInstayers
rename totInstayers stayers
rename totInexemptions inmigrants_total
replace stayers = stayers+inmig
tempfile inmigirs8389
save `inmigirs8389'

use ${data}/state_Inmigration_9016.dta, clear
*drop if year>2012
keep year statefipdestination totInexemptionsUS totInstayers
duplicates drop
rename totInexemptionsUS inmigrants_total
rename totInstayers stayers
rename statefipdes statefip

tempfile migirs
save `migirs'

************************************************
*** 2. Construct data - Census State level
************************************************
use ${data}/state_collapsed_1980_full.dta, clear
cap rename mig5yrstate mig1yrstate
collapse (sum) pop mig1yrstate, by(year state)
	
foreach j of numlist 1990(10)2000 2001/2017 {
	preserve
	use ${data}/state_collapsed_`j'_full.dta, clear
	cap rename mig5yrstate mig1yrstate
	collapse (sum) pop mig1yrstate, by(year state)
	tempfile d`j'
	save `d`j''
	restore
	append using `d`j''
}

use ${data}/state_collapsed_1980_full.dta, clear
cap rename mig5yrstate mig1yrstate
collapse (sum) pop mig1yrstate, by(year state)
	
foreach j of numlist 1990(10)2000 2001/2017 {
	preserve
	use ${data}/state_collapsed_`j'_full.dta, clear
	cap rename mig5yrstate mig1yrstate
	collapse (sum) pop mig1yrstate, by(year state)
	tempfile d`j'
	save `d`j''
	restore
	append using `d`j''
}

replace mig1yrstate = mig1yrstate/5 if year<2001

preserve
	use ${data}/state_collapsed_2000_vACS_full, clear
	rename pop pop_ACS 
	rename mig1yrstate mig1yrstate_ACS 
	collapse (sum) pop_ACS mig1yrstate_ACS, by(year state)
	tempfile dACS
	save `dACS'
restore
merge 1:1 state year using `dACS', nogen update

bys statefip: egen correction = max(mig1yrstate_ACS/mig1yrstate)
replace mig1yrstate = mig1yrstate*correction if year<=2000
qui summ correction 
local correction `r(mean)'

************************************************
*** 3. Graphs - state level (Figure 1)
************************************************
xtset statefip year
merge 1:1 statefip year using `migirs', gen(m_IRS)

gen x = pop/stayers
bys statefip: egen popfactor = mean(x)
replace pop = popfactor*stayers if pop==.

collapse (sum) inmigrants_total mig1yrstate pop stayers, by(year)
append using `inmigirs8389'

xtset, clear
gen time = 1 if year == 1980
replace time = 2 if year == 1983
replace time = year-1981 if year>1983
tsset time

replace mig1yrstate=. if mig1yrstate==0
replace inmigrants_total=. if inmigrants_total==0
tsset time
gen inmigrateIRS = 100*inmigrants_total/stayers
gen stateinmigrate = 100*mig1yrstate/pop  

la var stateinmigrate "FACEVALUE"
la var inmigrateIRS "IRS"

tw scatter stateinmigrate year if year>=1980 & year<2000, mcol(black) || ///
	line stateinmigrate year if year>=1980 & year<=2000, lw(medthick) lp(dot) lcol(black) || ///
	line stateinmigrate year if year>=2000, lw(medthick) lp(solid) lcol(black) || ///
 	line inmigrateIRS year if year>=1983&year<=2009, lw(medthick) lp(dash) lcol(gs9) ///
	legend(label(1 "Census (adj.'d)") label(3 "ACS") label(4 "IRS") row(1) order(1 3 4)) ///
	ylabel(1.5(.5)3.5, angle(0)) ytit("Inter-state migration rate (p.p.)") xtit("") xlabel(1980(10)2000 2001(2)2017, angle(45))
graph export $graphs/state_census_irs_1980_2017.png, as(png) replace
/*line inmigrateIRS year if year>=2013, lp(dot) /// */

export excel stateinmigrate inmigrateIRS year using $tables/descriptives_migration_trends.xls, first(var) replace sheet("states")

************************************************
*** 4. Construct data - IRS CZs level
************************************************
use ${data}/cz_inmigration_8315.dta, clear
*drop if year<2005 | year>2012 
keep czone year inmigrants_total inmigrants_total_upb stayers*
duplicates drop
rename czone cz
tempfile inmigirs
save `inmigirs'

gen popIRS = stayers_from+inmigrants_total
gen popIRS_upbound = stayers_upbound+inmigrants_total_upb

tempfile migirs
save `migirs'


************************************************
*** 5. Construct data - Census CZ level
************************************************
use ${data}/czone_collapsed_1980_full.dta, clear
	cap rename mig5yrpuma mig1yrpuma
	collapse (sum) pop mig1yrpuma, by(year cz)
	
foreach j of numlist 1990(10)2000 2005/2017 {
	preserve
	use ${data}/czone_collapsed_`j'_full.dta, clear
	cap rename mig5yrpuma mig1yrpuma
	collapse (sum) pop mig1yrpuma, by(year cz)
	tempfile d`j'
	save `d`j''
	restore
	append using `d`j''
}

replace mig1yrpuma = mig1yrpuma/5 if year<2001

************************************************
*** 6. Graphs - CZ level (Figure 2)
************************************************
merge 1:1 cz year using `migirs', gen(m_IRS)

collapse (sum) mig1yrpuma pop inmigrants_total* popIRS*, by(year)
xtset, clear
gen time = 1 if year == 1980
replace time = 2 if year == 1983
replace time = year-1981 if year>1983
tsset time

replace mig1yrpuma=. if mig1yrpuma==0
replace inmigrants_total=. if inmigrants_total==0

gen inmigrateIRS = 100*inmigrants_total/popIRS
gen inmigrateIRS_upbound = 100*inmigrants_total_upbound/popIRS_upbound
gen czinmigrate = 100*mig1yrpuma/pop 

gen czinmigrate_ACS = czinmigrate
replace czinmigrate_ACS = czinmigrate_ACS*`correction' if year<=2000

la var czinmigrate "FACEVALUE"
la var czinmigrate_ACS "CORRECTED PRE 1990"
la var inmigrateIRS "IRS"

tsset time
tw line czinmigrate year if year>=2005, lw(medthick) lp(solid) lcol(black) || ///
	line inmigrateIRS_upbound year if year<=2009, lw(medthick) lp(dash) lcol(gs9) ///
	legend(label(1 "ACS") label(2 "IRS") order(1 2)) ylabel(3(.5)6, angle(0)) ///
	ytit("Inter-CZ migration rate (p.p.)") xtit("") xlabel(1980(10)2000 2001(2)2017, angle(45))
graph export $graphs/cz_census_irs_1980_2017.png, as(png) replace

export excel czinmigrate inmigrateIRS_upbound year using $tables/descriptives_migration_trends.xls, first(var) sheetreplace sheet("CZs")

keep if year>=2005
la var czinmigrate "ACS"
tw line czinmigrate year if year>=2005, lcol(black) lp(solid) || ///
	line inmigrateIRS_upbound year if year<=2009, lcol(gs9) lp(dash) ///
	legend(label(1 "ACS") label(2 "IRS") order(1 2)) ylabel(3(.5)6, angle(0)) ///
	ytit("Inter-CZ migration rate (p.p.)") xtit("") xlabel(2005(1)2017, angle(60))
graph export $graphs/cz_census_irs_2005_2017.png, as(png) replace
/*line inmigrateIRS_upbound year if year>=2013, lp(shortdash) /// */

************************************************
*** 7. Lifetime state-level migration (Figure A1)
************************************************
use ${data}/state_collapsed_1980_full.dta, clear
keep if fborn==0
collapse (sum) pop miglifestate, by(year state)

foreach j of numlist 1990(10)2000 2001/2017 {
	preserve
	*use ${data}/state_collapsed_`j'.dta, clear
	use ${data}/state_collapsed_`j'_full.dta, clear
	keep if fborn==0
	collapse (sum) pop miglifestate, by(year state)
	tempfile d`j'
	save `d`j''
	restore
	merge 1:1 state year using `d`j'', nogen update
}

bys statefip: gen lifemigrate = 100*miglifestate/pop

collapse (mean) *migrate [aw=pop], by(year)

gen time = 1 if year == 1980
replace time = 2 if year == 1990
replace time = 3 if year == 2000
replace time = year-1997 if year >2000
tsset time

tw scatter lifemigrate year if year<2000, mcol(black) || ///
	line lifemigrate year if year<=2000, lw(medthick) lcol(black) lp(dot) || ///
	line lifemigrate year if year>=2000, lw(medthick) lcol(black) ylabel(29(.5)34,angle(0)) xtit("") ytit("") ///
	ytit("Lifetime inter-state migration rate (p.p.; 25-64-y.o.)") ///
	xlabel(1980 1990 2000 2001(2)2017, angle(45)) legend(off) /* ///
	tit("US born lifetime migration rate (x100)") name(g2, replace) */
graph export $graphs/state_miglifetime_19802017.png, as(png) replace

export excel lifemigrate year using $tables/descriptives_migration_trends.xls, first(var) sheetreplace sheet("lifetime")


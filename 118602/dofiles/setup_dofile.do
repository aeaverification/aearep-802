clear all
set more off

*** Set the directories (based on the global pdir set in master.do)
global dofiles "${pdir}/dofiles"
global tables "${pdir}/tables"
global graphs "${pdir}/graphs"
global data "${pdir}/data"

foreach pkg in oaxaca mat2txt binscatter estout distinct {
           cap quietly ssc install `pkg', replace
}
quietly net from "http://www.stata-journal.com/software/sj5-4"
quietly cap ado uninstall dm88_1.pkg
quietly cap net install dm88_1.pkg

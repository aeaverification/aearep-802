
cap label var year      `"Census year"'
cap label var datanum   `"Data set number"'
cap label var serial    `"Household serial number"'
cap label var hhwt      `"Household weight"'
cap label var cpi99     `"CPI-U adjustment factor to 1999 dollars"'
cap label var region    `"Census region and division"'
cap label var statefip  `"State (FIPS code)"'
cap label var countyfip `"County (FIPS code)"'
cap label var metro     `"Metropolitan status"'
cap label var metarea   `"Metropolitan area [general version]"'
cap label var metaread  `"Metropolitan area [detailed version]"'
cap label var puma      `"Public Use Microdata Area"'
cap label var pumasupr  `"Super Public Use Microdata Area"'
cap label var conspuma  `"Consistent PUMA, 1980-1990-2000"'
cap label var cpuma0010 `"Consistent PUMA, 2000-2010"'
cap label var cntry     `"Country"'
cap label var gq        `"Group quarters status"'
cap label var ownershp  `"Ownership of dwelling (tenure) [general version]"'
cap label var ownershpd `"Ownership of dwelling (tenure) [detailed version]"'
cap label var mortgage  `"Mortgage status"'
cap label var mortgag2  `"Second mortgage status"'
cap label var owncost   `"Selected monthly owner costs"'
cap label var rent      `"Monthly contract rent"'
cap label var rentgrs   `"Monthly gross rent"'
cap label var rentmeal  `"Meals included in rent"'
cap label var costelec  `"Annual electricity cost"'
cap label var costgas   `"Annual gas cost"'
cap label var costwatr  `"Annual water cost"'
cap label var costfuel  `"Annual home heating fuel cost"'
cap label var hhincome  `"Total household income"'
cap label var valueh    `"House value"'
cap label var rooms     `"Number of rooms"'
cap label var builtyr2  `"Age of structure, decade"'
cap label var bedrooms  `"Number of bedrooms"'
cap label var pernum    `"Person number in sample unit"'
cap label var perwt     `"Person weight"'
cap label var famsize   `"Number of own family members in household"'
cap label var nchild    `"Number of own children in the household"'
cap label var nchlt5    `"Number of own children under age 5 in household"'
cap label var eldch     `"Age of eldest own child in household"'
cap label var yngch     `"Age of youngest own child in household"'
cap label var relate    `"Relationship to household head [general version]"'
cap label var related   `"Relationship to household head [detailed version]"'
cap label var sex       `"Sex"'
cap label var age       `"Age"'
cap label var birthyr   `"Year of birth"'
cap label var race      `"Race [general version]"'
cap label var raced     `"Race [detailed version]"'
cap label var hispan    `"Hispanic origin [general version]"'
cap label var hispand   `"Hispanic origin [detailed version]"'
cap label var bpl       `"Birthplace [general version]"'
cap label var bpld      `"Birthplace [detailed version]"'
cap label var ancestr1  `"Ancestry, first response [general version]"'
cap label var ancestr1d `"Ancestry, first response [detailed version]"'
cap label var ancestr2  `"Ancestry, second response [general version]"'
cap label var ancestr2d `"Ancestry, second response [detailed version]"'
cap label var citizen   `"Citizenship status"'
cap label var yrimmig   `"Year of immigration"'
cap label var yrsusa1   `"Years in the United States"'
cap label var yrsusa2   `"Years in the United States, intervalled"'
cap label var language  `"Language spoken [general version]"'
cap label var languaged `"Language spoken [detailed version]"'
cap label var speakeng  `"Speaks English"'
cap label var school    `"School attendance"'
cap label var educ      `"Educational attainment [general version]"'
cap label var educd     `"Educational attainment [detailed version]"'
cap label var empstat   `"Employment status [general version]"'
cap label var empstatd  `"Employment status [detailed version]"'
cap label var labforce  `"Labor force status"'
cap label var occ       `"Occupation"'
cap label var occ1990   `"Occupation, 1990 basis"'
cap label var ind       `"Industry"'
cap label var ind1990   `"Industry, 1990 basis"'
cap label var classwkr  `"Class of worker [general version]"'
cap label var classwkrd `"Class of worker [detailed version]"'
cap label var occsoc    `"Occupation, SOC classification"'
cap label var wkswork1  `"Weeks worked last year"'
cap label var wkswork2  `"Weeks worked last year, intervalled"'
cap label var uhrswork  `"Usual hours worked per week"'
cap label var wrklstwk  `"Worked last week"'
cap label var looking   `"Looking for work"'
cap label var availble  `"Available for work"'
cap label var inctot    `"Total personal income"'
cap label var ftotinc   `"Total family income"'
cap label var incwage   `"Wage and salary income"'
cap label var incbus00  `"Business and farm income, 2000"'
cap label var incss     `"Social Security income"'
cap label var incwelfr  `"Welfare (public assistance) income"'
cap label var incinvst  `"Interest, dividend, and rental income"'
cap label var incretir  `"Retirement income"'
cap label var incsupp   `"Supplementary Security Income"'
cap label var incother  `"Other income"'
cap label var incearn   `"Total personal earned income"'
cap label var poverty   `"Poverty status"'
cap label var migrate1  `"Migration status, 1 year [general version]"'
cap label var migrate1d `"Migration status, 1 year [detailed version]"'
cap label var migplac1  `"State or country of residence 1 year ago"'
cap label var movedin   `"When occupant moved into residence"'
cap label var trantime  `"Travel time to work"'

cap label define year_lbl 1850 `"1850"'
cap label define year_lbl 1860 `"1860"', add
cap label define year_lbl 1870 `"1870"', add
cap label define year_lbl 1880 `"1880"', add
cap label define year_lbl 1900 `"1900"', add
cap label define year_lbl 1910 `"1910"', add
cap label define year_lbl 1920 `"1920"', add
cap label define year_lbl 1930 `"1930"', add
cap label define year_lbl 1940 `"1940"', add
cap label define year_lbl 1950 `"1950"', add
cap label define year_lbl 1960 `"1960"', add
cap label define year_lbl 1970 `"1970"', add
cap label define year_lbl 1980 `"1980"', add
cap label define year_lbl 1990 `"1990"', add
cap label define year_lbl 2000 `"2000"', add
cap label define year_lbl 2001 `"2001"', add
cap label define year_lbl 2002 `"2002"', add
cap label define year_lbl 2003 `"2003"', add
cap label define year_lbl 2004 `"2004"', add
cap label define year_lbl 2005 `"2005"', add
cap label define year_lbl 2006 `"2006"', add
cap label define year_lbl 2007 `"2007"', add
cap label define year_lbl 2008 `"2008"', add
cap label define year_lbl 2009 `"2009"', add
cap label define year_lbl 2010 `"2010"', add
cap label define year_lbl 2011 `"2011"', add
cap label define year_lbl 2012 `"2012"', add
cap label define year_lbl 2013 `"2013"', add
cap label define year_lbl 2014 `"2014"', add
cap label define year_lbl 2015 `"2015"', add
cap label define year_lbl 2016 `"2016"', add
cap label define year_lbl 2017 `"2017"', add
cap label values year year_lbl

cap label define region_lbl 11 `"New England Division"'
cap label define region_lbl 12 `"Middle Atlantic Division"', add
cap label define region_lbl 13 `"Mixed Northeast Divisions (1970 Metro)"', add
cap label define region_lbl 21 `"East North Central Div."', add
cap label define region_lbl 22 `"West North Central Div."', add
cap label define region_lbl 23 `"Mixed Midwest Divisions (1970 Metro)"', add
cap label define region_lbl 31 `"South Atlantic Division"', add
cap label define region_lbl 32 `"East South Central Div."', add
cap label define region_lbl 33 `"West South Central Div."', add
cap label define region_lbl 34 `"Mixed Southern Divisions (1970 Metro)"', add
cap label define region_lbl 41 `"Mountain Division"', add
cap label define region_lbl 42 `"Pacific Division"', add
cap label define region_lbl 43 `"Mixed Western Divisions (1970 Metro)"', add
cap label define region_lbl 91 `"Military/Military reservations"', add
cap label define region_lbl 92 `"PUMA boundaries cross state lines-1% sample"', add
cap label define region_lbl 97 `"State not identified"', add
cap label define region_lbl 99 `"Not identified"', add
cap label values region region_lbl

cap label define statefip_lbl 01 `"Alabama"'
cap label define statefip_lbl 02 `"Alaska"', add
cap label define statefip_lbl 04 `"Arizona"', add
cap label define statefip_lbl 05 `"Arkansas"', add
cap label define statefip_lbl 06 `"California"', add
cap label define statefip_lbl 08 `"Colorado"', add
cap label define statefip_lbl 09 `"Connecticut"', add
cap label define statefip_lbl 10 `"Delaware"', add
cap label define statefip_lbl 11 `"District of Columbia"', add
cap label define statefip_lbl 12 `"Florida"', add
cap label define statefip_lbl 13 `"Georgia"', add
cap label define statefip_lbl 15 `"Hawaii"', add
cap label define statefip_lbl 16 `"Idaho"', add
cap label define statefip_lbl 17 `"Illinois"', add
cap label define statefip_lbl 18 `"Indiana"', add
cap label define statefip_lbl 19 `"Iowa"', add
cap label define statefip_lbl 20 `"Kansas"', add
cap label define statefip_lbl 21 `"Kentucky"', add
cap label define statefip_lbl 22 `"Louisiana"', add
cap label define statefip_lbl 23 `"Maine"', add
cap label define statefip_lbl 24 `"Maryland"', add
cap label define statefip_lbl 25 `"Massachusetts"', add
cap label define statefip_lbl 26 `"Michigan"', add
cap label define statefip_lbl 27 `"Minnesota"', add
cap label define statefip_lbl 28 `"Mississippi"', add
cap label define statefip_lbl 29 `"Missouri"', add
cap label define statefip_lbl 30 `"Montana"', add
cap label define statefip_lbl 31 `"Nebraska"', add
cap label define statefip_lbl 32 `"Nevada"', add
cap label define statefip_lbl 33 `"New Hampshire"', add
cap label define statefip_lbl 34 `"New Jersey"', add
cap label define statefip_lbl 35 `"New Mexico"', add
cap label define statefip_lbl 36 `"New York"', add
cap label define statefip_lbl 37 `"North Carolina"', add
cap label define statefip_lbl 38 `"North Dakota"', add
cap label define statefip_lbl 39 `"Ohio"', add
cap label define statefip_lbl 40 `"Oklahoma"', add
cap label define statefip_lbl 41 `"Oregon"', add
cap label define statefip_lbl 42 `"Pennsylvania"', add
cap label define statefip_lbl 44 `"Rhode Island"', add
cap label define statefip_lbl 45 `"South Carolina"', add
cap label define statefip_lbl 46 `"South Dakota"', add
cap label define statefip_lbl 47 `"Tennessee"', add
cap label define statefip_lbl 48 `"Texas"', add
cap label define statefip_lbl 49 `"Utah"', add
cap label define statefip_lbl 50 `"Vermont"', add
cap label define statefip_lbl 51 `"Virginia"', add
cap label define statefip_lbl 53 `"Washington"', add
cap label define statefip_lbl 54 `"West Virginia"', add
cap label define statefip_lbl 55 `"Wisconsin"', add
cap label define statefip_lbl 56 `"Wyoming"', add
cap label define statefip_lbl 61 `"Maine-New Hampshire-Vermont"', add
cap label define statefip_lbl 62 `"Massachusetts-Rhode Island"', add
cap label define statefip_lbl 63 `"Minnesota-Iowa-Missouri-Kansas-Nebraska-S.Dakota-N.Dakota"', add
cap label define statefip_lbl 64 `"Maryland-Delaware"', add
cap label define statefip_lbl 65 `"Montana-Idaho-Wyoming"', add
cap label define statefip_lbl 66 `"Utah-Nevada"', add
cap label define statefip_lbl 67 `"Arizona-New Mexico"', add
cap label define statefip_lbl 68 `"Alaska-Hawaii"', add
cap label define statefip_lbl 72 `"Puerto Rico"', add
cap label define statefip_lbl 97 `"Military/Mil. Reservation"', add
cap label define statefip_lbl 99 `"State not identified"', add
cap label values statefip statefip_lbl

cap label define metro_lbl 0 `"Metropolitan status indeterminable (mixed)"'
cap label define metro_lbl 1 `"Not in metropolitan area"', add
cap label define metro_lbl 2 `"In metropolitan area: In central/principal city"', add
cap label define metro_lbl 3 `"In metropolitan area: Not in central/principal city"', add
cap label define metro_lbl 4 `"In metropolitan area: Central/principal city status indeterminable (mixed)"', add
cap label values metro metro_lbl

cap label define metarea_lbl 000 `"Not identifiable or not in an MSA"'
cap label define metarea_lbl 004 `"Abilene, TX"', add
cap label define metarea_lbl 006 `"Aguadilla, PR"', add
cap label define metarea_lbl 008 `"Akron, OH"', add
cap label define metarea_lbl 012 `"Albany, GA"', add
cap label define metarea_lbl 016 `"Albany-Schenectady-Troy, NY"', add
cap label define metarea_lbl 020 `"Albuquerque, NM"', add
cap label define metarea_lbl 022 `"Alexandria, LA"', add
cap label define metarea_lbl 024 `"Allentown-Bethlehem-Easton, PA/NJ"', add
cap label define metarea_lbl 028 `"Altoona, PA"', add
cap label define metarea_lbl 032 `"Amarillo, TX"', add
cap label define metarea_lbl 038 `"Anchorage, AK"', add
cap label define metarea_lbl 040 `"Anderson, IN"', add
cap label define metarea_lbl 044 `"Ann Arbor, MI"', add
cap label define metarea_lbl 045 `"Anniston, AL"', add
cap label define metarea_lbl 046 `"Appleton-Oshkosh-Neenah, WI"', add
cap label define metarea_lbl 047 `"Arecibo, PR"', add
cap label define metarea_lbl 048 `"Asheville, NC"', add
cap label define metarea_lbl 050 `"Athens, GA"', add
cap label define metarea_lbl 052 `"Atlanta, GA"', add
cap label define metarea_lbl 056 `"Atlantic City, NJ"', add
cap label define metarea_lbl 058 `"Auburn-Opekika, AL"', add
cap label define metarea_lbl 060 `"Augusta-Aiken, GA/SC"', add
cap label define metarea_lbl 064 `"Austin, TX"', add
cap label define metarea_lbl 068 `"Bakersfield, CA"', add
cap label define metarea_lbl 072 `"Baltimore, MD"', add
cap label define metarea_lbl 073 `"Bangor, ME"', add
cap label define metarea_lbl 074 `"Barnstable-Yarmouth, MA"', add
cap label define metarea_lbl 076 `"Baton Rouge, LA"', add
cap label define metarea_lbl 078 `"Battle Creek, MI"', add
cap label define metarea_lbl 084 `"Beaumont-Port Arthur-Orange, TX"', add
cap label define metarea_lbl 086 `"Bellingham, WA"', add
cap label define metarea_lbl 087 `"Benton Harbor, MI"', add
cap label define metarea_lbl 088 `"Billings, MT"', add
cap label define metarea_lbl 092 `"Biloxi-Gulfport, MS"', add
cap label define metarea_lbl 096 `"Binghamton, NY"', add
cap label define metarea_lbl 100 `"Birmingham, AL"', add
cap label define metarea_lbl 102 `"Bloomington, IN"', add
cap label define metarea_lbl 104 `"Bloomington-Normal, IL"', add
cap label define metarea_lbl 108 `"Boise City, ID"', add
cap label define metarea_lbl 112 `"Boston, MA/NH"', add
cap label define metarea_lbl 114 `"Bradenton, FL"', add
cap label define metarea_lbl 115 `"Bremerton, WA"', add
cap label define metarea_lbl 116 `"Bridgeport, CT"', add
cap label define metarea_lbl 120 `"Brockton, MA"', add
cap label define metarea_lbl 124 `"Brownsville-Harlingen-San Benito, TX"', add
cap label define metarea_lbl 126 `"Bryan-College Station, TX"', add
cap label define metarea_lbl 128 `"Buffalo-Niagara Falls, NY"', add
cap label define metarea_lbl 130 `"Burlington, NC"', add
cap label define metarea_lbl 131 `"Burlington, VT"', add
cap label define metarea_lbl 132 `"Canton, OH"', add
cap label define metarea_lbl 133 `"Caguas, PR"', add
cap label define metarea_lbl 135 `"Casper, WY"', add
cap label define metarea_lbl 136 `"Cedar Rapids, IA"', add
cap label define metarea_lbl 140 `"Champaign-Urbana-Rantoul, IL"', add
cap label define metarea_lbl 144 `"Charleston-N. Charleston, SC"', add
cap label define metarea_lbl 148 `"Charleston, WV"', add
cap label define metarea_lbl 152 `"Charlotte-Gastonia-Rock Hill, NC/SC"', add
cap label define metarea_lbl 154 `"Charlottesville, VA"', add
cap label define metarea_lbl 156 `"Chattanooga, TN/GA"', add
cap label define metarea_lbl 158 `"Cheyenne, WY"', add
cap label define metarea_lbl 160 `"Chicago, IL"', add
cap label define metarea_lbl 162 `"Chico, CA"', add
cap label define metarea_lbl 164 `"Cincinnati-Hamilton, OH/KY/IN"', add
cap label define metarea_lbl 166 `"Clarksville- Hopkinsville, TN/KY"', add
cap label define metarea_lbl 168 `"Cleveland, OH"', add
cap label define metarea_lbl 172 `"Colorado Springs, CO"', add
cap label define metarea_lbl 174 `"Columbia, MO"', add
cap label define metarea_lbl 176 `"Columbia, SC"', add
cap label define metarea_lbl 180 `"Columbus, GA/AL"', add
cap label define metarea_lbl 184 `"Columbus, OH"', add
cap label define metarea_lbl 188 `"Corpus Christi, TX"', add
cap label define metarea_lbl 190 `"Cumberland, MD/WV"', add
cap label define metarea_lbl 192 `"Dallas-Fort Worth, TX"', add
cap label define metarea_lbl 193 `"Danbury, CT"', add
cap label define metarea_lbl 195 `"Danville, VA"', add
cap label define metarea_lbl 196 `"Davenport, IA - Rock Island-Moline, IL"', add
cap label define metarea_lbl 200 `"Dayton-Springfield, OH"', add
cap label define metarea_lbl 202 `"Daytona Beach, FL"', add
cap label define metarea_lbl 203 `"Decatur, AL"', add
cap label define metarea_lbl 204 `"Decatur, IL"', add
cap label define metarea_lbl 208 `"Denver-Boulder, CO"', add
cap label define metarea_lbl 212 `"Des Moines, IA"', add
cap label define metarea_lbl 216 `"Detroit, MI"', add
cap label define metarea_lbl 218 `"Dothan, AL"', add
cap label define metarea_lbl 219 `"Dover, DE"', add
cap label define metarea_lbl 220 `"Dubuque, IA"', add
cap label define metarea_lbl 224 `"Duluth-Superior, MN/WI"', add
cap label define metarea_lbl 228 `"Dutchess Co., NY"', add
cap label define metarea_lbl 229 `"Eau Claire, WI"', add
cap label define metarea_lbl 231 `"El Paso, TX"', add
cap label define metarea_lbl 232 `"Elkhart-Goshen, IN"', add
cap label define metarea_lbl 233 `"Elmira, NY"', add
cap label define metarea_lbl 234 `"Enid, OK"', add
cap label define metarea_lbl 236 `"Erie, PA"', add
cap label define metarea_lbl 240 `"Eugene-Springfield, OR"', add
cap label define metarea_lbl 244 `"Evansville, IN/KY"', add
cap label define metarea_lbl 252 `"Fargo-Morehead, ND/MN"', add
cap label define metarea_lbl 256 `"Fayetteville, NC"', add
cap label define metarea_lbl 258 `"Fayetteville-Springdale, AR"', add
cap label define metarea_lbl 260 `"Fitchburg-Leominster, MA"', add
cap label define metarea_lbl 262 `"Flagstaff, AZ/UT"', add
cap label define metarea_lbl 264 `"Flint, MI"', add
cap label define metarea_lbl 265 `"Florence, AL"', add
cap label define metarea_lbl 266 `"Florence, SC"', add
cap label define metarea_lbl 267 `"Fort Collins-Loveland, CO"', add
cap label define metarea_lbl 268 `"Fort Lauderdale-Hollywood-Pompano Beach, FL"', add
cap label define metarea_lbl 270 `"Fort Myers-Cape Coral, FL"', add
cap label define metarea_lbl 271 `"Fort Pierce, FL"', add
cap label define metarea_lbl 272 `"Fort Smith, AR/OK"', add
cap label define metarea_lbl 275 `"Fort Walton Beach, FL"', add
cap label define metarea_lbl 276 `"Fort Wayne, IN"', add
cap label define metarea_lbl 284 `"Fresno, CA"', add
cap label define metarea_lbl 288 `"Gadsden, AL"', add
cap label define metarea_lbl 290 `"Gainesville, FL"', add
cap label define metarea_lbl 292 `"Galveston-Texas City, TX"', add
cap label define metarea_lbl 297 `"Glens Falls, NY"', add
cap label define metarea_lbl 298 `"Goldsboro, NC"', add
cap label define metarea_lbl 299 `"Grand Forks, ND"', add
cap label define metarea_lbl 300 `"Grand Rapids, MI"', add
cap label define metarea_lbl 301 `"Grand Junction, CO"', add
cap label define metarea_lbl 304 `"Great Falls, MT"', add
cap label define metarea_lbl 306 `"Greeley, CO"', add
cap label define metarea_lbl 308 `"Green Bay, WI"', add
cap label define metarea_lbl 312 `"Greensboro-Winston Salem-High Point, NC"', add
cap label define metarea_lbl 315 `"Greenville, NC"', add
cap label define metarea_lbl 316 `"Greenville-Spartenburg-Anderson, SC"', add
cap label define metarea_lbl 318 `"Hagerstown, MD"', add
cap label define metarea_lbl 320 `"Hamilton-Middleton, OH"', add
cap label define metarea_lbl 324 `"Harrisburg-Lebanon--Carlisle, PA"', add
cap label define metarea_lbl 328 `"Hartford-Bristol-Middleton- New Britain, CT"', add
cap label define metarea_lbl 329 `"Hickory-Morganton, NC"', add
cap label define metarea_lbl 330 `"Hattiesburg, MS"', add
cap label define metarea_lbl 332 `"Honolulu, HI"', add
cap label define metarea_lbl 335 `"Houma-Thibodoux, LA"', add
cap label define metarea_lbl 336 `"Houston-Brazoria, TX"', add
cap label define metarea_lbl 340 `"Huntington-Ashland, WV/KY/OH"', add
cap label define metarea_lbl 344 `"Huntsville, AL"', add
cap label define metarea_lbl 348 `"Indianapolis, IN"', add
cap label define metarea_lbl 350 `"Iowa City, IA"', add
cap label define metarea_lbl 352 `"Jackson, MI"', add
cap label define metarea_lbl 356 `"Jackson, MS"', add
cap label define metarea_lbl 358 `"Jackson, TN"', add
cap label define metarea_lbl 359 `"Jacksonville, FL"', add
cap label define metarea_lbl 360 `"Jacksonville, NC"', add
cap label define metarea_lbl 361 `"Jamestown-Dunkirk, NY"', add
cap label define metarea_lbl 362 `"Janesville-Beloit, WI"', add
cap label define metarea_lbl 366 `"Johnson City-Kingsport--Bristol, TN/VA"', add
cap label define metarea_lbl 368 `"Johnstown, PA"', add
cap label define metarea_lbl 371 `"Joplin, MO"', add
cap label define metarea_lbl 372 `"Kalamazoo-Portage, MI"', add
cap label define metarea_lbl 374 `"Kankakee, IL"', add
cap label define metarea_lbl 376 `"Kansas City, MO/KS"', add
cap label define metarea_lbl 380 `"Kenosha, WI"', add
cap label define metarea_lbl 381 `"Kileen-Temple, TX"', add
cap label define metarea_lbl 384 `"Knoxville, TN"', add
cap label define metarea_lbl 385 `"Kokomo, IN"', add
cap label define metarea_lbl 387 `"LaCrosse, WI"', add
cap label define metarea_lbl 388 `"Lafayette, LA"', add
cap label define metarea_lbl 392 `"Lafayette-W. Lafayette, IN"', add
cap label define metarea_lbl 396 `"Lake Charles, LA"', add
cap label define metarea_lbl 398 `"Lakeland-Winterhaven, FL"', add
cap label define metarea_lbl 400 `"Lancaster, PA"', add
cap label define metarea_lbl 404 `"Lansing-E. Lansing, MI"', add
cap label define metarea_lbl 408 `"Laredo, TX"', add
cap label define metarea_lbl 410 `"Las Cruces, NM"', add
cap label define metarea_lbl 412 `"Las Vegas, NV"', add
cap label define metarea_lbl 415 `"Lawrence, KS"', add
cap label define metarea_lbl 420 `"Lawton, OK"', add
cap label define metarea_lbl 424 `"Lewiston-Auburn, ME"', add
cap label define metarea_lbl 428 `"Lexington-Fayette, KY"', add
cap label define metarea_lbl 432 `"Lima, OH"', add
cap label define metarea_lbl 436 `"Lincoln, NE"', add
cap label define metarea_lbl 440 `"Little Rock-N. Little Rock, AR"', add
cap label define metarea_lbl 441 `"Long Branch-Asbury Park, NJ"', add
cap label define metarea_lbl 442 `"Longview-Marshall, TX"', add
cap label define metarea_lbl 444 `"Lorain-Elyria, OH"', add
cap label define metarea_lbl 448 `"Los Angeles-Long Beach, CA"', add
cap label define metarea_lbl 452 `"Louisville, KY/IN"', add
cap label define metarea_lbl 460 `"Lubbock, TX"', add
cap label define metarea_lbl 464 `"Lynchburg, VA"', add
cap label define metarea_lbl 468 `"Macon-Warner Robins, GA"', add
cap label define metarea_lbl 472 `"Madison, WI"', add
cap label define metarea_lbl 476 `"Manchester, NH"', add
cap label define metarea_lbl 480 `"Mansfield, OH"', add
cap label define metarea_lbl 484 `"Mayaguez, PR"', add
cap label define metarea_lbl 488 `"McAllen-Edinburg-Pharr-Mission, TX"', add
cap label define metarea_lbl 489 `"Medford, OR"', add
cap label define metarea_lbl 490 `"Melbourne-Titusville-Cocoa-Palm Bay, FL"', add
cap label define metarea_lbl 492 `"Memphis, TN/AR/MS"', add
cap label define metarea_lbl 494 `"Merced, CA"', add
cap label define metarea_lbl 500 `"Miami-Hialeah, FL"', add
cap label define metarea_lbl 504 `"Midland, TX"', add
cap label define metarea_lbl 508 `"Milwaukee, WI"', add
cap label define metarea_lbl 512 `"Minneapolis-St. Paul, MN"', add
cap label define metarea_lbl 514 `"Missoula, MT"', add
cap label define metarea_lbl 516 `"Mobile, AL"', add
cap label define metarea_lbl 517 `"Modesto, CA"', add
cap label define metarea_lbl 519 `"Monmouth-Ocean, NJ"', add
cap label define metarea_lbl 520 `"Monroe, LA"', add
cap label define metarea_lbl 524 `"Montgomery, AL"', add
cap label define metarea_lbl 528 `"Muncie, IN"', add
cap label define metarea_lbl 532 `"Muskegon-Norton Shores-Muskegon Heights, MI"', add
cap label define metarea_lbl 533 `"Myrtle Beach, SC"', add
cap label define metarea_lbl 534 `"Naples, FL"', add
cap label define metarea_lbl 535 `"Nashua, NH"', add
cap label define metarea_lbl 536 `"Nashville, TN"', add
cap label define metarea_lbl 540 `"New Bedford, MA"', add
cap label define metarea_lbl 546 `"New Brunswick-Perth Amboy-Sayreville, NJ"', add
cap label define metarea_lbl 548 `"New Haven-Meriden, CT"', add
cap label define metarea_lbl 552 `"New London-Norwich, CT/RI"', add
cap label define metarea_lbl 556 `"New Orleans, LA"', add
cap label define metarea_lbl 560 `"New York, NY-Northeastern NJ"', add
cap label define metarea_lbl 564 `"Newark, OH"', add
cap label define metarea_lbl 566 `"Newburgh-Middletown, NY"', add
cap label define metarea_lbl 572 `"Norfolk-VA Beach--Newport News, VA"', add
cap label define metarea_lbl 576 `"Norwalk, CT"', add
cap label define metarea_lbl 579 `"Ocala, FL"', add
cap label define metarea_lbl 580 `"Odessa, TX"', add
cap label define metarea_lbl 588 `"Oklahoma City, OK"', add
cap label define metarea_lbl 591 `"Olympia, WA"', add
cap label define metarea_lbl 592 `"Omaha, NE/IA"', add
cap label define metarea_lbl 595 `"Orange, NY"', add
cap label define metarea_lbl 596 `"Orlando, FL"', add
cap label define metarea_lbl 599 `"Owensboro, KY"', add
cap label define metarea_lbl 601 `"Panama City, FL"', add
cap label define metarea_lbl 602 `"Parkersburg-Marietta,WV/OH"', add
cap label define metarea_lbl 603 `"Pascagoula-Moss Point, MS"', add
cap label define metarea_lbl 608 `"Pensacola, FL"', add
cap label define metarea_lbl 612 `"Peoria, IL"', add
cap label define metarea_lbl 616 `"Philadelphia, PA/NJ"', add
cap label define metarea_lbl 620 `"Phoenix, AZ"', add
cap label define metarea_lbl 628 `"Pittsburgh, PA"', add
cap label define metarea_lbl 632 `"Pittsfield, MA"', add
cap label define metarea_lbl 636 `"Ponce, PR"', add
cap label define metarea_lbl 640 `"Portland, ME"', add
cap label define metarea_lbl 644 `"Portland, OR/WA"', add
cap label define metarea_lbl 645 `"Portsmouth-Dover--Rochester, NH/ME"', add
cap label define metarea_lbl 646 `"Poughkeepsie, NY"', add
cap label define metarea_lbl 648 `"Providence-Fall River-Pawtucket, MA/RI"', add
cap label define metarea_lbl 652 `"Provo-Orem, UT"', add
cap label define metarea_lbl 656 `"Pueblo, CO"', add
cap label define metarea_lbl 658 `"Punta Gorda, FL"', add
cap label define metarea_lbl 660 `"Racine, WI"', add
cap label define metarea_lbl 664 `"Raleigh-Durham, NC"', add
cap label define metarea_lbl 666 `"Rapid City, SD"', add
cap label define metarea_lbl 668 `"Reading, PA"', add
cap label define metarea_lbl 669 `"Redding, CA"', add
cap label define metarea_lbl 672 `"Reno, NV"', add
cap label define metarea_lbl 674 `"Richland-Kennewick-Pasco, WA"', add
cap label define metarea_lbl 676 `"Richmond-Petersburg, VA"', add
cap label define metarea_lbl 678 `"Riverside-San Bernardino, CA"', add
cap label define metarea_lbl 680 `"Roanoke, VA"', add
cap label define metarea_lbl 682 `"Rochester, MN"', add
cap label define metarea_lbl 684 `"Rochester, NY"', add
cap label define metarea_lbl 688 `"Rockford, IL"', add
cap label define metarea_lbl 689 `"Rocky Mount, NC"', add
cap label define metarea_lbl 692 `"Sacramento, CA"', add
cap label define metarea_lbl 696 `"Saginaw-Bay City-Midland, MI"', add
cap label define metarea_lbl 698 `"St. Cloud, MN"', add
cap label define metarea_lbl 700 `"St. Joseph, MO"', add
cap label define metarea_lbl 704 `"St. Louis, MO/IL"', add
cap label define metarea_lbl 708 `"Salem, OR"', add
cap label define metarea_lbl 712 `"Salinas-Sea Side-Monterey, CA"', add
cap label define metarea_lbl 714 `"Salisbury-Concord, NC"', add
cap label define metarea_lbl 716 `"Salt Lake City-Ogden, UT"', add
cap label define metarea_lbl 720 `"San Angelo, TX"', add
cap label define metarea_lbl 724 `"San Antonio, TX"', add
cap label define metarea_lbl 732 `"San Diego, CA"', add
cap label define metarea_lbl 736 `"San Francisco-Oakland-Vallejo, CA"', add
cap label define metarea_lbl 740 `"San Jose, CA"', add
cap label define metarea_lbl 744 `"San Juan-Bayamon, PR"', add
cap label define metarea_lbl 746 `"San Luis Obispo-Atascad-P Robles, CA"', add
cap label define metarea_lbl 747 `"Santa Barbara-Santa Maria-Lompoc, CA"', add
cap label define metarea_lbl 748 `"Santa Cruz, CA"', add
cap label define metarea_lbl 749 `"Santa Fe, NM"', add
cap label define metarea_lbl 750 `"Santa Rosa-Petaluma, CA"', add
cap label define metarea_lbl 751 `"Sarasota, FL"', add
cap label define metarea_lbl 752 `"Savannah, GA"', add
cap label define metarea_lbl 756 `"Scranton-Wilkes-Barre, PA"', add
cap label define metarea_lbl 760 `"Seattle-Everett, WA"', add
cap label define metarea_lbl 761 `"Sharon, PA"', add
cap label define metarea_lbl 762 `"Sheboygan, WI"', add
cap label define metarea_lbl 764 `"Sherman-Davidson, TX"', add
cap label define metarea_lbl 768 `"Shreveport, LA"', add
cap label define metarea_lbl 772 `"Sioux City, IA/NE"', add
cap label define metarea_lbl 776 `"Sioux Falls, SD"', add
cap label define metarea_lbl 780 `"South Bend-Mishawaka, IN"', add
cap label define metarea_lbl 784 `"Spokane, WA"', add
cap label define metarea_lbl 788 `"Springfield, IL"', add
cap label define metarea_lbl 792 `"Springfield, MO"', add
cap label define metarea_lbl 800 `"Springfield-Holyoke-Chicopee, MA"', add
cap label define metarea_lbl 804 `"Stamford, CT"', add
cap label define metarea_lbl 805 `"State College, PA"', add
cap label define metarea_lbl 808 `"Steubenville-Weirton,OH/WV"', add
cap label define metarea_lbl 812 `"Stockton, CA"', add
cap label define metarea_lbl 814 `"Sumter, SC"', add
cap label define metarea_lbl 816 `"Syracuse, NY"', add
cap label define metarea_lbl 820 `"Tacoma, WA"', add
cap label define metarea_lbl 824 `"Tallahassee, FL"', add
cap label define metarea_lbl 828 `"Tampa-St. Petersburg-Clearwater, FL"', add
cap label define metarea_lbl 832 `"Terre Haute, IN"', add
cap label define metarea_lbl 836 `"Texarkana, TX/AR"', add
cap label define metarea_lbl 840 `"Toledo, OH/MI"', add
cap label define metarea_lbl 844 `"Topeka, KS"', add
cap label define metarea_lbl 848 `"Trenton, NJ"', add
cap label define metarea_lbl 852 `"Tucson, AZ"', add
cap label define metarea_lbl 856 `"Tulsa, OK"', add
cap label define metarea_lbl 860 `"Tuscaloosa, AL"', add
cap label define metarea_lbl 864 `"Tyler, TX"', add
cap label define metarea_lbl 868 `"Utica-Rome, NY"', add
cap label define metarea_lbl 873 `"Ventura-Oxnard-Simi Valley, CA"', add
cap label define metarea_lbl 875 `"Victoria, TX"', add
cap label define metarea_lbl 876 `"Vineland-Milville-Bridgetown, NJ"', add
cap label define metarea_lbl 878 `"Visalia-Tulare-Porterville, CA"', add
cap label define metarea_lbl 880 `"Waco, TX"', add
cap label define metarea_lbl 884 `"Washington, DC/MD/VA"', add
cap label define metarea_lbl 888 `"Waterbury, CT"', add
cap label define metarea_lbl 892 `"Waterloo-Cedar Falls, IA"', add
cap label define metarea_lbl 894 `"Wausau, WI"', add
cap label define metarea_lbl 896 `"West Palm Beach-Boca Raton-Delray Beach, FL"', add
cap label define metarea_lbl 900 `"Wheeling, WV/OH"', add
cap label define metarea_lbl 904 `"Wichita, KS"', add
cap label define metarea_lbl 908 `"Wichita Falls, TX"', add
cap label define metarea_lbl 914 `"Williamsport, PA"', add
cap label define metarea_lbl 916 `"Wilmington, DE/NJ/MD"', add
cap label define metarea_lbl 920 `"Wilmington, NC"', add
cap label define metarea_lbl 924 `"Worcester, MA"', add
cap label define metarea_lbl 926 `"Yakima, WA"', add
cap label define metarea_lbl 927 `"Yolo, CA"', add
cap label define metarea_lbl 928 `"York, PA"', add
cap label define metarea_lbl 932 `"Youngstown-Warren, OH/PA"', add
cap label define metarea_lbl 934 `"Yuba City, CA"', add
cap label define metarea_lbl 936 `"Yuma, AZ"', add
cap label values metarea metarea_lbl

cap label define metaread_lbl 0000 `"Not identifiable or not in an MSA"'
cap label define metaread_lbl 0040 `"Abilene, TX"', add
cap label define metaread_lbl 0060 `"Aguadilla, PR"', add
cap label define metaread_lbl 0080 `"Akron, OH"', add
cap label define metaread_lbl 0120 `"Albany, GA"', add
cap label define metaread_lbl 0160 `"Albany-Schenectady-Troy, NY"', add
cap label define metaread_lbl 0200 `"Albuquerque, NM"', add
cap label define metaread_lbl 0220 `"Alexandria, LA"', add
cap label define metaread_lbl 0240 `"Allentown-Bethlehem-Easton, PA/NJ"', add
cap label define metaread_lbl 0280 `"Altoona, PA"', add
cap label define metaread_lbl 0320 `"Amarillo, TX"', add
cap label define metaread_lbl 0380 `"Anchorage, AK"', add
cap label define metaread_lbl 0400 `"Anderson, IN"', add
cap label define metaread_lbl 0440 `"Ann Arbor, MI"', add
cap label define metaread_lbl 0450 `"Anniston, AL"', add
cap label define metaread_lbl 0460 `"Appleton-Oshkosh-Neenah, WI"', add
cap label define metaread_lbl 0470 `"Arecibo, PR"', add
cap label define metaread_lbl 0480 `"Asheville, NC"', add
cap label define metaread_lbl 0500 `"Athens, GA"', add
cap label define metaread_lbl 0520 `"Atlanta, GA"', add
cap label define metaread_lbl 0560 `"Atlantic City, NJ"', add
cap label define metaread_lbl 0580 `"Auburn-Opelika, AL"', add
cap label define metaread_lbl 0600 `"Augusta-Aiken, GA/SC"', add
cap label define metaread_lbl 0640 `"Austin, TX"', add
cap label define metaread_lbl 0680 `"Bakersfield, CA"', add
cap label define metaread_lbl 0720 `"Baltimore, MD"', add
cap label define metaread_lbl 0730 `"Bangor, ME"', add
cap label define metaread_lbl 0740 `"Barnstable-Yarmouth, MA"', add
cap label define metaread_lbl 0760 `"Baton Rouge, LA"', add
cap label define metaread_lbl 0780 `"Battle Creek, MI"', add
cap label define metaread_lbl 0840 `"Beaumont-Port Arthur-Orange, TX"', add
cap label define metaread_lbl 0860 `"Bellingham, WA"', add
cap label define metaread_lbl 0870 `"Benton Harbor, MI"', add
cap label define metaread_lbl 0880 `"Billings, MT"', add
cap label define metaread_lbl 0920 `"Biloxi-Gulfport, MS"', add
cap label define metaread_lbl 0960 `"Binghamton, NY"', add
cap label define metaread_lbl 1000 `"Birmingham, AL"', add
cap label define metaread_lbl 1010 `"Bismarck, ND"', add
cap label define metaread_lbl 1020 `"Bloomington, IN"', add
cap label define metaread_lbl 1040 `"Bloomington-Normal, IL"', add
cap label define metaread_lbl 1080 `"Boise City, ID"', add
cap label define metaread_lbl 1120 `"Boston, MA"', add
cap label define metaread_lbl 1121 `"Lawrence-Haverhill, MA/NH"', add
cap label define metaread_lbl 1122 `"Lowell, MA/NH"', add
cap label define metaread_lbl 1123 `"Salem-Gloucester, MA"', add
cap label define metaread_lbl 1140 `"Bradenton, FL"', add
cap label define metaread_lbl 1150 `"Bremerton, WA"', add
cap label define metaread_lbl 1160 `"Bridgeport, CT"', add
cap label define metaread_lbl 1200 `"Brockton, MA"', add
cap label define metaread_lbl 1240 `"Brownsville-Harlingen-San Benito, TX"', add
cap label define metaread_lbl 1260 `"Bryan-College Station, TX"', add
cap label define metaread_lbl 1280 `"Buffalo-Niagara Falls, NY"', add
cap label define metaread_lbl 1281 `"Niagara Falls, NY"', add
cap label define metaread_lbl 1300 `"Burlington, NC"', add
cap label define metaread_lbl 1310 `"Burlington, VT"', add
cap label define metaread_lbl 1320 `"Canton, OH"', add
cap label define metaread_lbl 1330 `"Caguas, PR"', add
cap label define metaread_lbl 1350 `"Casper, WY"', add
cap label define metaread_lbl 1360 `"Cedar Rapids, IA"', add
cap label define metaread_lbl 1400 `"Champaign-Urbana-Rantoul, IL"', add
cap label define metaread_lbl 1440 `"Charleston-N. Charleston, SC"', add
cap label define metaread_lbl 1480 `"Charleston, WV"', add
cap label define metaread_lbl 1520 `"Charlotte-Gastonia-Rock Hill, SC"', add
cap label define metaread_lbl 1521 `"Rock Hill, SC"', add
cap label define metaread_lbl 1540 `"Charlottesville, VA"', add
cap label define metaread_lbl 1560 `"Chattanooga, TN/GA"', add
cap label define metaread_lbl 1580 `"Cheyenne, WY"', add
cap label define metaread_lbl 1600 `"Chicago-Gary-Lake, IL"', add
cap label define metaread_lbl 1601 `"Aurora-Elgin, IL"', add
cap label define metaread_lbl 1602 `"Gary-Hammond-East Chicago, IN"', add
cap label define metaread_lbl 1603 `"Joliet, IL"', add
cap label define metaread_lbl 1604 `"Lake County, IL"', add
cap label define metaread_lbl 1620 `"Chico, CA"', add
cap label define metaread_lbl 1640 `"Cincinnati, OH/KY/IN"', add
cap label define metaread_lbl 1660 `"Clarksville-Hopkinsville, TN/KY"', add
cap label define metaread_lbl 1680 `"Cleveland, OH"', add
cap label define metaread_lbl 1720 `"Colorado Springs, CO"', add
cap label define metaread_lbl 1740 `"Columbia, MO"', add
cap label define metaread_lbl 1760 `"Columbia, SC"', add
cap label define metaread_lbl 1800 `"Columbus, GA/AL"', add
cap label define metaread_lbl 1840 `"Columbus, OH"', add
cap label define metaread_lbl 1880 `"Corpus Christi, TX"', add
cap label define metaread_lbl 1900 `"Cumberland, MD/WV"', add
cap label define metaread_lbl 1920 `"Dallas-Fort Worth, TX"', add
cap label define metaread_lbl 1921 `"Fort Worth-Arlington, TX"', add
cap label define metaread_lbl 1930 `"Danbury, CT"', add
cap label define metaread_lbl 1950 `"Danville, VA"', add
cap label define metaread_lbl 1960 `"Davenport, IA - Rock Island-Moline, IL"', add
cap label define metaread_lbl 2000 `"Dayton-Springfield, OH"', add
cap label define metaread_lbl 2001 `"Springfield, OH"', add
cap label define metaread_lbl 2020 `"Daytona Beach, FL"', add
cap label define metaread_lbl 2030 `"Decatur, AL"', add
cap label define metaread_lbl 2040 `"Decatur, IL"', add
cap label define metaread_lbl 2080 `"Denver-Boulder-Longmont, CO"', add
cap label define metaread_lbl 2081 `"Boulder-Longmont, CO"', add
cap label define metaread_lbl 2120 `"Des Moines, IA"', add
cap label define metaread_lbl 2121 `"Polk, IA"', add
cap label define metaread_lbl 2160 `"Detroit, MI"', add
cap label define metaread_lbl 2180 `"Dothan, AL"', add
cap label define metaread_lbl 2190 `"Dover, DE"', add
cap label define metaread_lbl 2200 `"Dubuque, IA"', add
cap label define metaread_lbl 2240 `"Duluth-Superior, MN/WI"', add
cap label define metaread_lbl 2281 `"Dutchess Co., NY"', add
cap label define metaread_lbl 2290 `"Eau Claire, WI"', add
cap label define metaread_lbl 2310 `"El Paso, TX"', add
cap label define metaread_lbl 2320 `"Elkhart-Goshen, IN"', add
cap label define metaread_lbl 2330 `"Elmira, NY"', add
cap label define metaread_lbl 2340 `"Enid, OK"', add
cap label define metaread_lbl 2360 `"Erie, PA"', add
cap label define metaread_lbl 2400 `"Eugene-Springfield, OR"', add
cap label define metaread_lbl 2440 `"Evansville, IN/KY"', add
cap label define metaread_lbl 2520 `"Fargo-Morehead, ND/MN"', add
cap label define metaread_lbl 2560 `"Fayetteville, NC"', add
cap label define metaread_lbl 2580 `"Fayetteville-Springdale, AR"', add
cap label define metaread_lbl 2600 `"Fitchburg-Leominster, MA"', add
cap label define metaread_lbl 2620 `"Flagstaff, AZ/UT"', add
cap label define metaread_lbl 2640 `"Flint, MI"', add
cap label define metaread_lbl 2650 `"Florence, AL"', add
cap label define metaread_lbl 2660 `"Florence, SC"', add
cap label define metaread_lbl 2670 `"Fort Collins-Loveland, CO"', add
cap label define metaread_lbl 2680 `"Fort Lauderdale-Hollywood-Pompano Beach, FL"', add
cap label define metaread_lbl 2700 `"Fort Myers-Cape Coral, FL"', add
cap label define metaread_lbl 2710 `"Fort Pierce, FL"', add
cap label define metaread_lbl 2720 `"Fort Smith, AR/OK"', add
cap label define metaread_lbl 2750 `"Fort Walton Beach, FL"', add
cap label define metaread_lbl 2760 `"Fort Wayne, IN"', add
cap label define metaread_lbl 2840 `"Fresno, CA"', add
cap label define metaread_lbl 2880 `"Gadsden, AL"', add
cap label define metaread_lbl 2900 `"Gainesville, FL"', add
cap label define metaread_lbl 2920 `"Galveston-Texas City, TX"', add
cap label define metaread_lbl 2970 `"Glens Falls, NY"', add
cap label define metaread_lbl 2980 `"Goldsboro, NC"', add
cap label define metaread_lbl 2990 `"Grand Forks, ND/MN"', add
cap label define metaread_lbl 3000 `"Grand Rapids, MI"', add
cap label define metaread_lbl 3010 `"Grand Junction, CO"', add
cap label define metaread_lbl 3040 `"Great Falls, MT"', add
cap label define metaread_lbl 3060 `"Greeley, CO"', add
cap label define metaread_lbl 3080 `"Green Bay, WI"', add
cap label define metaread_lbl 3120 `"Greensboro-Winston Salem-High Point, NC"', add
cap label define metaread_lbl 3121 `"Winston-Salem, NC"', add
cap label define metaread_lbl 3150 `"Greenville, NC"', add
cap label define metaread_lbl 3160 `"Greenville-Spartenburg-Anderson, SC"', add
cap label define metaread_lbl 3161 `"Anderson, SC"', add
cap label define metaread_lbl 3180 `"Hagerstown, MD"', add
cap label define metaread_lbl 3200 `"Hamilton-Middleton, OH"', add
cap label define metaread_lbl 3240 `"Harrisburg-Lebanon-Carlisle, PA"', add
cap label define metaread_lbl 3280 `"Hartford-Bristol-Middleton-New Britain, CT"', add
cap label define metaread_lbl 3281 `"Bristol, CT"', add
cap label define metaread_lbl 3282 `"Middletown, CT"', add
cap label define metaread_lbl 3283 `"New Britain, CT"', add
cap label define metaread_lbl 3290 `"Hickory-Morganton, NC"', add
cap label define metaread_lbl 3300 `"Hattiesburg, MS"', add
cap label define metaread_lbl 3320 `"Honolulu, HI"', add
cap label define metaread_lbl 3350 `"Houma-Thibodoux, LA"', add
cap label define metaread_lbl 3360 `"Houston-Brazoria, TX"', add
cap label define metaread_lbl 3361 `"Brazoria, TX"', add
cap label define metaread_lbl 3400 `"Huntington-Ashland, WV/KY/OH"', add
cap label define metaread_lbl 3440 `"Huntsville, AL"', add
cap label define metaread_lbl 3480 `"Indianapolis, IN"', add
cap label define metaread_lbl 3500 `"Iowa City, IA"', add
cap label define metaread_lbl 3520 `"Jackson, MI"', add
cap label define metaread_lbl 3560 `"Jackson, MS"', add
cap label define metaread_lbl 3580 `"Jackson, TN"', add
cap label define metaread_lbl 3590 `"Jacksonville, FL"', add
cap label define metaread_lbl 3600 `"Jacksonville, NC"', add
cap label define metaread_lbl 3610 `"Jamestown-Dunkirk, NY"', add
cap label define metaread_lbl 3620 `"Janesville-Beloit, WI"', add
cap label define metaread_lbl 3660 `"Johnson City-Kingsport-Bristol, TN/VA"', add
cap label define metaread_lbl 3680 `"Johnstown, PA"', add
cap label define metaread_lbl 3710 `"Joplin, MO"', add
cap label define metaread_lbl 3720 `"Kalamazoo-Portage, MI"', add
cap label define metaread_lbl 3740 `"Kankakee, IL"', add
cap label define metaread_lbl 3760 `"Kansas City, MO/KS"', add
cap label define metaread_lbl 3800 `"Kenosha, WI"', add
cap label define metaread_lbl 3810 `"Kileen-Temple, TX"', add
cap label define metaread_lbl 3840 `"Knoxville, TN"', add
cap label define metaread_lbl 3850 `"Kokomo, IN"', add
cap label define metaread_lbl 3870 `"LaCrosse, WI"', add
cap label define metaread_lbl 3880 `"Lafayette, LA"', add
cap label define metaread_lbl 3920 `"Lafayette-W. Lafayette, IN"', add
cap label define metaread_lbl 3960 `"Lake Charles, LA"', add
cap label define metaread_lbl 3980 `"Lakeland-Winterhaven, FL"', add
cap label define metaread_lbl 4000 `"Lancaster, PA"', add
cap label define metaread_lbl 4040 `"Lansing-E. Lansing, MI"', add
cap label define metaread_lbl 4080 `"Laredo, TX"', add
cap label define metaread_lbl 4100 `"Las Cruces, NM"', add
cap label define metaread_lbl 4120 `"Las Vegas, NV"', add
cap label define metaread_lbl 4150 `"Lawrence, KS"', add
cap label define metaread_lbl 4200 `"Lawton, OK"', add
cap label define metaread_lbl 4240 `"Lewiston-Auburn, ME"', add
cap label define metaread_lbl 4280 `"Lexington-Fayette, KY"', add
cap label define metaread_lbl 4320 `"Lima, OH"', add
cap label define metaread_lbl 4360 `"Lincoln, NE"', add
cap label define metaread_lbl 4400 `"Little Rock-N. Little Rock, AR"', add
cap label define metaread_lbl 4410 `"Long Branch-Asbury Park, NJ"', add
cap label define metaread_lbl 4420 `"Longview-Marshall, TX"', add
cap label define metaread_lbl 4440 `"Lorain-Elyria, OH"', add
cap label define metaread_lbl 4480 `"Los Angeles-Long Beach, CA"', add
cap label define metaread_lbl 4481 `"Anaheim-Santa Ana-Garden Grove, CA"', add
cap label define metaread_lbl 4482 `"Orange County, CA"', add
cap label define metaread_lbl 4520 `"Louisville, KY/IN"', add
cap label define metaread_lbl 4600 `"Lubbock, TX"', add
cap label define metaread_lbl 4640 `"Lynchburg, VA"', add
cap label define metaread_lbl 4680 `"Macon-Warner Robins, GA"', add
cap label define metaread_lbl 4720 `"Madison, WI"', add
cap label define metaread_lbl 4760 `"Manchester, NH"', add
cap label define metaread_lbl 4800 `"Mansfield, OH"', add
cap label define metaread_lbl 4840 `"Mayaguez, PR"', add
cap label define metaread_lbl 4880 `"McAllen-Edinburg-Pharr-Mission, TX"', add
cap label define metaread_lbl 4890 `"Medford, OR"', add
cap label define metaread_lbl 4900 `"Melbourne-Titusville-Cocoa-Palm Bay, FL"', add
cap label define metaread_lbl 4920 `"Memphis, TN/AR/MS"', add
cap label define metaread_lbl 4940 `"Merced, CA"', add
cap label define metaread_lbl 5000 `"Miami-Hialeah, FL"', add
cap label define metaread_lbl 5040 `"Midland, TX"', add
cap label define metaread_lbl 5080 `"Milwaukee, WI"', add
cap label define metaread_lbl 5120 `"Minneapolis-St. Paul, MN"', add
cap label define metaread_lbl 5140 `"Missoula, MT"', add
cap label define metaread_lbl 5160 `"Mobile, AL"', add
cap label define metaread_lbl 5170 `"Modesto, CA"', add
cap label define metaread_lbl 5190 `"Monmouth-Ocean, NJ"', add
cap label define metaread_lbl 5200 `"Monroe, LA"', add
cap label define metaread_lbl 5240 `"Montgomery, AL"', add
cap label define metaread_lbl 5280 `"Muncie, IN"', add
cap label define metaread_lbl 5320 `"Muskegon-Norton Shores-Muskegon Heights, MI"', add
cap label define metaread_lbl 5330 `"Myrtle Beach, SC"', add
cap label define metaread_lbl 5340 `"Naples, FL"', add
cap label define metaread_lbl 5350 `"Nashua, NH"', add
cap label define metaread_lbl 5360 `"Nashville, TN"', add
cap label define metaread_lbl 5400 `"New Bedford, MA"', add
cap label define metaread_lbl 5460 `"New Brunswick-Perth Amboy-Sayreville, NJ"', add
cap label define metaread_lbl 5480 `"New Haven-Meriden, CT"', add
cap label define metaread_lbl 5481 `"Meriden"', add
cap label define metaread_lbl 5482 `"New Haven, CT"', add
cap label define metaread_lbl 5520 `"New London-Norwich, CT/RI"', add
cap label define metaread_lbl 5560 `"New Orleans, LA"', add
cap label define metaread_lbl 5600 `"New York, NY-Northeastern NJ"', add
cap label define metaread_lbl 5601 `"Nassau Co, NY"', add
cap label define metaread_lbl 5602 `"Bergen-Passaic, NJ"', add
cap label define metaread_lbl 5603 `"Jersey City, NJ"', add
cap label define metaread_lbl 5604 `"Middlesex-Somerset-Hunterdon, NJ"', add
cap label define metaread_lbl 5605 `"Newark, NJ"', add
cap label define metaread_lbl 5640 `"Newark, OH"', add
cap label define metaread_lbl 5660 `"Newburgh-Middletown, NY"', add
cap label define metaread_lbl 5720 `"Norfolk-VA Beach-Newport News, VA"', add
cap label define metaread_lbl 5721 `"Newport News-Hampton"', add
cap label define metaread_lbl 5722 `"Norfolk- VA Beach-Portsmouth"', add
cap label define metaread_lbl 5760 `"Norwalk, CT"', add
cap label define metaread_lbl 5790 `"Ocala, FL"', add
cap label define metaread_lbl 5800 `"Odessa, TX"', add
cap label define metaread_lbl 5880 `"Oklahoma City, OK"', add
cap label define metaread_lbl 5910 `"Olympia, WA"', add
cap label define metaread_lbl 5920 `"Omaha, NE/IA"', add
cap label define metaread_lbl 5950 `"Orange, NY"', add
cap label define metaread_lbl 5960 `"Orlando, FL"', add
cap label define metaread_lbl 5990 `"Owensboro, KY"', add
cap label define metaread_lbl 6010 `"Panama City, FL"', add
cap label define metaread_lbl 6020 `"Parkersburg-Marietta,WV/OH"', add
cap label define metaread_lbl 6030 `"Pascagoula-Moss Point, MS"', add
cap label define metaread_lbl 6080 `"Pensacola, FL"', add
cap label define metaread_lbl 6120 `"Peoria, IL"', add
cap label define metaread_lbl 6160 `"Philadelphia, PA/NJ"', add
cap label define metaread_lbl 6200 `"Phoenix, AZ"', add
cap label define metaread_lbl 6240 `"Pine Bluff, AR"', add
cap label define metaread_lbl 6280 `"Pittsburgh-Beaver Valley, PA"', add
cap label define metaread_lbl 6281 `"Beaver County, PA"', add
cap label define metaread_lbl 6320 `"Pittsfield, MA"', add
cap label define metaread_lbl 6360 `"Ponce, PR"', add
cap label define metaread_lbl 6400 `"Portland, ME"', add
cap label define metaread_lbl 6440 `"Portland-Vancouver, OR"', add
cap label define metaread_lbl 6441 `"Vancouver, WA"', add
cap label define metaread_lbl 6450 `"Portsmouth-Dover-Rochester, NH/ME"', add
cap label define metaread_lbl 6460 `"Poughkeepsie, NY"', add
cap label define metaread_lbl 6480 `"Providence-Fall River-Pawtucket, MA/RI"', add
cap label define metaread_lbl 6481 `"Fall River, MA/RI"', add
cap label define metaread_lbl 6482 `"Pawtuckett-Woonsocket-Attleboro, RI/MA"', add
cap label define metaread_lbl 6520 `"Provo-Orem, UT"', add
cap label define metaread_lbl 6560 `"Pueblo, CO"', add
cap label define metaread_lbl 6580 `"Punta Gorda, FL"', add
cap label define metaread_lbl 6600 `"Racine, WI"', add
cap label define metaread_lbl 6640 `"Raleigh-Durham, NC"', add
cap label define metaread_lbl 6641 `"Durham, NC"', add
cap label define metaread_lbl 6660 `"Rapid City, SD"', add
cap label define metaread_lbl 6680 `"Reading, PA"', add
cap label define metaread_lbl 6690 `"Redding, CA"', add
cap label define metaread_lbl 6720 `"Reno, NV"', add
cap label define metaread_lbl 6740 `"Richland-Kennewick-Pasco, WA"', add
cap label define metaread_lbl 6760 `"Richmond-Petersburg, VA"', add
cap label define metaread_lbl 6761 `"Petersburg-Colonial Heights, VA"', add
cap label define metaread_lbl 6780 `"Riverside-San Bernardino, CA"', add
cap label define metaread_lbl 6781 `"San Bernardino, CA"', add
cap label define metaread_lbl 6800 `"Roanoke, VA"', add
cap label define metaread_lbl 6820 `"Rochester, MN"', add
cap label define metaread_lbl 6840 `"Rochester, NY"', add
cap label define metaread_lbl 6880 `"Rockford, IL"', add
cap label define metaread_lbl 6895 `"Rocky Mount, NC"', add
cap label define metaread_lbl 6920 `"Sacramento, CA"', add
cap label define metaread_lbl 6960 `"Saginaw-Bay City-Midland, MI"', add
cap label define metaread_lbl 6961 `"Bay City, MI"', add
cap label define metaread_lbl 6980 `"St. Cloud, MN"', add
cap label define metaread_lbl 7000 `"St. Joseph, MO"', add
cap label define metaread_lbl 7040 `"St. Louis, MO/IL"', add
cap label define metaread_lbl 7080 `"Salem, OR"', add
cap label define metaread_lbl 7120 `"Salinas-Sea Side-Monterey, CA"', add
cap label define metaread_lbl 7140 `"Salisbury-Concord, NC"', add
cap label define metaread_lbl 7160 `"Salt Lake City-Ogden, UT"', add
cap label define metaread_lbl 7161 `"Ogden"', add
cap label define metaread_lbl 7200 `"San Angelo, TX"', add
cap label define metaread_lbl 7240 `"San Antonio, TX"', add
cap label define metaread_lbl 7320 `"San Diego, CA"', add
cap label define metaread_lbl 7360 `"San Francisco-Oakland-Vallejo, CA"', add
cap label define metaread_lbl 7361 `"Oakland, CA"', add
cap label define metaread_lbl 7362 `"Vallejo-Fairfield-Napa, CA"', add
cap label define metaread_lbl 7400 `"San Jose, CA"', add
cap label define metaread_lbl 7440 `"San Juan-Bayamon, PR"', add
cap label define metaread_lbl 7460 `"San Luis Obispo-Atascad-P Robles, CA"', add
cap label define metaread_lbl 7470 `"Santa Barbara-Santa Maria-Lompoc, CA"', add
cap label define metaread_lbl 7480 `"Santa Cruz, CA"', add
cap label define metaread_lbl 7490 `"Santa Fe, NM"', add
cap label define metaread_lbl 7500 `"Santa Rosa-Petaluma, CA"', add
cap label define metaread_lbl 7510 `"Sarasota, FL"', add
cap label define metaread_lbl 7520 `"Savannah, GA"', add
cap label define metaread_lbl 7560 `"Scranton-Wilkes-Barre, PA"', add
cap label define metaread_lbl 7561 `"Wilkes-Barre-Hazelton, PA"', add
cap label define metaread_lbl 7600 `"Seattle-Everett, WA"', add
cap label define metaread_lbl 7610 `"Sharon, PA"', add
cap label define metaread_lbl 7620 `"Sheboygan, WI"', add
cap label define metaread_lbl 7640 `"Sherman-Denison, TX"', add
cap label define metaread_lbl 7680 `"Shreveport, LA"', add
cap label define metaread_lbl 7720 `"Sioux City, IA/NE"', add
cap label define metaread_lbl 7760 `"Sioux Falls, SD"', add
cap label define metaread_lbl 7800 `"South Bend-Mishawaka, IN"', add
cap label define metaread_lbl 7840 `"Spokane, WA"', add
cap label define metaread_lbl 7880 `"Springfield, IL"', add
cap label define metaread_lbl 7920 `"Springfield, MO"', add
cap label define metaread_lbl 8000 `"Springfield-Holyoke-Chicopee, MA"', add
cap label define metaread_lbl 8040 `"Stamford, CT"', add
cap label define metaread_lbl 8050 `"State College, PA"', add
cap label define metaread_lbl 8080 `"Steubenville-Weirton,OH/WV"', add
cap label define metaread_lbl 8120 `"Stockton, CA"', add
cap label define metaread_lbl 8140 `"Sumter, SC"', add
cap label define metaread_lbl 8160 `"Syracuse, NY"', add
cap label define metaread_lbl 8200 `"Tacoma, WA"', add
cap label define metaread_lbl 8240 `"Tallahassee, FL"', add
cap label define metaread_lbl 8280 `"Tampa-St. Petersburg-Clearwater, FL"', add
cap label define metaread_lbl 8320 `"Terre Haute, IN"', add
cap label define metaread_lbl 8360 `"Texarkana, TX/AR"', add
cap label define metaread_lbl 8400 `"Toledo, OH/MI"', add
cap label define metaread_lbl 8440 `"Topeka, KS"', add
cap label define metaread_lbl 8480 `"Trenton, NJ"', add
cap label define metaread_lbl 8520 `"Tucson, AZ"', add
cap label define metaread_lbl 8560 `"Tulsa, OK"', add
cap label define metaread_lbl 8600 `"Tuscaloosa, AL"', add
cap label define metaread_lbl 8640 `"Tyler, TX"', add
cap label define metaread_lbl 8680 `"Utica-Rome, NY"', add
cap label define metaread_lbl 8730 `"Ventura-Oxnard-Simi Valley, CA"', add
cap label define metaread_lbl 8750 `"Victoria, TX"', add
cap label define metaread_lbl 8760 `"Vineland-Milville-Bridgetown, NJ"', add
cap label define metaread_lbl 8780 `"Visalia-Tulare-Porterville, CA"', add
cap label define metaread_lbl 8800 `"Waco, TX"', add
cap label define metaread_lbl 8840 `"Washington, DC/MD/VA"', add
cap label define metaread_lbl 8880 `"Waterbury, CT"', add
cap label define metaread_lbl 8920 `"Waterloo-Cedar Falls, IA"', add
cap label define metaread_lbl 8940 `"Wausau, WI"', add
cap label define metaread_lbl 8960 `"West Palm Beach-Boca Raton-Delray Beach, FL"', add
cap label define metaread_lbl 9000 `"Wheeling, WV/OH"', add
cap label define metaread_lbl 9040 `"Wichita, KS"', add
cap label define metaread_lbl 9080 `"Wichita Falls, TX"', add
cap label define metaread_lbl 9140 `"Williamsport, PA"', add
cap label define metaread_lbl 9160 `"Wilmington, DE/NJ/MD"', add
cap label define metaread_lbl 9200 `"Wilmington, NC"', add
cap label define metaread_lbl 9240 `"Worcester, MA"', add
cap label define metaread_lbl 9260 `"Yakima, WA"', add
cap label define metaread_lbl 9270 `"Yolo, CA"', add
cap label define metaread_lbl 9280 `"York, PA"', add
cap label define metaread_lbl 9320 `"Youngstown-Warren, OH/PA"', add
cap label define metaread_lbl 9340 `"Yuba City, CA"', add
cap label define metaread_lbl 9360 `"Yuma, AZ"', add
cap label values metaread metaread_lbl

cap label define cntry_lbl 630 `"Puerto Rico"'
cap label define cntry_lbl 840 `"United States"', add
cap label values cntry cntry_lbl

cap label define gq_lbl 0 `"Vacant unit"'
cap label define gq_lbl 1 `"Households under 1970 definition"', add
cap label define gq_lbl 2 `"Additional households under 1990 definition"', add
cap label define gq_lbl 3 `"Group quarters--Institutions"', add
cap label define gq_lbl 4 `"Other group quarters"', add
cap label define gq_lbl 5 `"Additional households under 2000 definition"', add
cap label define gq_lbl 6 `"Fragment"', add
cap label values gq gq_lbl

cap label define ownershp_lbl 0 `"N/A"'
cap label define ownershp_lbl 1 `"Owned or being bought (loan)"', add
cap label define ownershp_lbl 2 `"Rented"', add
cap label values ownershp ownershp_lbl

cap label define ownershpd_lbl 00 `"N/A"'
cap label define ownershpd_lbl 10 `"Owned or being bought"', add
cap label define ownershpd_lbl 11 `"Check mark (owns?)"', add
cap label define ownershpd_lbl 12 `"Owned free and clear"', add
cap label define ownershpd_lbl 13 `"Owned with mortgage or loan"', add
cap label define ownershpd_lbl 20 `"Rented"', add
cap label define ownershpd_lbl 21 `"No cash rent"', add
cap label define ownershpd_lbl 22 `"With cash rent"', add
cap label values ownershpd ownershpd_lbl

cap label define mortgage_lbl 0 `"N/A"'
cap label define mortgage_lbl 1 `"No, owned free and clear"', add
cap label define mortgage_lbl 2 `"Check mark on manuscript (probably yes)"', add
cap label define mortgage_lbl 3 `"Yes, mortgaged/ deed of trust or similar debt"', add
cap label define mortgage_lbl 4 `"Yes, contract to purchase"', add
cap label values mortgage mortgage_lbl

cap label define mortgag2_lbl 0 `"N/A"'
cap label define mortgag2_lbl 1 `"No"', add
cap label define mortgag2_lbl 2 `"Yes"', add
cap label define mortgag2_lbl 3 `"Yes, 2nd mortgage"', add
cap label define mortgag2_lbl 4 `"Yes, home equity loan"', add
cap label define mortgag2_lbl 5 `"Yes, 2nd mortgage and home equity loan"', add
cap label values mortgag2 mortgag2_lbl

cap label define rentmeal_lbl 0 `"N/A"'
cap label define rentmeal_lbl 1 `"No, meals not included"', add
cap label define rentmeal_lbl 2 `"Yes"', add
cap label values rentmeal rentmeal_lbl

cap label define rooms_lbl 00 `"N/A"'
cap label define rooms_lbl 01 `"1 room"', add
cap label define rooms_lbl 02 `"2"', add
cap label define rooms_lbl 03 `"3"', add
cap label define rooms_lbl 04 `"4"', add
cap label define rooms_lbl 05 `"5"', add
cap label define rooms_lbl 06 `"6"', add
cap label define rooms_lbl 07 `"7"', add
cap label define rooms_lbl 08 `"8"', add
cap label define rooms_lbl 09 `"9 (9+, 1960-2007)"', add
cap label define rooms_lbl 10 `"10"', add
cap label define rooms_lbl 11 `"11"', add
cap label define rooms_lbl 12 `"12"', add
cap label define rooms_lbl 13 `"13"', add
cap label define rooms_lbl 14 `"14"', add
cap label define rooms_lbl 15 `"15"', add
cap label define rooms_lbl 16 `"16"', add
cap label define rooms_lbl 17 `"17"', add
cap label define rooms_lbl 18 `"18"', add
cap label define rooms_lbl 19 `"19"', add
cap label define rooms_lbl 20 `"20"', add
cap label define rooms_lbl 21 `"21"', add
cap label define rooms_lbl 22 `"22"', add
cap label define rooms_lbl 23 `"23"', add
cap label define rooms_lbl 24 `"24"', add
cap label define rooms_lbl 25 `"25"', add
cap label define rooms_lbl 26 `"26"', add
cap label define rooms_lbl 27 `"27"', add
cap label define rooms_lbl 30 `"30"', add
cap label values rooms rooms_lbl

cap label define builtyr2_lbl 00 `"N/A"'
cap label define builtyr2_lbl 01 `"1939 or earlier"', add
cap label define builtyr2_lbl 02 `"1940-1949"', add
cap label define builtyr2_lbl 03 `"1950-1959"', add
cap label define builtyr2_lbl 04 `"1960-1969"', add
cap label define builtyr2_lbl 05 `"1970-1979"', add
cap label define builtyr2_lbl 06 `"1980-1989"', add
cap label define builtyr2_lbl 07 `"1990-1994 (1990-1999 in the 2005-onward ACS and the PRCS)"', add
cap label define builtyr2_lbl 08 `"1995-1999 (1995-1998 in the 2000-2002 ACS)"', add
cap label define builtyr2_lbl 09 `"2000-2004 (1999-2002 in the 2000-2002 ACS)"', add
cap label define builtyr2_lbl 10 `"2005 (2005 or later in datasets containing 2005, 2006, or 2007 ACS/PRCS data)"', add
cap label define builtyr2_lbl 11 `"2006"', add
cap label define builtyr2_lbl 12 `"2007"', add
cap label define builtyr2_lbl 13 `"2008"', add
cap label define builtyr2_lbl 14 `"2009"', add
cap label define builtyr2_lbl 15 `"2010"', add
cap label define builtyr2_lbl 16 `"2011"', add
cap label define builtyr2_lbl 17 `"2012"', add
cap label define builtyr2_lbl 18 `"2013"', add
cap label define builtyr2_lbl 19 `"2014"', add
cap label define builtyr2_lbl 20 `"2015"', add
cap label define builtyr2_lbl 21 `"2016"', add
cap label define builtyr2_lbl 22 `"2017"', add
cap label values builtyr2 builtyr2_lbl

cap label define bedrooms_lbl 00 `"N/A"'
cap label define bedrooms_lbl 01 `"No bedrooms"', add
cap label define bedrooms_lbl 02 `"1"', add
cap label define bedrooms_lbl 03 `"2"', add
cap label define bedrooms_lbl 04 `"3"', add
cap label define bedrooms_lbl 05 `"4 (1970-2000, 2000-2007 ACS/PRCS)"', add
cap label define bedrooms_lbl 06 `"5+ (1970-2000, 2000-2007 ACS/PRCS)"', add
cap label define bedrooms_lbl 07 `"6"', add
cap label define bedrooms_lbl 08 `"7"', add
cap label define bedrooms_lbl 09 `"8"', add
cap label define bedrooms_lbl 10 `"9"', add
cap label define bedrooms_lbl 11 `"10"', add
cap label define bedrooms_lbl 12 `"11"', add
cap label define bedrooms_lbl 13 `"12"', add
cap label define bedrooms_lbl 14 `"13"', add
cap label define bedrooms_lbl 15 `"14"', add
cap label define bedrooms_lbl 16 `"15"', add
cap label define bedrooms_lbl 17 `"16"', add
cap label define bedrooms_lbl 18 `"17"', add
cap label define bedrooms_lbl 19 `"18"', add
cap label define bedrooms_lbl 20 `"19"', add
cap label define bedrooms_lbl 21 `"20"', add
cap label define bedrooms_lbl 22 `"21"', add
cap label values bedrooms bedrooms_lbl

cap label define famsize_lbl 01 `"1 family member present"'
cap label define famsize_lbl 02 `"2 family members present"', add
cap label define famsize_lbl 03 `"3"', add
cap label define famsize_lbl 04 `"4"', add
cap label define famsize_lbl 05 `"5"', add
cap label define famsize_lbl 06 `"6"', add
cap label define famsize_lbl 07 `"7"', add
cap label define famsize_lbl 08 `"8"', add
cap label define famsize_lbl 09 `"9"', add
cap label define famsize_lbl 10 `"10"', add
cap label define famsize_lbl 11 `"11"', add
cap label define famsize_lbl 12 `"12"', add
cap label define famsize_lbl 13 `"13"', add
cap label define famsize_lbl 14 `"14"', add
cap label define famsize_lbl 15 `"15"', add
cap label define famsize_lbl 16 `"16"', add
cap label define famsize_lbl 17 `"17"', add
cap label define famsize_lbl 18 `"18"', add
cap label define famsize_lbl 19 `"19"', add
cap label define famsize_lbl 20 `"20"', add
cap label define famsize_lbl 21 `"21"', add
cap label define famsize_lbl 22 `"22"', add
cap label define famsize_lbl 23 `"23"', add
cap label define famsize_lbl 24 `"24"', add
cap label define famsize_lbl 25 `"25"', add
cap label define famsize_lbl 26 `"26"', add
cap label define famsize_lbl 27 `"27"', add
cap label define famsize_lbl 28 `"28"', add
cap label define famsize_lbl 29 `"29"', add
cap label values famsize famsize_lbl

cap label define nchild_lbl 0 `"0 children present"'
cap label define nchild_lbl 1 `"1 child present"', add
cap label define nchild_lbl 2 `"2"', add
cap label define nchild_lbl 3 `"3"', add
cap label define nchild_lbl 4 `"4"', add
cap label define nchild_lbl 5 `"5"', add
cap label define nchild_lbl 6 `"6"', add
cap label define nchild_lbl 7 `"7"', add
cap label define nchild_lbl 8 `"8"', add
cap label define nchild_lbl 9 `"9+"', add
cap label values nchild nchild_lbl

cap label define nchlt5_lbl 0 `"No children under age 5"'
cap label define nchlt5_lbl 1 `"1 child under age 5"', add
cap label define nchlt5_lbl 2 `"2"', add
cap label define nchlt5_lbl 3 `"3"', add
cap label define nchlt5_lbl 4 `"4"', add
cap label define nchlt5_lbl 5 `"5"', add
cap label define nchlt5_lbl 6 `"6"', add
cap label define nchlt5_lbl 7 `"7"', add
cap label define nchlt5_lbl 8 `"8"', add
cap label define nchlt5_lbl 9 `"9+"', add
cap label values nchlt5 nchlt5_lbl

cap label define eldch_lbl 00 `"Less than 1 year old"'
cap label define eldch_lbl 01 `"1"', add
cap label define eldch_lbl 02 `"2"', add
cap label define eldch_lbl 03 `"3"', add
cap label define eldch_lbl 04 `"4"', add
cap label define eldch_lbl 05 `"5"', add
cap label define eldch_lbl 06 `"6"', add
cap label define eldch_lbl 07 `"7"', add
cap label define eldch_lbl 08 `"8"', add
cap label define eldch_lbl 09 `"9"', add
cap label define eldch_lbl 10 `"10"', add
cap label define eldch_lbl 11 `"11"', add
cap label define eldch_lbl 12 `"12"', add
cap label define eldch_lbl 13 `"13"', add
cap label define eldch_lbl 14 `"14"', add
cap label define eldch_lbl 15 `"15"', add
cap label define eldch_lbl 16 `"16"', add
cap label define eldch_lbl 17 `"17"', add
cap label define eldch_lbl 18 `"18"', add
cap label define eldch_lbl 19 `"19"', add
cap label define eldch_lbl 20 `"20"', add
cap label define eldch_lbl 21 `"21"', add
cap label define eldch_lbl 22 `"22"', add
cap label define eldch_lbl 23 `"23"', add
cap label define eldch_lbl 24 `"24"', add
cap label define eldch_lbl 25 `"25"', add
cap label define eldch_lbl 26 `"26"', add
cap label define eldch_lbl 27 `"27"', add
cap label define eldch_lbl 28 `"28"', add
cap label define eldch_lbl 29 `"29"', add
cap label define eldch_lbl 30 `"30"', add
cap label define eldch_lbl 31 `"31"', add
cap label define eldch_lbl 32 `"32"', add
cap label define eldch_lbl 33 `"33"', add
cap label define eldch_lbl 34 `"34"', add
cap label define eldch_lbl 35 `"35"', add
cap label define eldch_lbl 36 `"36"', add
cap label define eldch_lbl 37 `"37"', add
cap label define eldch_lbl 38 `"38"', add
cap label define eldch_lbl 39 `"39"', add
cap label define eldch_lbl 40 `"40"', add
cap label define eldch_lbl 41 `"41"', add
cap label define eldch_lbl 42 `"42"', add
cap label define eldch_lbl 43 `"43"', add
cap label define eldch_lbl 44 `"44"', add
cap label define eldch_lbl 45 `"45"', add
cap label define eldch_lbl 46 `"46"', add
cap label define eldch_lbl 47 `"47"', add
cap label define eldch_lbl 48 `"48"', add
cap label define eldch_lbl 49 `"49"', add
cap label define eldch_lbl 50 `"50"', add
cap label define eldch_lbl 51 `"51"', add
cap label define eldch_lbl 52 `"52"', add
cap label define eldch_lbl 53 `"53"', add
cap label define eldch_lbl 54 `"54"', add
cap label define eldch_lbl 55 `"55"', add
cap label define eldch_lbl 56 `"56"', add
cap label define eldch_lbl 57 `"57"', add
cap label define eldch_lbl 58 `"58"', add
cap label define eldch_lbl 59 `"59"', add
cap label define eldch_lbl 60 `"60"', add
cap label define eldch_lbl 61 `"61"', add
cap label define eldch_lbl 62 `"62"', add
cap label define eldch_lbl 63 `"63"', add
cap label define eldch_lbl 64 `"64"', add
cap label define eldch_lbl 65 `"65"', add
cap label define eldch_lbl 66 `"66"', add
cap label define eldch_lbl 67 `"67"', add
cap label define eldch_lbl 68 `"68"', add
cap label define eldch_lbl 69 `"69"', add
cap label define eldch_lbl 70 `"70"', add
cap label define eldch_lbl 71 `"71"', add
cap label define eldch_lbl 72 `"72"', add
cap label define eldch_lbl 73 `"73"', add
cap label define eldch_lbl 74 `"74"', add
cap label define eldch_lbl 75 `"75"', add
cap label define eldch_lbl 76 `"76"', add
cap label define eldch_lbl 77 `"77"', add
cap label define eldch_lbl 78 `"78"', add
cap label define eldch_lbl 79 `"79"', add
cap label define eldch_lbl 80 `"80"', add
cap label define eldch_lbl 81 `"81"', add
cap label define eldch_lbl 82 `"82"', add
cap label define eldch_lbl 83 `"83"', add
cap label define eldch_lbl 84 `"84"', add
cap label define eldch_lbl 85 `"85"', add
cap label define eldch_lbl 86 `"86"', add
cap label define eldch_lbl 87 `"87"', add
cap label define eldch_lbl 88 `"88"', add
cap label define eldch_lbl 89 `"89"', add
cap label define eldch_lbl 90 `"90"', add
cap label define eldch_lbl 91 `"91"', add
cap label define eldch_lbl 92 `"92"', add
cap label define eldch_lbl 93 `"93"', add
cap label define eldch_lbl 94 `"94"', add
cap label define eldch_lbl 95 `"95"', add
cap label define eldch_lbl 96 `"96"', add
cap label define eldch_lbl 97 `"97"', add
cap label define eldch_lbl 98 `"98"', add
cap label define eldch_lbl 99 `"N/A"', add
cap label values eldch eldch_lbl

cap label define yngch_lbl 00 `"Less than 1 year old"'
cap label define yngch_lbl 01 `"1"', add
cap label define yngch_lbl 02 `"2"', add
cap label define yngch_lbl 03 `"3"', add
cap label define yngch_lbl 04 `"4"', add
cap label define yngch_lbl 05 `"5"', add
cap label define yngch_lbl 06 `"6"', add
cap label define yngch_lbl 07 `"7"', add
cap label define yngch_lbl 08 `"8"', add
cap label define yngch_lbl 09 `"9"', add
cap label define yngch_lbl 10 `"10"', add
cap label define yngch_lbl 11 `"11"', add
cap label define yngch_lbl 12 `"12"', add
cap label define yngch_lbl 13 `"13"', add
cap label define yngch_lbl 14 `"14"', add
cap label define yngch_lbl 15 `"15"', add
cap label define yngch_lbl 16 `"16"', add
cap label define yngch_lbl 17 `"17"', add
cap label define yngch_lbl 18 `"18"', add
cap label define yngch_lbl 19 `"19"', add
cap label define yngch_lbl 20 `"20"', add
cap label define yngch_lbl 21 `"21"', add
cap label define yngch_lbl 22 `"22"', add
cap label define yngch_lbl 23 `"23"', add
cap label define yngch_lbl 24 `"24"', add
cap label define yngch_lbl 25 `"25"', add
cap label define yngch_lbl 26 `"26"', add
cap label define yngch_lbl 27 `"27"', add
cap label define yngch_lbl 28 `"28"', add
cap label define yngch_lbl 29 `"29"', add
cap label define yngch_lbl 30 `"30"', add
cap label define yngch_lbl 31 `"31"', add
cap label define yngch_lbl 32 `"32"', add
cap label define yngch_lbl 33 `"33"', add
cap label define yngch_lbl 34 `"34"', add
cap label define yngch_lbl 35 `"35"', add
cap label define yngch_lbl 36 `"36"', add
cap label define yngch_lbl 37 `"37"', add
cap label define yngch_lbl 38 `"38"', add
cap label define yngch_lbl 39 `"39"', add
cap label define yngch_lbl 40 `"40"', add
cap label define yngch_lbl 41 `"41"', add
cap label define yngch_lbl 42 `"42"', add
cap label define yngch_lbl 43 `"43"', add
cap label define yngch_lbl 44 `"44"', add
cap label define yngch_lbl 45 `"45"', add
cap label define yngch_lbl 46 `"46"', add
cap label define yngch_lbl 47 `"47"', add
cap label define yngch_lbl 48 `"48"', add
cap label define yngch_lbl 49 `"49"', add
cap label define yngch_lbl 50 `"50"', add
cap label define yngch_lbl 51 `"51"', add
cap label define yngch_lbl 52 `"52"', add
cap label define yngch_lbl 53 `"53"', add
cap label define yngch_lbl 54 `"54"', add
cap label define yngch_lbl 55 `"55"', add
cap label define yngch_lbl 56 `"56"', add
cap label define yngch_lbl 57 `"57"', add
cap label define yngch_lbl 58 `"58"', add
cap label define yngch_lbl 59 `"59"', add
cap label define yngch_lbl 60 `"60"', add
cap label define yngch_lbl 61 `"61"', add
cap label define yngch_lbl 62 `"62"', add
cap label define yngch_lbl 63 `"63"', add
cap label define yngch_lbl 64 `"64"', add
cap label define yngch_lbl 65 `"65"', add
cap label define yngch_lbl 66 `"66"', add
cap label define yngch_lbl 67 `"67"', add
cap label define yngch_lbl 68 `"68"', add
cap label define yngch_lbl 69 `"69"', add
cap label define yngch_lbl 70 `"70"', add
cap label define yngch_lbl 71 `"71"', add
cap label define yngch_lbl 72 `"72"', add
cap label define yngch_lbl 73 `"73"', add
cap label define yngch_lbl 74 `"74"', add
cap label define yngch_lbl 75 `"75"', add
cap label define yngch_lbl 76 `"76"', add
cap label define yngch_lbl 77 `"77"', add
cap label define yngch_lbl 78 `"78"', add
cap label define yngch_lbl 79 `"79"', add
cap label define yngch_lbl 80 `"80"', add
cap label define yngch_lbl 81 `"81"', add
cap label define yngch_lbl 82 `"82"', add
cap label define yngch_lbl 83 `"83"', add
cap label define yngch_lbl 84 `"84"', add
cap label define yngch_lbl 85 `"85"', add
cap label define yngch_lbl 86 `"86"', add
cap label define yngch_lbl 87 `"87"', add
cap label define yngch_lbl 88 `"88"', add
cap label define yngch_lbl 89 `"89"', add
cap label define yngch_lbl 90 `"90"', add
cap label define yngch_lbl 91 `"91"', add
cap label define yngch_lbl 92 `"92"', add
cap label define yngch_lbl 93 `"93"', add
cap label define yngch_lbl 94 `"94"', add
cap label define yngch_lbl 95 `"95"', add
cap label define yngch_lbl 96 `"96"', add
cap label define yngch_lbl 97 `"97"', add
cap label define yngch_lbl 98 `"98"', add
cap label define yngch_lbl 99 `"N/A"', add
cap label values yngch yngch_lbl

cap label define relate_lbl 01 `"Head/Householder"'
cap label define relate_lbl 02 `"Spouse"', add
cap label define relate_lbl 03 `"Child"', add
cap label define relate_lbl 04 `"Child-in-law"', add
cap label define relate_lbl 05 `"Parent"', add
cap label define relate_lbl 06 `"Parent-in-Law"', add
cap label define relate_lbl 07 `"Sibling"', add
cap label define relate_lbl 08 `"Sibling-in-Law"', add
cap label define relate_lbl 09 `"Grandchild"', add
cap label define relate_lbl 10 `"Other relatives"', add
cap label define relate_lbl 11 `"Partner, friend, visitor"', add
cap label define relate_lbl 12 `"Other non-relatives"', add
cap label define relate_lbl 13 `"Institutional inmates"', add
cap label values relate relate_lbl

cap label define related_lbl 0101 `"Head/Householder"'
cap label define related_lbl 0201 `"Spouse"', add
cap label define related_lbl 0202 `"2nd/3rd Wife (Polygamous)"', add
cap label define related_lbl 0301 `"Child"', add
cap label define related_lbl 0302 `"Adopted Child"', add
cap label define related_lbl 0303 `"Stepchild"', add
cap label define related_lbl 0304 `"Adopted, n.s."', add
cap label define related_lbl 0401 `"Child-in-law"', add
cap label define related_lbl 0402 `"Step Child-in-law"', add
cap label define related_lbl 0501 `"Parent"', add
cap label define related_lbl 0502 `"Stepparent"', add
cap label define related_lbl 0601 `"Parent-in-Law"', add
cap label define related_lbl 0602 `"Stepparent-in-law"', add
cap label define related_lbl 0701 `"Sibling"', add
cap label define related_lbl 0702 `"Step/Half/Adopted Sibling"', add
cap label define related_lbl 0801 `"Sibling-in-Law"', add
cap label define related_lbl 0802 `"Step/Half Sibling-in-law"', add
cap label define related_lbl 0901 `"Grandchild"', add
cap label define related_lbl 0902 `"Adopted Grandchild"', add
cap label define related_lbl 0903 `"Step Grandchild"', add
cap label define related_lbl 0904 `"Grandchild-in-law"', add
cap label define related_lbl 1000 `"Other Relatives:"', add
cap label define related_lbl 1001 `"Other Relatives"', add
cap label define related_lbl 1011 `"Grandparent"', add
cap label define related_lbl 1012 `"Step Grandparent"', add
cap label define related_lbl 1013 `"Grandparent-in-law"', add
cap label define related_lbl 1021 `"Aunt or Uncle"', add
cap label define related_lbl 1022 `"Aunt,Uncle-in-law"', add
cap label define related_lbl 1031 `"Nephew, Niece"', add
cap label define related_lbl 1032 `"Neph/Niece-in-law"', add
cap label define related_lbl 1033 `"Step/Adopted Nephew/Niece"', add
cap label define related_lbl 1034 `"Grand Niece/Nephew"', add
cap label define related_lbl 1041 `"Cousin"', add
cap label define related_lbl 1042 `"Cousin-in-law"', add
cap label define related_lbl 1051 `"Great Grandchild"', add
cap label define related_lbl 1061 `"Other relatives, nec"', add
cap label define related_lbl 1100 `"Partner, Friend, Visitor"', add
cap label define related_lbl 1110 `"Partner/friend"', add
cap label define related_lbl 1111 `"Friend"', add
cap label define related_lbl 1112 `"Partner"', add
cap label define related_lbl 1113 `"Partner/roommate"', add
cap label define related_lbl 1114 `"Unmarried Partner"', add
cap label define related_lbl 1115 `"Housemate/Roomate"', add
cap label define related_lbl 1120 `"Relative of partner"', add
cap label define related_lbl 1130 `"Concubine/Mistress"', add
cap label define related_lbl 1131 `"Visitor"', add
cap label define related_lbl 1132 `"Companion and family of companion"', add
cap label define related_lbl 1139 `"Allocated partner/friend/visitor"', add
cap label define related_lbl 1200 `"Other non-relatives"', add
cap label define related_lbl 1201 `"Roomers/boarders/lodgers"', add
cap label define related_lbl 1202 `"Boarders"', add
cap label define related_lbl 1203 `"Lodgers"', add
cap label define related_lbl 1204 `"Roomer"', add
cap label define related_lbl 1205 `"Tenant"', add
cap label define related_lbl 1206 `"Foster child"', add
cap label define related_lbl 1210 `"Employees:"', add
cap label define related_lbl 1211 `"Servant"', add
cap label define related_lbl 1212 `"Housekeeper"', add
cap label define related_lbl 1213 `"Maid"', add
cap label define related_lbl 1214 `"Cook"', add
cap label define related_lbl 1215 `"Nurse"', add
cap label define related_lbl 1216 `"Other probable domestic employee"', add
cap label define related_lbl 1217 `"Other employee"', add
cap label define related_lbl 1219 `"Relative of employee"', add
cap label define related_lbl 1221 `"Military"', add
cap label define related_lbl 1222 `"Students"', add
cap label define related_lbl 1223 `"Members of religious orders"', add
cap label define related_lbl 1230 `"Other non-relatives"', add
cap label define related_lbl 1239 `"Allocated other non-relative"', add
cap label define related_lbl 1240 `"Roomers/boarders/lodgers and foster children"', add
cap label define related_lbl 1241 `"Roomers/boarders/lodgers"', add
cap label define related_lbl 1242 `"Foster children"', add
cap label define related_lbl 1250 `"Employees"', add
cap label define related_lbl 1251 `"Domestic employees"', add
cap label define related_lbl 1252 `"Non-domestic employees"', add
cap label define related_lbl 1253 `"Relative of employee"', add
cap label define related_lbl 1260 `"Other non-relatives (1990 includes employees)"', add
cap label define related_lbl 1270 `"Non-inmate 1990"', add
cap label define related_lbl 1281 `"Head of group quarters"', add
cap label define related_lbl 1282 `"Employees of group quarters"', add
cap label define related_lbl 1283 `"Relative of head, staff, or employee group quarters"', add
cap label define related_lbl 1284 `"Other non-inmate 1940-1959"', add
cap label define related_lbl 1291 `"Military"', add
cap label define related_lbl 1292 `"College dormitories"', add
cap label define related_lbl 1293 `"Residents of rooming houses"', add
cap label define related_lbl 1294 `"Other non-inmate 1980 (includes employees and non-inmates in"', add
cap label define related_lbl 1295 `"Other non-inmates 1960-1970 (includes employees)"', add
cap label define related_lbl 1296 `"Non-inmates in institutions"', add
cap label define related_lbl 1301 `"Institutional inmates"', add
cap label define related_lbl 9996 `"Unclassifiable"', add
cap label define related_lbl 9997 `"Unknown"', add
cap label define related_lbl 9998 `"Illegible"', add
cap label define related_lbl 9999 `"Missing"', add
cap label values related related_lbl

cap label define sex_lbl 1 `"Male"'
cap label define sex_lbl 2 `"Female"', add
cap label values sex sex_lbl

cap label define age_lbl 000 `"Less than 1 year old"'
cap label define age_lbl 001 `"1"', add
cap label define age_lbl 002 `"2"', add
cap label define age_lbl 003 `"3"', add
cap label define age_lbl 004 `"4"', add
cap label define age_lbl 005 `"5"', add
cap label define age_lbl 006 `"6"', add
cap label define age_lbl 007 `"7"', add
cap label define age_lbl 008 `"8"', add
cap label define age_lbl 009 `"9"', add
cap label define age_lbl 010 `"10"', add
cap label define age_lbl 011 `"11"', add
cap label define age_lbl 012 `"12"', add
cap label define age_lbl 013 `"13"', add
cap label define age_lbl 014 `"14"', add
cap label define age_lbl 015 `"15"', add
cap label define age_lbl 016 `"16"', add
cap label define age_lbl 017 `"17"', add
cap label define age_lbl 018 `"18"', add
cap label define age_lbl 019 `"19"', add
cap label define age_lbl 020 `"20"', add
cap label define age_lbl 021 `"21"', add
cap label define age_lbl 022 `"22"', add
cap label define age_lbl 023 `"23"', add
cap label define age_lbl 024 `"24"', add
cap label define age_lbl 025 `"25"', add
cap label define age_lbl 026 `"26"', add
cap label define age_lbl 027 `"27"', add
cap label define age_lbl 028 `"28"', add
cap label define age_lbl 029 `"29"', add
cap label define age_lbl 030 `"30"', add
cap label define age_lbl 031 `"31"', add
cap label define age_lbl 032 `"32"', add
cap label define age_lbl 033 `"33"', add
cap label define age_lbl 034 `"34"', add
cap label define age_lbl 035 `"35"', add
cap label define age_lbl 036 `"36"', add
cap label define age_lbl 037 `"37"', add
cap label define age_lbl 038 `"38"', add
cap label define age_lbl 039 `"39"', add
cap label define age_lbl 040 `"40"', add
cap label define age_lbl 041 `"41"', add
cap label define age_lbl 042 `"42"', add
cap label define age_lbl 043 `"43"', add
cap label define age_lbl 044 `"44"', add
cap label define age_lbl 045 `"45"', add
cap label define age_lbl 046 `"46"', add
cap label define age_lbl 047 `"47"', add
cap label define age_lbl 048 `"48"', add
cap label define age_lbl 049 `"49"', add
cap label define age_lbl 050 `"50"', add
cap label define age_lbl 051 `"51"', add
cap label define age_lbl 052 `"52"', add
cap label define age_lbl 053 `"53"', add
cap label define age_lbl 054 `"54"', add
cap label define age_lbl 055 `"55"', add
cap label define age_lbl 056 `"56"', add
cap label define age_lbl 057 `"57"', add
cap label define age_lbl 058 `"58"', add
cap label define age_lbl 059 `"59"', add
cap label define age_lbl 060 `"60"', add
cap label define age_lbl 061 `"61"', add
cap label define age_lbl 062 `"62"', add
cap label define age_lbl 063 `"63"', add
cap label define age_lbl 064 `"64"', add
cap label define age_lbl 065 `"65"', add
cap label define age_lbl 066 `"66"', add
cap label define age_lbl 067 `"67"', add
cap label define age_lbl 068 `"68"', add
cap label define age_lbl 069 `"69"', add
cap label define age_lbl 070 `"70"', add
cap label define age_lbl 071 `"71"', add
cap label define age_lbl 072 `"72"', add
cap label define age_lbl 073 `"73"', add
cap label define age_lbl 074 `"74"', add
cap label define age_lbl 075 `"75"', add
cap label define age_lbl 076 `"76"', add
cap label define age_lbl 077 `"77"', add
cap label define age_lbl 078 `"78"', add
cap label define age_lbl 079 `"79"', add
cap label define age_lbl 080 `"80"', add
cap label define age_lbl 081 `"81"', add
cap label define age_lbl 082 `"82"', add
cap label define age_lbl 083 `"83"', add
cap label define age_lbl 084 `"84"', add
cap label define age_lbl 085 `"85"', add
cap label define age_lbl 086 `"86"', add
cap label define age_lbl 087 `"87"', add
cap label define age_lbl 088 `"88"', add
cap label define age_lbl 089 `"89"', add
cap label define age_lbl 090 `"90 (90+ in 1980 and 1990)"', add
cap label define age_lbl 091 `"91"', add
cap label define age_lbl 092 `"92"', add
cap label define age_lbl 093 `"93"', add
cap label define age_lbl 094 `"94"', add
cap label define age_lbl 095 `"95"', add
cap label define age_lbl 096 `"96"', add
cap label define age_lbl 097 `"97"', add
cap label define age_lbl 098 `"98"', add
cap label define age_lbl 099 `"99"', add
cap label define age_lbl 100 `"100 (100+ in 1960-1970)"', add
cap label define age_lbl 101 `"101"', add
cap label define age_lbl 102 `"102"', add
cap label define age_lbl 103 `"103"', add
cap label define age_lbl 104 `"104"', add
cap label define age_lbl 105 `"105"', add
cap label define age_lbl 106 `"106"', add
cap label define age_lbl 107 `"107"', add
cap label define age_lbl 108 `"108"', add
cap label define age_lbl 109 `"109"', add
cap label define age_lbl 110 `"110"', add
cap label define age_lbl 111 `"111"', add
cap label define age_lbl 112 `"112 (112+ in the 1980 internal data)"', add
cap label define age_lbl 113 `"113"', add
cap label define age_lbl 114 `"114"', add
cap label define age_lbl 115 `"115 (115+ in the 1990 internal data)"', add
cap label define age_lbl 116 `"116"', add
cap label define age_lbl 117 `"117"', add
cap label define age_lbl 118 `"118"', add
cap label define age_lbl 119 `"119"', add
cap label define age_lbl 120 `"120"', add
cap label define age_lbl 121 `"121"', add
cap label define age_lbl 122 `"122"', add
cap label define age_lbl 123 `"123"', add
cap label define age_lbl 124 `"124"', add
cap label define age_lbl 125 `"125"', add
cap label define age_lbl 126 `"126"', add
cap label define age_lbl 129 `"129"', add
cap label define age_lbl 130 `"130"', add
cap label define age_lbl 135 `"135"', add
cap label values age age_lbl

cap label define race_lbl 1 `"White"'
cap label define race_lbl 2 `"Black/African American/Negro"', add
cap label define race_lbl 3 `"American Indian or Alaska Native"', add
cap label define race_lbl 4 `"Chinese"', add
cap label define race_lbl 5 `"Japanese"', add
cap label define race_lbl 6 `"Other Asian or Pacific Islander"', add
cap label define race_lbl 7 `"Other race, nec"', add
cap label define race_lbl 8 `"Two major races"', add
cap label define race_lbl 9 `"Three or more major races"', add
cap label values race race_lbl

cap label define raced_lbl 100 `"White"'
cap label define raced_lbl 110 `"Spanish write_in"', add
cap label define raced_lbl 120 `"Blank (white) (1850)"', add
cap label define raced_lbl 130 `"Portuguese"', add
cap label define raced_lbl 140 `"Mexican (1930)"', add
cap label define raced_lbl 150 `"Puerto Rican (1910 Hawaii)"', add
cap label define raced_lbl 200 `"Black/African American/Negro"', add
cap label define raced_lbl 210 `"Mulatto"', add
cap label define raced_lbl 300 `"American Indian/Alaska Native"', add
cap label define raced_lbl 302 `"Apache"', add
cap label define raced_lbl 303 `"Blackfoot"', add
cap label define raced_lbl 304 `"Cherokee"', add
cap label define raced_lbl 305 `"Cheyenne"', add
cap label define raced_lbl 306 `"Chickasaw"', add
cap label define raced_lbl 307 `"Chippewa"', add
cap label define raced_lbl 308 `"Choctaw"', add
cap label define raced_lbl 309 `"Comanche"', add
cap label define raced_lbl 310 `"Creek"', add
cap label define raced_lbl 311 `"Crow"', add
cap label define raced_lbl 312 `"Iroquois"', add
cap label define raced_lbl 313 `"Kiowa"', add
cap label define raced_lbl 314 `"Lumbee"', add
cap label define raced_lbl 315 `"Navajo"', add
cap label define raced_lbl 316 `"Osage"', add
cap label define raced_lbl 317 `"Paiute"', add
cap label define raced_lbl 318 `"Pima"', add
cap label define raced_lbl 319 `"Potawatomi"', add
cap label define raced_lbl 320 `"Pueblo"', add
cap label define raced_lbl 321 `"Seminole"', add
cap label define raced_lbl 322 `"Shoshone"', add
cap label define raced_lbl 323 `"Sioux"', add
cap label define raced_lbl 324 `"Tlingit (Tlingit_Haida, 2000/ACS)"', add
cap label define raced_lbl 325 `"Tohono O Odham"', add
cap label define raced_lbl 326 `"All other tribes (1990)"', add
cap label define raced_lbl 328 `"Hopi"', add
cap label define raced_lbl 329 `"Central American Indian"', add
cap label define raced_lbl 330 `"Spanish American Indian"', add
cap label define raced_lbl 350 `"Delaware"', add
cap label define raced_lbl 351 `"Latin American Indian"', add
cap label define raced_lbl 352 `"Puget Sound Salish"', add
cap label define raced_lbl 353 `"Yakama"', add
cap label define raced_lbl 354 `"Yaqui"', add
cap label define raced_lbl 355 `"Colville"', add
cap label define raced_lbl 356 `"Houma"', add
cap label define raced_lbl 357 `"Menominee"', add
cap label define raced_lbl 358 `"Yuman"', add
cap label define raced_lbl 359 `"South American Indian"', add
cap label define raced_lbl 360 `"Mexican American Indian"', add
cap label define raced_lbl 361 `"Other Amer. Indian tribe (2000,ACS)"', add
cap label define raced_lbl 362 `"2+ Amer. Indian tribes (2000,ACS)"', add
cap label define raced_lbl 370 `"Alaskan Athabaskan"', add
cap label define raced_lbl 371 `"Aleut"', add
cap label define raced_lbl 372 `"Eskimo"', add
cap label define raced_lbl 373 `"Alaskan mixed"', add
cap label define raced_lbl 374 `"Inupiat"', add
cap label define raced_lbl 375 `"Yup'ik"', add
cap label define raced_lbl 379 `"Other Alaska Native tribe(s) (2000,ACS)"', add
cap label define raced_lbl 398 `"Both Am. Ind. and Alaska Native (2000,ACS)"', add
cap label define raced_lbl 399 `"Tribe not specified"', add
cap label define raced_lbl 400 `"Chinese"', add
cap label define raced_lbl 410 `"Taiwanese"', add
cap label define raced_lbl 420 `"Chinese and Taiwanese"', add
cap label define raced_lbl 500 `"Japanese"', add
cap label define raced_lbl 600 `"Filipino"', add
cap label define raced_lbl 610 `"Asian Indian (Hindu 1920_1940)"', add
cap label define raced_lbl 620 `"Korean"', add
cap label define raced_lbl 630 `"Hawaiian"', add
cap label define raced_lbl 631 `"Hawaiian and Asian (1900,1920)"', add
cap label define raced_lbl 632 `"Hawaiian and European (1900,1920)"', add
cap label define raced_lbl 634 `"Hawaiian mixed"', add
cap label define raced_lbl 640 `"Vietnamese"', add
cap label define raced_lbl 641 `"Bhutanese"', add
cap label define raced_lbl 642 `"Mongolian"', add
cap label define raced_lbl 643 `"Nepalese"', add
cap label define raced_lbl 650 `"Other Asian or Pacific Islander (1920,1980)"', add
cap label define raced_lbl 651 `"Asian only (CPS)"', add
cap label define raced_lbl 652 `"Pacific Islander only (CPS)"', add
cap label define raced_lbl 653 `"Asian or Pacific Islander, n.s. (1990 Internal Census files)"', add
cap label define raced_lbl 660 `"Cambodian"', add
cap label define raced_lbl 661 `"Hmong"', add
cap label define raced_lbl 662 `"Laotian"', add
cap label define raced_lbl 663 `"Thai"', add
cap label define raced_lbl 664 `"Bangladeshi"', add
cap label define raced_lbl 665 `"Burmese"', add
cap label define raced_lbl 666 `"Indonesian"', add
cap label define raced_lbl 667 `"Malaysian"', add
cap label define raced_lbl 668 `"Okinawan"', add
cap label define raced_lbl 669 `"Pakistani"', add
cap label define raced_lbl 670 `"Sri Lankan"', add
cap label define raced_lbl 671 `"Other Asian, n.e.c."', add
cap label define raced_lbl 672 `"Asian, not specified"', add
cap label define raced_lbl 673 `"Chinese and Japanese"', add
cap label define raced_lbl 674 `"Chinese and Filipino"', add
cap label define raced_lbl 675 `"Chinese and Vietnamese"', add
cap label define raced_lbl 676 `"Chinese and Asian write_in"', add
cap label define raced_lbl 677 `"Japanese and Filipino"', add
cap label define raced_lbl 678 `"Asian Indian and Asian write_in"', add
cap label define raced_lbl 679 `"Other Asian race combinations"', add
cap label define raced_lbl 680 `"Samoan"', add
cap label define raced_lbl 681 `"Tahitian"', add
cap label define raced_lbl 682 `"Tongan"', add
cap label define raced_lbl 683 `"Other Polynesian (1990)"', add
cap label define raced_lbl 684 `"1+ other Polynesian races (2000,ACS)"', add
cap label define raced_lbl 685 `"Guamanian/Chamorro"', add
cap label define raced_lbl 686 `"Northern Mariana Islander"', add
cap label define raced_lbl 687 `"Palauan"', add
cap label define raced_lbl 688 `"Other Micronesian (1990)"', add
cap label define raced_lbl 689 `"1+ other Micronesian races (2000,ACS)"', add
cap label define raced_lbl 690 `"Fijian"', add
cap label define raced_lbl 691 `"Other Melanesian (1990)"', add
cap label define raced_lbl 692 `"1+ other Melanesian races (2000,ACS)"', add
cap label define raced_lbl 698 `"2+ PI races from 2+ PI regions"', add
cap label define raced_lbl 699 `"Pacific Islander, n.s."', add
cap label define raced_lbl 700 `"Other race, n.e.c."', add
cap label define raced_lbl 801 `"White and Black"', add
cap label define raced_lbl 802 `"White and AIAN"', add
cap label define raced_lbl 810 `"White and Asian"', add
cap label define raced_lbl 811 `"White and Chinese"', add
cap label define raced_lbl 812 `"White and Japanese"', add
cap label define raced_lbl 813 `"White and Filipino"', add
cap label define raced_lbl 814 `"White and Asian Indian"', add
cap label define raced_lbl 815 `"White and Korean"', add
cap label define raced_lbl 816 `"White and Vietnamese"', add
cap label define raced_lbl 817 `"White and Asian write_in"', add
cap label define raced_lbl 818 `"White and other Asian race(s)"', add
cap label define raced_lbl 819 `"White and two or more Asian groups"', add
cap label define raced_lbl 820 `"White and PI"', add
cap label define raced_lbl 821 `"White and Native Hawaiian"', add
cap label define raced_lbl 822 `"White and Samoan"', add
cap label define raced_lbl 823 `"White and Guamanian"', add
cap label define raced_lbl 824 `"White and PI write_in"', add
cap label define raced_lbl 825 `"White and other PI race(s)"', add
cap label define raced_lbl 826 `"White and other race write_in"', add
cap label define raced_lbl 827 `"White and other race, n.e.c."', add
cap label define raced_lbl 830 `"Black and AIAN"', add
cap label define raced_lbl 831 `"Black and Asian"', add
cap label define raced_lbl 832 `"Black and Chinese"', add
cap label define raced_lbl 833 `"Black and Japanese"', add
cap label define raced_lbl 834 `"Black and Filipino"', add
cap label define raced_lbl 835 `"Black and Asian Indian"', add
cap label define raced_lbl 836 `"Black and Korean"', add
cap label define raced_lbl 837 `"Black and Asian write_in"', add
cap label define raced_lbl 838 `"Black and other Asian race(s)"', add
cap label define raced_lbl 840 `"Black and PI"', add
cap label define raced_lbl 841 `"Black and PI write_in"', add
cap label define raced_lbl 842 `"Black and other PI race(s)"', add
cap label define raced_lbl 845 `"Black and other race write_in"', add
cap label define raced_lbl 850 `"AIAN and Asian"', add
cap label define raced_lbl 851 `"AIAN and Filipino (2000 1%)"', add
cap label define raced_lbl 852 `"AIAN and Asian Indian"', add
cap label define raced_lbl 853 `"AIAN and Asian write_in (2000 1%)"', add
cap label define raced_lbl 854 `"AIAN and other Asian race(s)"', add
cap label define raced_lbl 855 `"AIAN and PI"', add
cap label define raced_lbl 856 `"AIAN and other race write_in"', add
cap label define raced_lbl 860 `"Asian and PI"', add
cap label define raced_lbl 861 `"Chinese and Hawaiian"', add
cap label define raced_lbl 862 `"Chinese, Filipino, Hawaiian (2000 1%)"', add
cap label define raced_lbl 863 `"Japanese and Hawaiian (2000 1%)"', add
cap label define raced_lbl 864 `"Filipino and Hawaiian"', add
cap label define raced_lbl 865 `"Filipino and PI write_in"', add
cap label define raced_lbl 866 `"Asian Indian and PI write_in (2000 1%)"', add
cap label define raced_lbl 867 `"Asian write_in and PI write_in"', add
cap label define raced_lbl 868 `"Other Asian race(s) and PI race(s)"', add
cap label define raced_lbl 869 `"Japanese and Korean (ACS)"', add
cap label define raced_lbl 880 `"Asian and other race write_in"', add
cap label define raced_lbl 881 `"Chinese and other race write_in"', add
cap label define raced_lbl 882 `"Japanese and other race write_in"', add
cap label define raced_lbl 883 `"Filipino and other race write_in"', add
cap label define raced_lbl 884 `"Asian Indian and other race write_in"', add
cap label define raced_lbl 885 `"Asian write_in and other race write_in"', add
cap label define raced_lbl 886 `"Other Asian race(s) and other race write_in"', add
cap label define raced_lbl 887 `"Chinese and Korean"', add
cap label define raced_lbl 890 `"PI and other race write_in:"', add
cap label define raced_lbl 891 `"PI write_in and other race write_in"', add
cap label define raced_lbl 892 `"Other PI race(s) and other race write_in"', add
cap label define raced_lbl 893 `"Native Hawaiian or PI other race(s)"', add
cap label define raced_lbl 899 `"API and other race write_in"', add
cap label define raced_lbl 901 `"White, Black, AIAN"', add
cap label define raced_lbl 902 `"White, Black, Asian"', add
cap label define raced_lbl 903 `"White, Black, PI"', add
cap label define raced_lbl 904 `"White, Black, other race write_in"', add
cap label define raced_lbl 905 `"White, AIAN, Asian"', add
cap label define raced_lbl 906 `"White, AIAN, PI"', add
cap label define raced_lbl 907 `"White, AIAN, other race write_in"', add
cap label define raced_lbl 910 `"White, Asian, PI"', add
cap label define raced_lbl 911 `"White, Chinese, Hawaiian"', add
cap label define raced_lbl 912 `"White, Chinese, Filipino, Hawaiian (2000 1%)"', add
cap label define raced_lbl 913 `"White, Japanese, Hawaiian (2000 1%)"', add
cap label define raced_lbl 914 `"White, Filipino, Hawaiian"', add
cap label define raced_lbl 915 `"Other White, Asian race(s), PI race(s)"', add
cap label define raced_lbl 916 `"White, AIAN and Filipino"', add
cap label define raced_lbl 917 `"White, Black, and Filipino"', add
cap label define raced_lbl 920 `"White, Asian, other race write_in"', add
cap label define raced_lbl 921 `"White, Filipino, other race write_in (2000 1%)"', add
cap label define raced_lbl 922 `"White, Asian write_in, other race write_in (2000 1%)"', add
cap label define raced_lbl 923 `"Other White, Asian race(s), other race write_in (2000 1%)"', add
cap label define raced_lbl 925 `"White, PI, other race write_in"', add
cap label define raced_lbl 930 `"Black, AIAN, Asian"', add
cap label define raced_lbl 931 `"Black, AIAN, PI"', add
cap label define raced_lbl 932 `"Black, AIAN, other race write_in"', add
cap label define raced_lbl 933 `"Black, Asian, PI"', add
cap label define raced_lbl 934 `"Black, Asian, other race write_in"', add
cap label define raced_lbl 935 `"Black, PI, other race write_in"', add
cap label define raced_lbl 940 `"AIAN, Asian, PI"', add
cap label define raced_lbl 941 `"AIAN, Asian, other race write_in"', add
cap label define raced_lbl 942 `"AIAN, PI, other race write_in"', add
cap label define raced_lbl 943 `"Asian, PI, other race write_in"', add
cap label define raced_lbl 944 `"Asian (Chinese, Japanese, Korean, Vietnamese); and Native Hawaiian or PI; and Other"', add
cap label define raced_lbl 949 `"2 or 3 races (CPS)"', add
cap label define raced_lbl 950 `"White, Black, AIAN, Asian"', add
cap label define raced_lbl 951 `"White, Black, AIAN, PI"', add
cap label define raced_lbl 952 `"White, Black, AIAN, other race write_in"', add
cap label define raced_lbl 953 `"White, Black, Asian, PI"', add
cap label define raced_lbl 954 `"White, Black, Asian, other race write_in"', add
cap label define raced_lbl 955 `"White, Black, PI, other race write_in"', add
cap label define raced_lbl 960 `"White, AIAN, Asian, PI"', add
cap label define raced_lbl 961 `"White, AIAN, Asian, other race write_in"', add
cap label define raced_lbl 962 `"White, AIAN, PI, other race write_in"', add
cap label define raced_lbl 963 `"White, Asian, PI, other race write_in"', add
cap label define raced_lbl 964 `"White, Chinese, Japanese, Native Hawaiian"', add
cap label define raced_lbl 970 `"Black, AIAN, Asian, PI"', add
cap label define raced_lbl 971 `"Black, AIAN, Asian, other race write_in"', add
cap label define raced_lbl 972 `"Black, AIAN, PI, other race write_in"', add
cap label define raced_lbl 973 `"Black, Asian, PI, other race write_in"', add
cap label define raced_lbl 974 `"AIAN, Asian, PI, other race write_in"', add
cap label define raced_lbl 975 `"AIAN, Asian, PI, Hawaiian other race write_in"', add
cap label define raced_lbl 976 `"Two specified Asian  (Chinese and other Asian, Chinese and Japanese, Japanese and other Asian, Korean and other Asian); Native Hawaiian/PI; and Other Race"', add
cap label define raced_lbl 980 `"White, Black, AIAN, Asian, PI"', add
cap label define raced_lbl 981 `"White, Black, AIAN, Asian, other race write_in"', add
cap label define raced_lbl 982 `"White, Black, AIAN, PI, other race write_in"', add
cap label define raced_lbl 983 `"White, Black, Asian, PI, other race write_in"', add
cap label define raced_lbl 984 `"White, AIAN, Asian, PI, other race write_in"', add
cap label define raced_lbl 985 `"Black, AIAN, Asian, PI, other race write_in"', add
cap label define raced_lbl 986 `"Black, AIAN, Asian, PI, Hawaiian, other race write_in"', add
cap label define raced_lbl 989 `"4 or 5 races (CPS)"', add
cap label define raced_lbl 990 `"White, Black, AIAN, Asian, PI, other race write_in"', add
cap label define raced_lbl 991 `"White race; Some other race; Black or African American race and/or American Indian and Alaska Native race and/or Asian groups and/or Native Hawaiian and Other Pacific Islander groups"', add
cap label define raced_lbl 996 `"2+ races, n.e.c. (CPS)"', add
cap label values raced raced_lbl

cap label define hispan_lbl 0 `"Not Hispanic"'
cap label define hispan_lbl 1 `"Mexican"', add
cap label define hispan_lbl 2 `"Puerto Rican"', add
cap label define hispan_lbl 3 `"Cuban"', add
cap label define hispan_lbl 4 `"Other"', add
cap label define hispan_lbl 9 `"Not Reported"', add
cap label values hispan hispan_lbl

cap label define hispand_lbl 000 `"Not Hispanic"'
cap label define hispand_lbl 100 `"Mexican"', add
cap label define hispand_lbl 102 `"Mexican American"', add
cap label define hispand_lbl 103 `"Mexicano/Mexicana"', add
cap label define hispand_lbl 104 `"Chicano/Chicana"', add
cap label define hispand_lbl 105 `"La Raza"', add
cap label define hispand_lbl 106 `"Mexican American Indian"', add
cap label define hispand_lbl 107 `"Mexico"', add
cap label define hispand_lbl 200 `"Puerto Rican"', add
cap label define hispand_lbl 300 `"Cuban"', add
cap label define hispand_lbl 401 `"Central American Indian"', add
cap label define hispand_lbl 402 `"Canal Zone"', add
cap label define hispand_lbl 411 `"Costa Rican"', add
cap label define hispand_lbl 412 `"Guatemalan"', add
cap label define hispand_lbl 413 `"Honduran"', add
cap label define hispand_lbl 414 `"Nicaraguan"', add
cap label define hispand_lbl 415 `"Panamanian"', add
cap label define hispand_lbl 416 `"Salvadoran"', add
cap label define hispand_lbl 417 `"Central American, n.e.c."', add
cap label define hispand_lbl 420 `"Argentinean"', add
cap label define hispand_lbl 421 `"Bolivian"', add
cap label define hispand_lbl 422 `"Chilean"', add
cap label define hispand_lbl 423 `"Colombian"', add
cap label define hispand_lbl 424 `"Ecuadorian"', add
cap label define hispand_lbl 425 `"Paraguayan"', add
cap label define hispand_lbl 426 `"Peruvian"', add
cap label define hispand_lbl 427 `"Uruguayan"', add
cap label define hispand_lbl 428 `"Venezuelan"', add
cap label define hispand_lbl 429 `"South American Indian"', add
cap label define hispand_lbl 430 `"Criollo"', add
cap label define hispand_lbl 431 `"South American, n.e.c."', add
cap label define hispand_lbl 450 `"Spaniard"', add
cap label define hispand_lbl 451 `"Andalusian"', add
cap label define hispand_lbl 452 `"Asturian"', add
cap label define hispand_lbl 453 `"Castillian"', add
cap label define hispand_lbl 454 `"Catalonian"', add
cap label define hispand_lbl 455 `"Balearic Islander"', add
cap label define hispand_lbl 456 `"Gallego"', add
cap label define hispand_lbl 457 `"Valencian"', add
cap label define hispand_lbl 458 `"Canarian"', add
cap label define hispand_lbl 459 `"Spanish Basque"', add
cap label define hispand_lbl 460 `"Dominican"', add
cap label define hispand_lbl 465 `"Latin American"', add
cap label define hispand_lbl 470 `"Hispanic"', add
cap label define hispand_lbl 480 `"Spanish"', add
cap label define hispand_lbl 490 `"Californio"', add
cap label define hispand_lbl 491 `"Tejano"', add
cap label define hispand_lbl 492 `"Nuevo Mexicano"', add
cap label define hispand_lbl 493 `"Spanish American"', add
cap label define hispand_lbl 494 `"Spanish American Indian"', add
cap label define hispand_lbl 495 `"Meso American Indian"', add
cap label define hispand_lbl 496 `"Mestizo"', add
cap label define hispand_lbl 498 `"Other, n.s."', add
cap label define hispand_lbl 499 `"Other, n.e.c."', add
cap label define hispand_lbl 900 `"Not Reported"', add
cap label values hispand hispand_lbl

cap label define bpl_lbl 001 `"Alabama"'
cap label define bpl_lbl 002 `"Alaska"', add
cap label define bpl_lbl 004 `"Arizona"', add
cap label define bpl_lbl 005 `"Arkansas"', add
cap label define bpl_lbl 006 `"California"', add
cap label define bpl_lbl 008 `"Colorado"', add
cap label define bpl_lbl 009 `"Connecticut"', add
cap label define bpl_lbl 010 `"Delaware"', add
cap label define bpl_lbl 011 `"District of Columbia"', add
cap label define bpl_lbl 012 `"Florida"', add
cap label define bpl_lbl 013 `"Georgia"', add
cap label define bpl_lbl 015 `"Hawaii"', add
cap label define bpl_lbl 016 `"Idaho"', add
cap label define bpl_lbl 017 `"Illinois"', add
cap label define bpl_lbl 018 `"Indiana"', add
cap label define bpl_lbl 019 `"Iowa"', add
cap label define bpl_lbl 020 `"Kansas"', add
cap label define bpl_lbl 021 `"Kentucky"', add
cap label define bpl_lbl 022 `"Louisiana"', add
cap label define bpl_lbl 023 `"Maine"', add
cap label define bpl_lbl 024 `"Maryland"', add
cap label define bpl_lbl 025 `"Massachusetts"', add
cap label define bpl_lbl 026 `"Michigan"', add
cap label define bpl_lbl 027 `"Minnesota"', add
cap label define bpl_lbl 028 `"Mississippi"', add
cap label define bpl_lbl 029 `"Missouri"', add
cap label define bpl_lbl 030 `"Montana"', add
cap label define bpl_lbl 031 `"Nebraska"', add
cap label define bpl_lbl 032 `"Nevada"', add
cap label define bpl_lbl 033 `"New Hampshire"', add
cap label define bpl_lbl 034 `"New Jersey"', add
cap label define bpl_lbl 035 `"New Mexico"', add
cap label define bpl_lbl 036 `"New York"', add
cap label define bpl_lbl 037 `"North Carolina"', add
cap label define bpl_lbl 038 `"North Dakota"', add
cap label define bpl_lbl 039 `"Ohio"', add
cap label define bpl_lbl 040 `"Oklahoma"', add
cap label define bpl_lbl 041 `"Oregon"', add
cap label define bpl_lbl 042 `"Pennsylvania"', add
cap label define bpl_lbl 044 `"Rhode Island"', add
cap label define bpl_lbl 045 `"South Carolina"', add
cap label define bpl_lbl 046 `"South Dakota"', add
cap label define bpl_lbl 047 `"Tennessee"', add
cap label define bpl_lbl 048 `"Texas"', add
cap label define bpl_lbl 049 `"Utah"', add
cap label define bpl_lbl 050 `"Vermont"', add
cap label define bpl_lbl 051 `"Virginia"', add
cap label define bpl_lbl 053 `"Washington"', add
cap label define bpl_lbl 054 `"West Virginia"', add
cap label define bpl_lbl 055 `"Wisconsin"', add
cap label define bpl_lbl 056 `"Wyoming"', add
cap label define bpl_lbl 090 `"Native American"', add
cap label define bpl_lbl 099 `"United States, ns"', add
cap label define bpl_lbl 100 `"American Samoa"', add
cap label define bpl_lbl 105 `"Guam"', add
cap label define bpl_lbl 110 `"Puerto Rico"', add
cap label define bpl_lbl 115 `"U.S. Virgin Islands"', add
cap label define bpl_lbl 120 `"Other US Possessions"', add
cap label define bpl_lbl 150 `"Canada"', add
cap label define bpl_lbl 155 `"St. Pierre and Miquelon"', add
cap label define bpl_lbl 160 `"Atlantic Islands"', add
cap label define bpl_lbl 199 `"North America, ns"', add
cap label define bpl_lbl 200 `"Mexico"', add
cap label define bpl_lbl 210 `"Central America"', add
cap label define bpl_lbl 250 `"Cuba"', add
cap label define bpl_lbl 260 `"West Indies"', add
cap label define bpl_lbl 299 `"Americas, n.s."', add
cap label define bpl_lbl 300 `"SOUTH AMERICA"', add
cap label define bpl_lbl 400 `"Denmark"', add
cap label define bpl_lbl 401 `"Finland"', add
cap label define bpl_lbl 402 `"Iceland"', add
cap label define bpl_lbl 403 `"Lapland, n.s."', add
cap label define bpl_lbl 404 `"Norway"', add
cap label define bpl_lbl 405 `"Sweden"', add
cap label define bpl_lbl 410 `"England"', add
cap label define bpl_lbl 411 `"Scotland"', add
cap label define bpl_lbl 412 `"Wales"', add
cap label define bpl_lbl 413 `"United Kingdom, ns"', add
cap label define bpl_lbl 414 `"Ireland"', add
cap label define bpl_lbl 419 `"Northern Europe, ns"', add
cap label define bpl_lbl 420 `"Belgium"', add
cap label define bpl_lbl 421 `"France"', add
cap label define bpl_lbl 422 `"Liechtenstein"', add
cap label define bpl_lbl 423 `"Luxembourg"', add
cap label define bpl_lbl 424 `"Monaco"', add
cap label define bpl_lbl 425 `"Netherlands"', add
cap label define bpl_lbl 426 `"Switzerland"', add
cap label define bpl_lbl 429 `"Western Europe, ns"', add
cap label define bpl_lbl 430 `"Albania"', add
cap label define bpl_lbl 431 `"Andorra"', add
cap label define bpl_lbl 432 `"Gibraltar"', add
cap label define bpl_lbl 433 `"Greece"', add
cap label define bpl_lbl 434 `"Italy"', add
cap label define bpl_lbl 435 `"Malta"', add
cap label define bpl_lbl 436 `"Portugal"', add
cap label define bpl_lbl 437 `"San Marino"', add
cap label define bpl_lbl 438 `"Spain"', add
cap label define bpl_lbl 439 `"Vatican City"', add
cap label define bpl_lbl 440 `"Southern Europe, ns"', add
cap label define bpl_lbl 450 `"Austria"', add
cap label define bpl_lbl 451 `"Bulgaria"', add
cap label define bpl_lbl 452 `"Czechoslovakia"', add
cap label define bpl_lbl 453 `"Germany"', add
cap label define bpl_lbl 454 `"Hungary"', add
cap label define bpl_lbl 455 `"Poland"', add
cap label define bpl_lbl 456 `"Romania"', add
cap label define bpl_lbl 457 `"Yugoslavia"', add
cap label define bpl_lbl 458 `"Central Europe, ns"', add
cap label define bpl_lbl 459 `"Eastern Europe, ns"', add
cap label define bpl_lbl 460 `"Estonia"', add
cap label define bpl_lbl 461 `"Latvia"', add
cap label define bpl_lbl 462 `"Lithuania"', add
cap label define bpl_lbl 463 `"Baltic States, ns"', add
cap label define bpl_lbl 465 `"Other USSR/Russia"', add
cap label define bpl_lbl 499 `"Europe, ns"', add
cap label define bpl_lbl 500 `"China"', add
cap label define bpl_lbl 501 `"Japan"', add
cap label define bpl_lbl 502 `"Korea"', add
cap label define bpl_lbl 509 `"East Asia, ns"', add
cap label define bpl_lbl 510 `"Brunei"', add
cap label define bpl_lbl 511 `"Cambodia (Kampuchea)"', add
cap label define bpl_lbl 512 `"Indonesia"', add
cap label define bpl_lbl 513 `"Laos"', add
cap label define bpl_lbl 514 `"Malaysia"', add
cap label define bpl_lbl 515 `"Philippines"', add
cap label define bpl_lbl 516 `"Singapore"', add
cap label define bpl_lbl 517 `"Thailand"', add
cap label define bpl_lbl 518 `"Vietnam"', add
cap label define bpl_lbl 519 `"Southeast Asia, ns"', add
cap label define bpl_lbl 520 `"Afghanistan"', add
cap label define bpl_lbl 521 `"India"', add
cap label define bpl_lbl 522 `"Iran"', add
cap label define bpl_lbl 523 `"Maldives"', add
cap label define bpl_lbl 524 `"Nepal"', add
cap label define bpl_lbl 530 `"Bahrain"', add
cap label define bpl_lbl 531 `"Cyprus"', add
cap label define bpl_lbl 532 `"Iraq"', add
cap label define bpl_lbl 533 `"Iraq/Saudi Arabia"', add
cap label define bpl_lbl 534 `"Israel/Palestine"', add
cap label define bpl_lbl 535 `"Jordan"', add
cap label define bpl_lbl 536 `"Kuwait"', add
cap label define bpl_lbl 537 `"Lebanon"', add
cap label define bpl_lbl 538 `"Oman"', add
cap label define bpl_lbl 539 `"Qatar"', add
cap label define bpl_lbl 540 `"Saudi Arabia"', add
cap label define bpl_lbl 541 `"Syria"', add
cap label define bpl_lbl 542 `"Turkey"', add
cap label define bpl_lbl 543 `"United Arab Emirates"', add
cap label define bpl_lbl 544 `"Yemen Arab Republic (North)"', add
cap label define bpl_lbl 545 `"Yemen, PDR (South)"', add
cap label define bpl_lbl 546 `"Persian Gulf States, n.s."', add
cap label define bpl_lbl 547 `"Middle East, ns"', add
cap label define bpl_lbl 548 `"Southwest Asia, nec/ns"', add
cap label define bpl_lbl 549 `"Asia Minor, ns"', add
cap label define bpl_lbl 550 `"South Asia, nec"', add
cap label define bpl_lbl 599 `"Asia, nec/ns"', add
cap label define bpl_lbl 600 `"AFRICA"', add
cap label define bpl_lbl 700 `"Australia and New Zealand"', add
cap label define bpl_lbl 710 `"Pacific Islands"', add
cap label define bpl_lbl 800 `"Antarctica, ns/nec"', add
cap label define bpl_lbl 900 `"Abroad (unknown) or at sea"', add
cap label define bpl_lbl 950 `"Other n.e.c."', add
cap label define bpl_lbl 999 `"Missing/blank"', add
cap label values bpl bpl_lbl

cap label define bpld_lbl 00100 `"Alabama"'
cap label define bpld_lbl 00200 `"Alaska"', add
cap label define bpld_lbl 00400 `"Arizona"', add
cap label define bpld_lbl 00500 `"Arkansas"', add
cap label define bpld_lbl 00600 `"California"', add
cap label define bpld_lbl 00800 `"Colorado"', add
cap label define bpld_lbl 00900 `"Connecticut"', add
cap label define bpld_lbl 01000 `"Delaware"', add
cap label define bpld_lbl 01100 `"District of Columbia"', add
cap label define bpld_lbl 01200 `"Florida"', add
cap label define bpld_lbl 01300 `"Georgia"', add
cap label define bpld_lbl 01500 `"Hawaii"', add
cap label define bpld_lbl 01600 `"Idaho"', add
cap label define bpld_lbl 01610 `"Idaho Territory"', add
cap label define bpld_lbl 01700 `"Illinois"', add
cap label define bpld_lbl 01800 `"Indiana"', add
cap label define bpld_lbl 01900 `"Iowa"', add
cap label define bpld_lbl 02000 `"Kansas"', add
cap label define bpld_lbl 02100 `"Kentucky"', add
cap label define bpld_lbl 02200 `"Louisiana"', add
cap label define bpld_lbl 02300 `"Maine"', add
cap label define bpld_lbl 02400 `"Maryland"', add
cap label define bpld_lbl 02500 `"Massachusetts"', add
cap label define bpld_lbl 02600 `"Michigan"', add
cap label define bpld_lbl 02700 `"Minnesota"', add
cap label define bpld_lbl 02800 `"Mississippi"', add
cap label define bpld_lbl 02900 `"Missouri"', add
cap label define bpld_lbl 03000 `"Montana"', add
cap label define bpld_lbl 03100 `"Nebraska"', add
cap label define bpld_lbl 03200 `"Nevada"', add
cap label define bpld_lbl 03300 `"New Hampshire"', add
cap label define bpld_lbl 03400 `"New Jersey"', add
cap label define bpld_lbl 03500 `"New Mexico"', add
cap label define bpld_lbl 03510 `"New Mexico Territory"', add
cap label define bpld_lbl 03600 `"New York"', add
cap label define bpld_lbl 03700 `"North Carolina"', add
cap label define bpld_lbl 03800 `"North Dakota"', add
cap label define bpld_lbl 03900 `"Ohio"', add
cap label define bpld_lbl 04000 `"Oklahoma"', add
cap label define bpld_lbl 04010 `"Indian Territory"', add
cap label define bpld_lbl 04100 `"Oregon"', add
cap label define bpld_lbl 04200 `"Pennsylvania"', add
cap label define bpld_lbl 04400 `"Rhode Island"', add
cap label define bpld_lbl 04500 `"South Carolina"', add
cap label define bpld_lbl 04600 `"South Dakota"', add
cap label define bpld_lbl 04610 `"Dakota Territory"', add
cap label define bpld_lbl 04700 `"Tennessee"', add
cap label define bpld_lbl 04800 `"Texas"', add
cap label define bpld_lbl 04900 `"Utah"', add
cap label define bpld_lbl 04910 `"Utah Territory"', add
cap label define bpld_lbl 05000 `"Vermont"', add
cap label define bpld_lbl 05100 `"Virginia"', add
cap label define bpld_lbl 05300 `"Washington"', add
cap label define bpld_lbl 05400 `"West Virginia"', add
cap label define bpld_lbl 05500 `"Wisconsin"', add
cap label define bpld_lbl 05600 `"Wyoming"', add
cap label define bpld_lbl 05610 `"Wyoming Territory"', add
cap label define bpld_lbl 09000 `"Native American"', add
cap label define bpld_lbl 09900 `"United States, ns"', add
cap label define bpld_lbl 10000 `"American Samoa"', add
cap label define bpld_lbl 10010 `"Samoa, 1940-1950"', add
cap label define bpld_lbl 10500 `"Guam"', add
cap label define bpld_lbl 11000 `"Puerto Rico"', add
cap label define bpld_lbl 11500 `"U.S. Virgin Islands"', add
cap label define bpld_lbl 11510 `"St. Croix"', add
cap label define bpld_lbl 11520 `"St. John"', add
cap label define bpld_lbl 11530 `"St. Thomas"', add
cap label define bpld_lbl 12000 `"Other US Possessions:"', add
cap label define bpld_lbl 12010 `"Johnston Atoll"', add
cap label define bpld_lbl 12020 `"Midway Islands"', add
cap label define bpld_lbl 12030 `"Wake Island"', add
cap label define bpld_lbl 12040 `"Other US Caribbean Islands"', add
cap label define bpld_lbl 12041 `"Navassa Island"', add
cap label define bpld_lbl 12050 `"Other US Pacific Islands"', add
cap label define bpld_lbl 12051 `"Baker Island"', add
cap label define bpld_lbl 12052 `"Howland Island"', add
cap label define bpld_lbl 12053 `"Jarvis Island"', add
cap label define bpld_lbl 12054 `"Kingman Reef"', add
cap label define bpld_lbl 12055 `"Palmyra Atoll"', add
cap label define bpld_lbl 12056 `"Canton and Enderbury Island"', add
cap label define bpld_lbl 12090 `"US outlying areas, ns"', add
cap label define bpld_lbl 12091 `"US possessions, ns"', add
cap label define bpld_lbl 12092 `"US territory, ns"', add
cap label define bpld_lbl 15000 `"Canada"', add
cap label define bpld_lbl 15010 `"English Canada"', add
cap label define bpld_lbl 15011 `"British Columbia"', add
cap label define bpld_lbl 15013 `"Alberta"', add
cap label define bpld_lbl 15015 `"Saskatchewan"', add
cap label define bpld_lbl 15017 `"Northwest"', add
cap label define bpld_lbl 15019 `"Ruperts Land"', add
cap label define bpld_lbl 15020 `"Manitoba"', add
cap label define bpld_lbl 15021 `"Red River"', add
cap label define bpld_lbl 15030 `"Ontario/Upper Canada"', add
cap label define bpld_lbl 15031 `"Upper Canada"', add
cap label define bpld_lbl 15032 `"Canada West"', add
cap label define bpld_lbl 15040 `"New Brunswick"', add
cap label define bpld_lbl 15050 `"Nova Scotia"', add
cap label define bpld_lbl 15051 `"Cape Breton"', add
cap label define bpld_lbl 15052 `"Halifax"', add
cap label define bpld_lbl 15060 `"Prince Edward Island"', add
cap label define bpld_lbl 15070 `"Newfoundland"', add
cap label define bpld_lbl 15080 `"French Canada"', add
cap label define bpld_lbl 15081 `"Quebec"', add
cap label define bpld_lbl 15082 `"Lower Canada"', add
cap label define bpld_lbl 15083 `"Canada East"', add
cap label define bpld_lbl 15500 `"St. Pierre and Miquelon"', add
cap label define bpld_lbl 16000 `"Atlantic Islands"', add
cap label define bpld_lbl 16010 `"Bermuda"', add
cap label define bpld_lbl 16020 `"Cape Verde"', add
cap label define bpld_lbl 16030 `"Falkland Islands"', add
cap label define bpld_lbl 16040 `"Greenland"', add
cap label define bpld_lbl 16050 `"St. Helena and Ascension"', add
cap label define bpld_lbl 16060 `"Canary Islands"', add
cap label define bpld_lbl 19900 `"North America, ns"', add
cap label define bpld_lbl 20000 `"Mexico"', add
cap label define bpld_lbl 21000 `"Central America"', add
cap label define bpld_lbl 21010 `"Belize/British Honduras"', add
cap label define bpld_lbl 21020 `"Costa Rica"', add
cap label define bpld_lbl 21030 `"El Salvador"', add
cap label define bpld_lbl 21040 `"Guatemala"', add
cap label define bpld_lbl 21050 `"Honduras"', add
cap label define bpld_lbl 21060 `"Nicaragua"', add
cap label define bpld_lbl 21070 `"Panama"', add
cap label define bpld_lbl 21071 `"Canal Zone"', add
cap label define bpld_lbl 21090 `"Central America, ns"', add
cap label define bpld_lbl 25000 `"Cuba"', add
cap label define bpld_lbl 26000 `"West Indies"', add
cap label define bpld_lbl 26010 `"Dominican Republic"', add
cap label define bpld_lbl 26020 `"Haiti"', add
cap label define bpld_lbl 26030 `"Jamaica"', add
cap label define bpld_lbl 26040 `"British West Indies"', add
cap label define bpld_lbl 26041 `"Anguilla"', add
cap label define bpld_lbl 26042 `"Antigua-Barbuda"', add
cap label define bpld_lbl 26043 `"Bahamas"', add
cap label define bpld_lbl 26044 `"Barbados"', add
cap label define bpld_lbl 26045 `"British Virgin Islands"', add
cap label define bpld_lbl 26046 `"Anegada"', add
cap label define bpld_lbl 26047 `"Cooper"', add
cap label define bpld_lbl 26048 `"Jost Van Dyke"', add
cap label define bpld_lbl 26049 `"Peter"', add
cap label define bpld_lbl 26050 `"Tortola"', add
cap label define bpld_lbl 26051 `"Virgin Gorda"', add
cap label define bpld_lbl 26052 `"Br. Virgin Islands, ns"', add
cap label define bpld_lbl 26053 `"Cayman Islands"', add
cap label define bpld_lbl 26054 `"Dominica"', add
cap label define bpld_lbl 26055 `"Grenada"', add
cap label define bpld_lbl 26056 `"Montserrat"', add
cap label define bpld_lbl 26057 `"St. Kitts-Nevis"', add
cap label define bpld_lbl 26058 `"St. Lucia"', add
cap label define bpld_lbl 26059 `"St. Vincent"', add
cap label define bpld_lbl 26060 `"Trinidad and Tobago"', add
cap label define bpld_lbl 26061 `"Turks and Caicos"', add
cap label define bpld_lbl 26069 `"Br. Virgin Islands, ns"', add
cap label define bpld_lbl 26070 `"Other West Indies"', add
cap label define bpld_lbl 26071 `"Aruba"', add
cap label define bpld_lbl 26072 `"Netherlands Antilles"', add
cap label define bpld_lbl 26073 `"Bonaire"', add
cap label define bpld_lbl 26074 `"Curacao"', add
cap label define bpld_lbl 26075 `"Dutch St. Maarten"', add
cap label define bpld_lbl 26076 `"Saba"', add
cap label define bpld_lbl 26077 `"St. Eustatius"', add
cap label define bpld_lbl 26079 `"Dutch Caribbean, ns"', add
cap label define bpld_lbl 26080 `"French St. Maarten"', add
cap label define bpld_lbl 26081 `"Guadeloupe"', add
cap label define bpld_lbl 26082 `"Martinique"', add
cap label define bpld_lbl 26083 `"St. Barthelemy"', add
cap label define bpld_lbl 26089 `"French Caribbean, ns"', add
cap label define bpld_lbl 26090 `"Antilles, ns"', add
cap label define bpld_lbl 26091 `"Caribbean, ns"', add
cap label define bpld_lbl 26092 `"Latin America, ns"', add
cap label define bpld_lbl 26093 `"Leeward Islands, ns"', add
cap label define bpld_lbl 26094 `"West Indies, ns"', add
cap label define bpld_lbl 26095 `"Windward Islands, ns"', add
cap label define bpld_lbl 29900 `"Americas, ns"', add
cap label define bpld_lbl 30000 `"South America"', add
cap label define bpld_lbl 30005 `"Argentina"', add
cap label define bpld_lbl 30010 `"Bolivia"', add
cap label define bpld_lbl 30015 `"Brazil"', add
cap label define bpld_lbl 30020 `"Chile"', add
cap label define bpld_lbl 30025 `"Colombia"', add
cap label define bpld_lbl 30030 `"Ecuador"', add
cap label define bpld_lbl 30035 `"French Guiana"', add
cap label define bpld_lbl 30040 `"Guyana/British Guiana"', add
cap label define bpld_lbl 30045 `"Paraguay"', add
cap label define bpld_lbl 30050 `"Peru"', add
cap label define bpld_lbl 30055 `"Suriname"', add
cap label define bpld_lbl 30060 `"Uruguay"', add
cap label define bpld_lbl 30065 `"Venezuela"', add
cap label define bpld_lbl 30090 `"South America, ns"', add
cap label define bpld_lbl 30091 `"South and Central America, n.s."', add
cap label define bpld_lbl 40000 `"Denmark"', add
cap label define bpld_lbl 40010 `"Faeroe Islands"', add
cap label define bpld_lbl 40100 `"Finland"', add
cap label define bpld_lbl 40200 `"Iceland"', add
cap label define bpld_lbl 40300 `"Lapland, ns"', add
cap label define bpld_lbl 40400 `"Norway"', add
cap label define bpld_lbl 40410 `"Svalbard and Jan Meyen"', add
cap label define bpld_lbl 40411 `"Svalbard"', add
cap label define bpld_lbl 40412 `"Jan Meyen"', add
cap label define bpld_lbl 40500 `"Sweden"', add
cap label define bpld_lbl 41000 `"England"', add
cap label define bpld_lbl 41010 `"Channel Islands"', add
cap label define bpld_lbl 41011 `"Guernsey"', add
cap label define bpld_lbl 41012 `"Jersey"', add
cap label define bpld_lbl 41020 `"Isle of Man"', add
cap label define bpld_lbl 41100 `"Scotland"', add
cap label define bpld_lbl 41200 `"Wales"', add
cap label define bpld_lbl 41300 `"United Kingdom, ns"', add
cap label define bpld_lbl 41400 `"Ireland"', add
cap label define bpld_lbl 41410 `"Northern Ireland"', add
cap label define bpld_lbl 41900 `"Northern Europe, ns"', add
cap label define bpld_lbl 42000 `"Belgium"', add
cap label define bpld_lbl 42100 `"France"', add
cap label define bpld_lbl 42110 `"Alsace-Lorraine"', add
cap label define bpld_lbl 42111 `"Alsace"', add
cap label define bpld_lbl 42112 `"Lorraine"', add
cap label define bpld_lbl 42200 `"Liechtenstein"', add
cap label define bpld_lbl 42300 `"Luxembourg"', add
cap label define bpld_lbl 42400 `"Monaco"', add
cap label define bpld_lbl 42500 `"Netherlands"', add
cap label define bpld_lbl 42600 `"Switzerland"', add
cap label define bpld_lbl 42900 `"Western Europe, ns"', add
cap label define bpld_lbl 43000 `"Albania"', add
cap label define bpld_lbl 43100 `"Andorra"', add
cap label define bpld_lbl 43200 `"Gibraltar"', add
cap label define bpld_lbl 43300 `"Greece"', add
cap label define bpld_lbl 43310 `"Dodecanese Islands"', add
cap label define bpld_lbl 43320 `"Turkey Greece"', add
cap label define bpld_lbl 43330 `"Macedonia"', add
cap label define bpld_lbl 43400 `"Italy"', add
cap label define bpld_lbl 43500 `"Malta"', add
cap label define bpld_lbl 43600 `"Portugal"', add
cap label define bpld_lbl 43610 `"Azores"', add
cap label define bpld_lbl 43620 `"Madeira Islands"', add
cap label define bpld_lbl 43630 `"Cape Verde Islands"', add
cap label define bpld_lbl 43640 `"St. Miguel"', add
cap label define bpld_lbl 43700 `"San Marino"', add
cap label define bpld_lbl 43800 `"Spain"', add
cap label define bpld_lbl 43900 `"Vatican City"', add
cap label define bpld_lbl 44000 `"Southern Europe, ns"', add
cap label define bpld_lbl 45000 `"Austria"', add
cap label define bpld_lbl 45010 `"Austria-Hungary"', add
cap label define bpld_lbl 45020 `"Austria-Graz"', add
cap label define bpld_lbl 45030 `"Austria-Linz"', add
cap label define bpld_lbl 45040 `"Austria-Salzburg"', add
cap label define bpld_lbl 45050 `"Austria-Tyrol"', add
cap label define bpld_lbl 45060 `"Austria-Vienna"', add
cap label define bpld_lbl 45070 `"Austria-Kaernsten"', add
cap label define bpld_lbl 45080 `"Austria-Neustadt"', add
cap label define bpld_lbl 45100 `"Bulgaria"', add
cap label define bpld_lbl 45200 `"Czechoslovakia"', add
cap label define bpld_lbl 45210 `"Bohemia"', add
cap label define bpld_lbl 45211 `"Bohemia-Moravia"', add
cap label define bpld_lbl 45212 `"Slovakia"', add
cap label define bpld_lbl 45213 `"Czech Republic"', add
cap label define bpld_lbl 45300 `"Germany"', add
cap label define bpld_lbl 45301 `"Berlin"', add
cap label define bpld_lbl 45302 `"West Berlin"', add
cap label define bpld_lbl 45303 `"East Berlin"', add
cap label define bpld_lbl 45310 `"West Germany"', add
cap label define bpld_lbl 45311 `"Baden"', add
cap label define bpld_lbl 45312 `"Bavaria"', add
cap label define bpld_lbl 45313 `"Braunschweig"', add
cap label define bpld_lbl 45314 `"Bremen"', add
cap label define bpld_lbl 45315 `"Hamburg"', add
cap label define bpld_lbl 45316 `"Hanover"', add
cap label define bpld_lbl 45317 `"Hessen"', add
cap label define bpld_lbl 45318 `"Hesse-Nassau"', add
cap label define bpld_lbl 45319 `"Lippe"', add
cap label define bpld_lbl 45320 `"Lubeck"', add
cap label define bpld_lbl 45321 `"Oldenburg"', add
cap label define bpld_lbl 45322 `"Rheinland"', add
cap label define bpld_lbl 45323 `"Schaumburg-Lippe"', add
cap label define bpld_lbl 45324 `"Schleswig"', add
cap label define bpld_lbl 45325 `"Sigmaringen"', add
cap label define bpld_lbl 45326 `"Schwarzburg"', add
cap label define bpld_lbl 45327 `"Westphalia"', add
cap label define bpld_lbl 45328 `"Wurttemberg"', add
cap label define bpld_lbl 45329 `"Waldeck"', add
cap label define bpld_lbl 45330 `"Wittenberg"', add
cap label define bpld_lbl 45331 `"Frankfurt"', add
cap label define bpld_lbl 45332 `"Saarland"', add
cap label define bpld_lbl 45333 `"Nordrhein-Westfalen"', add
cap label define bpld_lbl 45340 `"East Germany"', add
cap label define bpld_lbl 45341 `"Anhalt"', add
cap label define bpld_lbl 45342 `"Brandenburg"', add
cap label define bpld_lbl 45344 `"Kingdom of Saxony"', add
cap label define bpld_lbl 45345 `"Mecklenburg"', add
cap label define bpld_lbl 45346 `"Saxony"', add
cap label define bpld_lbl 45347 `"Thuringian States"', add
cap label define bpld_lbl 45348 `"Sachsen-Meiningen"', add
cap label define bpld_lbl 45349 `"Sachsen-Weimar-Eisenach"', add
cap label define bpld_lbl 45350 `"Probable Saxony"', add
cap label define bpld_lbl 45351 `"Schwerin"', add
cap label define bpld_lbl 45352 `"Strelitz"', add
cap label define bpld_lbl 45353 `"Probably Thuringian States"', add
cap label define bpld_lbl 45360 `"Prussia, nec"', add
cap label define bpld_lbl 45361 `"Hohenzollern"', add
cap label define bpld_lbl 45362 `"Niedersachsen"', add
cap label define bpld_lbl 45400 `"Hungary"', add
cap label define bpld_lbl 45500 `"Poland"', add
cap label define bpld_lbl 45510 `"Austrian Poland"', add
cap label define bpld_lbl 45511 `"Galicia"', add
cap label define bpld_lbl 45520 `"German Poland"', add
cap label define bpld_lbl 45521 `"East Prussia"', add
cap label define bpld_lbl 45522 `"Pomerania"', add
cap label define bpld_lbl 45523 `"Posen"', add
cap label define bpld_lbl 45524 `"Prussian Poland"', add
cap label define bpld_lbl 45525 `"Silesia"', add
cap label define bpld_lbl 45526 `"West Prussia"', add
cap label define bpld_lbl 45530 `"Russian Poland"', add
cap label define bpld_lbl 45600 `"Romania"', add
cap label define bpld_lbl 45610 `"Transylvania"', add
cap label define bpld_lbl 45700 `"Yugoslavia"', add
cap label define bpld_lbl 45710 `"Croatia"', add
cap label define bpld_lbl 45720 `"Montenegro"', add
cap label define bpld_lbl 45730 `"Serbia"', add
cap label define bpld_lbl 45740 `"Bosnia"', add
cap label define bpld_lbl 45750 `"Dalmatia"', add
cap label define bpld_lbl 45760 `"Slovonia"', add
cap label define bpld_lbl 45770 `"Carniola"', add
cap label define bpld_lbl 45780 `"Slovenia"', add
cap label define bpld_lbl 45790 `"Kosovo"', add
cap label define bpld_lbl 45800 `"Central Europe, ns"', add
cap label define bpld_lbl 45900 `"Eastern Europe, ns"', add
cap label define bpld_lbl 46000 `"Estonia"', add
cap label define bpld_lbl 46100 `"Latvia"', add
cap label define bpld_lbl 46200 `"Lithuania"', add
cap label define bpld_lbl 46300 `"Baltic States, ns"', add
cap label define bpld_lbl 46500 `"Other USSR/Russia"', add
cap label define bpld_lbl 46510 `"Byelorussia"', add
cap label define bpld_lbl 46520 `"Moldavia"', add
cap label define bpld_lbl 46521 `"Bessarabia"', add
cap label define bpld_lbl 46530 `"Ukraine"', add
cap label define bpld_lbl 46540 `"Armenia"', add
cap label define bpld_lbl 46541 `"Azerbaijan"', add
cap label define bpld_lbl 46542 `"Republic of Georgia"', add
cap label define bpld_lbl 46543 `"Kazakhstan"', add
cap label define bpld_lbl 46544 `"Kirghizia"', add
cap label define bpld_lbl 46545 `"Tadzhik"', add
cap label define bpld_lbl 46546 `"Turkmenistan"', add
cap label define bpld_lbl 46547 `"Uzbekistan"', add
cap label define bpld_lbl 46548 `"Siberia"', add
cap label define bpld_lbl 46590 `"USSR, ns"', add
cap label define bpld_lbl 49900 `"Europe, ns."', add
cap label define bpld_lbl 50000 `"China"', add
cap label define bpld_lbl 50010 `"Hong Kong"', add
cap label define bpld_lbl 50020 `"Macau"', add
cap label define bpld_lbl 50030 `"Mongolia"', add
cap label define bpld_lbl 50040 `"Taiwan"', add
cap label define bpld_lbl 50100 `"Japan"', add
cap label define bpld_lbl 50200 `"Korea"', add
cap label define bpld_lbl 50210 `"North Korea"', add
cap label define bpld_lbl 50220 `"South Korea"', add
cap label define bpld_lbl 50900 `"East Asia, ns"', add
cap label define bpld_lbl 51000 `"Brunei"', add
cap label define bpld_lbl 51100 `"Cambodia (Kampuchea)"', add
cap label define bpld_lbl 51200 `"Indonesia"', add
cap label define bpld_lbl 51210 `"East Indies"', add
cap label define bpld_lbl 51220 `"East Timor"', add
cap label define bpld_lbl 51300 `"Laos"', add
cap label define bpld_lbl 51400 `"Malaysia"', add
cap label define bpld_lbl 51500 `"Philippines"', add
cap label define bpld_lbl 51600 `"Singapore"', add
cap label define bpld_lbl 51700 `"Thailand"', add
cap label define bpld_lbl 51800 `"Vietnam"', add
cap label define bpld_lbl 51900 `"Southeast Asia, ns"', add
cap label define bpld_lbl 51910 `"Indochina, ns"', add
cap label define bpld_lbl 52000 `"Afghanistan"', add
cap label define bpld_lbl 52100 `"India"', add
cap label define bpld_lbl 52110 `"Bangladesh"', add
cap label define bpld_lbl 52120 `"Bhutan"', add
cap label define bpld_lbl 52130 `"Burma (Myanmar)"', add
cap label define bpld_lbl 52140 `"Pakistan"', add
cap label define bpld_lbl 52150 `"Sri Lanka (Ceylon)"', add
cap label define bpld_lbl 52200 `"Iran"', add
cap label define bpld_lbl 52300 `"Maldives"', add
cap label define bpld_lbl 52400 `"Nepal"', add
cap label define bpld_lbl 53000 `"Bahrain"', add
cap label define bpld_lbl 53100 `"Cyprus"', add
cap label define bpld_lbl 53200 `"Iraq"', add
cap label define bpld_lbl 53210 `"Mesopotamia"', add
cap label define bpld_lbl 53300 `"Iraq/Saudi Arabia"', add
cap label define bpld_lbl 53400 `"Israel/Palestine"', add
cap label define bpld_lbl 53410 `"Gaza Strip"', add
cap label define bpld_lbl 53420 `"Palestine"', add
cap label define bpld_lbl 53430 `"West Bank"', add
cap label define bpld_lbl 53440 `"Israel"', add
cap label define bpld_lbl 53500 `"Jordan"', add
cap label define bpld_lbl 53600 `"Kuwait"', add
cap label define bpld_lbl 53700 `"Lebanon"', add
cap label define bpld_lbl 53800 `"Oman"', add
cap label define bpld_lbl 53900 `"Qatar"', add
cap label define bpld_lbl 54000 `"Saudi Arabia"', add
cap label define bpld_lbl 54100 `"Syria"', add
cap label define bpld_lbl 54200 `"Turkey"', add
cap label define bpld_lbl 54210 `"European Turkey"', add
cap label define bpld_lbl 54220 `"Asian Turkey"', add
cap label define bpld_lbl 54300 `"United Arab Emirates"', add
cap label define bpld_lbl 54400 `"Yemen Arab Republic (North)"', add
cap label define bpld_lbl 54500 `"Yemen, PDR (South)"', add
cap label define bpld_lbl 54600 `"Persian Gulf States, ns"', add
cap label define bpld_lbl 54700 `"Middle East, ns"', add
cap label define bpld_lbl 54800 `"Southwest Asia, nec/ns"', add
cap label define bpld_lbl 54900 `"Asia Minor, ns"', add
cap label define bpld_lbl 55000 `"South Asia, nec"', add
cap label define bpld_lbl 59900 `"Asia, nec/ns"', add
cap label define bpld_lbl 60000 `"Africa"', add
cap label define bpld_lbl 60010 `"Northern Africa"', add
cap label define bpld_lbl 60011 `"Algeria"', add
cap label define bpld_lbl 60012 `"Egypt/United Arab Rep."', add
cap label define bpld_lbl 60013 `"Libya"', add
cap label define bpld_lbl 60014 `"Morocco"', add
cap label define bpld_lbl 60015 `"Sudan"', add
cap label define bpld_lbl 60016 `"Tunisia"', add
cap label define bpld_lbl 60017 `"Western Sahara"', add
cap label define bpld_lbl 60019 `"North Africa, ns"', add
cap label define bpld_lbl 60020 `"Benin"', add
cap label define bpld_lbl 60021 `"Burkina Faso"', add
cap label define bpld_lbl 60022 `"Gambia"', add
cap label define bpld_lbl 60023 `"Ghana"', add
cap label define bpld_lbl 60024 `"Guinea"', add
cap label define bpld_lbl 60025 `"Guinea-Bissau"', add
cap label define bpld_lbl 60026 `"Ivory Coast"', add
cap label define bpld_lbl 60027 `"Liberia"', add
cap label define bpld_lbl 60028 `"Mali"', add
cap label define bpld_lbl 60029 `"Mauritania"', add
cap label define bpld_lbl 60030 `"Niger"', add
cap label define bpld_lbl 60031 `"Nigeria"', add
cap label define bpld_lbl 60032 `"Senegal"', add
cap label define bpld_lbl 60033 `"Sierra Leone"', add
cap label define bpld_lbl 60034 `"Togo"', add
cap label define bpld_lbl 60038 `"Western Africa, ns"', add
cap label define bpld_lbl 60039 `"French West Africa, ns"', add
cap label define bpld_lbl 60040 `"British Indian Ocean Territory"', add
cap label define bpld_lbl 60041 `"Burundi"', add
cap label define bpld_lbl 60042 `"Comoros"', add
cap label define bpld_lbl 60043 `"Djibouti"', add
cap label define bpld_lbl 60044 `"Ethiopia"', add
cap label define bpld_lbl 60045 `"Kenya"', add
cap label define bpld_lbl 60046 `"Madagascar"', add
cap label define bpld_lbl 60047 `"Malawi"', add
cap label define bpld_lbl 60048 `"Mauritius"', add
cap label define bpld_lbl 60049 `"Mozambique"', add
cap label define bpld_lbl 60050 `"Reunion"', add
cap label define bpld_lbl 60051 `"Rwanda"', add
cap label define bpld_lbl 60052 `"Seychelles"', add
cap label define bpld_lbl 60053 `"Somalia"', add
cap label define bpld_lbl 60054 `"Tanzania"', add
cap label define bpld_lbl 60055 `"Uganda"', add
cap label define bpld_lbl 60056 `"Zambia"', add
cap label define bpld_lbl 60057 `"Zimbabwe"', add
cap label define bpld_lbl 60058 `"Bassas de India"', add
cap label define bpld_lbl 60059 `"Europa"', add
cap label define bpld_lbl 60060 `"Gloriosos"', add
cap label define bpld_lbl 60061 `"Juan de Nova"', add
cap label define bpld_lbl 60062 `"Mayotte"', add
cap label define bpld_lbl 60063 `"Tromelin"', add
cap label define bpld_lbl 60064 `"Eastern Africa, nec/ns"', add
cap label define bpld_lbl 60065 `"Eritrea"', add
cap label define bpld_lbl 60066 `"South Sudan"', add
cap label define bpld_lbl 60070 `"Central Africa"', add
cap label define bpld_lbl 60071 `"Angola"', add
cap label define bpld_lbl 60072 `"Cameroon"', add
cap label define bpld_lbl 60073 `"Central African Republic"', add
cap label define bpld_lbl 60074 `"Chad"', add
cap label define bpld_lbl 60075 `"Congo"', add
cap label define bpld_lbl 60076 `"Equatorial Guinea"', add
cap label define bpld_lbl 60077 `"Gabon"', add
cap label define bpld_lbl 60078 `"Sao Tome and Principe"', add
cap label define bpld_lbl 60079 `"Zaire"', add
cap label define bpld_lbl 60080 `"Central Africa, ns"', add
cap label define bpld_lbl 60081 `"Equatorial Africa, ns"', add
cap label define bpld_lbl 60082 `"French Equatorial Africa, ns"', add
cap label define bpld_lbl 60090 `"Southern Africa"', add
cap label define bpld_lbl 60091 `"Botswana"', add
cap label define bpld_lbl 60092 `"Lesotho"', add
cap label define bpld_lbl 60093 `"Namibia"', add
cap label define bpld_lbl 60094 `"South Africa (Union of)"', add
cap label define bpld_lbl 60095 `"Swaziland"', add
cap label define bpld_lbl 60096 `"Southern Africa, ns"', add
cap label define bpld_lbl 60099 `"Africa, ns/nec"', add
cap label define bpld_lbl 70000 `"Australia and New Zealand"', add
cap label define bpld_lbl 70010 `"Australia"', add
cap label define bpld_lbl 70011 `"Ashmore and Cartier Islands"', add
cap label define bpld_lbl 70012 `"Coral Sea Islands Territory"', add
cap label define bpld_lbl 70013 `"Christmas Island"', add
cap label define bpld_lbl 70014 `"Cocos Islands"', add
cap label define bpld_lbl 70020 `"New Zealand"', add
cap label define bpld_lbl 71000 `"Pacific Islands"', add
cap label define bpld_lbl 71010 `"New Caledonia"', add
cap label define bpld_lbl 71012 `"Papua New Guinea"', add
cap label define bpld_lbl 71013 `"Solomon Islands"', add
cap label define bpld_lbl 71014 `"Vanuatu (New Hebrides)"', add
cap label define bpld_lbl 71015 `"Fiji"', add
cap label define bpld_lbl 71016 `"Melanesia, ns"', add
cap label define bpld_lbl 71017 `"Norfolk Islands"', add
cap label define bpld_lbl 71018 `"Niue"', add
cap label define bpld_lbl 71020 `"Cook Islands"', add
cap label define bpld_lbl 71022 `"French Polynesia"', add
cap label define bpld_lbl 71023 `"Tonga"', add
cap label define bpld_lbl 71024 `"Wallis and Futuna Islands"', add
cap label define bpld_lbl 71025 `"Western Samoa"', add
cap label define bpld_lbl 71026 `"Pitcairn Island"', add
cap label define bpld_lbl 71027 `"Tokelau"', add
cap label define bpld_lbl 71028 `"Tuvalu"', add
cap label define bpld_lbl 71029 `"Polynesia, ns"', add
cap label define bpld_lbl 71032 `"Kiribati"', add
cap label define bpld_lbl 71033 `"Canton and Enderbury"', add
cap label define bpld_lbl 71034 `"Nauru"', add
cap label define bpld_lbl 71039 `"Micronesia, ns"', add
cap label define bpld_lbl 71040 `"US Pacific Trust Territories"', add
cap label define bpld_lbl 71041 `"Marshall Islands"', add
cap label define bpld_lbl 71042 `"Micronesia"', add
cap label define bpld_lbl 71043 `"Kosrae"', add
cap label define bpld_lbl 71044 `"Pohnpei"', add
cap label define bpld_lbl 71045 `"Truk"', add
cap label define bpld_lbl 71046 `"Yap"', add
cap label define bpld_lbl 71047 `"Northern Mariana Islands"', add
cap label define bpld_lbl 71048 `"Palau"', add
cap label define bpld_lbl 71049 `"Pacific Trust Terr, ns"', add
cap label define bpld_lbl 71050 `"Clipperton Island"', add
cap label define bpld_lbl 71090 `"Oceania, ns/nec"', add
cap label define bpld_lbl 80000 `"Antarctica, ns/nec"', add
cap label define bpld_lbl 80010 `"Bouvet Islands"', add
cap label define bpld_lbl 80020 `"British Antarctic Terr."', add
cap label define bpld_lbl 80030 `"Dronning Maud Land"', add
cap label define bpld_lbl 80040 `"French Southern and Antarctic Lands"', add
cap label define bpld_lbl 80050 `"Heard and McDonald Islands"', add
cap label define bpld_lbl 90000 `"Abroad (unknown) or at sea"', add
cap label define bpld_lbl 90010 `"Abroad, ns"', add
cap label define bpld_lbl 90011 `"Abroad (US citizen)"', add
cap label define bpld_lbl 90020 `"At sea"', add
cap label define bpld_lbl 90021 `"At sea (US citizen)"', add
cap label define bpld_lbl 90022 `"At sea or abroad (U.S. citizen)"', add
cap label define bpld_lbl 95000 `"Other n.e.c."', add
cap label define bpld_lbl 99900 `"Missing/blank"', add
cap label values bpld bpld_lbl

cap label define ancestr1_lbl 001 `"Alsatian, Alsace-Lorraine"'
cap label define ancestr1_lbl 002 `"Andorran"', add
cap label define ancestr1_lbl 003 `"Austrian"', add
cap label define ancestr1_lbl 004 `"Tirolean"', add
cap label define ancestr1_lbl 005 `"Basque"', add
cap label define ancestr1_lbl 006 `"French Basque"', add
cap label define ancestr1_lbl 008 `"Belgian"', add
cap label define ancestr1_lbl 009 `"Flemish"', add
cap label define ancestr1_lbl 010 `"Walloon"', add
cap label define ancestr1_lbl 011 `"British"', add
cap label define ancestr1_lbl 012 `"British Isles"', add
cap label define ancestr1_lbl 013 `"Channel Islander"', add
cap label define ancestr1_lbl 014 `"Gibraltan"', add
cap label define ancestr1_lbl 015 `"Cornish"', add
cap label define ancestr1_lbl 016 `"Corsican"', add
cap label define ancestr1_lbl 017 `"Cypriot"', add
cap label define ancestr1_lbl 018 `"Greek Cypriote"', add
cap label define ancestr1_lbl 019 `"Turkish Cypriote"', add
cap label define ancestr1_lbl 020 `"Danish"', add
cap label define ancestr1_lbl 021 `"Dutch"', add
cap label define ancestr1_lbl 022 `"English"', add
cap label define ancestr1_lbl 023 `"Faeroe Islander"', add
cap label define ancestr1_lbl 024 `"Finnish"', add
cap label define ancestr1_lbl 025 `"Karelian"', add
cap label define ancestr1_lbl 026 `"French"', add
cap label define ancestr1_lbl 027 `"Lorrainian"', add
cap label define ancestr1_lbl 028 `"Breton"', add
cap label define ancestr1_lbl 029 `"Frisian"', add
cap label define ancestr1_lbl 030 `"Friulian"', add
cap label define ancestr1_lbl 032 `"German"', add
cap label define ancestr1_lbl 033 `"Bavarian"', add
cap label define ancestr1_lbl 034 `"Berliner"', add
cap label define ancestr1_lbl 035 `"Hamburger"', add
cap label define ancestr1_lbl 036 `"Hanoverian"', add
cap label define ancestr1_lbl 037 `"Hessian"', add
cap label define ancestr1_lbl 038 `"Lubecker"', add
cap label define ancestr1_lbl 039 `"Pomeranian"', add
cap label define ancestr1_lbl 040 `"Prussian"', add
cap label define ancestr1_lbl 041 `"Saxon"', add
cap label define ancestr1_lbl 042 `"Sudetenlander"', add
cap label define ancestr1_lbl 043 `"Westphalian"', add
cap label define ancestr1_lbl 046 `"Greek"', add
cap label define ancestr1_lbl 047 `"Cretan"', add
cap label define ancestr1_lbl 048 `"Cycladic Islander, Dodecanese Islander, Peloponnesian"', add
cap label define ancestr1_lbl 049 `"Icelander"', add
cap label define ancestr1_lbl 050 `"Irish, various subheads,"', add
cap label define ancestr1_lbl 051 `"Italian"', add
cap label define ancestr1_lbl 053 `"Abruzzi"', add
cap label define ancestr1_lbl 054 `"Apulian"', add
cap label define ancestr1_lbl 055 `"Basilicata"', add
cap label define ancestr1_lbl 056 `"Calabrian"', add
cap label define ancestr1_lbl 057 `"Amalfin"', add
cap label define ancestr1_lbl 058 `"Emilia Romagna"', add
cap label define ancestr1_lbl 059 `"Rome"', add
cap label define ancestr1_lbl 060 `"Ligurian"', add
cap label define ancestr1_lbl 061 `"Lombardian"', add
cap label define ancestr1_lbl 062 `"Marches"', add
cap label define ancestr1_lbl 063 `"Molise"', add
cap label define ancestr1_lbl 064 `"Neapolitan"', add
cap label define ancestr1_lbl 065 `"Piedmontese"', add
cap label define ancestr1_lbl 066 `"Puglia"', add
cap label define ancestr1_lbl 067 `"Sardinian"', add
cap label define ancestr1_lbl 068 `"Sicilian"', add
cap label define ancestr1_lbl 069 `"Tuscan"', add
cap label define ancestr1_lbl 070 `"Trentino"', add
cap label define ancestr1_lbl 071 `"Umbrian"', add
cap label define ancestr1_lbl 072 `"Valle dAosta"', add
cap label define ancestr1_lbl 073 `"Venetian"', add
cap label define ancestr1_lbl 075 `"Lapp"', add
cap label define ancestr1_lbl 076 `"Liechtensteiner"', add
cap label define ancestr1_lbl 077 `"Luxemburger"', add
cap label define ancestr1_lbl 078 `"Maltese"', add
cap label define ancestr1_lbl 079 `"Manx"', add
cap label define ancestr1_lbl 080 `"Monegasque"', add
cap label define ancestr1_lbl 081 `"Northern Irelander"', add
cap label define ancestr1_lbl 082 `"Norwegian"', add
cap label define ancestr1_lbl 084 `"Portuguese"', add
cap label define ancestr1_lbl 085 `"Azorean"', add
cap label define ancestr1_lbl 086 `"Madeiran"', add
cap label define ancestr1_lbl 087 `"Scotch Irish"', add
cap label define ancestr1_lbl 088 `"Scottish"', add
cap label define ancestr1_lbl 089 `"Swedish"', add
cap label define ancestr1_lbl 090 `"Aland Islander"', add
cap label define ancestr1_lbl 091 `"Swiss"', add
cap label define ancestr1_lbl 092 `"Suisse"', add
cap label define ancestr1_lbl 095 `"Romansch"', add
cap label define ancestr1_lbl 096 `"Suisse Romane"', add
cap label define ancestr1_lbl 097 `"Welsh"', add
cap label define ancestr1_lbl 098 `"Scandinavian, Nordic"', add
cap label define ancestr1_lbl 100 `"Albanian"', add
cap label define ancestr1_lbl 101 `"Azerbaijani"', add
cap label define ancestr1_lbl 102 `"Belorussian"', add
cap label define ancestr1_lbl 103 `"Bulgarian"', add
cap label define ancestr1_lbl 105 `"Carpathian"', add
cap label define ancestr1_lbl 108 `"Cossack"', add
cap label define ancestr1_lbl 109 `"Croatian"', add
cap label define ancestr1_lbl 111 `"Czechoslovakian"', add
cap label define ancestr1_lbl 112 `"Bohemian"', add
cap label define ancestr1_lbl 115 `"Estonian"', add
cap label define ancestr1_lbl 116 `"Livonian"', add
cap label define ancestr1_lbl 117 `"Finno Ugrian"', add
cap label define ancestr1_lbl 118 `"Mordovian"', add
cap label define ancestr1_lbl 119 `"Voytak"', add
cap label define ancestr1_lbl 120 `"Georgian"', add
cap label define ancestr1_lbl 122 `"Germans from Russia"', add
cap label define ancestr1_lbl 123 `"Gruziia"', add
cap label define ancestr1_lbl 124 `"Rom"', add
cap label define ancestr1_lbl 125 `"Hungarian"', add
cap label define ancestr1_lbl 126 `"Magyar"', add
cap label define ancestr1_lbl 128 `"Latvian"', add
cap label define ancestr1_lbl 129 `"Lithuanian"', add
cap label define ancestr1_lbl 130 `"Macedonian"', add
cap label define ancestr1_lbl 132 `"North Caucasian"', add
cap label define ancestr1_lbl 133 `"North Caucasian Turkic"', add
cap label define ancestr1_lbl 140 `"Ossetian"', add
cap label define ancestr1_lbl 142 `"Polish"', add
cap label define ancestr1_lbl 143 `"Kashubian"', add
cap label define ancestr1_lbl 144 `"Romanian"', add
cap label define ancestr1_lbl 145 `"Bessarabian"', add
cap label define ancestr1_lbl 146 `"Moldavian"', add
cap label define ancestr1_lbl 147 `"Wallachian"', add
cap label define ancestr1_lbl 148 `"Russian"', add
cap label define ancestr1_lbl 150 `"Muscovite"', add
cap label define ancestr1_lbl 152 `"Serbian"', add
cap label define ancestr1_lbl 153 `"Slovak"', add
cap label define ancestr1_lbl 154 `"Slovene"', add
cap label define ancestr1_lbl 155 `"Sorb/Wend"', add
cap label define ancestr1_lbl 156 `"Soviet Turkic"', add
cap label define ancestr1_lbl 157 `"Bashkir"', add
cap label define ancestr1_lbl 158 `"Chevash"', add
cap label define ancestr1_lbl 159 `"Gagauz"', add
cap label define ancestr1_lbl 160 `"Mesknetian"', add
cap label define ancestr1_lbl 163 `"Yakut"', add
cap label define ancestr1_lbl 164 `"Soviet Union, nec"', add
cap label define ancestr1_lbl 165 `"Tatar"', add
cap label define ancestr1_lbl 169 `"Uzbek"', add
cap label define ancestr1_lbl 171 `"Ukrainian"', add
cap label define ancestr1_lbl 176 `"Yugoslavian"', add
cap label define ancestr1_lbl 178 `"Slav"', add
cap label define ancestr1_lbl 179 `"Slavonian"', add
cap label define ancestr1_lbl 181 `"Central European, nec"', add
cap label define ancestr1_lbl 183 `"Northern European, nec"', add
cap label define ancestr1_lbl 185 `"Southern European, nec"', add
cap label define ancestr1_lbl 187 `"Western European, nec"', add
cap label define ancestr1_lbl 190 `"Eastern European, nec"', add
cap label define ancestr1_lbl 195 `"European, nec"', add
cap label define ancestr1_lbl 200 `"Spaniard"', add
cap label define ancestr1_lbl 201 `"Andalusian"', add
cap label define ancestr1_lbl 202 `"Astorian"', add
cap label define ancestr1_lbl 204 `"Catalonian"', add
cap label define ancestr1_lbl 205 `"Balearic Islander"', add
cap label define ancestr1_lbl 206 `"Galician"', add
cap label define ancestr1_lbl 210 `"Mexican"', add
cap label define ancestr1_lbl 211 `"Mexican American"', add
cap label define ancestr1_lbl 213 `"Chicano/Chicana"', add
cap label define ancestr1_lbl 218 `"Nuevo Mexicano"', add
cap label define ancestr1_lbl 219 `"Californio"', add
cap label define ancestr1_lbl 221 `"Costa Rican"', add
cap label define ancestr1_lbl 222 `"Guatemalan"', add
cap label define ancestr1_lbl 223 `"Honduran"', add
cap label define ancestr1_lbl 224 `"Nicaraguan"', add
cap label define ancestr1_lbl 225 `"Panamanian"', add
cap label define ancestr1_lbl 226 `"Salvadoran"', add
cap label define ancestr1_lbl 227 `"Latin American"', add
cap label define ancestr1_lbl 231 `"Argentinean"', add
cap label define ancestr1_lbl 232 `"Bolivian"', add
cap label define ancestr1_lbl 233 `"Chilean"', add
cap label define ancestr1_lbl 234 `"Colombian"', add
cap label define ancestr1_lbl 235 `"Ecuadorian"', add
cap label define ancestr1_lbl 236 `"Paraguayan"', add
cap label define ancestr1_lbl 237 `"Peruvian"', add
cap label define ancestr1_lbl 238 `"Uruguayan"', add
cap label define ancestr1_lbl 239 `"Venezuelan"', add
cap label define ancestr1_lbl 248 `"South American"', add
cap label define ancestr1_lbl 261 `"Puerto Rican"', add
cap label define ancestr1_lbl 271 `"Cuban"', add
cap label define ancestr1_lbl 275 `"Dominican"', add
cap label define ancestr1_lbl 290 `"Hispanic"', add
cap label define ancestr1_lbl 291 `"Spanish"', add
cap label define ancestr1_lbl 295 `"Spanish American"', add
cap label define ancestr1_lbl 296 `"Other Spanish/Hispanic"', add
cap label define ancestr1_lbl 300 `"Bahamian"', add
cap label define ancestr1_lbl 301 `"Barbadian"', add
cap label define ancestr1_lbl 302 `"Belizean"', add
cap label define ancestr1_lbl 303 `"Bermudan"', add
cap label define ancestr1_lbl 304 `"Cayman Islander"', add
cap label define ancestr1_lbl 308 `"Jamaican"', add
cap label define ancestr1_lbl 310 `"Dutch West Indies"', add
cap label define ancestr1_lbl 311 `"Aruba Islander"', add
cap label define ancestr1_lbl 312 `"St Maarten Islander"', add
cap label define ancestr1_lbl 314 `"Trinidadian/Tobagonian"', add
cap label define ancestr1_lbl 315 `"Trinidadian"', add
cap label define ancestr1_lbl 316 `"Tobagonian"', add
cap label define ancestr1_lbl 317 `"U.S. Virgin Islander"', add
cap label define ancestr1_lbl 321 `"British Virgin Islander"', add
cap label define ancestr1_lbl 322 `"British West Indian"', add
cap label define ancestr1_lbl 323 `"Turks and Caicos Islander"', add
cap label define ancestr1_lbl 324 `"Anguilla Islander"', add
cap label define ancestr1_lbl 328 `"Dominica Islander"', add
cap label define ancestr1_lbl 329 `"Grenadian"', add
cap label define ancestr1_lbl 331 `"St Lucia Islander"', add
cap label define ancestr1_lbl 332 `"French West Indies"', add
cap label define ancestr1_lbl 333 `"Guadeloupe Islander"', add
cap label define ancestr1_lbl 334 `"Cayenne"', add
cap label define ancestr1_lbl 335 `"West Indian"', add
cap label define ancestr1_lbl 336 `"Haitian"', add
cap label define ancestr1_lbl 337 `"Other West Indian"', add
cap label define ancestr1_lbl 360 `"Brazilian"', add
cap label define ancestr1_lbl 365 `"San Andres"', add
cap label define ancestr1_lbl 370 `"Guyanese/British Guiana"', add
cap label define ancestr1_lbl 375 `"Providencia"', add
cap label define ancestr1_lbl 380 `"Surinam/Dutch Guiana"', add
cap label define ancestr1_lbl 400 `"Algerian"', add
cap label define ancestr1_lbl 402 `"Egyptian"', add
cap label define ancestr1_lbl 404 `"Libyan"', add
cap label define ancestr1_lbl 406 `"Moroccan"', add
cap label define ancestr1_lbl 407 `"Ifni"', add
cap label define ancestr1_lbl 408 `"Tunisian"', add
cap label define ancestr1_lbl 411 `"North African"', add
cap label define ancestr1_lbl 412 `"Alhucemas"', add
cap label define ancestr1_lbl 413 `"Berber"', add
cap label define ancestr1_lbl 414 `"Rio de Oro"', add
cap label define ancestr1_lbl 415 `"Bahraini"', add
cap label define ancestr1_lbl 416 `"Iranian"', add
cap label define ancestr1_lbl 417 `"Iraqi"', add
cap label define ancestr1_lbl 419 `"Israeli"', add
cap label define ancestr1_lbl 421 `"Jordanian"', add
cap label define ancestr1_lbl 423 `"Kuwaiti"', add
cap label define ancestr1_lbl 425 `"Lebanese"', add
cap label define ancestr1_lbl 427 `"Saudi Arabian"', add
cap label define ancestr1_lbl 429 `"Syrian"', add
cap label define ancestr1_lbl 431 `"Armenian"', add
cap label define ancestr1_lbl 434 `"Turkish"', add
cap label define ancestr1_lbl 435 `"Yemeni"', add
cap label define ancestr1_lbl 436 `"Omani"', add
cap label define ancestr1_lbl 437 `"Muscat"', add
cap label define ancestr1_lbl 438 `"Trucial Oman"', add
cap label define ancestr1_lbl 439 `"Qatar"', add
cap label define ancestr1_lbl 442 `"Kurdish"', add
cap label define ancestr1_lbl 444 `"Kuria Muria Islander"', add
cap label define ancestr1_lbl 465 `"Palestinian"', add
cap label define ancestr1_lbl 466 `"Gazan"', add
cap label define ancestr1_lbl 467 `"West Bank"', add
cap label define ancestr1_lbl 470 `"South Yemeni"', add
cap label define ancestr1_lbl 471 `"Aden"', add
cap label define ancestr1_lbl 480 `"United Arab Emirates"', add
cap label define ancestr1_lbl 482 `"Assyrian/Chaldean/Syriac"', add
cap label define ancestr1_lbl 490 `"Middle Eastern"', add
cap label define ancestr1_lbl 495 `"Arab"', add
cap label define ancestr1_lbl 496 `"Other Arab"', add
cap label define ancestr1_lbl 500 `"Angolan"', add
cap label define ancestr1_lbl 502 `"Benin"', add
cap label define ancestr1_lbl 504 `"Botswana"', add
cap label define ancestr1_lbl 506 `"Burundian"', add
cap label define ancestr1_lbl 508 `"Cameroonian"', add
cap label define ancestr1_lbl 510 `"Cape Verdean"', add
cap label define ancestr1_lbl 513 `"Chadian"', add
cap label define ancestr1_lbl 515 `"Congolese"', add
cap label define ancestr1_lbl 516 `"Congo-Brazzaville"', add
cap label define ancestr1_lbl 519 `"Djibouti"', add
cap label define ancestr1_lbl 520 `"Equatorial Guinea"', add
cap label define ancestr1_lbl 522 `"Ethiopian"', add
cap label define ancestr1_lbl 523 `"Eritrean"', add
cap label define ancestr1_lbl 525 `"Gabonese"', add
cap label define ancestr1_lbl 527 `"Gambian"', add
cap label define ancestr1_lbl 529 `"Ghanian"', add
cap label define ancestr1_lbl 530 `"Guinean"', add
cap label define ancestr1_lbl 531 `"Guinea Bissau"', add
cap label define ancestr1_lbl 532 `"Ivory Coast"', add
cap label define ancestr1_lbl 534 `"Kenyan"', add
cap label define ancestr1_lbl 538 `"Lesotho"', add
cap label define ancestr1_lbl 541 `"Liberian"', add
cap label define ancestr1_lbl 543 `"Madagascan"', add
cap label define ancestr1_lbl 545 `"Malawian"', add
cap label define ancestr1_lbl 546 `"Malian"', add
cap label define ancestr1_lbl 549 `"Mozambican"', add
cap label define ancestr1_lbl 550 `"Namibian"', add
cap label define ancestr1_lbl 551 `"Niger"', add
cap label define ancestr1_lbl 553 `"Nigerian"', add
cap label define ancestr1_lbl 554 `"Fulani"', add
cap label define ancestr1_lbl 555 `"Hausa"', add
cap label define ancestr1_lbl 556 `"Ibo"', add
cap label define ancestr1_lbl 557 `"Tiv"', add
cap label define ancestr1_lbl 561 `"Rwandan"', add
cap label define ancestr1_lbl 564 `"Senegalese"', add
cap label define ancestr1_lbl 566 `"Sierra Leonean"', add
cap label define ancestr1_lbl 568 `"Somalian"', add
cap label define ancestr1_lbl 569 `"Swaziland"', add
cap label define ancestr1_lbl 570 `"South African"', add
cap label define ancestr1_lbl 571 `"Union of South Africa"', add
cap label define ancestr1_lbl 572 `"Afrikaner"', add
cap label define ancestr1_lbl 573 `"Natalian"', add
cap label define ancestr1_lbl 574 `"Zulu"', add
cap label define ancestr1_lbl 576 `"Sudanese"', add
cap label define ancestr1_lbl 577 `"Dinka"', add
cap label define ancestr1_lbl 578 `"Nuer"', add
cap label define ancestr1_lbl 579 `"Fur"', add
cap label define ancestr1_lbl 582 `"Tanzanian"', add
cap label define ancestr1_lbl 583 `"Tanganyikan"', add
cap label define ancestr1_lbl 584 `"Zanzibar Islande"', add
cap label define ancestr1_lbl 586 `"Togo"', add
cap label define ancestr1_lbl 588 `"Ugandan"', add
cap label define ancestr1_lbl 589 `"Upper Voltan"', add
cap label define ancestr1_lbl 591 `"Zairian"', add
cap label define ancestr1_lbl 592 `"Zambian"', add
cap label define ancestr1_lbl 593 `"Zimbabwean"', add
cap label define ancestr1_lbl 594 `"African Islands"', add
cap label define ancestr1_lbl 595 `"Other Subsaharan Africa"', add
cap label define ancestr1_lbl 596 `"Central African"', add
cap label define ancestr1_lbl 597 `"East African"', add
cap label define ancestr1_lbl 598 `"West African"', add
cap label define ancestr1_lbl 599 `"African"', add
cap label define ancestr1_lbl 600 `"Afghan"', add
cap label define ancestr1_lbl 601 `"Baluchi"', add
cap label define ancestr1_lbl 602 `"Pathan"', add
cap label define ancestr1_lbl 603 `"Bengali"', add
cap label define ancestr1_lbl 607 `"Bhutanese"', add
cap label define ancestr1_lbl 609 `"Nepali"', add
cap label define ancestr1_lbl 615 `"Asian Indian"', add
cap label define ancestr1_lbl 622 `"Andaman Islander"', add
cap label define ancestr1_lbl 624 `"Andhra Pradesh"', add
cap label define ancestr1_lbl 626 `"Assamese"', add
cap label define ancestr1_lbl 628 `"Goanese"', add
cap label define ancestr1_lbl 630 `"Gujarati"', add
cap label define ancestr1_lbl 632 `"Karnatakan"', add
cap label define ancestr1_lbl 634 `"Keralan"', add
cap label define ancestr1_lbl 638 `"Maharashtran"', add
cap label define ancestr1_lbl 640 `"Madrasi"', add
cap label define ancestr1_lbl 642 `"Mysore"', add
cap label define ancestr1_lbl 644 `"Naga"', add
cap label define ancestr1_lbl 648 `"Pondicherry"', add
cap label define ancestr1_lbl 650 `"Punjabi"', add
cap label define ancestr1_lbl 656 `"Tamil"', add
cap label define ancestr1_lbl 675 `"East Indies"', add
cap label define ancestr1_lbl 680 `"Pakistani"', add
cap label define ancestr1_lbl 690 `"Sri Lankan"', add
cap label define ancestr1_lbl 691 `"Singhalese"', add
cap label define ancestr1_lbl 692 `"Veddah"', add
cap label define ancestr1_lbl 695 `"Maldivian"', add
cap label define ancestr1_lbl 700 `"Burmese"', add
cap label define ancestr1_lbl 702 `"Shan"', add
cap label define ancestr1_lbl 703 `"Cambodian"', add
cap label define ancestr1_lbl 704 `"Khmer"', add
cap label define ancestr1_lbl 706 `"Chinese"', add
cap label define ancestr1_lbl 707 `"Cantonese"', add
cap label define ancestr1_lbl 708 `"Manchurian"', add
cap label define ancestr1_lbl 709 `"Mandarin"', add
cap label define ancestr1_lbl 712 `"Mongolian"', add
cap label define ancestr1_lbl 714 `"Tibetan"', add
cap label define ancestr1_lbl 716 `"Hong Kong"', add
cap label define ancestr1_lbl 718 `"Macao"', add
cap label define ancestr1_lbl 720 `"Filipino"', add
cap label define ancestr1_lbl 730 `"Indonesian"', add
cap label define ancestr1_lbl 740 `"Japanese"', add
cap label define ancestr1_lbl 746 `"Ryukyu Islander"', add
cap label define ancestr1_lbl 748 `"Okinawan"', add
cap label define ancestr1_lbl 750 `"Korean"', add
cap label define ancestr1_lbl 765 `"Laotian"', add
cap label define ancestr1_lbl 766 `"Meo"', add
cap label define ancestr1_lbl 768 `"Hmong"', add
cap label define ancestr1_lbl 770 `"Malaysian"', add
cap label define ancestr1_lbl 774 `"Singaporean"', add
cap label define ancestr1_lbl 776 `"Thai"', add
cap label define ancestr1_lbl 777 `"Black Thai"', add
cap label define ancestr1_lbl 778 `"Western Lao"', add
cap label define ancestr1_lbl 782 `"Taiwanese"', add
cap label define ancestr1_lbl 785 `"Vietnamese"', add
cap label define ancestr1_lbl 786 `"Katu"', add
cap label define ancestr1_lbl 787 `"Ma"', add
cap label define ancestr1_lbl 788 `"Mnong"', add
cap label define ancestr1_lbl 790 `"Montagnard"', add
cap label define ancestr1_lbl 792 `"Indochinese"', add
cap label define ancestr1_lbl 793 `"Eurasian"', add
cap label define ancestr1_lbl 795 `"Asian"', add
cap label define ancestr1_lbl 796 `"Other Asian"', add
cap label define ancestr1_lbl 800 `"Australian"', add
cap label define ancestr1_lbl 801 `"Tasmanian"', add
cap label define ancestr1_lbl 802 `"Australian Aborigine"', add
cap label define ancestr1_lbl 803 `"New Zealander"', add
cap label define ancestr1_lbl 808 `"Polynesian"', add
cap label define ancestr1_lbl 810 `"Maori"', add
cap label define ancestr1_lbl 811 `"Hawaiian"', add
cap label define ancestr1_lbl 813 `"Part Hawaiian"', add
cap label define ancestr1_lbl 814 `"Samoan"', add
cap label define ancestr1_lbl 815 `"Tongan"', add
cap label define ancestr1_lbl 816 `"Tokelauan"', add
cap label define ancestr1_lbl 817 `"Cook Islander"', add
cap label define ancestr1_lbl 818 `"Tahitian"', add
cap label define ancestr1_lbl 819 `"Niuean"', add
cap label define ancestr1_lbl 820 `"Micronesian"', add
cap label define ancestr1_lbl 821 `"Guamanian"', add
cap label define ancestr1_lbl 822 `"Chamorro Islander"', add
cap label define ancestr1_lbl 823 `"Saipanese"', add
cap label define ancestr1_lbl 824 `"Palauan"', add
cap label define ancestr1_lbl 825 `"Marshall Islander"', add
cap label define ancestr1_lbl 826 `"Kosraean"', add
cap label define ancestr1_lbl 827 `"Ponapean"', add
cap label define ancestr1_lbl 828 `"Chuukese"', add
cap label define ancestr1_lbl 829 `"Yap Islander"', add
cap label define ancestr1_lbl 830 `"Caroline Islander"', add
cap label define ancestr1_lbl 831 `"Kiribatese"', add
cap label define ancestr1_lbl 832 `"Nauruan"', add
cap label define ancestr1_lbl 833 `"Tarawa Islander"', add
cap label define ancestr1_lbl 834 `"Tinian Islander"', add
cap label define ancestr1_lbl 840 `"Melanesian Islander"', add
cap label define ancestr1_lbl 841 `"Fijian"', add
cap label define ancestr1_lbl 843 `"New Guinean"', add
cap label define ancestr1_lbl 844 `"Papuan"', add
cap label define ancestr1_lbl 845 `"Solomon Islander"', add
cap label define ancestr1_lbl 846 `"New Caledonian Islander"', add
cap label define ancestr1_lbl 847 `"Vanuatuan"', add
cap label define ancestr1_lbl 850 `"Pacific Islander"', add
cap label define ancestr1_lbl 860 `"Oceania"', add
cap label define ancestr1_lbl 862 `"Chamolinian"', add
cap label define ancestr1_lbl 863 `"Reserved Codes"', add
cap label define ancestr1_lbl 870 `"Other Pacific"', add
cap label define ancestr1_lbl 900 `"Afro-American"', add
cap label define ancestr1_lbl 902 `"African-American"', add
cap label define ancestr1_lbl 913 `"Central American Indian"', add
cap label define ancestr1_lbl 914 `"South American Indian"', add
cap label define ancestr1_lbl 920 `"American Indian  (all tribes)"', add
cap label define ancestr1_lbl 921 `"Aleut"', add
cap label define ancestr1_lbl 922 `"Eskimo"', add
cap label define ancestr1_lbl 923 `"Inuit"', add
cap label define ancestr1_lbl 924 `"White/Caucasian"', add
cap label define ancestr1_lbl 930 `"Greenlander"', add
cap label define ancestr1_lbl 931 `"Canadian"', add
cap label define ancestr1_lbl 933 `"Newfoundland"', add
cap label define ancestr1_lbl 934 `"Nova Scotian"', add
cap label define ancestr1_lbl 935 `"French Canadian"', add
cap label define ancestr1_lbl 936 `"Acadian"', add
cap label define ancestr1_lbl 939 `"American"', add
cap label define ancestr1_lbl 940 `"United States"', add
cap label define ancestr1_lbl 941 `"Alabama"', add
cap label define ancestr1_lbl 942 `"Alaska"', add
cap label define ancestr1_lbl 943 `"Arizona"', add
cap label define ancestr1_lbl 944 `"Arkansas"', add
cap label define ancestr1_lbl 945 `"California"', add
cap label define ancestr1_lbl 946 `"Colorado"', add
cap label define ancestr1_lbl 947 `"Connecticut"', add
cap label define ancestr1_lbl 948 `"District of Columbia"', add
cap label define ancestr1_lbl 949 `"Delaware"', add
cap label define ancestr1_lbl 950 `"Florida"', add
cap label define ancestr1_lbl 951 `"Georgia"', add
cap label define ancestr1_lbl 952 `"Idaho"', add
cap label define ancestr1_lbl 953 `"Illinois"', add
cap label define ancestr1_lbl 954 `"Indiana"', add
cap label define ancestr1_lbl 955 `"Iowa"', add
cap label define ancestr1_lbl 956 `"Kansas"', add
cap label define ancestr1_lbl 957 `"Kentucky"', add
cap label define ancestr1_lbl 958 `"Louisiana"', add
cap label define ancestr1_lbl 959 `"Maine"', add
cap label define ancestr1_lbl 960 `"Maryland"', add
cap label define ancestr1_lbl 961 `"Massachusetts"', add
cap label define ancestr1_lbl 962 `"Michigan"', add
cap label define ancestr1_lbl 963 `"Minnesota"', add
cap label define ancestr1_lbl 964 `"Mississippi"', add
cap label define ancestr1_lbl 965 `"Missouri"', add
cap label define ancestr1_lbl 966 `"Montana"', add
cap label define ancestr1_lbl 967 `"Nebraska"', add
cap label define ancestr1_lbl 968 `"Nevada"', add
cap label define ancestr1_lbl 969 `"New Hampshire"', add
cap label define ancestr1_lbl 970 `"New Jersey"', add
cap label define ancestr1_lbl 971 `"New Mexico"', add
cap label define ancestr1_lbl 972 `"New York"', add
cap label define ancestr1_lbl 973 `"North Carolina"', add
cap label define ancestr1_lbl 974 `"North Dakota"', add
cap label define ancestr1_lbl 975 `"Ohio"', add
cap label define ancestr1_lbl 976 `"Oklahoma"', add
cap label define ancestr1_lbl 977 `"Oregon"', add
cap label define ancestr1_lbl 978 `"Pennsylvania"', add
cap label define ancestr1_lbl 979 `"Rhode Island"', add
cap label define ancestr1_lbl 980 `"South Carolina"', add
cap label define ancestr1_lbl 981 `"South Dakota"', add
cap label define ancestr1_lbl 982 `"Tennessee"', add
cap label define ancestr1_lbl 983 `"Texas"', add
cap label define ancestr1_lbl 984 `"Utah"', add
cap label define ancestr1_lbl 985 `"Vermont"', add
cap label define ancestr1_lbl 986 `"Virginia"', add
cap label define ancestr1_lbl 987 `"Washington"', add
cap label define ancestr1_lbl 988 `"West Virginia"', add
cap label define ancestr1_lbl 989 `"Wisconsin"', add
cap label define ancestr1_lbl 990 `"Wyoming"', add
cap label define ancestr1_lbl 993 `"Southerner"', add
cap label define ancestr1_lbl 994 `"North American"', add
cap label define ancestr1_lbl 995 `"Mixture"', add
cap label define ancestr1_lbl 996 `"Uncodable"', add
cap label define ancestr1_lbl 998 `"Other"', add
cap label define ancestr1_lbl 999 `"Not Reported"', add
cap label values ancestr1 ancestr1_lbl

cap label define ancestr1d_lbl 0010 `"Alsatian"'
cap label define ancestr1d_lbl 0020 `"Andorran"', add
cap label define ancestr1d_lbl 0030 `"Austrian"', add
cap label define ancestr1d_lbl 0040 `"Tirolean"', add
cap label define ancestr1d_lbl 0051 `"Basque (1980)"', add
cap label define ancestr1d_lbl 0052 `"Spanish Basque (1980)"', add
cap label define ancestr1d_lbl 0053 `"Basque (1990-2000)"', add
cap label define ancestr1d_lbl 0054 `"Spanish Basque (1990-2000, 2001-2004 ACS)"', add
cap label define ancestr1d_lbl 0060 `"French Basque"', add
cap label define ancestr1d_lbl 0080 `"Belgian"', add
cap label define ancestr1d_lbl 0090 `"Flemish"', add
cap label define ancestr1d_lbl 0100 `"Walloon"', add
cap label define ancestr1d_lbl 0110 `"British"', add
cap label define ancestr1d_lbl 0120 `"British Isles"', add
cap label define ancestr1d_lbl 0130 `"Channel Islander"', add
cap label define ancestr1d_lbl 0140 `"Gibraltan"', add
cap label define ancestr1d_lbl 0150 `"Cornish"', add
cap label define ancestr1d_lbl 0160 `"Corsican"', add
cap label define ancestr1d_lbl 0170 `"Cypriot"', add
cap label define ancestr1d_lbl 0180 `"Greek Cypriote"', add
cap label define ancestr1d_lbl 0190 `"Turkish Cypriote"', add
cap label define ancestr1d_lbl 0200 `"Danish"', add
cap label define ancestr1d_lbl 0210 `"Dutch"', add
cap label define ancestr1d_lbl 0211 `"Dutch-French-Irish"', add
cap label define ancestr1d_lbl 0212 `"Dutch-German-Irish"', add
cap label define ancestr1d_lbl 0213 `"Dutch-Irish-Scotch"', add
cap label define ancestr1d_lbl 0220 `"English"', add
cap label define ancestr1d_lbl 0221 `"English-French-German"', add
cap label define ancestr1d_lbl 0222 `"English-French-Irish"', add
cap label define ancestr1d_lbl 0223 `"English-German-Irish"', add
cap label define ancestr1d_lbl 0224 `"English-German-Swedish"', add
cap label define ancestr1d_lbl 0225 `"English-Irish-Scotch"', add
cap label define ancestr1d_lbl 0226 `"English-Scotch-Welsh"', add
cap label define ancestr1d_lbl 0230 `"Faeroe Islander"', add
cap label define ancestr1d_lbl 0240 `"Finnish"', add
cap label define ancestr1d_lbl 0250 `"Karelian"', add
cap label define ancestr1d_lbl 0260 `"French (1980)"', add
cap label define ancestr1d_lbl 0261 `"French (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 0262 `"Occitan (1990-2000)"', add
cap label define ancestr1d_lbl 0270 `"Lorrainian"', add
cap label define ancestr1d_lbl 0280 `"Breton"', add
cap label define ancestr1d_lbl 0290 `"Frisian"', add
cap label define ancestr1d_lbl 0300 `"Friulian"', add
cap label define ancestr1d_lbl 0320 `"German (1980)"', add
cap label define ancestr1d_lbl 0321 `"German (1990-2000, ACS/PRCS)"', add
cap label define ancestr1d_lbl 0322 `"Pennsylvania German (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 0323 `"East German (1990-2000)"', add
cap label define ancestr1d_lbl 0324 `"West German (2000)"', add
cap label define ancestr1d_lbl 0325 `"German-French-Irish"', add
cap label define ancestr1d_lbl 0326 `"German-Irish-Italian"', add
cap label define ancestr1d_lbl 0327 `"German-Irish-Scotch"', add
cap label define ancestr1d_lbl 0328 `"German-Irish-Swedish"', add
cap label define ancestr1d_lbl 0329 `"Germanic"', add
cap label define ancestr1d_lbl 0330 `"Bavarian"', add
cap label define ancestr1d_lbl 0340 `"Berliner"', add
cap label define ancestr1d_lbl 0350 `"Hamburger"', add
cap label define ancestr1d_lbl 0360 `"Hanoverian"', add
cap label define ancestr1d_lbl 0370 `"Hessian"', add
cap label define ancestr1d_lbl 0380 `"Lubecker"', add
cap label define ancestr1d_lbl 0390 `"Pomeranian (1980)"', add
cap label define ancestr1d_lbl 0391 `"Pomeranian (1990-2000)"', add
cap label define ancestr1d_lbl 0392 `"Silesian (1990-2000)"', add
cap label define ancestr1d_lbl 0400 `"Prussian"', add
cap label define ancestr1d_lbl 0410 `"Saxon"', add
cap label define ancestr1d_lbl 0420 `"Sudetenlander"', add
cap label define ancestr1d_lbl 0430 `"Westphalian"', add
cap label define ancestr1d_lbl 0460 `"Greek"', add
cap label define ancestr1d_lbl 0470 `"Cretan"', add
cap label define ancestr1d_lbl 0480 `"Cycladic Islander"', add
cap label define ancestr1d_lbl 0490 `"Icelander"', add
cap label define ancestr1d_lbl 0500 `"Irish"', add
cap label define ancestr1d_lbl 0501 `"Celtic"', add
cap label define ancestr1d_lbl 0502 `"Irish Scotch"', add
cap label define ancestr1d_lbl 0510 `"Italian (1980)"', add
cap label define ancestr1d_lbl 0511 `"Italian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 0512 `"Trieste (1990-2000)"', add
cap label define ancestr1d_lbl 0513 `"San Marino (1990-2000)"', add
cap label define ancestr1d_lbl 0530 `"Abruzzi"', add
cap label define ancestr1d_lbl 0540 `"Apulian"', add
cap label define ancestr1d_lbl 0550 `"Basilicata"', add
cap label define ancestr1d_lbl 0560 `"Calabrian"', add
cap label define ancestr1d_lbl 0570 `"Amalfi"', add
cap label define ancestr1d_lbl 0580 `"Emilia Romagna"', add
cap label define ancestr1d_lbl 0590 `"Rome"', add
cap label define ancestr1d_lbl 0600 `"Ligurian"', add
cap label define ancestr1d_lbl 0610 `"Lombardian"', add
cap label define ancestr1d_lbl 0620 `"Marches"', add
cap label define ancestr1d_lbl 0630 `"Molise"', add
cap label define ancestr1d_lbl 0640 `"Neapolitan"', add
cap label define ancestr1d_lbl 0650 `"Piedmontese"', add
cap label define ancestr1d_lbl 0660 `"Puglia"', add
cap label define ancestr1d_lbl 0670 `"Sardinian"', add
cap label define ancestr1d_lbl 0680 `"Sicilian"', add
cap label define ancestr1d_lbl 0690 `"Tuscan"', add
cap label define ancestr1d_lbl 0700 `"Trentino"', add
cap label define ancestr1d_lbl 0710 `"Umbrian"', add
cap label define ancestr1d_lbl 0720 `"Valle dAosta"', add
cap label define ancestr1d_lbl 0730 `"Venetian"', add
cap label define ancestr1d_lbl 0750 `"Lapp"', add
cap label define ancestr1d_lbl 0760 `"Liechtensteiner"', add
cap label define ancestr1d_lbl 0770 `"Luxemburger"', add
cap label define ancestr1d_lbl 0780 `"Maltese"', add
cap label define ancestr1d_lbl 0790 `"Manx"', add
cap label define ancestr1d_lbl 0800 `"Monegasque"', add
cap label define ancestr1d_lbl 0810 `"Northern Irelander"', add
cap label define ancestr1d_lbl 0820 `"Norwegian"', add
cap label define ancestr1d_lbl 0840 `"Portuguese"', add
cap label define ancestr1d_lbl 0850 `"Azorean"', add
cap label define ancestr1d_lbl 0860 `"Madeiran"', add
cap label define ancestr1d_lbl 0870 `"Scotch Irish"', add
cap label define ancestr1d_lbl 0880 `"Scottish"', add
cap label define ancestr1d_lbl 0890 `"Swedish"', add
cap label define ancestr1d_lbl 0900 `"Aland Islander"', add
cap label define ancestr1d_lbl 0910 `"Swiss"', add
cap label define ancestr1d_lbl 0920 `"Suisse (1980)"', add
cap label define ancestr1d_lbl 0921 `"Suisse (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 0922 `"Switzer (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 0950 `"Romansch (1980, ACS)"', add
cap label define ancestr1d_lbl 0951 `"Romanscho (1990-2000)"', add
cap label define ancestr1d_lbl 0952 `"Ladin (1990-2000)"', add
cap label define ancestr1d_lbl 0960 `"Suisse Romane (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 0961 `"Suisse Romane (1980)"', add
cap label define ancestr1d_lbl 0962 `"Ticino"', add
cap label define ancestr1d_lbl 0970 `"Welsh"', add
cap label define ancestr1d_lbl 0980 `"Scandinavian, Nordic"', add
cap label define ancestr1d_lbl 1000 `"Albanian"', add
cap label define ancestr1d_lbl 1010 `"Azerbaijani"', add
cap label define ancestr1d_lbl 1020 `"Belorussian"', add
cap label define ancestr1d_lbl 1030 `"Bulgarian"', add
cap label define ancestr1d_lbl 1050 `"Carpathian"', add
cap label define ancestr1d_lbl 1051 `"Carpatho Rusyn"', add
cap label define ancestr1d_lbl 1052 `"Rusyn"', add
cap label define ancestr1d_lbl 1080 `"Cossack (1990-2000)"', add
cap label define ancestr1d_lbl 1081 `"Cossack (1980)"', add
cap label define ancestr1d_lbl 1082 `"Turkestani (1990-2000, 2012 ACS)"', add
cap label define ancestr1d_lbl 1083 `"Kirghiz (1980)"', add
cap label define ancestr1d_lbl 1084 `"Turcoman (1980)"', add
cap label define ancestr1d_lbl 1090 `"Croatian"', add
cap label define ancestr1d_lbl 1110 `"Czechoslovakian"', add
cap label define ancestr1d_lbl 1111 `"Czech"', add
cap label define ancestr1d_lbl 1120 `"Bohemian"', add
cap label define ancestr1d_lbl 1121 `"Bohemian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 1122 `"Moravian (1990-2000)"', add
cap label define ancestr1d_lbl 1150 `"Estonian"', add
cap label define ancestr1d_lbl 1160 `"Livonian"', add
cap label define ancestr1d_lbl 1170 `"Finno Ugrian (1990-2000)"', add
cap label define ancestr1d_lbl 1171 `"Udmert"', add
cap label define ancestr1d_lbl 1180 `"Mordovian"', add
cap label define ancestr1d_lbl 1190 `"Voytak"', add
cap label define ancestr1d_lbl 1200 `"Georgian"', add
cap label define ancestr1d_lbl 1220 `"Germans from Russia"', add
cap label define ancestr1d_lbl 1221 `"Volga"', add
cap label define ancestr1d_lbl 1222 `"German from Russia (1990-2000); German Russian (ACS, PRCS)"', add
cap label define ancestr1d_lbl 1230 `"Gruziia (1990-2000)"', add
cap label define ancestr1d_lbl 1240 `"Rom"', add
cap label define ancestr1d_lbl 1250 `"Hungarian"', add
cap label define ancestr1d_lbl 1260 `"Magyar"', add
cap label define ancestr1d_lbl 1280 `"Latvian"', add
cap label define ancestr1d_lbl 1290 `"Lithuanian"', add
cap label define ancestr1d_lbl 1300 `"Macedonian"', add
cap label define ancestr1d_lbl 1320 `"North Caucasian"', add
cap label define ancestr1d_lbl 1330 `"North Caucasian Turkic (1990-2000)"', add
cap label define ancestr1d_lbl 1400 `"Ossetian"', add
cap label define ancestr1d_lbl 1420 `"Polish"', add
cap label define ancestr1d_lbl 1430 `"Kashubian"', add
cap label define ancestr1d_lbl 1440 `"Romanian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 1441 `"Rumanian (1980)"', add
cap label define ancestr1d_lbl 1442 `"Transylvanian"', add
cap label define ancestr1d_lbl 1450 `"Bessarabian (1980)"', add
cap label define ancestr1d_lbl 1451 `"Bessarabian (1990-2000)"', add
cap label define ancestr1d_lbl 1452 `"Bucovina"', add
cap label define ancestr1d_lbl 1460 `"Moldavian"', add
cap label define ancestr1d_lbl 1470 `"Wallachian"', add
cap label define ancestr1d_lbl 1480 `"Russian"', add
cap label define ancestr1d_lbl 1500 `"Muscovite"', add
cap label define ancestr1d_lbl 1520 `"Serbian (1980)"', add
cap label define ancestr1d_lbl 1521 `"Serbian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 1522 `"Bosnian (1990) Herzegovinian (2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 1523 `"Montenegrin (1990-2000, 2012 ACS)"', add
cap label define ancestr1d_lbl 1530 `"Slovak"', add
cap label define ancestr1d_lbl 1540 `"Slovene"', add
cap label define ancestr1d_lbl 1550 `"Sorb/Wend"', add
cap label define ancestr1d_lbl 1560 `"Soviet Turkic (1990-2000)"', add
cap label define ancestr1d_lbl 1570 `"Bashkir"', add
cap label define ancestr1d_lbl 1580 `"Chevash"', add
cap label define ancestr1d_lbl 1590 `"Gagauz (1990-2000)"', add
cap label define ancestr1d_lbl 1600 `"Mesknetian (1990-2000)"', add
cap label define ancestr1d_lbl 1630 `"Yakut"', add
cap label define ancestr1d_lbl 1640 `"Soviet Union, nec"', add
cap label define ancestr1d_lbl 1650 `"Tatar (1990-2000)"', add
cap label define ancestr1d_lbl 1651 `"Tartar (1980)"', add
cap label define ancestr1d_lbl 1652 `"Crimean (1980)"', add
cap label define ancestr1d_lbl 1653 `"Tuvinian (1990-2000)"', add
cap label define ancestr1d_lbl 1654 `"Soviet Central Asia (1990-2000)"', add
cap label define ancestr1d_lbl 1655 `"Tadzhik (1980, 2000)"', add
cap label define ancestr1d_lbl 1690 `"Uzbek"', add
cap label define ancestr1d_lbl 1710 `"Ukrainian (1980)"', add
cap label define ancestr1d_lbl 1711 `"Ukrainian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 1712 `"Ruthenian (1980)"', add
cap label define ancestr1d_lbl 1713 `"Ruthenian (1990-2000)"', add
cap label define ancestr1d_lbl 1714 `"Lemko"', add
cap label define ancestr1d_lbl 1715 `"Bioko"', add
cap label define ancestr1d_lbl 1716 `"Husel"', add
cap label define ancestr1d_lbl 1717 `"Windish"', add
cap label define ancestr1d_lbl 1760 `"Yugoslavian"', add
cap label define ancestr1d_lbl 1780 `"Slav"', add
cap label define ancestr1d_lbl 1790 `"Slavonian"', add
cap label define ancestr1d_lbl 1810 `"Central European, nec"', add
cap label define ancestr1d_lbl 1830 `"Northern European, nec"', add
cap label define ancestr1d_lbl 1850 `"Southern European, nec"', add
cap label define ancestr1d_lbl 1870 `"Western European, nec"', add
cap label define ancestr1d_lbl 1900 `"Eastern European, nec"', add
cap label define ancestr1d_lbl 1950 `"European, nec"', add
cap label define ancestr1d_lbl 2000 `"Spaniard (1980)"', add
cap label define ancestr1d_lbl 2001 `"Spaniard (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2002 `"Castillian (1990-2000)"', add
cap label define ancestr1d_lbl 2003 `"Valencian (1990-2000)"', add
cap label define ancestr1d_lbl 2010 `"Andalusian (1990-2000)"', add
cap label define ancestr1d_lbl 2020 `"Asturian (1990-2000)"', add
cap label define ancestr1d_lbl 2040 `"Catalonian"', add
cap label define ancestr1d_lbl 2050 `"Balearic Islander (1980)"', add
cap label define ancestr1d_lbl 2051 `"Balearic Islander (1990-2000)"', add
cap label define ancestr1d_lbl 2052 `"Canary Islander (1990-2000)"', add
cap label define ancestr1d_lbl 2060 `"Galician (1980)"', add
cap label define ancestr1d_lbl 2061 `"Gallego (1990-2000)"', add
cap label define ancestr1d_lbl 2062 `"Galician (1990-2000)"', add
cap label define ancestr1d_lbl 2100 `"Mexican"', add
cap label define ancestr1d_lbl 2101 `"Mexican (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2102 `"Mexicano/Mexicana (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2103 `"Mexican Indian"', add
cap label define ancestr1d_lbl 2110 `"Mexican American"', add
cap label define ancestr1d_lbl 2111 `"Mexican American Indian"', add
cap label define ancestr1d_lbl 2130 `"Chicano/Chicana"', add
cap label define ancestr1d_lbl 2180 `"Nuevo Mexicano"', add
cap label define ancestr1d_lbl 2181 `"Nuevo Mexicano (1990-2000)"', add
cap label define ancestr1d_lbl 2182 `"La Raza (1990-2000)"', add
cap label define ancestr1d_lbl 2183 `"Mexican state (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2184 `"Tejano/Tejana (1990-2000)"', add
cap label define ancestr1d_lbl 2190 `"Californio"', add
cap label define ancestr1d_lbl 2210 `"Costa Rican"', add
cap label define ancestr1d_lbl 2220 `"Guatemalan"', add
cap label define ancestr1d_lbl 2230 `"Honduran"', add
cap label define ancestr1d_lbl 2240 `"Nicaraguan"', add
cap label define ancestr1d_lbl 2250 `"Panamanian (1980)"', add
cap label define ancestr1d_lbl 2251 `"Panamanian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2252 `"Canal Zone (1990-2000)"', add
cap label define ancestr1d_lbl 2260 `"Salvadoran"', add
cap label define ancestr1d_lbl 2270 `"Latin American (1980)"', add
cap label define ancestr1d_lbl 2271 `"Central American (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2272 `"Latin American (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2273 `"Latino/Latina (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2274 `"Latin (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2310 `"Argentinean"', add
cap label define ancestr1d_lbl 2320 `"Bolivian"', add
cap label define ancestr1d_lbl 2330 `"Chilean"', add
cap label define ancestr1d_lbl 2340 `"Colombian"', add
cap label define ancestr1d_lbl 2350 `"Ecuadorian"', add
cap label define ancestr1d_lbl 2360 `"Paraguayan"', add
cap label define ancestr1d_lbl 2370 `"Peruvian"', add
cap label define ancestr1d_lbl 2380 `"Uruguayan"', add
cap label define ancestr1d_lbl 2390 `"Venezuelan"', add
cap label define ancestr1d_lbl 2480 `"South American (1980)"', add
cap label define ancestr1d_lbl 2481 `"South American (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 2482 `"Criollo/Criolla (1990-2000)"', add
cap label define ancestr1d_lbl 2610 `"Puerto Rican"', add
cap label define ancestr1d_lbl 2710 `"Cuban"', add
cap label define ancestr1d_lbl 2750 `"Dominican"', add
cap label define ancestr1d_lbl 2900 `"Hispanic"', add
cap label define ancestr1d_lbl 2910 `"Spanish"', add
cap label define ancestr1d_lbl 2950 `"Spanish American"', add
cap label define ancestr1d_lbl 2960 `"Other Spanish/Hispanic"', add
cap label define ancestr1d_lbl 3000 `"Bahamian"', add
cap label define ancestr1d_lbl 3010 `"Barbadian"', add
cap label define ancestr1d_lbl 3020 `"Belizean"', add
cap label define ancestr1d_lbl 3030 `"Bermudan"', add
cap label define ancestr1d_lbl 3040 `"Cayman Islander"', add
cap label define ancestr1d_lbl 3080 `"Jamaican"', add
cap label define ancestr1d_lbl 3100 `"Dutch West Indies"', add
cap label define ancestr1d_lbl 3110 `"Aruba Islander"', add
cap label define ancestr1d_lbl 3120 `"St Maarten Islander"', add
cap label define ancestr1d_lbl 3140 `"Trinidadian/Tobagonian"', add
cap label define ancestr1d_lbl 3150 `"Trinidadian"', add
cap label define ancestr1d_lbl 3160 `"Tobagonian"', add
cap label define ancestr1d_lbl 3170 `"U.S. Virgin Islander (1980)"', add
cap label define ancestr1d_lbl 3171 `"U.S. Virgin Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3172 `"St. Croix Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3173 `"St. John Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3174 `"St. Thomas Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3210 `"British Virgin Islander (1980)"', add
cap label define ancestr1d_lbl 3211 `"British Virgin Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3212 `"Antigua (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 3220 `"British West Indian"', add
cap label define ancestr1d_lbl 3230 `"Turks and Caicos Islander"', add
cap label define ancestr1d_lbl 3240 `"Anguilla Islander (1980)"', add
cap label define ancestr1d_lbl 3241 `"Anguilla Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3242 `"Montserrat Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3243 `"Kitts/Nevis Islander (1990-2000)"', add
cap label define ancestr1d_lbl 3244 `"St. Christopher (1980)"', add
cap label define ancestr1d_lbl 3245 `"St Vincent Islander (1990); Vincent-Grenadine Islander (2000 Census, 2005 ACS, 2005 PRCS)"', add
cap label define ancestr1d_lbl 3280 `"Dominica Islander"', add
cap label define ancestr1d_lbl 3290 `"Grenadian"', add
cap label define ancestr1d_lbl 3310 `"St Lucia Islander"', add
cap label define ancestr1d_lbl 3320 `"French West Indies"', add
cap label define ancestr1d_lbl 3330 `"Guadeloupe Islander"', add
cap label define ancestr1d_lbl 3340 `"Cayenne"', add
cap label define ancestr1d_lbl 3350 `"West Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 3351 `"West Indian (1980)"', add
cap label define ancestr1d_lbl 3352 `"Caribbean (1980)"', add
cap label define ancestr1d_lbl 3353 `"Arawak (1980)"', add
cap label define ancestr1d_lbl 3360 `"Haitian"', add
cap label define ancestr1d_lbl 3370 `"Other West Indian"', add
cap label define ancestr1d_lbl 3600 `"Brazilian"', add
cap label define ancestr1d_lbl 3650 `"San Andres"', add
cap label define ancestr1d_lbl 3700 `"Guyanese/British Guiana"', add
cap label define ancestr1d_lbl 3750 `"Providencia"', add
cap label define ancestr1d_lbl 3800 `"Surinam/Dutch Guiana"', add
cap label define ancestr1d_lbl 4000 `"Algerian"', add
cap label define ancestr1d_lbl 4020 `"Egyptian"', add
cap label define ancestr1d_lbl 4040 `"Libyan"', add
cap label define ancestr1d_lbl 4060 `"Moroccan (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 4061 `"Moroccan (1980)"', add
cap label define ancestr1d_lbl 4062 `"Moor (1980)"', add
cap label define ancestr1d_lbl 4070 `"Ifni"', add
cap label define ancestr1d_lbl 4080 `"Tunisian"', add
cap label define ancestr1d_lbl 4110 `"North African"', add
cap label define ancestr1d_lbl 4120 `"Alhucemas"', add
cap label define ancestr1d_lbl 4130 `"Berber"', add
cap label define ancestr1d_lbl 4140 `"Rio de Oro"', add
cap label define ancestr1d_lbl 4150 `"Bahraini"', add
cap label define ancestr1d_lbl 4160 `"Iranian"', add
cap label define ancestr1d_lbl 4170 `"Iraqi"', add
cap label define ancestr1d_lbl 4190 `"Israeli"', add
cap label define ancestr1d_lbl 4210 `"Jordanian"', add
cap label define ancestr1d_lbl 4220 `"Transjordan"', add
cap label define ancestr1d_lbl 4230 `"Kuwaiti"', add
cap label define ancestr1d_lbl 4250 `"Lebanese"', add
cap label define ancestr1d_lbl 4270 `"Saudi Arabian"', add
cap label define ancestr1d_lbl 4290 `"Syrian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 4291 `"Syrian (1980)"', add
cap label define ancestr1d_lbl 4292 `"Latakian (1980)"', add
cap label define ancestr1d_lbl 4293 `"Jebel Druse (1980)"', add
cap label define ancestr1d_lbl 4310 `"Armenian"', add
cap label define ancestr1d_lbl 4340 `"Turkish"', add
cap label define ancestr1d_lbl 4350 `"Yemeni"', add
cap label define ancestr1d_lbl 4360 `"Omani"', add
cap label define ancestr1d_lbl 4370 `"Muscat"', add
cap label define ancestr1d_lbl 4380 `"Trucial Oman"', add
cap label define ancestr1d_lbl 4390 `"Qatar"', add
cap label define ancestr1d_lbl 4410 `"Bedouin"', add
cap label define ancestr1d_lbl 4420 `"Kurdish"', add
cap label define ancestr1d_lbl 4440 `"Kuria Muria Islander"', add
cap label define ancestr1d_lbl 4650 `"Palestinian"', add
cap label define ancestr1d_lbl 4660 `"Gazan"', add
cap label define ancestr1d_lbl 4670 `"West Bank"', add
cap label define ancestr1d_lbl 4700 `"South Yemeni"', add
cap label define ancestr1d_lbl 4710 `"Aden"', add
cap label define ancestr1d_lbl 4800 `"United Arab Emirates"', add
cap label define ancestr1d_lbl 4820 `"Assyrian/Chaldean/Syriac (1990-2000)"', add
cap label define ancestr1d_lbl 4821 `"Assyrian"', add
cap label define ancestr1d_lbl 4822 `"Syriac (1980, 2000)"', add
cap label define ancestr1d_lbl 4823 `"Chaldean (2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 4900 `"Middle Eastern"', add
cap label define ancestr1d_lbl 4950 `"Arab"', add
cap label define ancestr1d_lbl 4951 `"Arabic (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 4960 `"Other Arab"', add
cap label define ancestr1d_lbl 5000 `"Angolan"', add
cap label define ancestr1d_lbl 5020 `"Benin"', add
cap label define ancestr1d_lbl 5040 `"Botswana"', add
cap label define ancestr1d_lbl 5060 `"Burundian"', add
cap label define ancestr1d_lbl 5080 `"Cameroonian"', add
cap label define ancestr1d_lbl 5100 `"Cape Verdean"', add
cap label define ancestr1d_lbl 5120 `"Central African Republic"', add
cap label define ancestr1d_lbl 5130 `"Chadian"', add
cap label define ancestr1d_lbl 5150 `"Congolese"', add
cap label define ancestr1d_lbl 5160 `"Congo-Brazzaville"', add
cap label define ancestr1d_lbl 5190 `"Djibouti"', add
cap label define ancestr1d_lbl 5200 `"Equatorial Guinea"', add
cap label define ancestr1d_lbl 5210 `"Corsico Islander"', add
cap label define ancestr1d_lbl 5220 `"Ethiopian"', add
cap label define ancestr1d_lbl 5230 `"Eritrean"', add
cap label define ancestr1d_lbl 5250 `"Gabonese"', add
cap label define ancestr1d_lbl 5270 `"Gambian"', add
cap label define ancestr1d_lbl 5290 `"Ghanian"', add
cap label define ancestr1d_lbl 5300 `"Guinean"', add
cap label define ancestr1d_lbl 5310 `"Guinea Bissau"', add
cap label define ancestr1d_lbl 5320 `"Ivory Coast"', add
cap label define ancestr1d_lbl 5340 `"Kenyan"', add
cap label define ancestr1d_lbl 5380 `"Lesotho"', add
cap label define ancestr1d_lbl 5410 `"Liberian"', add
cap label define ancestr1d_lbl 5430 `"Madagascan"', add
cap label define ancestr1d_lbl 5450 `"Malawian"', add
cap label define ancestr1d_lbl 5460 `"Malian"', add
cap label define ancestr1d_lbl 5470 `"Mauritanian"', add
cap label define ancestr1d_lbl 5490 `"Mozambican"', add
cap label define ancestr1d_lbl 5500 `"Namibian"', add
cap label define ancestr1d_lbl 5510 `"Niger"', add
cap label define ancestr1d_lbl 5530 `"Nigerian"', add
cap label define ancestr1d_lbl 5540 `"Fulani"', add
cap label define ancestr1d_lbl 5550 `"Hausa"', add
cap label define ancestr1d_lbl 5560 `"Ibo"', add
cap label define ancestr1d_lbl 5570 `"Tiv (1980)"', add
cap label define ancestr1d_lbl 5571 `"Tiv (1990-2000)"', add
cap label define ancestr1d_lbl 5572 `"Yoruba (1990-2000)"', add
cap label define ancestr1d_lbl 5610 `"Rwandan"', add
cap label define ancestr1d_lbl 5640 `"Senegalese"', add
cap label define ancestr1d_lbl 5660 `"Sierra Leonean"', add
cap label define ancestr1d_lbl 5680 `"Somalian"', add
cap label define ancestr1d_lbl 5690 `"Swaziland"', add
cap label define ancestr1d_lbl 5700 `"South African"', add
cap label define ancestr1d_lbl 5710 `"Union of South Africa"', add
cap label define ancestr1d_lbl 5720 `"Afrikaner"', add
cap label define ancestr1d_lbl 5730 `"Natalian"', add
cap label define ancestr1d_lbl 5740 `"Zulu"', add
cap label define ancestr1d_lbl 5760 `"Sudanese"', add
cap label define ancestr1d_lbl 5770 `"Dinka"', add
cap label define ancestr1d_lbl 5780 `"Nuer"', add
cap label define ancestr1d_lbl 5790 `"Fur"', add
cap label define ancestr1d_lbl 5800 `"Baggara"', add
cap label define ancestr1d_lbl 5820 `"Tanzanian"', add
cap label define ancestr1d_lbl 5830 `"Tanganyikan"', add
cap label define ancestr1d_lbl 5840 `"Zanzibar"', add
cap label define ancestr1d_lbl 5860 `"Togo"', add
cap label define ancestr1d_lbl 5880 `"Ugandan"', add
cap label define ancestr1d_lbl 5890 `"Upper Voltan"', add
cap label define ancestr1d_lbl 5900 `"Volta"', add
cap label define ancestr1d_lbl 5910 `"Zairian"', add
cap label define ancestr1d_lbl 5920 `"Zambian"', add
cap label define ancestr1d_lbl 5930 `"Zimbabwean"', add
cap label define ancestr1d_lbl 5940 `"African Islands (1980)"', add
cap label define ancestr1d_lbl 5941 `"African Islands (1990-2000)"', add
cap label define ancestr1d_lbl 5942 `"Mauritius (1990-2000)"', add
cap label define ancestr1d_lbl 5950 `"Other Subsaharan Africa"', add
cap label define ancestr1d_lbl 5960 `"Central African"', add
cap label define ancestr1d_lbl 5970 `"East African"', add
cap label define ancestr1d_lbl 5980 `"West African"', add
cap label define ancestr1d_lbl 5990 `"African"', add
cap label define ancestr1d_lbl 6000 `"Afghan"', add
cap label define ancestr1d_lbl 6010 `"Baluchi"', add
cap label define ancestr1d_lbl 6020 `"Pathan"', add
cap label define ancestr1d_lbl 6030 `"Bengali (1980)"', add
cap label define ancestr1d_lbl 6031 `"Bangladeshi (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 6032 `"Bengali (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 6070 `"Bhutanese"', add
cap label define ancestr1d_lbl 6090 `"Nepali"', add
cap label define ancestr1d_lbl 6150 `"Asian Indian (1980)"', add
cap label define ancestr1d_lbl 6151 `"India (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 6152 `"East Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 6153 `"Madhya Pradesh (1990-2000)"', add
cap label define ancestr1d_lbl 6154 `"Orissa (1990-2000)"', add
cap label define ancestr1d_lbl 6155 `"Rajasthani (1990-2000)"', add
cap label define ancestr1d_lbl 6156 `"Sikkim (1990-2000)"', add
cap label define ancestr1d_lbl 6157 `"Uttar Pradesh (1990-2000)"', add
cap label define ancestr1d_lbl 6220 `"Andaman Islander"', add
cap label define ancestr1d_lbl 6240 `"Andhra Pradesh"', add
cap label define ancestr1d_lbl 6260 `"Assamese"', add
cap label define ancestr1d_lbl 6280 `"Goanese"', add
cap label define ancestr1d_lbl 6300 `"Gujarati"', add
cap label define ancestr1d_lbl 6320 `"Karnatakan"', add
cap label define ancestr1d_lbl 6340 `"Keralan"', add
cap label define ancestr1d_lbl 6380 `"Maharashtran"', add
cap label define ancestr1d_lbl 6400 `"Madrasi"', add
cap label define ancestr1d_lbl 6420 `"Mysore"', add
cap label define ancestr1d_lbl 6440 `"Naga"', add
cap label define ancestr1d_lbl 6480 `"Pondicherry"', add
cap label define ancestr1d_lbl 6500 `"Punjabi"', add
cap label define ancestr1d_lbl 6560 `"Tamil"', add
cap label define ancestr1d_lbl 6750 `"East Indies (1990-2000)"', add
cap label define ancestr1d_lbl 6800 `"Pakistani (1980)"', add
cap label define ancestr1d_lbl 6801 `"Pakistani (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 6802 `"Kashmiri (1990-2000)"', add
cap label define ancestr1d_lbl 6900 `"Sri Lankan"', add
cap label define ancestr1d_lbl 6910 `"Singhalese"', add
cap label define ancestr1d_lbl 6920 `"Veddah"', add
cap label define ancestr1d_lbl 6950 `"Maldivian"', add
cap label define ancestr1d_lbl 7000 `"Burmese (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 7001 `"Burmese (1980)"', add
cap label define ancestr1d_lbl 7002 `"Burman (1980)"', add
cap label define ancestr1d_lbl 7020 `"Shan"', add
cap label define ancestr1d_lbl 7030 `"Cambodian"', add
cap label define ancestr1d_lbl 7040 `"Khmer"', add
cap label define ancestr1d_lbl 7060 `"Chinese"', add
cap label define ancestr1d_lbl 7070 `"Cantonese (1980)"', add
cap label define ancestr1d_lbl 7071 `"Cantonese (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 7072 `"Formosan (1990-2000)"', add
cap label define ancestr1d_lbl 7080 `"Manchurian"', add
cap label define ancestr1d_lbl 7090 `"Mandarin (1990-2000)"', add
cap label define ancestr1d_lbl 7120 `"Mongolian (1980)"', add
cap label define ancestr1d_lbl 7121 `"Mongolian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 7122 `"Kalmyk (1990-2000)"', add
cap label define ancestr1d_lbl 7140 `"Tibetan"', add
cap label define ancestr1d_lbl 7160 `"Hong Kong (1990-2000)"', add
cap label define ancestr1d_lbl 7161 `"Hong Kong (1980)"', add
cap label define ancestr1d_lbl 7162 `"Eastern Archipelago (1980)"', add
cap label define ancestr1d_lbl 7180 `"Macao"', add
cap label define ancestr1d_lbl 7200 `"Filipino"', add
cap label define ancestr1d_lbl 7300 `"Indonesian (1980)"', add
cap label define ancestr1d_lbl 7301 `"Indonesian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 7302 `"Borneo (1990-2000)"', add
cap label define ancestr1d_lbl 7303 `"Java (1990-2000)"', add
cap label define ancestr1d_lbl 7304 `"Sumatran (1990-2000)"', add
cap label define ancestr1d_lbl 7400 `"Japanese (1980)"', add
cap label define ancestr1d_lbl 7401 `"Japanese (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 7402 `"Issei (1990-2000)"', add
cap label define ancestr1d_lbl 7403 `"Nisei (1990-2000)"', add
cap label define ancestr1d_lbl 7404 `"Sansei (1990-2000)"', add
cap label define ancestr1d_lbl 7405 `"Yonsei (1990-2000)"', add
cap label define ancestr1d_lbl 7406 `"Gosei (1990-2000)"', add
cap label define ancestr1d_lbl 7460 `"Ryukyu Islander"', add
cap label define ancestr1d_lbl 7480 `"Okinawan"', add
cap label define ancestr1d_lbl 7500 `"Korean"', add
cap label define ancestr1d_lbl 7650 `"Laotian"', add
cap label define ancestr1d_lbl 7660 `"Meo"', add
cap label define ancestr1d_lbl 7680 `"Hmong"', add
cap label define ancestr1d_lbl 7700 `"Malaysian (1980)"', add
cap label define ancestr1d_lbl 7701 `"Malaysian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 7702 `"North Borneo (1990-2000)"', add
cap label define ancestr1d_lbl 7740 `"Singaporean"', add
cap label define ancestr1d_lbl 7760 `"Thai"', add
cap label define ancestr1d_lbl 7770 `"Black Thai"', add
cap label define ancestr1d_lbl 7780 `"Western Lao"', add
cap label define ancestr1d_lbl 7820 `"Taiwanese"', add
cap label define ancestr1d_lbl 7850 `"Vietnamese"', add
cap label define ancestr1d_lbl 7860 `"Katu"', add
cap label define ancestr1d_lbl 7870 `"Ma"', add
cap label define ancestr1d_lbl 7880 `"Mnong"', add
cap label define ancestr1d_lbl 7900 `"Montagnard"', add
cap label define ancestr1d_lbl 7920 `"Indochinese"', add
cap label define ancestr1d_lbl 7930 `"Eurasian"', add
cap label define ancestr1d_lbl 7931 `"Amerasian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 7950 `"Asian"', add
cap label define ancestr1d_lbl 7960 `"Other Asian"', add
cap label define ancestr1d_lbl 8000 `"Australian"', add
cap label define ancestr1d_lbl 8010 `"Tasmanian"', add
cap label define ancestr1d_lbl 8020 `"Australian Aborigine (1990-2000)"', add
cap label define ancestr1d_lbl 8030 `"New Zealander"', add
cap label define ancestr1d_lbl 8080 `"Polynesian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 8081 `"Polynesian (1980)"', add
cap label define ancestr1d_lbl 8082 `"Norfolk Islander (1980)"', add
cap label define ancestr1d_lbl 8090 `"Kapinagamarangan (1990-2000)"', add
cap label define ancestr1d_lbl 8091 `"Kapinagamarangan (1980)"', add
cap label define ancestr1d_lbl 8092 `"Nukuoroan (1980)"', add
cap label define ancestr1d_lbl 8100 `"Maori"', add
cap label define ancestr1d_lbl 8110 `"Hawaiian"', add
cap label define ancestr1d_lbl 8130 `"Part Hawaiian"', add
cap label define ancestr1d_lbl 8140 `"Samoan (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 8141 `"Samoan (1980)"', add
cap label define ancestr1d_lbl 8142 `"American Samoan (1980)"', add
cap label define ancestr1d_lbl 8143 `"French Samoan"', add
cap label define ancestr1d_lbl 8144 `"Part Samoan (1990-2000)"', add
cap label define ancestr1d_lbl 8150 `"Tongan"', add
cap label define ancestr1d_lbl 8160 `"Tokelauan"', add
cap label define ancestr1d_lbl 8170 `"Cook Islander"', add
cap label define ancestr1d_lbl 8180 `"Tahitian"', add
cap label define ancestr1d_lbl 8190 `"Niuean"', add
cap label define ancestr1d_lbl 8200 `"Micronesian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 8201 `"Micronesian (1980)"', add
cap label define ancestr1d_lbl 8202 `"U.S. Trust Terr of the Pacific (1980)"', add
cap label define ancestr1d_lbl 8210 `"Guamanian"', add
cap label define ancestr1d_lbl 8220 `"Chamorro Islander"', add
cap label define ancestr1d_lbl 8230 `"Saipanese (1990-2000)"', add
cap label define ancestr1d_lbl 8231 `"Saipanese (1980)"', add
cap label define ancestr1d_lbl 8232 `"Northern Marianas (1980)"', add
cap label define ancestr1d_lbl 8240 `"Palauan"', add
cap label define ancestr1d_lbl 8250 `"Marshall Islander"', add
cap label define ancestr1d_lbl 8260 `"Kosraean"', add
cap label define ancestr1d_lbl 8270 `"Ponapean (1990-2000)"', add
cap label define ancestr1d_lbl 8271 `"Ponapean (1980)"', add
cap label define ancestr1d_lbl 8272 `"Mokilese (1980)"', add
cap label define ancestr1d_lbl 8273 `"Ngatikese (1980)"', add
cap label define ancestr1d_lbl 8274 `"Pingelapese (1980)"', add
cap label define ancestr1d_lbl 8280 `"Chuukese (1990-2000)"', add
cap label define ancestr1d_lbl 8281 `"Hall Islander (1980)"', add
cap label define ancestr1d_lbl 8282 `"Mortlockese (1980)"', add
cap label define ancestr1d_lbl 8283 `"Namanouito (1980)"', add
cap label define ancestr1d_lbl 8284 `"Pulawatese (1980)"', add
cap label define ancestr1d_lbl 8285 `"Truk Islander"', add
cap label define ancestr1d_lbl 8290 `"Yap Islander"', add
cap label define ancestr1d_lbl 8300 `"Caroline Islander (1990-2000)"', add
cap label define ancestr1d_lbl 8301 `"Caroline Islander (1980)"', add
cap label define ancestr1d_lbl 8302 `"Lamotrekese (1980)"', add
cap label define ancestr1d_lbl 8303 `"Ulithian (1980)"', add
cap label define ancestr1d_lbl 8304 `"Woleaian (1980)"', add
cap label define ancestr1d_lbl 8310 `"Kiribatese"', add
cap label define ancestr1d_lbl 8320 `"Nauruan"', add
cap label define ancestr1d_lbl 8330 `"Tarawa Islander (1990-2000)"', add
cap label define ancestr1d_lbl 8340 `"Tinian Islander (1990-2000)"', add
cap label define ancestr1d_lbl 8400 `"Melanesian Islander"', add
cap label define ancestr1d_lbl 8410 `"Fijian"', add
cap label define ancestr1d_lbl 8430 `"New Guinean"', add
cap label define ancestr1d_lbl 8440 `"Papuan"', add
cap label define ancestr1d_lbl 8450 `"Solomon Islander"', add
cap label define ancestr1d_lbl 8460 `"New Caledonian Islander"', add
cap label define ancestr1d_lbl 8470 `"Vanuatuan"', add
cap label define ancestr1d_lbl 8500 `"Pacific Islander (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 8501 `"Campbell Islander (1980)"', add
cap label define ancestr1d_lbl 8502 `"Christmas Islander (1980)"', add
cap label define ancestr1d_lbl 8503 `"Kermadec Islander (1980)"', add
cap label define ancestr1d_lbl 8504 `"Midway Islander (1980)"', add
cap label define ancestr1d_lbl 8505 `"Phoenix Islander (1980)"', add
cap label define ancestr1d_lbl 8506 `"Wake Islander (1980)"', add
cap label define ancestr1d_lbl 8600 `"Oceania"', add
cap label define ancestr1d_lbl 8620 `"Chamolinian (1990-2000)"', add
cap label define ancestr1d_lbl 8630 `"Reserved Codes"', add
cap label define ancestr1d_lbl 8700 `"Other Pacific"', add
cap label define ancestr1d_lbl 9000 `"Afro-American"', add
cap label define ancestr1d_lbl 9001 `"Afro-American (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9002 `"Black (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9003 `"Negro (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9004 `"Nonwhite (1990-2000)"', add
cap label define ancestr1d_lbl 9005 `"Colored (1990-2000)"', add
cap label define ancestr1d_lbl 9006 `"Creole (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9007 `"Mulatto (1990-2000)"', add
cap label define ancestr1d_lbl 9008 `"Afro"', add
cap label define ancestr1d_lbl 9020 `"African-American (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9130 `"Central American Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9140 `"South American Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9200 `"American Indian (all tribes)"', add
cap label define ancestr1d_lbl 9201 `"American Indian-English-French"', add
cap label define ancestr1d_lbl 9202 `"American Indian-English-German"', add
cap label define ancestr1d_lbl 9203 `"American Indian-English-Irish"', add
cap label define ancestr1d_lbl 9204 `"American Indian-German-Irish"', add
cap label define ancestr1d_lbl 9205 `"Cherokee"', add
cap label define ancestr1d_lbl 9206 `"Native American"', add
cap label define ancestr1d_lbl 9207 `"Indian"', add
cap label define ancestr1d_lbl 9210 `"Aleut"', add
cap label define ancestr1d_lbl 9220 `"Eskimo"', add
cap label define ancestr1d_lbl 9230 `"Inuit"', add
cap label define ancestr1d_lbl 9240 `"White/Caucasian"', add
cap label define ancestr1d_lbl 9241 `"White/Caucasian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9242 `"Anglo (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9243 `"Appalachian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9244 `"Aryan (1990-2000)"', add
cap label define ancestr1d_lbl 9300 `"Greenlander"', add
cap label define ancestr1d_lbl 9310 `"Canadian"', add
cap label define ancestr1d_lbl 9330 `"Newfoundland"', add
cap label define ancestr1d_lbl 9340 `"Nova Scotian"', add
cap label define ancestr1d_lbl 9350 `"French Canadian"', add
cap label define ancestr1d_lbl 9360 `"Acadian"', add
cap label define ancestr1d_lbl 9361 `"Acadian (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9362 `"Cajun (1990-2000, ACS, PRCS)"', add
cap label define ancestr1d_lbl 9390 `"American"', add
cap label define ancestr1d_lbl 9391 `"American/United States"', add
cap label define ancestr1d_lbl 9400 `"United States"', add
cap label define ancestr1d_lbl 9410 `"Alabama"', add
cap label define ancestr1d_lbl 9420 `"Alaska"', add
cap label define ancestr1d_lbl 9430 `"Arizona"', add
cap label define ancestr1d_lbl 9440 `"Arkansas"', add
cap label define ancestr1d_lbl 9450 `"California"', add
cap label define ancestr1d_lbl 9460 `"Colorado"', add
cap label define ancestr1d_lbl 9470 `"Connecticut"', add
cap label define ancestr1d_lbl 9480 `"District of Columbia"', add
cap label define ancestr1d_lbl 9490 `"Delaware"', add
cap label define ancestr1d_lbl 9500 `"Florida"', add
cap label define ancestr1d_lbl 9510 `"Georgia"', add
cap label define ancestr1d_lbl 9520 `"Idaho"', add
cap label define ancestr1d_lbl 9530 `"Illinois"', add
cap label define ancestr1d_lbl 9540 `"Indiana"', add
cap label define ancestr1d_lbl 9550 `"Iowa"', add
cap label define ancestr1d_lbl 9560 `"Kansas"', add
cap label define ancestr1d_lbl 9570 `"Kentucky"', add
cap label define ancestr1d_lbl 9580 `"Louisiana"', add
cap label define ancestr1d_lbl 9590 `"Maine"', add
cap label define ancestr1d_lbl 9600 `"Maryland"', add
cap label define ancestr1d_lbl 9610 `"Massachusetts"', add
cap label define ancestr1d_lbl 9620 `"Michigan"', add
cap label define ancestr1d_lbl 9630 `"Minnesota"', add
cap label define ancestr1d_lbl 9640 `"Mississippi"', add
cap label define ancestr1d_lbl 9650 `"Missouri"', add
cap label define ancestr1d_lbl 9660 `"Montana"', add
cap label define ancestr1d_lbl 9670 `"Nebraska"', add
cap label define ancestr1d_lbl 9680 `"Nevada"', add
cap label define ancestr1d_lbl 9690 `"New Hampshire"', add
cap label define ancestr1d_lbl 9700 `"New Jersey"', add
cap label define ancestr1d_lbl 9710 `"New Mexico"', add
cap label define ancestr1d_lbl 9720 `"New York"', add
cap label define ancestr1d_lbl 9730 `"North Carolina"', add
cap label define ancestr1d_lbl 9740 `"North Dakota"', add
cap label define ancestr1d_lbl 9750 `"Ohio"', add
cap label define ancestr1d_lbl 9760 `"Oklahoma"', add
cap label define ancestr1d_lbl 9770 `"Oregon"', add
cap label define ancestr1d_lbl 9780 `"Pennsylvania"', add
cap label define ancestr1d_lbl 9790 `"Rhode Island"', add
cap label define ancestr1d_lbl 9800 `"South Carolina"', add
cap label define ancestr1d_lbl 9810 `"South Dakota"', add
cap label define ancestr1d_lbl 9820 `"Tennessee"', add
cap label define ancestr1d_lbl 9830 `"Texas"', add
cap label define ancestr1d_lbl 9840 `"Utah"', add
cap label define ancestr1d_lbl 9850 `"Vermont"', add
cap label define ancestr1d_lbl 9860 `"Virginia"', add
cap label define ancestr1d_lbl 9870 `"Washington"', add
cap label define ancestr1d_lbl 9880 `"West Virginia"', add
cap label define ancestr1d_lbl 9890 `"Wisconsin"', add
cap label define ancestr1d_lbl 9900 `"Wyoming"', add
cap label define ancestr1d_lbl 9930 `"Southerner"', add
cap label define ancestr1d_lbl 9940 `"North American"', add
cap label define ancestr1d_lbl 9950 `"Mixture"', add
cap label define ancestr1d_lbl 9960 `"Uncodable"', add
cap label define ancestr1d_lbl 9961 `"Not Classified"', add
cap label define ancestr1d_lbl 9980 `"Other"', add
cap label define ancestr1d_lbl 9990 `"Not Reported"', add
cap label values ancestr1d ancestr1d_lbl

cap label define ancestr2_lbl 001 `"Alsatian, Alsace-Lorraine"'
cap label define ancestr2_lbl 002 `"Andorran"', add
cap label define ancestr2_lbl 003 `"Austrian"', add
cap label define ancestr2_lbl 004 `"Tirolean"', add
cap label define ancestr2_lbl 005 `"Basque"', add
cap label define ancestr2_lbl 006 `"French Basque"', add
cap label define ancestr2_lbl 008 `"Belgian"', add
cap label define ancestr2_lbl 009 `"Flemish"', add
cap label define ancestr2_lbl 010 `"Walloon"', add
cap label define ancestr2_lbl 011 `"British"', add
cap label define ancestr2_lbl 012 `"British Isles"', add
cap label define ancestr2_lbl 013 `"Channel Islander"', add
cap label define ancestr2_lbl 014 `"Gibraltan"', add
cap label define ancestr2_lbl 015 `"Cornish"', add
cap label define ancestr2_lbl 016 `"Corsican"', add
cap label define ancestr2_lbl 017 `"Cypriot"', add
cap label define ancestr2_lbl 018 `"Greek Cypriote"', add
cap label define ancestr2_lbl 019 `"Turkish Cypriote"', add
cap label define ancestr2_lbl 020 `"Danish"', add
cap label define ancestr2_lbl 021 `"Dutch"', add
cap label define ancestr2_lbl 022 `"English"', add
cap label define ancestr2_lbl 023 `"Faeroe Islander"', add
cap label define ancestr2_lbl 024 `"Finnish"', add
cap label define ancestr2_lbl 025 `"Karelian"', add
cap label define ancestr2_lbl 026 `"French"', add
cap label define ancestr2_lbl 027 `"Lorrainian"', add
cap label define ancestr2_lbl 028 `"Breton"', add
cap label define ancestr2_lbl 029 `"Frisian"', add
cap label define ancestr2_lbl 030 `"Friulian"', add
cap label define ancestr2_lbl 032 `"German"', add
cap label define ancestr2_lbl 033 `"Bavarian"', add
cap label define ancestr2_lbl 034 `"Berliner"', add
cap label define ancestr2_lbl 035 `"Hamburger"', add
cap label define ancestr2_lbl 036 `"Hanoverian"', add
cap label define ancestr2_lbl 037 `"Hessian"', add
cap label define ancestr2_lbl 038 `"Lubecker"', add
cap label define ancestr2_lbl 039 `"Pomeranian"', add
cap label define ancestr2_lbl 040 `"Prussian"', add
cap label define ancestr2_lbl 041 `"Saxon"', add
cap label define ancestr2_lbl 042 `"Sudetenlander"', add
cap label define ancestr2_lbl 043 `"Westphalian"', add
cap label define ancestr2_lbl 046 `"Greek"', add
cap label define ancestr2_lbl 047 `"Cretan"', add
cap label define ancestr2_lbl 048 `"Cycladic Islander"', add
cap label define ancestr2_lbl 049 `"Icelander"', add
cap label define ancestr2_lbl 050 `"Irish"', add
cap label define ancestr2_lbl 051 `"Italian"', add
cap label define ancestr2_lbl 053 `"Abruzzi"', add
cap label define ancestr2_lbl 054 `"Apulian"', add
cap label define ancestr2_lbl 055 `"Basilicata"', add
cap label define ancestr2_lbl 056 `"Calabrian"', add
cap label define ancestr2_lbl 057 `"Amalfin"', add
cap label define ancestr2_lbl 058 `"Emilia Romagna"', add
cap label define ancestr2_lbl 059 `"Rome"', add
cap label define ancestr2_lbl 060 `"Ligurian"', add
cap label define ancestr2_lbl 061 `"Lombardian"', add
cap label define ancestr2_lbl 062 `"Marches"', add
cap label define ancestr2_lbl 063 `"Molise"', add
cap label define ancestr2_lbl 064 `"Neapolitan"', add
cap label define ancestr2_lbl 065 `"Piedmontese"', add
cap label define ancestr2_lbl 066 `"Puglia"', add
cap label define ancestr2_lbl 067 `"Sardinian"', add
cap label define ancestr2_lbl 068 `"Sicilian"', add
cap label define ancestr2_lbl 069 `"Toscana"', add
cap label define ancestr2_lbl 070 `"Trentino"', add
cap label define ancestr2_lbl 071 `"Umbrian"', add
cap label define ancestr2_lbl 072 `"Valle dAosta"', add
cap label define ancestr2_lbl 073 `"Venetian"', add
cap label define ancestr2_lbl 075 `"Lapp"', add
cap label define ancestr2_lbl 076 `"Liechtensteiner"', add
cap label define ancestr2_lbl 077 `"Luxemburger"', add
cap label define ancestr2_lbl 078 `"Maltese"', add
cap label define ancestr2_lbl 079 `"Manx"', add
cap label define ancestr2_lbl 080 `"Monegasque"', add
cap label define ancestr2_lbl 081 `"Northern Irelander"', add
cap label define ancestr2_lbl 082 `"Norwegian"', add
cap label define ancestr2_lbl 084 `"Portuguese"', add
cap label define ancestr2_lbl 085 `"Azorean"', add
cap label define ancestr2_lbl 086 `"Madeiran"', add
cap label define ancestr2_lbl 087 `"Scotch Irish"', add
cap label define ancestr2_lbl 088 `"Scottish"', add
cap label define ancestr2_lbl 089 `"Swedish"', add
cap label define ancestr2_lbl 090 `"Aland Islander"', add
cap label define ancestr2_lbl 091 `"Swiss"', add
cap label define ancestr2_lbl 092 `"Suisse"', add
cap label define ancestr2_lbl 095 `"Romansch"', add
cap label define ancestr2_lbl 096 `"Suisse Romane"', add
cap label define ancestr2_lbl 097 `"Welsh"', add
cap label define ancestr2_lbl 098 `"Scandinavian, Nordic"', add
cap label define ancestr2_lbl 100 `"Albanian"', add
cap label define ancestr2_lbl 101 `"Azerbaijani"', add
cap label define ancestr2_lbl 102 `"Belourussian"', add
cap label define ancestr2_lbl 103 `"Bulgarian"', add
cap label define ancestr2_lbl 105 `"Carpathian"', add
cap label define ancestr2_lbl 108 `"Cossack"', add
cap label define ancestr2_lbl 109 `"Croatian"', add
cap label define ancestr2_lbl 111 `"Czechoslovakian"', add
cap label define ancestr2_lbl 112 `"Bohemian"', add
cap label define ancestr2_lbl 115 `"Estonian"', add
cap label define ancestr2_lbl 116 `"Livonian"', add
cap label define ancestr2_lbl 117 `"Finno Ugrian"', add
cap label define ancestr2_lbl 118 `"Mordovian"', add
cap label define ancestr2_lbl 119 `"Voytak"', add
cap label define ancestr2_lbl 120 `"Georgian"', add
cap label define ancestr2_lbl 122 `"Germans from Russia"', add
cap label define ancestr2_lbl 123 `"Gruziia"', add
cap label define ancestr2_lbl 124 `"Rom"', add
cap label define ancestr2_lbl 125 `"Hungarian"', add
cap label define ancestr2_lbl 126 `"Magyar"', add
cap label define ancestr2_lbl 128 `"Latvian"', add
cap label define ancestr2_lbl 129 `"Lithuanian"', add
cap label define ancestr2_lbl 130 `"Macedonian"', add
cap label define ancestr2_lbl 132 `"North Caucasian"', add
cap label define ancestr2_lbl 133 `"North Caucasian Turkic"', add
cap label define ancestr2_lbl 140 `"Ossetian"', add
cap label define ancestr2_lbl 142 `"Polish"', add
cap label define ancestr2_lbl 143 `"Kashubian"', add
cap label define ancestr2_lbl 144 `"Romanian"', add
cap label define ancestr2_lbl 145 `"Bessarabian"', add
cap label define ancestr2_lbl 146 `"Moldavian"', add
cap label define ancestr2_lbl 147 `"Wallachian"', add
cap label define ancestr2_lbl 148 `"Russian"', add
cap label define ancestr2_lbl 150 `"Muscovite"', add
cap label define ancestr2_lbl 152 `"Serbian"', add
cap label define ancestr2_lbl 153 `"Slovak"', add
cap label define ancestr2_lbl 154 `"Slovene"', add
cap label define ancestr2_lbl 155 `"Sorb/Wend"', add
cap label define ancestr2_lbl 156 `"Soviet Turkic"', add
cap label define ancestr2_lbl 157 `"Bashkir"', add
cap label define ancestr2_lbl 158 `"Chevash"', add
cap label define ancestr2_lbl 159 `"Gagauz"', add
cap label define ancestr2_lbl 160 `"Mesknetian"', add
cap label define ancestr2_lbl 163 `"Yakut"', add
cap label define ancestr2_lbl 164 `"Soviet Union, nec"', add
cap label define ancestr2_lbl 165 `"Tatar"', add
cap label define ancestr2_lbl 169 `"Uzbek"', add
cap label define ancestr2_lbl 171 `"Ukrainian"', add
cap label define ancestr2_lbl 176 `"Yugoslavian"', add
cap label define ancestr2_lbl 178 `"Slav"', add
cap label define ancestr2_lbl 179 `"Slavonian"', add
cap label define ancestr2_lbl 181 `"Central European, nec"', add
cap label define ancestr2_lbl 183 `"Northern European, nec"', add
cap label define ancestr2_lbl 185 `"Southern European, nec"', add
cap label define ancestr2_lbl 187 `"Western European, nec"', add
cap label define ancestr2_lbl 190 `"Eastern European, nec"', add
cap label define ancestr2_lbl 195 `"European, nec"', add
cap label define ancestr2_lbl 200 `"Spaniard"', add
cap label define ancestr2_lbl 201 `"Andalusian"', add
cap label define ancestr2_lbl 202 `"Astorian"', add
cap label define ancestr2_lbl 204 `"Catalonian"', add
cap label define ancestr2_lbl 205 `"Balearic Islander"', add
cap label define ancestr2_lbl 206 `"Galician"', add
cap label define ancestr2_lbl 210 `"Mexican"', add
cap label define ancestr2_lbl 211 `"Mexican American"', add
cap label define ancestr2_lbl 213 `"Chicano/Chicana"', add
cap label define ancestr2_lbl 218 `"Nuevo Mexicano"', add
cap label define ancestr2_lbl 219 `"Californio"', add
cap label define ancestr2_lbl 221 `"Costa Rican"', add
cap label define ancestr2_lbl 222 `"Guatemalan"', add
cap label define ancestr2_lbl 223 `"Honduran"', add
cap label define ancestr2_lbl 224 `"Nicaraguan"', add
cap label define ancestr2_lbl 225 `"Panamanian"', add
cap label define ancestr2_lbl 226 `"Salvadoran"', add
cap label define ancestr2_lbl 227 `"Latin American"', add
cap label define ancestr2_lbl 231 `"Argentinean"', add
cap label define ancestr2_lbl 232 `"Bolivian"', add
cap label define ancestr2_lbl 233 `"Chilean"', add
cap label define ancestr2_lbl 234 `"Colombian"', add
cap label define ancestr2_lbl 235 `"Ecuadorian"', add
cap label define ancestr2_lbl 236 `"Paraguayan"', add
cap label define ancestr2_lbl 237 `"Peruvian"', add
cap label define ancestr2_lbl 238 `"Uruguayan"', add
cap label define ancestr2_lbl 239 `"Venezuelan"', add
cap label define ancestr2_lbl 248 `"South American"', add
cap label define ancestr2_lbl 261 `"Puerto Rican"', add
cap label define ancestr2_lbl 271 `"Cuban"', add
cap label define ancestr2_lbl 275 `"Dominican"', add
cap label define ancestr2_lbl 290 `"Hispanic"', add
cap label define ancestr2_lbl 291 `"Spanish"', add
cap label define ancestr2_lbl 295 `"Spanish American"', add
cap label define ancestr2_lbl 296 `"Other Spanish/Hispanic"', add
cap label define ancestr2_lbl 300 `"Bahamian"', add
cap label define ancestr2_lbl 301 `"Barbadian"', add
cap label define ancestr2_lbl 302 `"Belizean"', add
cap label define ancestr2_lbl 303 `"Bermudan"', add
cap label define ancestr2_lbl 304 `"Cayman Islander"', add
cap label define ancestr2_lbl 308 `"Jamaican"', add
cap label define ancestr2_lbl 310 `"Dutch West Indies"', add
cap label define ancestr2_lbl 311 `"Aruba Islander"', add
cap label define ancestr2_lbl 312 `"St Maarten Islander"', add
cap label define ancestr2_lbl 314 `"Trinidadian/Tobagonian"', add
cap label define ancestr2_lbl 315 `"Trinidadian"', add
cap label define ancestr2_lbl 316 `"Tobagonian"', add
cap label define ancestr2_lbl 317 `"U.S. Virgin Islander"', add
cap label define ancestr2_lbl 321 `"British Virgin Islander"', add
cap label define ancestr2_lbl 322 `"British West Indian"', add
cap label define ancestr2_lbl 323 `"Turks and Caicos Islander"', add
cap label define ancestr2_lbl 324 `"Anguilla Islander"', add
cap label define ancestr2_lbl 328 `"Dominica Islander"', add
cap label define ancestr2_lbl 329 `"Grenadian"', add
cap label define ancestr2_lbl 331 `"St Lucia Islander"', add
cap label define ancestr2_lbl 332 `"French West Indies"', add
cap label define ancestr2_lbl 333 `"Guadeloupe Islander"', add
cap label define ancestr2_lbl 334 `"Cayenne"', add
cap label define ancestr2_lbl 335 `"West Indian"', add
cap label define ancestr2_lbl 336 `"Haitian"', add
cap label define ancestr2_lbl 337 `"Other West Indian"', add
cap label define ancestr2_lbl 360 `"Brazilian"', add
cap label define ancestr2_lbl 365 `"San Andres"', add
cap label define ancestr2_lbl 370 `"Guyanese/British Guiana"', add
cap label define ancestr2_lbl 375 `"Providencia"', add
cap label define ancestr2_lbl 380 `"Surinam/Dutch Guiana"', add
cap label define ancestr2_lbl 400 `"Algerian"', add
cap label define ancestr2_lbl 402 `"Egyptian"', add
cap label define ancestr2_lbl 404 `"Libyan"', add
cap label define ancestr2_lbl 406 `"Moroccan"', add
cap label define ancestr2_lbl 407 `"Ifni"', add
cap label define ancestr2_lbl 408 `"Tunisian"', add
cap label define ancestr2_lbl 411 `"North African"', add
cap label define ancestr2_lbl 412 `"Alhucemas"', add
cap label define ancestr2_lbl 413 `"Berber"', add
cap label define ancestr2_lbl 414 `"Rio de Oro"', add
cap label define ancestr2_lbl 415 `"Bahraini"', add
cap label define ancestr2_lbl 416 `"Iranian"', add
cap label define ancestr2_lbl 417 `"Iraqi"', add
cap label define ancestr2_lbl 419 `"Israeli"', add
cap label define ancestr2_lbl 421 `"Jordanian"', add
cap label define ancestr2_lbl 422 `"Transjordan"', add
cap label define ancestr2_lbl 423 `"Kuwaiti"', add
cap label define ancestr2_lbl 425 `"Lebanese"', add
cap label define ancestr2_lbl 427 `"Saudi Arabian"', add
cap label define ancestr2_lbl 429 `"Syrian"', add
cap label define ancestr2_lbl 431 `"Armenian"', add
cap label define ancestr2_lbl 434 `"Turkish"', add
cap label define ancestr2_lbl 435 `"Yemeni"', add
cap label define ancestr2_lbl 436 `"Omani"', add
cap label define ancestr2_lbl 437 `"Muscat"', add
cap label define ancestr2_lbl 438 `"Trucial Oman"', add
cap label define ancestr2_lbl 439 `"Qatar"', add
cap label define ancestr2_lbl 441 `"Bedouin"', add
cap label define ancestr2_lbl 442 `"Kurdish"', add
cap label define ancestr2_lbl 444 `"Kuria Muria Islander"', add
cap label define ancestr2_lbl 465 `"Palestinian"', add
cap label define ancestr2_lbl 466 `"Gazan"', add
cap label define ancestr2_lbl 467 `"West Bank"', add
cap label define ancestr2_lbl 470 `"South Yemeni"', add
cap label define ancestr2_lbl 471 `"Aden"', add
cap label define ancestr2_lbl 480 `"United Arab Emirates"', add
cap label define ancestr2_lbl 482 `"Assyrian/Chaldean/Syriac"', add
cap label define ancestr2_lbl 490 `"Middle Eastern"', add
cap label define ancestr2_lbl 495 `"Arab"', add
cap label define ancestr2_lbl 496 `"Other Arab"', add
cap label define ancestr2_lbl 500 `"Angolan"', add
cap label define ancestr2_lbl 502 `"Benin"', add
cap label define ancestr2_lbl 504 `"Botswana"', add
cap label define ancestr2_lbl 506 `"Burundian"', add
cap label define ancestr2_lbl 508 `"Cameroonian"', add
cap label define ancestr2_lbl 510 `"Cape Verdean"', add
cap label define ancestr2_lbl 513 `"Chadian"', add
cap label define ancestr2_lbl 515 `"Congolese"', add
cap label define ancestr2_lbl 516 `"Congo-Brazzaville"', add
cap label define ancestr2_lbl 519 `"Djibouti"', add
cap label define ancestr2_lbl 520 `"Equatorial Guinea"', add
cap label define ancestr2_lbl 521 `"Corsico Islander"', add
cap label define ancestr2_lbl 522 `"Ethiopian"', add
cap label define ancestr2_lbl 523 `"Eritrean"', add
cap label define ancestr2_lbl 525 `"Gabonese"', add
cap label define ancestr2_lbl 527 `"Gambian"', add
cap label define ancestr2_lbl 529 `"Ghanian"', add
cap label define ancestr2_lbl 530 `"Guinean"', add
cap label define ancestr2_lbl 531 `"Guinea Bissau"', add
cap label define ancestr2_lbl 532 `"Ivory Coast"', add
cap label define ancestr2_lbl 534 `"Kenyan"', add
cap label define ancestr2_lbl 538 `"Lesotho"', add
cap label define ancestr2_lbl 541 `"Liberian"', add
cap label define ancestr2_lbl 543 `"Madagascan"', add
cap label define ancestr2_lbl 545 `"Malawian"', add
cap label define ancestr2_lbl 546 `"Malian"', add
cap label define ancestr2_lbl 547 `"Mauritanian"', add
cap label define ancestr2_lbl 549 `"Mozambican"', add
cap label define ancestr2_lbl 550 `"Namibian"', add
cap label define ancestr2_lbl 551 `"Niger"', add
cap label define ancestr2_lbl 553 `"Nigerian"', add
cap label define ancestr2_lbl 554 `"Fulani"', add
cap label define ancestr2_lbl 555 `"Hausa"', add
cap label define ancestr2_lbl 556 `"Ibo"', add
cap label define ancestr2_lbl 557 `"Tiv"', add
cap label define ancestr2_lbl 561 `"Rwandan"', add
cap label define ancestr2_lbl 564 `"Senegalese"', add
cap label define ancestr2_lbl 566 `"Sierra Leonean"', add
cap label define ancestr2_lbl 568 `"Somalian"', add
cap label define ancestr2_lbl 569 `"Swaziland"', add
cap label define ancestr2_lbl 570 `"South African"', add
cap label define ancestr2_lbl 571 `"Union of South Africa"', add
cap label define ancestr2_lbl 572 `"Afrikaner"', add
cap label define ancestr2_lbl 573 `"Natalian"', add
cap label define ancestr2_lbl 574 `"Zulu"', add
cap label define ancestr2_lbl 576 `"Sudanese"', add
cap label define ancestr2_lbl 577 `"Dinka"', add
cap label define ancestr2_lbl 578 `"Nuer"', add
cap label define ancestr2_lbl 579 `"Fur"', add
cap label define ancestr2_lbl 580 `"Baggara"', add
cap label define ancestr2_lbl 582 `"Tanzanian"', add
cap label define ancestr2_lbl 583 `"Tanganyikan"', add
cap label define ancestr2_lbl 584 `"Zanzibar Islande"', add
cap label define ancestr2_lbl 586 `"Togo"', add
cap label define ancestr2_lbl 588 `"Ugandan"', add
cap label define ancestr2_lbl 589 `"Upper Voltan"', add
cap label define ancestr2_lbl 590 `"Voltan"', add
cap label define ancestr2_lbl 591 `"Zairian"', add
cap label define ancestr2_lbl 592 `"Zambian"', add
cap label define ancestr2_lbl 593 `"Zimbabwean"', add
cap label define ancestr2_lbl 594 `"African Islands"', add
cap label define ancestr2_lbl 595 `"Other Subsaharan Africa"', add
cap label define ancestr2_lbl 596 `"Central African"', add
cap label define ancestr2_lbl 597 `"East African"', add
cap label define ancestr2_lbl 598 `"West African"', add
cap label define ancestr2_lbl 599 `"African"', add
cap label define ancestr2_lbl 600 `"Afghan"', add
cap label define ancestr2_lbl 601 `"Baluchi"', add
cap label define ancestr2_lbl 602 `"Pathan"', add
cap label define ancestr2_lbl 603 `"Bengali"', add
cap label define ancestr2_lbl 607 `"Bhutanese"', add
cap label define ancestr2_lbl 609 `"Nepali"', add
cap label define ancestr2_lbl 615 `"Asian Indian"', add
cap label define ancestr2_lbl 622 `"Andaman Islander"', add
cap label define ancestr2_lbl 624 `"Andhra Pradesh"', add
cap label define ancestr2_lbl 626 `"Assamese"', add
cap label define ancestr2_lbl 628 `"Goanese"', add
cap label define ancestr2_lbl 630 `"Gujarati"', add
cap label define ancestr2_lbl 632 `"Karnatakan"', add
cap label define ancestr2_lbl 634 `"Keralan"', add
cap label define ancestr2_lbl 638 `"Maharashtran"', add
cap label define ancestr2_lbl 640 `"Madrasi"', add
cap label define ancestr2_lbl 642 `"Mysore"', add
cap label define ancestr2_lbl 644 `"Naga"', add
cap label define ancestr2_lbl 648 `"Pondicherry"', add
cap label define ancestr2_lbl 650 `"Punjabi"', add
cap label define ancestr2_lbl 656 `"Tamil"', add
cap label define ancestr2_lbl 675 `"East Indies"', add
cap label define ancestr2_lbl 680 `"Pakistani"', add
cap label define ancestr2_lbl 690 `"Sri Lankan"', add
cap label define ancestr2_lbl 691 `"Singhalese"', add
cap label define ancestr2_lbl 692 `"Veddah"', add
cap label define ancestr2_lbl 695 `"Maldivian"', add
cap label define ancestr2_lbl 700 `"Burmese"', add
cap label define ancestr2_lbl 702 `"Shan"', add
cap label define ancestr2_lbl 703 `"Cambodian"', add
cap label define ancestr2_lbl 704 `"Khmer"', add
cap label define ancestr2_lbl 706 `"Chinese"', add
cap label define ancestr2_lbl 707 `"Cantonese"', add
cap label define ancestr2_lbl 708 `"Manchurian"', add
cap label define ancestr2_lbl 709 `"Mandarin"', add
cap label define ancestr2_lbl 712 `"Mongolian"', add
cap label define ancestr2_lbl 714 `"Tibetan"', add
cap label define ancestr2_lbl 716 `"Hong Kong"', add
cap label define ancestr2_lbl 718 `"Macao"', add
cap label define ancestr2_lbl 720 `"Filipino"', add
cap label define ancestr2_lbl 730 `"Indonesian"', add
cap label define ancestr2_lbl 740 `"Japanese"', add
cap label define ancestr2_lbl 746 `"Ryukyu Islander"', add
cap label define ancestr2_lbl 748 `"Okinawan"', add
cap label define ancestr2_lbl 750 `"Korean"', add
cap label define ancestr2_lbl 765 `"Laotian"', add
cap label define ancestr2_lbl 766 `"Meo"', add
cap label define ancestr2_lbl 768 `"Hmong"', add
cap label define ancestr2_lbl 770 `"Malaysian"', add
cap label define ancestr2_lbl 774 `"Singaporean"', add
cap label define ancestr2_lbl 776 `"Thai"', add
cap label define ancestr2_lbl 777 `"Black Thai"', add
cap label define ancestr2_lbl 778 `"Western Lao"', add
cap label define ancestr2_lbl 782 `"Taiwanese"', add
cap label define ancestr2_lbl 785 `"Vietnamese"', add
cap label define ancestr2_lbl 786 `"Katu"', add
cap label define ancestr2_lbl 787 `"Ma"', add
cap label define ancestr2_lbl 788 `"Mnong"', add
cap label define ancestr2_lbl 790 `"Montagnard"', add
cap label define ancestr2_lbl 792 `"Indochinese"', add
cap label define ancestr2_lbl 793 `"Eurasian"', add
cap label define ancestr2_lbl 795 `"Asian"', add
cap label define ancestr2_lbl 796 `"Other Asian"', add
cap label define ancestr2_lbl 800 `"Australian"', add
cap label define ancestr2_lbl 801 `"Tasmanian"', add
cap label define ancestr2_lbl 802 `"Australian Aborigine"', add
cap label define ancestr2_lbl 803 `"New Zealander"', add
cap label define ancestr2_lbl 808 `"Polynesian"', add
cap label define ancestr2_lbl 809 `"Kapinagamarangan"', add
cap label define ancestr2_lbl 810 `"Maori"', add
cap label define ancestr2_lbl 811 `"Hawaiian"', add
cap label define ancestr2_lbl 813 `"Part Hawaiian"', add
cap label define ancestr2_lbl 814 `"Samoan"', add
cap label define ancestr2_lbl 815 `"Tongan"', add
cap label define ancestr2_lbl 816 `"Tokelauan"', add
cap label define ancestr2_lbl 817 `"Cook Islander"', add
cap label define ancestr2_lbl 818 `"Tahitian"', add
cap label define ancestr2_lbl 819 `"Niuean"', add
cap label define ancestr2_lbl 820 `"Micronesian"', add
cap label define ancestr2_lbl 821 `"Guamanian"', add
cap label define ancestr2_lbl 822 `"Chamorro Islander"', add
cap label define ancestr2_lbl 823 `"Saipanese"', add
cap label define ancestr2_lbl 824 `"Palauan"', add
cap label define ancestr2_lbl 825 `"Marshall Islander"', add
cap label define ancestr2_lbl 826 `"Kosraean"', add
cap label define ancestr2_lbl 827 `"Ponapean"', add
cap label define ancestr2_lbl 828 `"Chuukese"', add
cap label define ancestr2_lbl 829 `"Yap Islander"', add
cap label define ancestr2_lbl 830 `"Caroline Islander"', add
cap label define ancestr2_lbl 831 `"Kiribatese"', add
cap label define ancestr2_lbl 832 `"Nauruan"', add
cap label define ancestr2_lbl 833 `"Tarawa Islander"', add
cap label define ancestr2_lbl 834 `"Tinian Islander"', add
cap label define ancestr2_lbl 840 `"Melanesian Islander"', add
cap label define ancestr2_lbl 841 `"Fijian"', add
cap label define ancestr2_lbl 843 `"New Guinean"', add
cap label define ancestr2_lbl 844 `"Papuan"', add
cap label define ancestr2_lbl 845 `"Solomon Islander"', add
cap label define ancestr2_lbl 846 `"New Caledonian Islander"', add
cap label define ancestr2_lbl 847 `"Vanuatuan"', add
cap label define ancestr2_lbl 850 `"Pacific Islander"', add
cap label define ancestr2_lbl 860 `"Oceania"', add
cap label define ancestr2_lbl 862 `"Chamolinian"', add
cap label define ancestr2_lbl 863 `"Reserved Codes"', add
cap label define ancestr2_lbl 870 `"Other Pacific"', add
cap label define ancestr2_lbl 900 `"Afro-American"', add
cap label define ancestr2_lbl 902 `"African-American"', add
cap label define ancestr2_lbl 913 `"Central American Indian"', add
cap label define ancestr2_lbl 914 `"South American Indian"', add
cap label define ancestr2_lbl 920 `"American Indian  (all tribes)"', add
cap label define ancestr2_lbl 921 `"Aleut"', add
cap label define ancestr2_lbl 922 `"Eskimo"', add
cap label define ancestr2_lbl 923 `"Inuit"', add
cap label define ancestr2_lbl 924 `"White/Caucasian"', add
cap label define ancestr2_lbl 930 `"Greenlander"', add
cap label define ancestr2_lbl 931 `"Canadian (most provinces)"', add
cap label define ancestr2_lbl 933 `"Newfoundland"', add
cap label define ancestr2_lbl 934 `"Nova Scotian"', add
cap label define ancestr2_lbl 935 `"French Canadian"', add
cap label define ancestr2_lbl 936 `"Acadian"', add
cap label define ancestr2_lbl 939 `"American"', add
cap label define ancestr2_lbl 940 `"United States"', add
cap label define ancestr2_lbl 941 `"Alabama"', add
cap label define ancestr2_lbl 942 `"Alaska"', add
cap label define ancestr2_lbl 943 `"Arizona"', add
cap label define ancestr2_lbl 944 `"Arkansas"', add
cap label define ancestr2_lbl 945 `"California"', add
cap label define ancestr2_lbl 946 `"Colorado"', add
cap label define ancestr2_lbl 947 `"Connecticut"', add
cap label define ancestr2_lbl 948 `"District of Columbia"', add
cap label define ancestr2_lbl 949 `"Delaware"', add
cap label define ancestr2_lbl 950 `"Florida"', add
cap label define ancestr2_lbl 951 `"Georgia"', add
cap label define ancestr2_lbl 952 `"Idaho"', add
cap label define ancestr2_lbl 953 `"Illinois"', add
cap label define ancestr2_lbl 954 `"Indiana"', add
cap label define ancestr2_lbl 955 `"Iowa"', add
cap label define ancestr2_lbl 956 `"Kansas"', add
cap label define ancestr2_lbl 957 `"Kentucky"', add
cap label define ancestr2_lbl 958 `"Louisiana"', add
cap label define ancestr2_lbl 959 `"Maine"', add
cap label define ancestr2_lbl 960 `"Maryland"', add
cap label define ancestr2_lbl 961 `"Massachusetts"', add
cap label define ancestr2_lbl 962 `"Michigan"', add
cap label define ancestr2_lbl 963 `"Minnesota"', add
cap label define ancestr2_lbl 964 `"Mississippi"', add
cap label define ancestr2_lbl 965 `"Missouri"', add
cap label define ancestr2_lbl 966 `"Montana"', add
cap label define ancestr2_lbl 967 `"Nebraska"', add
cap label define ancestr2_lbl 968 `"Nevada"', add
cap label define ancestr2_lbl 969 `"New Hampshire"', add
cap label define ancestr2_lbl 970 `"New Jersey"', add
cap label define ancestr2_lbl 971 `"New Mexico"', add
cap label define ancestr2_lbl 972 `"New York"', add
cap label define ancestr2_lbl 973 `"North Carolina"', add
cap label define ancestr2_lbl 974 `"North Dakota"', add
cap label define ancestr2_lbl 975 `"Ohio"', add
cap label define ancestr2_lbl 976 `"Oklahoma"', add
cap label define ancestr2_lbl 977 `"Oregon"', add
cap label define ancestr2_lbl 978 `"Pennsylvania"', add
cap label define ancestr2_lbl 979 `"Rhode Island"', add
cap label define ancestr2_lbl 980 `"South Carolina"', add
cap label define ancestr2_lbl 981 `"South Dakota"', add
cap label define ancestr2_lbl 982 `"Tennessee"', add
cap label define ancestr2_lbl 983 `"Texas"', add
cap label define ancestr2_lbl 984 `"Utah"', add
cap label define ancestr2_lbl 985 `"Vermont"', add
cap label define ancestr2_lbl 986 `"Virginia"', add
cap label define ancestr2_lbl 987 `"Washington"', add
cap label define ancestr2_lbl 988 `"West Virginia"', add
cap label define ancestr2_lbl 989 `"Wisconsin"', add
cap label define ancestr2_lbl 990 `"Wyoming"', add
cap label define ancestr2_lbl 993 `"Southerner"', add
cap label define ancestr2_lbl 994 `"North American"', add
cap label define ancestr2_lbl 995 `"Mixture"', add
cap label define ancestr2_lbl 996 `"Uncodable"', add
cap label define ancestr2_lbl 997 `"Deferred Cases"', add
cap label define ancestr2_lbl 998 `"Other (Usually a Religion)"', add
cap label define ancestr2_lbl 999 `"Not Reported"', add
cap label values ancestr2 ancestr2_lbl

cap label define ancestr2d_lbl 0010 `"Alsatian"'
cap label define ancestr2d_lbl 0020 `"Andorran"', add
cap label define ancestr2d_lbl 0030 `"Austrian"', add
cap label define ancestr2d_lbl 0040 `"Tirolean"', add
cap label define ancestr2d_lbl 0051 `"Basque (1980)"', add
cap label define ancestr2d_lbl 0052 `"Spanish Basque (1980)"', add
cap label define ancestr2d_lbl 0053 `"Basque (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 0054 `"Spanish Basque (1990-2000, 2001-2004 ACS)"', add
cap label define ancestr2d_lbl 0060 `"French Basque"', add
cap label define ancestr2d_lbl 0080 `"Belgian"', add
cap label define ancestr2d_lbl 0090 `"Flemish"', add
cap label define ancestr2d_lbl 0100 `"Walloon"', add
cap label define ancestr2d_lbl 0110 `"British"', add
cap label define ancestr2d_lbl 0120 `"British Isles"', add
cap label define ancestr2d_lbl 0130 `"Channel Islander"', add
cap label define ancestr2d_lbl 0140 `"Gibraltan"', add
cap label define ancestr2d_lbl 0150 `"Cornish"', add
cap label define ancestr2d_lbl 0160 `"Corsican"', add
cap label define ancestr2d_lbl 0170 `"Cypriot"', add
cap label define ancestr2d_lbl 0180 `"Greek Cypriote"', add
cap label define ancestr2d_lbl 0190 `"Turkish Cypriote"', add
cap label define ancestr2d_lbl 0200 `"Danish"', add
cap label define ancestr2d_lbl 0210 `"Dutch"', add
cap label define ancestr2d_lbl 0211 `"Dutch-French-Irish"', add
cap label define ancestr2d_lbl 0212 `"Dutch-German-Irish"', add
cap label define ancestr2d_lbl 0213 `"Dutch-Irish-Scotch"', add
cap label define ancestr2d_lbl 0220 `"English"', add
cap label define ancestr2d_lbl 0221 `"English-French-German (1980)"', add
cap label define ancestr2d_lbl 0222 `"English-French-Irish (1980)"', add
cap label define ancestr2d_lbl 0223 `"English-German-Irish (1980)"', add
cap label define ancestr2d_lbl 0224 `"English-German-Swedish (1980)"', add
cap label define ancestr2d_lbl 0225 `"English-Irish-Scotch (1980)"', add
cap label define ancestr2d_lbl 0226 `"English-Scotch-Welsh (1980)"', add
cap label define ancestr2d_lbl 0230 `"Faeroe Islander"', add
cap label define ancestr2d_lbl 0240 `"Finnish"', add
cap label define ancestr2d_lbl 0250 `"Karelian"', add
cap label define ancestr2d_lbl 0260 `"French (1980)"', add
cap label define ancestr2d_lbl 0261 `"French (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 0262 `"Occitan (1990-2000)"', add
cap label define ancestr2d_lbl 0270 `"Lorrainian"', add
cap label define ancestr2d_lbl 0280 `"Breton"', add
cap label define ancestr2d_lbl 0290 `"Frisian"', add
cap label define ancestr2d_lbl 0300 `"Friulian"', add
cap label define ancestr2d_lbl 0320 `"German (1980)"', add
cap label define ancestr2d_lbl 0321 `"German (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 0322 `"Pennsylvania German (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 0323 `"East German (1990-2000)"', add
cap label define ancestr2d_lbl 0324 `"West German (2000)"', add
cap label define ancestr2d_lbl 0325 `"German-French-Irish (1980)"', add
cap label define ancestr2d_lbl 0326 `"German-Irish-Italian (1980)"', add
cap label define ancestr2d_lbl 0327 `"German-Irish-Scotch (1980)"', add
cap label define ancestr2d_lbl 0328 `"German-Irish-Swedish (1980)"', add
cap label define ancestr2d_lbl 0329 `"Germanic"', add
cap label define ancestr2d_lbl 0330 `"Bavarian"', add
cap label define ancestr2d_lbl 0340 `"Berliner"', add
cap label define ancestr2d_lbl 0350 `"Hamburger"', add
cap label define ancestr2d_lbl 0360 `"Hanoverian"', add
cap label define ancestr2d_lbl 0370 `"Hessian"', add
cap label define ancestr2d_lbl 0380 `"Lubecker"', add
cap label define ancestr2d_lbl 0390 `"Pomeranian (1980)"', add
cap label define ancestr2d_lbl 0391 `"Pomeranian (1990-2000)"', add
cap label define ancestr2d_lbl 0392 `"Silesian (1990-2000)"', add
cap label define ancestr2d_lbl 0400 `"Prussian"', add
cap label define ancestr2d_lbl 0410 `"Saxon"', add
cap label define ancestr2d_lbl 0420 `"Sudetenlander"', add
cap label define ancestr2d_lbl 0430 `"Westphalian"', add
cap label define ancestr2d_lbl 0460 `"Greek"', add
cap label define ancestr2d_lbl 0470 `"Cretan"', add
cap label define ancestr2d_lbl 0480 `"Cycladic Islander"', add
cap label define ancestr2d_lbl 0490 `"Icelander"', add
cap label define ancestr2d_lbl 0500 `"Irish"', add
cap label define ancestr2d_lbl 0501 `"Celtic"', add
cap label define ancestr2d_lbl 0502 `"Irish Scotch"', add
cap label define ancestr2d_lbl 0510 `"Italian (1980)"', add
cap label define ancestr2d_lbl 0511 `"Italian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 0512 `"Trieste (1990-2000)"', add
cap label define ancestr2d_lbl 0513 `"San Marino (1990-2000)"', add
cap label define ancestr2d_lbl 0530 `"Abruzzi"', add
cap label define ancestr2d_lbl 0540 `"Apulian"', add
cap label define ancestr2d_lbl 0550 `"Basilicata"', add
cap label define ancestr2d_lbl 0560 `"Calabrian"', add
cap label define ancestr2d_lbl 0570 `"Amalfi"', add
cap label define ancestr2d_lbl 0580 `"Emilia Romagna"', add
cap label define ancestr2d_lbl 0590 `"Rome"', add
cap label define ancestr2d_lbl 0600 `"Ligurian"', add
cap label define ancestr2d_lbl 0610 `"Lombardian"', add
cap label define ancestr2d_lbl 0620 `"Marches"', add
cap label define ancestr2d_lbl 0630 `"Molise"', add
cap label define ancestr2d_lbl 0640 `"Neapolitan"', add
cap label define ancestr2d_lbl 0650 `"Piedmontese"', add
cap label define ancestr2d_lbl 0660 `"Puglia"', add
cap label define ancestr2d_lbl 0670 `"Sardinian"', add
cap label define ancestr2d_lbl 0680 `"Sicilian"', add
cap label define ancestr2d_lbl 0690 `"Toscana"', add
cap label define ancestr2d_lbl 0700 `"Trentino"', add
cap label define ancestr2d_lbl 0710 `"Umbrian"', add
cap label define ancestr2d_lbl 0720 `"Valle dAosta"', add
cap label define ancestr2d_lbl 0730 `"Venetian"', add
cap label define ancestr2d_lbl 0750 `"Lapp"', add
cap label define ancestr2d_lbl 0760 `"Liechtensteiner"', add
cap label define ancestr2d_lbl 0770 `"Luxemburger"', add
cap label define ancestr2d_lbl 0780 `"Maltese"', add
cap label define ancestr2d_lbl 0790 `"Manx"', add
cap label define ancestr2d_lbl 0800 `"Monegasque"', add
cap label define ancestr2d_lbl 0810 `"Northern Irelander"', add
cap label define ancestr2d_lbl 0820 `"Norwegian"', add
cap label define ancestr2d_lbl 0840 `"Portuguese"', add
cap label define ancestr2d_lbl 0850 `"Azorean"', add
cap label define ancestr2d_lbl 0860 `"Madeiran"', add
cap label define ancestr2d_lbl 0870 `"Scotch Irish"', add
cap label define ancestr2d_lbl 0880 `"Scottish"', add
cap label define ancestr2d_lbl 0890 `"Swedish"', add
cap label define ancestr2d_lbl 0900 `"Aland Islander"', add
cap label define ancestr2d_lbl 0910 `"Swiss"', add
cap label define ancestr2d_lbl 0920 `"Suisse (1980)"', add
cap label define ancestr2d_lbl 0921 `"Suisse (1990-2000)"', add
cap label define ancestr2d_lbl 0922 `"Switzer (1990-2000)"', add
cap label define ancestr2d_lbl 0950 `"Romansch (1980)"', add
cap label define ancestr2d_lbl 0951 `"Romanscho (1990-2000)"', add
cap label define ancestr2d_lbl 0952 `"Ladin (1990-2000)"', add
cap label define ancestr2d_lbl 0960 `"Suisse Romane (1990-2000)"', add
cap label define ancestr2d_lbl 0961 `"Suisse Romane (1980)"', add
cap label define ancestr2d_lbl 0962 `"Ticino"', add
cap label define ancestr2d_lbl 0970 `"Welsh"', add
cap label define ancestr2d_lbl 0980 `"Scandinavian, Nordic"', add
cap label define ancestr2d_lbl 1000 `"Albanian"', add
cap label define ancestr2d_lbl 1010 `"Azerbaijani"', add
cap label define ancestr2d_lbl 1020 `"Belorussian"', add
cap label define ancestr2d_lbl 1030 `"Bulgarian"', add
cap label define ancestr2d_lbl 1050 `"Carpathian"', add
cap label define ancestr2d_lbl 1051 `"Carpatho Rusyn"', add
cap label define ancestr2d_lbl 1052 `"Rusyn"', add
cap label define ancestr2d_lbl 1080 `"Cossack (1990-2000)"', add
cap label define ancestr2d_lbl 1081 `"Cossack (1980)"', add
cap label define ancestr2d_lbl 1082 `"Turkestani (1990-2000, 2012 ACS)"', add
cap label define ancestr2d_lbl 1083 `"Kirghiz (1980)"', add
cap label define ancestr2d_lbl 1084 `"Turcoman (1980)"', add
cap label define ancestr2d_lbl 1090 `"Croatian"', add
cap label define ancestr2d_lbl 1110 `"Czechoslovakian"', add
cap label define ancestr2d_lbl 1111 `"Czech"', add
cap label define ancestr2d_lbl 1120 `"Bohemian (1980)"', add
cap label define ancestr2d_lbl 1121 `"Bohemian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 1122 `"Moravian (1990-2000)"', add
cap label define ancestr2d_lbl 1150 `"Estonian"', add
cap label define ancestr2d_lbl 1160 `"Livonian"', add
cap label define ancestr2d_lbl 1170 `"Finno Ugrian (1990-2000)"', add
cap label define ancestr2d_lbl 1171 `"Udmert"', add
cap label define ancestr2d_lbl 1180 `"Mordovian"', add
cap label define ancestr2d_lbl 1190 `"Voytak"', add
cap label define ancestr2d_lbl 1200 `"Georgian"', add
cap label define ancestr2d_lbl 1220 `"Germans from Russia"', add
cap label define ancestr2d_lbl 1221 `"Volga"', add
cap label define ancestr2d_lbl 1222 `"German from Russia (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 1230 `"Gruziia (1990-2000)"', add
cap label define ancestr2d_lbl 1240 `"Rom"', add
cap label define ancestr2d_lbl 1250 `"Hungarian"', add
cap label define ancestr2d_lbl 1260 `"Magyar"', add
cap label define ancestr2d_lbl 1280 `"Latvian"', add
cap label define ancestr2d_lbl 1290 `"Lithuanian"', add
cap label define ancestr2d_lbl 1300 `"Macedonian"', add
cap label define ancestr2d_lbl 1320 `"North Caucasian (1990-2000)"', add
cap label define ancestr2d_lbl 1330 `"North Caucasian Turkic (1990-2000)"', add
cap label define ancestr2d_lbl 1400 `"Ossetian"', add
cap label define ancestr2d_lbl 1420 `"Polish"', add
cap label define ancestr2d_lbl 1430 `"Kashubian"', add
cap label define ancestr2d_lbl 1440 `"Romanian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 1441 `"Romanian (1980)"', add
cap label define ancestr2d_lbl 1442 `"Transylvanian"', add
cap label define ancestr2d_lbl 1450 `"Bessarabian (1980)"', add
cap label define ancestr2d_lbl 1451 `"Bessarabian (1990-2000)"', add
cap label define ancestr2d_lbl 1452 `"Bucovina"', add
cap label define ancestr2d_lbl 1460 `"Moldavian"', add
cap label define ancestr2d_lbl 1470 `"Wallachian"', add
cap label define ancestr2d_lbl 1480 `"Russian"', add
cap label define ancestr2d_lbl 1500 `"Muscovite"', add
cap label define ancestr2d_lbl 1520 `"Serbian (1980)"', add
cap label define ancestr2d_lbl 1521 `"Serbian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 1522 `"Bosnian (1990) Herzegovinian (2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 1523 `"Montenegrin (1990-2000, 2012 ACS)"', add
cap label define ancestr2d_lbl 1530 `"Slovak"', add
cap label define ancestr2d_lbl 1540 `"Slovene"', add
cap label define ancestr2d_lbl 1550 `"Sorb/Wend"', add
cap label define ancestr2d_lbl 1560 `"Soviet Turkic (1990-2000)"', add
cap label define ancestr2d_lbl 1570 `"Bashkir"', add
cap label define ancestr2d_lbl 1580 `"Chevash"', add
cap label define ancestr2d_lbl 1590 `"Gagauz (1990-2000)"', add
cap label define ancestr2d_lbl 1600 `"Mesknetian (1990-2000)"', add
cap label define ancestr2d_lbl 1630 `"Yakut"', add
cap label define ancestr2d_lbl 1640 `"Soviet Union, nec"', add
cap label define ancestr2d_lbl 1650 `"Tatar (1990-2000)"', add
cap label define ancestr2d_lbl 1651 `"Tatar (1980)"', add
cap label define ancestr2d_lbl 1652 `"Crimean (1980)"', add
cap label define ancestr2d_lbl 1653 `"Tuvinian (1990-2000)"', add
cap label define ancestr2d_lbl 1654 `"Soviet Central Asian (1990-2000)"', add
cap label define ancestr2d_lbl 1655 `"Tadzhik (1980, 2000)"', add
cap label define ancestr2d_lbl 1690 `"Uzbek"', add
cap label define ancestr2d_lbl 1710 `"Ukrainian (1980)"', add
cap label define ancestr2d_lbl 1711 `"Ukrainian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 1712 `"Ruthenian (1980)"', add
cap label define ancestr2d_lbl 1713 `"Ruthenian (1990-2000)"', add
cap label define ancestr2d_lbl 1714 `"Lemko"', add
cap label define ancestr2d_lbl 1715 `"Bioko"', add
cap label define ancestr2d_lbl 1716 `"Hesel"', add
cap label define ancestr2d_lbl 1717 `"Windish"', add
cap label define ancestr2d_lbl 1760 `"Yugoslavian"', add
cap label define ancestr2d_lbl 1780 `"Slav"', add
cap label define ancestr2d_lbl 1790 `"Slavonian"', add
cap label define ancestr2d_lbl 1810 `"Central European, nec"', add
cap label define ancestr2d_lbl 1830 `"Northern European, nec"', add
cap label define ancestr2d_lbl 1850 `"Southern European, nec"', add
cap label define ancestr2d_lbl 1870 `"Western European, nec"', add
cap label define ancestr2d_lbl 1900 `"Eastern European, nec"', add
cap label define ancestr2d_lbl 1950 `"European, nec"', add
cap label define ancestr2d_lbl 2000 `"Spaniard (1980)"', add
cap label define ancestr2d_lbl 2001 `"Spaniard (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2002 `"Castillian (1990-2000)"', add
cap label define ancestr2d_lbl 2003 `"Valencian (1990-2000)"', add
cap label define ancestr2d_lbl 2010 `"Andalusian (1990-2000)"', add
cap label define ancestr2d_lbl 2020 `"Asturian (1990-2000)"', add
cap label define ancestr2d_lbl 2040 `"Catalonian"', add
cap label define ancestr2d_lbl 2050 `"Balearic Islander (1980)"', add
cap label define ancestr2d_lbl 2051 `"Balearic Islander (1990-2000)"', add
cap label define ancestr2d_lbl 2052 `"Canary Islander (1990-2000)"', add
cap label define ancestr2d_lbl 2060 `"Galician (1980)"', add
cap label define ancestr2d_lbl 2061 `"Gallego (1990-2000)"', add
cap label define ancestr2d_lbl 2062 `"Galician (1990-2000)"', add
cap label define ancestr2d_lbl 2100 `"Mexican"', add
cap label define ancestr2d_lbl 2101 `"Mexican (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2102 `"Mexicano/Mexicana (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2103 `"Mexican Indian"', add
cap label define ancestr2d_lbl 2110 `"Mexican American"', add
cap label define ancestr2d_lbl 2111 `"Mexican American Indian"', add
cap label define ancestr2d_lbl 2130 `"Chicano/Chicana"', add
cap label define ancestr2d_lbl 2180 `"Nuevo Mexicano"', add
cap label define ancestr2d_lbl 2181 `"Nuevo Mexicano (1990-2000)"', add
cap label define ancestr2d_lbl 2182 `"La Raza (1990-2000)"', add
cap label define ancestr2d_lbl 2183 `"Mexican state (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2184 `"Tejano/Tejana (1990-2000)"', add
cap label define ancestr2d_lbl 2190 `"Californio"', add
cap label define ancestr2d_lbl 2210 `"Costa Rican"', add
cap label define ancestr2d_lbl 2220 `"Guatemalan"', add
cap label define ancestr2d_lbl 2230 `"Honduran"', add
cap label define ancestr2d_lbl 2240 `"Nicaraguan"', add
cap label define ancestr2d_lbl 2250 `"Panamanian (1980)"', add
cap label define ancestr2d_lbl 2251 `"Panamanian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2252 `"Canal Zone (1990-2000)"', add
cap label define ancestr2d_lbl 2260 `"Salvadoran"', add
cap label define ancestr2d_lbl 2270 `"Latin American (1980)"', add
cap label define ancestr2d_lbl 2271 `"Central American (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2272 `"Latin American (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2273 `"Latino/Latina (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2274 `"Latin (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2310 `"Argentinean"', add
cap label define ancestr2d_lbl 2320 `"Bolivian"', add
cap label define ancestr2d_lbl 2330 `"Chilean"', add
cap label define ancestr2d_lbl 2340 `"Colombian"', add
cap label define ancestr2d_lbl 2350 `"Ecuadorian"', add
cap label define ancestr2d_lbl 2360 `"Paraguayan"', add
cap label define ancestr2d_lbl 2370 `"Peruvian"', add
cap label define ancestr2d_lbl 2380 `"Uruguayan"', add
cap label define ancestr2d_lbl 2390 `"Venezuelan"', add
cap label define ancestr2d_lbl 2480 `"South American (1980)"', add
cap label define ancestr2d_lbl 2481 `"South American (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 2482 `"Criollo/Criolla (1990-2000)"', add
cap label define ancestr2d_lbl 2610 `"Puerto Rican"', add
cap label define ancestr2d_lbl 2710 `"Cuban"', add
cap label define ancestr2d_lbl 2750 `"Dominican"', add
cap label define ancestr2d_lbl 2900 `"Hispanic"', add
cap label define ancestr2d_lbl 2910 `"Spanish"', add
cap label define ancestr2d_lbl 2950 `"Spanish American"', add
cap label define ancestr2d_lbl 2960 `"Other Spanish/Hispanic"', add
cap label define ancestr2d_lbl 3000 `"Bahamian"', add
cap label define ancestr2d_lbl 3010 `"Barbadian"', add
cap label define ancestr2d_lbl 3020 `"Belizean"', add
cap label define ancestr2d_lbl 3030 `"Bermudan"', add
cap label define ancestr2d_lbl 3040 `"Cayman Islander"', add
cap label define ancestr2d_lbl 3080 `"Jamaican"', add
cap label define ancestr2d_lbl 3100 `"Dutch West Indies"', add
cap label define ancestr2d_lbl 3110 `"Aruba Islander"', add
cap label define ancestr2d_lbl 3120 `"St Maarten Islander"', add
cap label define ancestr2d_lbl 3140 `"Trinidadian/Tobagonian"', add
cap label define ancestr2d_lbl 3150 `"Trinidadian"', add
cap label define ancestr2d_lbl 3160 `"Tobagonian"', add
cap label define ancestr2d_lbl 3170 `"U.S. Virgin Islander (1980)"', add
cap label define ancestr2d_lbl 3171 `"U.S. Virgin Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3172 `"St. Croix Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3173 `"St. John Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3174 `"St. Thomas Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3210 `"British Virgin Islander (1980)"', add
cap label define ancestr2d_lbl 3211 `"British Virgin Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3212 `"Antigua (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 3220 `"British West Indian"', add
cap label define ancestr2d_lbl 3230 `"Turks and Caicos Islander"', add
cap label define ancestr2d_lbl 3240 `"Anguilla Islander (1980)"', add
cap label define ancestr2d_lbl 3241 `"Anguilla Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3242 `"Montserrat Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3243 `"Kitts/Nevis Islander (1990-2000)"', add
cap label define ancestr2d_lbl 3244 `"St. Christopher (1980)"', add
cap label define ancestr2d_lbl 3245 `"St Vincent Islander"', add
cap label define ancestr2d_lbl 3280 `"Dominica Islander"', add
cap label define ancestr2d_lbl 3290 `"Grenadian"', add
cap label define ancestr2d_lbl 3310 `"St Lucia Islander"', add
cap label define ancestr2d_lbl 3320 `"French West Indies"', add
cap label define ancestr2d_lbl 3330 `"Guadeloupe Islander"', add
cap label define ancestr2d_lbl 3340 `"Cayenne"', add
cap label define ancestr2d_lbl 3350 `"West Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 3351 `"West Indian (1980)"', add
cap label define ancestr2d_lbl 3352 `"Caribbean (1980)"', add
cap label define ancestr2d_lbl 3353 `"Arawak (1980)"', add
cap label define ancestr2d_lbl 3360 `"Haitian"', add
cap label define ancestr2d_lbl 3370 `"Other West Indian"', add
cap label define ancestr2d_lbl 3600 `"Brazilian"', add
cap label define ancestr2d_lbl 3650 `"San Andres"', add
cap label define ancestr2d_lbl 3700 `"Guyanese/British Guiana"', add
cap label define ancestr2d_lbl 3750 `"Providencia"', add
cap label define ancestr2d_lbl 3800 `"Surinam/Dutch Guiana"', add
cap label define ancestr2d_lbl 4000 `"Algerian"', add
cap label define ancestr2d_lbl 4020 `"Egyptian"', add
cap label define ancestr2d_lbl 4040 `"Libyan"', add
cap label define ancestr2d_lbl 4060 `"Moroccan (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 4061 `"Moroccan (1980)"', add
cap label define ancestr2d_lbl 4062 `"Moor (1980)"', add
cap label define ancestr2d_lbl 4070 `"Ifni"', add
cap label define ancestr2d_lbl 4080 `"Tunisian"', add
cap label define ancestr2d_lbl 4110 `"North African"', add
cap label define ancestr2d_lbl 4120 `"Alhucemas"', add
cap label define ancestr2d_lbl 4130 `"Berber"', add
cap label define ancestr2d_lbl 4140 `"Rio de Oro"', add
cap label define ancestr2d_lbl 4150 `"Bahraini"', add
cap label define ancestr2d_lbl 4160 `"Iranian"', add
cap label define ancestr2d_lbl 4170 `"Iraqi"', add
cap label define ancestr2d_lbl 4190 `"Israeli"', add
cap label define ancestr2d_lbl 4210 `"Jordanian"', add
cap label define ancestr2d_lbl 4220 `"Transjordan"', add
cap label define ancestr2d_lbl 4230 `"Kuwaiti"', add
cap label define ancestr2d_lbl 4250 `"Lebanese"', add
cap label define ancestr2d_lbl 4270 `"Saudi Arabian"', add
cap label define ancestr2d_lbl 4290 `"Syrian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 4291 `"Syrian (1980)"', add
cap label define ancestr2d_lbl 4292 `"Latakian (1980)"', add
cap label define ancestr2d_lbl 4293 `"Jebel Druse (1980)"', add
cap label define ancestr2d_lbl 4310 `"Armenian"', add
cap label define ancestr2d_lbl 4340 `"Turkish"', add
cap label define ancestr2d_lbl 4350 `"Yemeni"', add
cap label define ancestr2d_lbl 4360 `"Omani"', add
cap label define ancestr2d_lbl 4370 `"Muscat"', add
cap label define ancestr2d_lbl 4380 `"Trucial Oman"', add
cap label define ancestr2d_lbl 4390 `"Qatar"', add
cap label define ancestr2d_lbl 4410 `"Bedouin"', add
cap label define ancestr2d_lbl 4420 `"Kurdish"', add
cap label define ancestr2d_lbl 4440 `"Kuria Muria Islander"', add
cap label define ancestr2d_lbl 4650 `"Palestinian"', add
cap label define ancestr2d_lbl 4660 `"Gazan"', add
cap label define ancestr2d_lbl 4670 `"West Bank"', add
cap label define ancestr2d_lbl 4700 `"South Yemeni"', add
cap label define ancestr2d_lbl 4710 `"Aden"', add
cap label define ancestr2d_lbl 4800 `"United Arab Emirates"', add
cap label define ancestr2d_lbl 4820 `"Assyrian/Chaldean/Syriac (1990-2000)"', add
cap label define ancestr2d_lbl 4821 `"Assyrian"', add
cap label define ancestr2d_lbl 4822 `"Syriac (1980, 2000)"', add
cap label define ancestr2d_lbl 4823 `"Chaldean (2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 4900 `"Middle Eastern"', add
cap label define ancestr2d_lbl 4950 `"Arab"', add
cap label define ancestr2d_lbl 4951 `"Arabic (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 4960 `"Other Arab"', add
cap label define ancestr2d_lbl 5000 `"Angolan"', add
cap label define ancestr2d_lbl 5020 `"Benin"', add
cap label define ancestr2d_lbl 5040 `"Botswana"', add
cap label define ancestr2d_lbl 5060 `"Burundian"', add
cap label define ancestr2d_lbl 5080 `"Cameroonian"', add
cap label define ancestr2d_lbl 5100 `"Cape Verdean"', add
cap label define ancestr2d_lbl 5120 `"Central African Republic"', add
cap label define ancestr2d_lbl 5130 `"Chadian"', add
cap label define ancestr2d_lbl 5150 `"Congolese"', add
cap label define ancestr2d_lbl 5160 `"Congo-Brazzaville"', add
cap label define ancestr2d_lbl 5190 `"Djibouti"', add
cap label define ancestr2d_lbl 5200 `"Equatorial Guinea"', add
cap label define ancestr2d_lbl 5210 `"Corsico Islander"', add
cap label define ancestr2d_lbl 5220 `"Ethiopian"', add
cap label define ancestr2d_lbl 5230 `"Eritrean"', add
cap label define ancestr2d_lbl 5250 `"Gabonese"', add
cap label define ancestr2d_lbl 5270 `"Gambian"', add
cap label define ancestr2d_lbl 5290 `"Ghanian"', add
cap label define ancestr2d_lbl 5300 `"Guinean"', add
cap label define ancestr2d_lbl 5310 `"Guinea Bissau"', add
cap label define ancestr2d_lbl 5320 `"Ivory Coast"', add
cap label define ancestr2d_lbl 5340 `"Kenyan"', add
cap label define ancestr2d_lbl 5380 `"Lesotho"', add
cap label define ancestr2d_lbl 5410 `"Liberian"', add
cap label define ancestr2d_lbl 5430 `"Madagascan"', add
cap label define ancestr2d_lbl 5450 `"Malawian"', add
cap label define ancestr2d_lbl 5460 `"Malian"', add
cap label define ancestr2d_lbl 5470 `"Mauritanian"', add
cap label define ancestr2d_lbl 5490 `"Mozambican"', add
cap label define ancestr2d_lbl 5500 `"Namibian"', add
cap label define ancestr2d_lbl 5510 `"Niger"', add
cap label define ancestr2d_lbl 5530 `"Nigerian"', add
cap label define ancestr2d_lbl 5540 `"Fulani"', add
cap label define ancestr2d_lbl 5550 `"Hausa"', add
cap label define ancestr2d_lbl 5560 `"Ibo"', add
cap label define ancestr2d_lbl 5570 `"Tiv (1980)"', add
cap label define ancestr2d_lbl 5571 `"Tiv (1990-2000)"', add
cap label define ancestr2d_lbl 5572 `"Yoruba (1990-2000)"', add
cap label define ancestr2d_lbl 5610 `"Rwandan"', add
cap label define ancestr2d_lbl 5640 `"Senegalese"', add
cap label define ancestr2d_lbl 5660 `"Sierra Leonean"', add
cap label define ancestr2d_lbl 5680 `"Somalian"', add
cap label define ancestr2d_lbl 5690 `"Swaziland"', add
cap label define ancestr2d_lbl 5700 `"South African"', add
cap label define ancestr2d_lbl 5710 `"Union of South Africa"', add
cap label define ancestr2d_lbl 5720 `"Afrikaner"', add
cap label define ancestr2d_lbl 5730 `"Natalian"', add
cap label define ancestr2d_lbl 5740 `"Zulu"', add
cap label define ancestr2d_lbl 5760 `"Sudanese"', add
cap label define ancestr2d_lbl 5770 `"Dinka"', add
cap label define ancestr2d_lbl 5780 `"Nuer"', add
cap label define ancestr2d_lbl 5790 `"Fur"', add
cap label define ancestr2d_lbl 5800 `"Baggara"', add
cap label define ancestr2d_lbl 5820 `"Tanzanian"', add
cap label define ancestr2d_lbl 5830 `"Tanganyikan"', add
cap label define ancestr2d_lbl 5840 `"Zanzibar"', add
cap label define ancestr2d_lbl 5860 `"Togo"', add
cap label define ancestr2d_lbl 5880 `"Ugandan"', add
cap label define ancestr2d_lbl 5890 `"Upper Voltan"', add
cap label define ancestr2d_lbl 5900 `"Voltan"', add
cap label define ancestr2d_lbl 5910 `"Zairian"', add
cap label define ancestr2d_lbl 5920 `"Zambian"', add
cap label define ancestr2d_lbl 5930 `"Zimbabwean"', add
cap label define ancestr2d_lbl 5940 `"African Islands (1980)"', add
cap label define ancestr2d_lbl 5941 `"African Islands (1990-2000)"', add
cap label define ancestr2d_lbl 5942 `"Mauritius (1990-2000)"', add
cap label define ancestr2d_lbl 5950 `"Other Subsaharan Africa"', add
cap label define ancestr2d_lbl 5960 `"Central African"', add
cap label define ancestr2d_lbl 5970 `"East African"', add
cap label define ancestr2d_lbl 5980 `"West African"', add
cap label define ancestr2d_lbl 5990 `"African"', add
cap label define ancestr2d_lbl 6000 `"Afghan"', add
cap label define ancestr2d_lbl 6010 `"Baluchi"', add
cap label define ancestr2d_lbl 6020 `"Pathan"', add
cap label define ancestr2d_lbl 6030 `"Bengali (1980)"', add
cap label define ancestr2d_lbl 6031 `"Bangladeshi (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 6032 `"Bengali (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 6070 `"Bhutanese"', add
cap label define ancestr2d_lbl 6090 `"Nepali"', add
cap label define ancestr2d_lbl 6150 `"Asian Indian (1980)"', add
cap label define ancestr2d_lbl 6151 `"India (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 6152 `"East Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 6153 `"Madhya Pradesh (1990-2000)"', add
cap label define ancestr2d_lbl 6154 `"Orissa (1990-2000)"', add
cap label define ancestr2d_lbl 6155 `"Rajasthani (1990-2000)"', add
cap label define ancestr2d_lbl 6156 `"Sikkim (1990-2000)"', add
cap label define ancestr2d_lbl 6157 `"Uttar Pradesh (1990-2000)"', add
cap label define ancestr2d_lbl 6220 `"Andaman Islander"', add
cap label define ancestr2d_lbl 6240 `"Andhra Pradesh"', add
cap label define ancestr2d_lbl 6260 `"Assamese"', add
cap label define ancestr2d_lbl 6280 `"Goanese"', add
cap label define ancestr2d_lbl 6300 `"Gujarati"', add
cap label define ancestr2d_lbl 6320 `"Karnatakan"', add
cap label define ancestr2d_lbl 6340 `"Keralan"', add
cap label define ancestr2d_lbl 6380 `"Maharashtran"', add
cap label define ancestr2d_lbl 6400 `"Madrasi"', add
cap label define ancestr2d_lbl 6420 `"Mysore"', add
cap label define ancestr2d_lbl 6440 `"Naga"', add
cap label define ancestr2d_lbl 6480 `"Pondicherry"', add
cap label define ancestr2d_lbl 6500 `"Punjabi"', add
cap label define ancestr2d_lbl 6560 `"Tamil"', add
cap label define ancestr2d_lbl 6750 `"East Indies (1990-2000)"', add
cap label define ancestr2d_lbl 6800 `"Pakistani (1980)"', add
cap label define ancestr2d_lbl 6801 `"Pakistani (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 6802 `"Kashmiri (1990-2000)"', add
cap label define ancestr2d_lbl 6900 `"Sri Lankan"', add
cap label define ancestr2d_lbl 6910 `"Singhalese"', add
cap label define ancestr2d_lbl 6920 `"Veddah"', add
cap label define ancestr2d_lbl 6950 `"Maldivian"', add
cap label define ancestr2d_lbl 7000 `"Burmese (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 7001 `"Burmese (1980)"', add
cap label define ancestr2d_lbl 7002 `"Burman (1980)"', add
cap label define ancestr2d_lbl 7020 `"Shan"', add
cap label define ancestr2d_lbl 7030 `"Cambodian"', add
cap label define ancestr2d_lbl 7040 `"Khmer"', add
cap label define ancestr2d_lbl 7060 `"Chinese"', add
cap label define ancestr2d_lbl 7070 `"Cantonese (1980)"', add
cap label define ancestr2d_lbl 7071 `"Cantonese (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 7072 `"Formosan (1990-2000)"', add
cap label define ancestr2d_lbl 7080 `"Manchurian"', add
cap label define ancestr2d_lbl 7090 `"Mandarin (1990-2000)"', add
cap label define ancestr2d_lbl 7120 `"Mongolian (1980)"', add
cap label define ancestr2d_lbl 7121 `"Mongolian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 7122 `"Kalmyk (1990-2000)"', add
cap label define ancestr2d_lbl 7140 `"Tibetan"', add
cap label define ancestr2d_lbl 7160 `"Hong Kong (1990-2000)"', add
cap label define ancestr2d_lbl 7161 `"Hong Kong (1980)"', add
cap label define ancestr2d_lbl 7162 `"Eastern Archipelago (1980)"', add
cap label define ancestr2d_lbl 7180 `"Macao"', add
cap label define ancestr2d_lbl 7200 `"Filipino"', add
cap label define ancestr2d_lbl 7300 `"Indonesian (1980)"', add
cap label define ancestr2d_lbl 7301 `"Indonesian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 7302 `"Borneo (1990-2000)"', add
cap label define ancestr2d_lbl 7303 `"Java (1990-2000)"', add
cap label define ancestr2d_lbl 7304 `"Sumatran (1990-2000)"', add
cap label define ancestr2d_lbl 7400 `"Japanese (1980)"', add
cap label define ancestr2d_lbl 7401 `"Japanese (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 7402 `"Issei (1990-2000)"', add
cap label define ancestr2d_lbl 7403 `"Nisei (1990-2000)"', add
cap label define ancestr2d_lbl 7404 `"Sansei (1990-2000)"', add
cap label define ancestr2d_lbl 7405 `"Yonsei (1990-2000)"', add
cap label define ancestr2d_lbl 7406 `"Gosei (1990-2000)"', add
cap label define ancestr2d_lbl 7460 `"Ryukyu Islander"', add
cap label define ancestr2d_lbl 7480 `"Okinawan"', add
cap label define ancestr2d_lbl 7500 `"Korean"', add
cap label define ancestr2d_lbl 7650 `"Laotian"', add
cap label define ancestr2d_lbl 7660 `"Meo"', add
cap label define ancestr2d_lbl 7680 `"Hmong"', add
cap label define ancestr2d_lbl 7700 `"Malaysian (1980)"', add
cap label define ancestr2d_lbl 7701 `"Malaysian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 7702 `"North Borneo (1990-2000)"', add
cap label define ancestr2d_lbl 7740 `"Singaporean"', add
cap label define ancestr2d_lbl 7760 `"Thai"', add
cap label define ancestr2d_lbl 7770 `"Black Thai"', add
cap label define ancestr2d_lbl 7780 `"Western Lao"', add
cap label define ancestr2d_lbl 7820 `"Taiwanese"', add
cap label define ancestr2d_lbl 7850 `"Vietnamese"', add
cap label define ancestr2d_lbl 7860 `"Katu"', add
cap label define ancestr2d_lbl 7870 `"Ma"', add
cap label define ancestr2d_lbl 7880 `"Mnong"', add
cap label define ancestr2d_lbl 7900 `"Montagnard"', add
cap label define ancestr2d_lbl 7920 `"Indochinese"', add
cap label define ancestr2d_lbl 7930 `"Eurasian"', add
cap label define ancestr2d_lbl 7931 `"Amerasian"', add
cap label define ancestr2d_lbl 7950 `"Asian"', add
cap label define ancestr2d_lbl 7960 `"Other Asian"', add
cap label define ancestr2d_lbl 8000 `"Australian"', add
cap label define ancestr2d_lbl 8010 `"Tasmanian"', add
cap label define ancestr2d_lbl 8020 `"Australian Aborigine (1990-2000)"', add
cap label define ancestr2d_lbl 8030 `"New Zealander"', add
cap label define ancestr2d_lbl 8080 `"Polynesian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 8081 `"Polynesian (1980)"', add
cap label define ancestr2d_lbl 8082 `"Norfolk Islander (1980)"', add
cap label define ancestr2d_lbl 8090 `"Kapinagamarangan (1990-2000)"', add
cap label define ancestr2d_lbl 8091 `"Kapinagamarangan (1980)"', add
cap label define ancestr2d_lbl 8092 `"Nukuoroan (1980)"', add
cap label define ancestr2d_lbl 8100 `"Maori"', add
cap label define ancestr2d_lbl 8110 `"Hawaiian"', add
cap label define ancestr2d_lbl 8130 `"Part Hawaiian"', add
cap label define ancestr2d_lbl 8140 `"Samoan (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 8141 `"Samoan (1980)"', add
cap label define ancestr2d_lbl 8142 `"American Samoan (1980)"', add
cap label define ancestr2d_lbl 8143 `"French Samoan"', add
cap label define ancestr2d_lbl 8144 `"Part Samoan (1990-2000)"', add
cap label define ancestr2d_lbl 8150 `"Tongan"', add
cap label define ancestr2d_lbl 8160 `"Tokelauan"', add
cap label define ancestr2d_lbl 8170 `"Cook Islander"', add
cap label define ancestr2d_lbl 8180 `"Tahitian"', add
cap label define ancestr2d_lbl 8190 `"Niuean"', add
cap label define ancestr2d_lbl 8200 `"Micronesian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 8201 `"Micronesian (1980)"', add
cap label define ancestr2d_lbl 8202 `"U.S. Trust Terr of the Pacific"', add
cap label define ancestr2d_lbl 8210 `"Guamanian"', add
cap label define ancestr2d_lbl 8220 `"Chamorro Islander"', add
cap label define ancestr2d_lbl 8230 `"Saipanese (1990-2000)"', add
cap label define ancestr2d_lbl 8231 `"Saipanese (1980)"', add
cap label define ancestr2d_lbl 8232 `"Northern Marianas (1980)"', add
cap label define ancestr2d_lbl 8240 `"Palauan"', add
cap label define ancestr2d_lbl 8250 `"Marshall Islander"', add
cap label define ancestr2d_lbl 8260 `"Kosraean"', add
cap label define ancestr2d_lbl 8270 `"Ponapean (1990-2000)"', add
cap label define ancestr2d_lbl 8271 `"Ponapean (1980)"', add
cap label define ancestr2d_lbl 8272 `"Mokilese (1980)"', add
cap label define ancestr2d_lbl 8273 `"Ngatikese (1980)"', add
cap label define ancestr2d_lbl 8274 `"Pingelapese (1980)"', add
cap label define ancestr2d_lbl 8280 `"Chuukese"', add
cap label define ancestr2d_lbl 8281 `"Hall Islander (1980)"', add
cap label define ancestr2d_lbl 8282 `"Mortlockese (1980)"', add
cap label define ancestr2d_lbl 8283 `"Namanouito (1980)"', add
cap label define ancestr2d_lbl 8284 `"Pulawatese (1980)"', add
cap label define ancestr2d_lbl 8285 `"Truk Islander"', add
cap label define ancestr2d_lbl 8290 `"Yap Islander"', add
cap label define ancestr2d_lbl 8300 `"Caroline Islander (1990-2000)"', add
cap label define ancestr2d_lbl 8301 `"Caroline Islander (1980)"', add
cap label define ancestr2d_lbl 8302 `"Lamotrekese (1980)"', add
cap label define ancestr2d_lbl 8303 `"Ulithian (1980)"', add
cap label define ancestr2d_lbl 8304 `"Woleaian (1980)"', add
cap label define ancestr2d_lbl 8310 `"Kiribatese"', add
cap label define ancestr2d_lbl 8320 `"Nauruan"', add
cap label define ancestr2d_lbl 8330 `"Tarawa Islander (1990-2000)"', add
cap label define ancestr2d_lbl 8340 `"Tinian Islander (1990-2000)"', add
cap label define ancestr2d_lbl 8400 `"Melanesian Islander"', add
cap label define ancestr2d_lbl 8410 `"Fijian"', add
cap label define ancestr2d_lbl 8430 `"New Guinean"', add
cap label define ancestr2d_lbl 8440 `"Papuan"', add
cap label define ancestr2d_lbl 8450 `"Solomon Islander"', add
cap label define ancestr2d_lbl 8460 `"New Caledonian Islander"', add
cap label define ancestr2d_lbl 8470 `"Vanuatuan"', add
cap label define ancestr2d_lbl 8500 `"Pacific Islander (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 8501 `"Campbell Islander (1980)"', add
cap label define ancestr2d_lbl 8502 `"Christmas Islander (1980)"', add
cap label define ancestr2d_lbl 8503 `"Kermadec Islander (1980)"', add
cap label define ancestr2d_lbl 8504 `"Midway Islander (1980)"', add
cap label define ancestr2d_lbl 8505 `"Phoenix Islander (1980)"', add
cap label define ancestr2d_lbl 8506 `"Wake Islander (1980)"', add
cap label define ancestr2d_lbl 8600 `"Oceania"', add
cap label define ancestr2d_lbl 8620 `"Chamolinian (1990-2000)"', add
cap label define ancestr2d_lbl 8630 `"Reserved Codes"', add
cap label define ancestr2d_lbl 8700 `"Other Pacific"', add
cap label define ancestr2d_lbl 9000 `"Afro-American"', add
cap label define ancestr2d_lbl 9001 `"Afro-American (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9002 `"Black (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9003 `"Negro (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9004 `"Nonwhite (1990-2000)"', add
cap label define ancestr2d_lbl 9005 `"Colored (1990-2000)"', add
cap label define ancestr2d_lbl 9006 `"Creole (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9007 `"Mulatto (1990-2000)"', add
cap label define ancestr2d_lbl 9008 `"Afro"', add
cap label define ancestr2d_lbl 9020 `"African-American (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9130 `"Central American Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9140 `"South American Indian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9200 `"American Indian (all tribes)"', add
cap label define ancestr2d_lbl 9201 `"American Indian-English-French"', add
cap label define ancestr2d_lbl 9202 `"American Indian-English-German"', add
cap label define ancestr2d_lbl 9203 `"American Indian-English-Irish"', add
cap label define ancestr2d_lbl 9204 `"American Indian-German-Irish"', add
cap label define ancestr2d_lbl 9205 `"Cherokee"', add
cap label define ancestr2d_lbl 9206 `"Native American"', add
cap label define ancestr2d_lbl 9207 `"Indian"', add
cap label define ancestr2d_lbl 9210 `"Aleut"', add
cap label define ancestr2d_lbl 9220 `"Eskimo"', add
cap label define ancestr2d_lbl 9230 `"Inuit"', add
cap label define ancestr2d_lbl 9240 `"White/Caucasian"', add
cap label define ancestr2d_lbl 9241 `"White/Caucasian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9242 `"Anglo (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9243 `"Appalachian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9244 `"Aryan (1990-2000)"', add
cap label define ancestr2d_lbl 9300 `"Greenlander"', add
cap label define ancestr2d_lbl 9310 `"Canadian"', add
cap label define ancestr2d_lbl 9330 `"Newfoundland"', add
cap label define ancestr2d_lbl 9340 `"Nova Scotian"', add
cap label define ancestr2d_lbl 9350 `"French Canadian"', add
cap label define ancestr2d_lbl 9360 `"Acadian"', add
cap label define ancestr2d_lbl 9361 `"Acadian (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9362 `"Cajun (1990-2000, ACS, PRCS)"', add
cap label define ancestr2d_lbl 9390 `"American"', add
cap label define ancestr2d_lbl 9391 `"American/Unites States"', add
cap label define ancestr2d_lbl 9400 `"United States"', add
cap label define ancestr2d_lbl 9410 `"Alabama"', add
cap label define ancestr2d_lbl 9420 `"Alaska"', add
cap label define ancestr2d_lbl 9430 `"Arizona"', add
cap label define ancestr2d_lbl 9440 `"Arkansas"', add
cap label define ancestr2d_lbl 9450 `"California"', add
cap label define ancestr2d_lbl 9460 `"Colorado"', add
cap label define ancestr2d_lbl 9470 `"Connecticut"', add
cap label define ancestr2d_lbl 9480 `"District of Columbia"', add
cap label define ancestr2d_lbl 9490 `"Delaware"', add
cap label define ancestr2d_lbl 9500 `"Florida"', add
cap label define ancestr2d_lbl 9510 `"Georgia"', add
cap label define ancestr2d_lbl 9520 `"Idaho"', add
cap label define ancestr2d_lbl 9530 `"Illinois"', add
cap label define ancestr2d_lbl 9540 `"Indiana"', add
cap label define ancestr2d_lbl 9550 `"Iowa"', add
cap label define ancestr2d_lbl 9560 `"Kansas"', add
cap label define ancestr2d_lbl 9570 `"Kentucky"', add
cap label define ancestr2d_lbl 9580 `"Louisiana"', add
cap label define ancestr2d_lbl 9590 `"Maine"', add
cap label define ancestr2d_lbl 9600 `"Maryland"', add
cap label define ancestr2d_lbl 9610 `"Massachusetts"', add
cap label define ancestr2d_lbl 9620 `"Michigan"', add
cap label define ancestr2d_lbl 9630 `"Minnesota"', add
cap label define ancestr2d_lbl 9640 `"Mississippi"', add
cap label define ancestr2d_lbl 9650 `"Missouri"', add
cap label define ancestr2d_lbl 9660 `"Montana"', add
cap label define ancestr2d_lbl 9670 `"Nebraska"', add
cap label define ancestr2d_lbl 9680 `"Nevada"', add
cap label define ancestr2d_lbl 9690 `"New Hampshire"', add
cap label define ancestr2d_lbl 9700 `"New Jersey"', add
cap label define ancestr2d_lbl 9710 `"New Mexico"', add
cap label define ancestr2d_lbl 9720 `"New York"', add
cap label define ancestr2d_lbl 9730 `"North Carolina"', add
cap label define ancestr2d_lbl 9740 `"North Dakota"', add
cap label define ancestr2d_lbl 9750 `"Ohio"', add
cap label define ancestr2d_lbl 9760 `"Oklahoma"', add
cap label define ancestr2d_lbl 9770 `"Oregon"', add
cap label define ancestr2d_lbl 9780 `"Pennsylvania"', add
cap label define ancestr2d_lbl 9790 `"Rhode Island"', add
cap label define ancestr2d_lbl 9800 `"South Carolina"', add
cap label define ancestr2d_lbl 9810 `"South Dakota"', add
cap label define ancestr2d_lbl 9820 `"Tennessee"', add
cap label define ancestr2d_lbl 9830 `"Texas"', add
cap label define ancestr2d_lbl 9840 `"Utah"', add
cap label define ancestr2d_lbl 9850 `"Vermont"', add
cap label define ancestr2d_lbl 9860 `"Virginia"', add
cap label define ancestr2d_lbl 9870 `"Washington"', add
cap label define ancestr2d_lbl 9880 `"West Virginia"', add
cap label define ancestr2d_lbl 9890 `"Wisconsin"', add
cap label define ancestr2d_lbl 9900 `"Wyoming"', add
cap label define ancestr2d_lbl 9930 `"Southerner"', add
cap label define ancestr2d_lbl 9940 `"North American"', add
cap label define ancestr2d_lbl 9950 `"Mixture"', add
cap label define ancestr2d_lbl 9960 `"Uncodable"', add
cap label define ancestr2d_lbl 9961 `"Not Classified"', add
cap label define ancestr2d_lbl 9970 `"Deferred Cases"', add
cap label define ancestr2d_lbl 9980 `"Other"', add
cap label define ancestr2d_lbl 9990 `"Not Reported"', add
cap label values ancestr2d ancestr2d_lbl

cap label define citizen_lbl 0 `"N/A"'
cap label define citizen_lbl 1 `"Born abroad of American parents"', add
cap label define citizen_lbl 2 `"Naturalized citizen"', add
cap label define citizen_lbl 3 `"Not a citizen"', add
cap label define citizen_lbl 4 `"Not a citizen, but has received first papers"', add
cap label define citizen_lbl 5 `"Foreign born, citizenship status not reported"', add
cap label values citizen citizen_lbl

cap label define yrsusa2_lbl 0 `"N/A"'
cap label define yrsusa2_lbl 1 `"0-5 years"', add
cap label define yrsusa2_lbl 2 `"6-10 years"', add
cap label define yrsusa2_lbl 3 `"11-15 years"', add
cap label define yrsusa2_lbl 4 `"16-20 years"', add
cap label define yrsusa2_lbl 5 `"21+ years"', add
cap label define yrsusa2_lbl 9 `"Missing"', add
cap label values yrsusa2 yrsusa2_lbl

cap label define language_lbl 00 `"N/A or blank"'
cap label define language_lbl 01 `"English"', add
cap label define language_lbl 02 `"German"', add
cap label define language_lbl 03 `"Yiddish, Jewish"', add
cap label define language_lbl 04 `"Dutch"', add
cap label define language_lbl 05 `"Swedish"', add
cap label define language_lbl 06 `"Danish"', add
cap label define language_lbl 07 `"Norwegian"', add
cap label define language_lbl 08 `"Icelandic"', add
cap label define language_lbl 09 `"Scandinavian"', add
cap label define language_lbl 10 `"Italian"', add
cap label define language_lbl 11 `"French"', add
cap label define language_lbl 12 `"Spanish"', add
cap label define language_lbl 13 `"Portuguese"', add
cap label define language_lbl 14 `"Rumanian"', add
cap label define language_lbl 15 `"Celtic"', add
cap label define language_lbl 16 `"Greek"', add
cap label define language_lbl 17 `"Albanian"', add
cap label define language_lbl 18 `"Russian"', add
cap label define language_lbl 19 `"Ukrainian, Ruthenian, Little Russian"', add
cap label define language_lbl 20 `"Czech"', add
cap label define language_lbl 21 `"Polish"', add
cap label define language_lbl 22 `"Slovak"', add
cap label define language_lbl 23 `"Serbo-Croatian, Yugoslavian, Slavonian"', add
cap label define language_lbl 24 `"Slovene"', add
cap label define language_lbl 25 `"Lithuanian"', add
cap label define language_lbl 26 `"Other Balto-Slavic"', add
cap label define language_lbl 27 `"Slavic unknown"', add
cap label define language_lbl 28 `"Armenian"', add
cap label define language_lbl 29 `"Persian, Iranian, Farsi"', add
cap label define language_lbl 30 `"Other Persian dialects"', add
cap label define language_lbl 31 `"Hindi and related"', add
cap label define language_lbl 32 `"Romany, Gypsy"', add
cap label define language_lbl 33 `"Finnish"', add
cap label define language_lbl 34 `"Magyar, Hungarian"', add
cap label define language_lbl 35 `"Uralic"', add
cap label define language_lbl 36 `"Turkish"', add
cap label define language_lbl 37 `"Other Altaic"', add
cap label define language_lbl 38 `"Caucasian, Georgian, Avar"', add
cap label define language_lbl 39 `"Basque"', add
cap label define language_lbl 40 `"Dravidian"', add
cap label define language_lbl 41 `"Kurukh"', add
cap label define language_lbl 42 `"Burushaski"', add
cap label define language_lbl 43 `"Chinese"', add
cap label define language_lbl 44 `"Tibetan"', add
cap label define language_lbl 45 `"Burmese, Lisu, Lolo"', add
cap label define language_lbl 46 `"Kachin"', add
cap label define language_lbl 47 `"Thai, Siamese, Lao"', add
cap label define language_lbl 48 `"Japanese"', add
cap label define language_lbl 49 `"Korean"', add
cap label define language_lbl 50 `"Vietnamese"', add
cap label define language_lbl 51 `"Other East/Southeast Asian"', add
cap label define language_lbl 52 `"Indonesian"', add
cap label define language_lbl 53 `"Other Malayan"', add
cap label define language_lbl 54 `"Filipino, Tagalog"', add
cap label define language_lbl 55 `"Micronesian, Polynesian"', add
cap label define language_lbl 56 `"Hawaiian"', add
cap label define language_lbl 57 `"Arabic"', add
cap label define language_lbl 58 `"Near East Arabic dialect"', add
cap label define language_lbl 59 `"Hebrew, Israeli"', add
cap label define language_lbl 60 `"Amharic, Ethiopian, etc."', add
cap label define language_lbl 61 `"Hamitic"', add
cap label define language_lbl 62 `"Other Afro-Asiatic languages"', add
cap label define language_lbl 63 `"Sub-Saharan Africa"', add
cap label define language_lbl 64 `"African, n.s."', add
cap label define language_lbl 70 `"American Indian (all)"', add
cap label define language_lbl 71 `"Aleut, Eskimo"', add
cap label define language_lbl 72 `"Algonquian"', add
cap label define language_lbl 73 `"Salish, Flathead"', add
cap label define language_lbl 74 `"Athapascan"', add
cap label define language_lbl 75 `"Navajo"', add
cap label define language_lbl 76 `"Penutian-Sahaptin"', add
cap label define language_lbl 77 `"Other Penutian"', add
cap label define language_lbl 78 `"Zuni"', add
cap label define language_lbl 79 `"Yuman"', add
cap label define language_lbl 80 `"Other Hokan languages"', add
cap label define language_lbl 81 `"Siouan languages"', add
cap label define language_lbl 82 `"Muskogean"', add
cap label define language_lbl 83 `"Keres"', add
cap label define language_lbl 84 `"Iroquoian"', add
cap label define language_lbl 85 `"Caddoan"', add
cap label define language_lbl 86 `"Shoshonean/Hopi"', add
cap label define language_lbl 87 `"Pima, Papago"', add
cap label define language_lbl 88 `"Yaqui and other Sonoran, nec"', add
cap label define language_lbl 89 `"Aztecan, Nahuatl, Uto-Aztecan"', add
cap label define language_lbl 90 `"Tanoan languages"', add
cap label define language_lbl 91 `"Other Indian languages"', add
cap label define language_lbl 92 `"Mayan languages"', add
cap label define language_lbl 93 `"American Indian, n.s."', add
cap label define language_lbl 94 `"Native"', add
cap label define language_lbl 95 `"No language"', add
cap label define language_lbl 96 `"Other or not reported"', add
cap label values language language_lbl

cap label define languaged_lbl 0000 `"N/A or blank"'
cap label define languaged_lbl 0100 `"English"', add
cap label define languaged_lbl 0110 `"Jamaican Creole"', add
cap label define languaged_lbl 0120 `"Krio, Pidgin Krio"', add
cap label define languaged_lbl 0130 `"Hawaiian Pidgin"', add
cap label define languaged_lbl 0140 `"Pidgin"', add
cap label define languaged_lbl 0150 `"Gullah, Geechee"', add
cap label define languaged_lbl 0160 `"Saramacca"', add
cap label define languaged_lbl 0170 `"Other English-based Creole languages"', add
cap label define languaged_lbl 0200 `"German"', add
cap label define languaged_lbl 0210 `"Austrian"', add
cap label define languaged_lbl 0220 `"Swiss"', add
cap label define languaged_lbl 0230 `"Luxembourgian"', add
cap label define languaged_lbl 0240 `"Pennsylvania Dutch"', add
cap label define languaged_lbl 0300 `"Yiddish, Jewish"', add
cap label define languaged_lbl 0310 `"Jewish"', add
cap label define languaged_lbl 0320 `"Yiddish"', add
cap label define languaged_lbl 0400 `"Dutch"', add
cap label define languaged_lbl 0410 `"Dutch, Flemish, Belgian"', add
cap label define languaged_lbl 0420 `"Afrikaans"', add
cap label define languaged_lbl 0430 `"Frisian"', add
cap label define languaged_lbl 0440 `"Dutch, Afrikaans, Frisian"', add
cap label define languaged_lbl 0450 `"Belgian, Flemish"', add
cap label define languaged_lbl 0460 `"Belgian"', add
cap label define languaged_lbl 0470 `"Flemish"', add
cap label define languaged_lbl 0500 `"Swedish"', add
cap label define languaged_lbl 0600 `"Danish"', add
cap label define languaged_lbl 0700 `"Norwegian"', add
cap label define languaged_lbl 0800 `"Icelandic"', add
cap label define languaged_lbl 0810 `"Faroese"', add
cap label define languaged_lbl 0900 `"Scandinavian"', add
cap label define languaged_lbl 1000 `"Italian"', add
cap label define languaged_lbl 1010 `"Rhaeto-Romanic, Ladin"', add
cap label define languaged_lbl 1020 `"Friulian"', add
cap label define languaged_lbl 1030 `"Romansh"', add
cap label define languaged_lbl 1100 `"French"', add
cap label define languaged_lbl 1110 `"French, Walloon"', add
cap label define languaged_lbl 1120 `"Provencal"', add
cap label define languaged_lbl 1130 `"Patois"', add
cap label define languaged_lbl 1140 `"French or Haitian Creole"', add
cap label define languaged_lbl 1150 `"Cajun"', add
cap label define languaged_lbl 1200 `"Spanish"', add
cap label define languaged_lbl 1210 `"Catalonian, Valencian"', add
cap label define languaged_lbl 1220 `"Ladino, Sefaradit, Spanol"', add
cap label define languaged_lbl 1230 `"Pachuco"', add
cap label define languaged_lbl 1250 `"Mexican"', add
cap label define languaged_lbl 1300 `"Portuguese"', add
cap label define languaged_lbl 1310 `"Papia Mentae"', add
cap label define languaged_lbl 1320 `"Cape Verdean Creole"', add
cap label define languaged_lbl 1400 `"Rumanian"', add
cap label define languaged_lbl 1500 `"Celtic"', add
cap label define languaged_lbl 1510 `"Welsh, Breton, Cornish"', add
cap label define languaged_lbl 1520 `"Welsh"', add
cap label define languaged_lbl 1530 `"Breton"', add
cap label define languaged_lbl 1540 `"Irish Gaelic, Gaelic"', add
cap label define languaged_lbl 1550 `"Gaelic"', add
cap label define languaged_lbl 1560 `"Irish"', add
cap label define languaged_lbl 1570 `"Scottish Gaelic"', add
cap label define languaged_lbl 1580 `"Scotch"', add
cap label define languaged_lbl 1590 `"Manx, Manx Gaelic"', add
cap label define languaged_lbl 1600 `"Greek"', add
cap label define languaged_lbl 1700 `"Albanian"', add
cap label define languaged_lbl 1800 `"Russian"', add
cap label define languaged_lbl 1810 `"Russian, Great Russian"', add
cap label define languaged_lbl 1811 `"Great Russian"', add
cap label define languaged_lbl 1820 `"Bielo-, White Russian"', add
cap label define languaged_lbl 1900 `"Ukrainian, Ruthenian, Little Russian"', add
cap label define languaged_lbl 1910 `"Ruthenian"', add
cap label define languaged_lbl 1920 `"Little Russian"', add
cap label define languaged_lbl 1930 `"Ukrainian"', add
cap label define languaged_lbl 2000 `"Czech"', add
cap label define languaged_lbl 2010 `"Bohemian"', add
cap label define languaged_lbl 2020 `"Moravian"', add
cap label define languaged_lbl 2100 `"Polish"', add
cap label define languaged_lbl 2110 `"Kashubian, Slovincian"', add
cap label define languaged_lbl 2200 `"Slovak"', add
cap label define languaged_lbl 2300 `"Serbo-Croatian, Yugoslavian, Slavonian"', add
cap label define languaged_lbl 2310 `"Croatian"', add
cap label define languaged_lbl 2320 `"Serbian"', add
cap label define languaged_lbl 2321 `"Bosnian"', add
cap label define languaged_lbl 2330 `"Dalmatian, Montenegrin"', add
cap label define languaged_lbl 2331 `"Dalmatian"', add
cap label define languaged_lbl 2332 `"Montenegrin"', add
cap label define languaged_lbl 2400 `"Slovene"', add
cap label define languaged_lbl 2500 `"Lithuanian"', add
cap label define languaged_lbl 2510 `"Lettish, Latvian"', add
cap label define languaged_lbl 2600 `"Other Balto-Slavic"', add
cap label define languaged_lbl 2610 `"Bulgarian"', add
cap label define languaged_lbl 2620 `"Lusatian, Sorbian, Wendish"', add
cap label define languaged_lbl 2621 `"Wendish"', add
cap label define languaged_lbl 2630 `"Macedonian"', add
cap label define languaged_lbl 2700 `"Slavic unknown"', add
cap label define languaged_lbl 2800 `"Armenian"', add
cap label define languaged_lbl 2900 `"Persian, Iranian, Farsi"', add
cap label define languaged_lbl 2910 `"Persian"', add
cap label define languaged_lbl 2920 `"Dari"', add
cap label define languaged_lbl 3000 `"Other Persian dialects"', add
cap label define languaged_lbl 3010 `"Pashto, Afghan"', add
cap label define languaged_lbl 3020 `"Kurdish"', add
cap label define languaged_lbl 3030 `"Balochi"', add
cap label define languaged_lbl 3040 `"Tadzhik"', add
cap label define languaged_lbl 3050 `"Ossete"', add
cap label define languaged_lbl 3100 `"Hindi and related"', add
cap label define languaged_lbl 3101 `"Hindi, Hindustani, Indic, Jaipuri, Pali, Urdu"', add
cap label define languaged_lbl 3102 `"Hindi"', add
cap label define languaged_lbl 3103 `"Urdu"', add
cap label define languaged_lbl 3104 `"Other Indo-Iranian languages"', add
cap label define languaged_lbl 3110 `"Other Indo-Aryan"', add
cap label define languaged_lbl 3111 `"Sanskrit"', add
cap label define languaged_lbl 3112 `"Bengali"', add
cap label define languaged_lbl 3113 `"Panjabi"', add
cap label define languaged_lbl 3114 `"Marathi"', add
cap label define languaged_lbl 3115 `"Gujarathi"', add
cap label define languaged_lbl 3116 `"Bihari"', add
cap label define languaged_lbl 3117 `"Rajasthani"', add
cap label define languaged_lbl 3118 `"Oriya"', add
cap label define languaged_lbl 3119 `"Assamese"', add
cap label define languaged_lbl 3120 `"Kashmiri"', add
cap label define languaged_lbl 3121 `"Sindhi"', add
cap label define languaged_lbl 3122 `"Maldivian"', add
cap label define languaged_lbl 3123 `"Sinhalese"', add
cap label define languaged_lbl 3130 `"Kannada"', add
cap label define languaged_lbl 3140 `"India nec"', add
cap label define languaged_lbl 3150 `"Pakistan nec"', add
cap label define languaged_lbl 3190 `"Other Indo-European languages"', add
cap label define languaged_lbl 3200 `"Romany, Gypsy"', add
cap label define languaged_lbl 3210 `"Gypsy"', add
cap label define languaged_lbl 3300 `"Finnish"', add
cap label define languaged_lbl 3400 `"Magyar, Hungarian"', add
cap label define languaged_lbl 3401 `"Magyar"', add
cap label define languaged_lbl 3402 `"Hungarian"', add
cap label define languaged_lbl 3500 `"Uralic"', add
cap label define languaged_lbl 3510 `"Estonian, Ingrian, Livonian, Vepsian,  Votic"', add
cap label define languaged_lbl 3511 `"Estonian"', add
cap label define languaged_lbl 3520 `"Lapp, Inari, Kola, Lule, Pite, Ruija, Skolt, Ume"', add
cap label define languaged_lbl 3521 `"Lappish"', add
cap label define languaged_lbl 3530 `"Other Uralic"', add
cap label define languaged_lbl 3600 `"Turkish"', add
cap label define languaged_lbl 3700 `"Other Altaic"', add
cap label define languaged_lbl 3701 `"Chuvash"', add
cap label define languaged_lbl 3702 `"Karakalpak"', add
cap label define languaged_lbl 3703 `"Kazakh"', add
cap label define languaged_lbl 3704 `"Kirghiz"', add
cap label define languaged_lbl 3705 `"Karachay, Tatar, Balkar, Bashkir, Kumyk"', add
cap label define languaged_lbl 3706 `"Uzbek, Uighur"', add
cap label define languaged_lbl 3707 `"Azerbaijani"', add
cap label define languaged_lbl 3708 `"Turkmen"', add
cap label define languaged_lbl 3709 `"Yakut"', add
cap label define languaged_lbl 3710 `"Mongolian"', add
cap label define languaged_lbl 3711 `"Tungus"', add
cap label define languaged_lbl 3800 `"Caucasian, Georgian, Avar"', add
cap label define languaged_lbl 3810 `"Georgian"', add
cap label define languaged_lbl 3900 `"Basque"', add
cap label define languaged_lbl 4000 `"Dravidian"', add
cap label define languaged_lbl 4001 `"Brahui"', add
cap label define languaged_lbl 4002 `"Gondi"', add
cap label define languaged_lbl 4003 `"Telugu"', add
cap label define languaged_lbl 4004 `"Malayalam"', add
cap label define languaged_lbl 4005 `"Tamil"', add
cap label define languaged_lbl 4010 `"Bhili"', add
cap label define languaged_lbl 4011 `"Nepali"', add
cap label define languaged_lbl 4100 `"Kurukh"', add
cap label define languaged_lbl 4110 `"Munda"', add
cap label define languaged_lbl 4200 `"Burushaski"', add
cap label define languaged_lbl 4300 `"Chinese"', add
cap label define languaged_lbl 4301 `"Chinese, Cantonese, Min, Yueh"', add
cap label define languaged_lbl 4302 `"Cantonese"', add
cap label define languaged_lbl 4303 `"Mandarin"', add
cap label define languaged_lbl 4310 `"Other Chinese"', add
cap label define languaged_lbl 4311 `"Hakka, Fukien, Kechia"', add
cap label define languaged_lbl 4312 `"Kan, Nan Chang"', add
cap label define languaged_lbl 4313 `"Hsiang, Chansa, Hunan, Iyan"', add
cap label define languaged_lbl 4314 `"Fuchow, Min Pei"', add
cap label define languaged_lbl 4315 `"Wu"', add
cap label define languaged_lbl 4400 `"Tibetan"', add
cap label define languaged_lbl 4410 `"Miao-Yao, Mien"', add
cap label define languaged_lbl 4420 `"Miao, Hmong"', add
cap label define languaged_lbl 4430 `"Iu Mien"', add
cap label define languaged_lbl 4500 `"Burmese, Lisu, Lolo"', add
cap label define languaged_lbl 4510 `"Karen"', add
cap label define languaged_lbl 4520 `"Chin languages"', add
cap label define languaged_lbl 4600 `"Kachin"', add
cap label define languaged_lbl 4700 `"Thai, Siamese, Lao"', add
cap label define languaged_lbl 4710 `"Thai"', add
cap label define languaged_lbl 4720 `"Laotian"', add
cap label define languaged_lbl 4800 `"Japanese"', add
cap label define languaged_lbl 4900 `"Korean"', add
cap label define languaged_lbl 5000 `"Vietnamese"', add
cap label define languaged_lbl 5100 `"Other East/Southeast Asian"', add
cap label define languaged_lbl 5110 `"Ainu"', add
cap label define languaged_lbl 5120 `"Mon-Khmer, Cambodian"', add
cap label define languaged_lbl 5130 `"Siberian, n.e.c."', add
cap label define languaged_lbl 5140 `"Yukagir"', add
cap label define languaged_lbl 5150 `"Muong"', add
cap label define languaged_lbl 5200 `"Indonesian"', add
cap label define languaged_lbl 5210 `"Buginese"', add
cap label define languaged_lbl 5220 `"Moluccan"', add
cap label define languaged_lbl 5230 `"Achinese"', add
cap label define languaged_lbl 5240 `"Balinese"', add
cap label define languaged_lbl 5250 `"Cham"', add
cap label define languaged_lbl 5260 `"Madurese"', add
cap label define languaged_lbl 5270 `"Malay"', add
cap label define languaged_lbl 5280 `"Minangkabau"', add
cap label define languaged_lbl 5290 `"Other Asian languages"', add
cap label define languaged_lbl 5300 `"Other Malayan"', add
cap label define languaged_lbl 5310 `"Formosan, Taiwanese"', add
cap label define languaged_lbl 5320 `"Javanese"', add
cap label define languaged_lbl 5330 `"Malagasy"', add
cap label define languaged_lbl 5340 `"Sundanese"', add
cap label define languaged_lbl 5400 `"Filipino, Tagalog"', add
cap label define languaged_lbl 5410 `"Bisayan"', add
cap label define languaged_lbl 5420 `"Sebuano"', add
cap label define languaged_lbl 5430 `"Pangasinan"', add
cap label define languaged_lbl 5440 `"Llocano, Hocano"', add
cap label define languaged_lbl 5450 `"Bikol"', add
cap label define languaged_lbl 5460 `"Pampangan"', add
cap label define languaged_lbl 5470 `"Gorontalo"', add
cap label define languaged_lbl 5480 `"Palau"', add
cap label define languaged_lbl 5500 `"Micronesian, Polynesian"', add
cap label define languaged_lbl 5501 `"Micronesian"', add
cap label define languaged_lbl 5502 `"Carolinian"', add
cap label define languaged_lbl 5503 `"Chamorro, Guamanian"', add
cap label define languaged_lbl 5504 `"Gilbertese"', add
cap label define languaged_lbl 5505 `"Kusaiean"', add
cap label define languaged_lbl 5506 `"Marshallese"', add
cap label define languaged_lbl 5507 `"Mokilese"', add
cap label define languaged_lbl 5508 `"Mortlockese"', add
cap label define languaged_lbl 5509 `"Nauruan"', add
cap label define languaged_lbl 5510 `"Ponapean"', add
cap label define languaged_lbl 5511 `"Trukese"', add
cap label define languaged_lbl 5512 `"Ulithean, Fais"', add
cap label define languaged_lbl 5513 `"Woleai-Ulithi"', add
cap label define languaged_lbl 5514 `"Yapese"', add
cap label define languaged_lbl 5520 `"Melanesian"', add
cap label define languaged_lbl 5521 `"Polynesian"', add
cap label define languaged_lbl 5522 `"Samoan"', add
cap label define languaged_lbl 5523 `"Tongan"', add
cap label define languaged_lbl 5524 `"Niuean"', add
cap label define languaged_lbl 5525 `"Tokelauan"', add
cap label define languaged_lbl 5526 `"Fijian"', add
cap label define languaged_lbl 5527 `"Marquesan"', add
cap label define languaged_lbl 5528 `"Rarotongan"', add
cap label define languaged_lbl 5529 `"Maori"', add
cap label define languaged_lbl 5530 `"Nukuoro, Kapingarangan"', add
cap label define languaged_lbl 5590 `"Other Pacific Island languages"', add
cap label define languaged_lbl 5600 `"Hawaiian"', add
cap label define languaged_lbl 5700 `"Arabic"', add
cap label define languaged_lbl 5710 `"Algerian, Moroccan, Tunisian"', add
cap label define languaged_lbl 5720 `"Egyptian"', add
cap label define languaged_lbl 5730 `"Iraqi"', add
cap label define languaged_lbl 5740 `"Libyan"', add
cap label define languaged_lbl 5750 `"Maltese"', add
cap label define languaged_lbl 5800 `"Near East Arabic dialect"', add
cap label define languaged_lbl 5810 `"Syriac, Aramaic, Chaldean"', add
cap label define languaged_lbl 5820 `"Syrian"', add
cap label define languaged_lbl 5900 `"Hebrew, Israeli"', add
cap label define languaged_lbl 6000 `"Amharic, Ethiopian, etc."', add
cap label define languaged_lbl 6100 `"Hamitic"', add
cap label define languaged_lbl 6110 `"Berber"', add
cap label define languaged_lbl 6120 `"Chadic, Hamitic, Hausa"', add
cap label define languaged_lbl 6130 `"Cushite, Beja, Somali"', add
cap label define languaged_lbl 6200 `"Other Afro-Asiatic languages"', add
cap label define languaged_lbl 6300 `"Nilotic"', add
cap label define languaged_lbl 6301 `"Nilo-Hamitic"', add
cap label define languaged_lbl 6302 `"Nubian"', add
cap label define languaged_lbl 6303 `"Saharan"', add
cap label define languaged_lbl 6304 `"Nilo-Saharan, Fur, Songhai"', add
cap label define languaged_lbl 6305 `"Khoisan"', add
cap label define languaged_lbl 6306 `"Sudanic"', add
cap label define languaged_lbl 6307 `"Bantu (many subheads)"', add
cap label define languaged_lbl 6308 `"Swahili"', add
cap label define languaged_lbl 6309 `"Mande"', add
cap label define languaged_lbl 6310 `"Fulani"', add
cap label define languaged_lbl 6311 `"Gur"', add
cap label define languaged_lbl 6312 `"Kru"', add
cap label define languaged_lbl 6313 `"Efik, Ibibio, Tiv"', add
cap label define languaged_lbl 6314 `"Mbum, Gbaya, Sango, Zande"', add
cap label define languaged_lbl 6320 `"Eastern Sudanic and Khoisan"', add
cap label define languaged_lbl 6321 `"Niger-Congo regions (many subheads)"', add
cap label define languaged_lbl 6322 `"Congo, Kongo, Luba, Ruanda, Rundi, Santali, Swahili"', add
cap label define languaged_lbl 6390 `"Other specified African languages"', add
cap label define languaged_lbl 6400 `"African, n.s."', add
cap label define languaged_lbl 7000 `"American Indian (all)"', add
cap label define languaged_lbl 7100 `"Aleut, Eskimo"', add
cap label define languaged_lbl 7110 `"Aleut"', add
cap label define languaged_lbl 7120 `"Pacific Gulf Yupik"', add
cap label define languaged_lbl 7130 `"Eskimo"', add
cap label define languaged_lbl 7140 `"Inupik, Innuit"', add
cap label define languaged_lbl 7150 `"St. Lawrence Isl. Yupik"', add
cap label define languaged_lbl 7160 `"Yupik"', add
cap label define languaged_lbl 7200 `"Algonquian"', add
cap label define languaged_lbl 7201 `"Arapaho"', add
cap label define languaged_lbl 7202 `"Atsina, Gros Ventre"', add
cap label define languaged_lbl 7203 `"Blackfoot"', add
cap label define languaged_lbl 7204 `"Cheyenne"', add
cap label define languaged_lbl 7205 `"Cree"', add
cap label define languaged_lbl 7206 `"Delaware, Lenni-Lenape"', add
cap label define languaged_lbl 7207 `"Fox, Sac"', add
cap label define languaged_lbl 7208 `"Kickapoo"', add
cap label define languaged_lbl 7209 `"Menomini"', add
cap label define languaged_lbl 7210 `"Metis, French Cree"', add
cap label define languaged_lbl 7211 `"Miami"', add
cap label define languaged_lbl 7212 `"Micmac"', add
cap label define languaged_lbl 7213 `"Ojibwa, Chippewa"', add
cap label define languaged_lbl 7214 `"Ottawa"', add
cap label define languaged_lbl 7215 `"Passamaquoddy, Malecite"', add
cap label define languaged_lbl 7216 `"Penobscot"', add
cap label define languaged_lbl 7217 `"Abnaki"', add
cap label define languaged_lbl 7218 `"Potawatomi"', add
cap label define languaged_lbl 7219 `"Shawnee"', add
cap label define languaged_lbl 7300 `"Salish, Flathead"', add
cap label define languaged_lbl 7301 `"Lower Chehalis"', add
cap label define languaged_lbl 7302 `"Upper Chehalis, Chelalis, Satsop"', add
cap label define languaged_lbl 7303 `"Clallam"', add
cap label define languaged_lbl 7304 `"Coeur dAlene, Skitsamish"', add
cap label define languaged_lbl 7305 `"Columbia, Chelan, Wenatchee"', add
cap label define languaged_lbl 7306 `"Cowlitz"', add
cap label define languaged_lbl 7307 `"Nootsack"', add
cap label define languaged_lbl 7308 `"Okanogan"', add
cap label define languaged_lbl 7309 `"Puget Sound Salish"', add
cap label define languaged_lbl 7310 `"Quinault, Queets"', add
cap label define languaged_lbl 7311 `"Tillamook"', add
cap label define languaged_lbl 7312 `"Twana"', add
cap label define languaged_lbl 7313 `"Kalispel"', add
cap label define languaged_lbl 7314 `"Spokane"', add
cap label define languaged_lbl 7400 `"Athapascan"', add
cap label define languaged_lbl 7401 `"Ahtena"', add
cap label define languaged_lbl 7402 `"Han"', add
cap label define languaged_lbl 7403 `"Ingalit"', add
cap label define languaged_lbl 7404 `"Koyukon"', add
cap label define languaged_lbl 7405 `"Kuchin"', add
cap label define languaged_lbl 7406 `"Upper Kuskokwim"', add
cap label define languaged_lbl 7407 `"Tanaina"', add
cap label define languaged_lbl 7408 `"Tanana, Minto"', add
cap label define languaged_lbl 7409 `"Tanacross"', add
cap label define languaged_lbl 7410 `"Upper Tanana, Nabesena, Tetlin"', add
cap label define languaged_lbl 7411 `"Tutchone"', add
cap label define languaged_lbl 7412 `"Chasta Costa, Chetco, Coquille, Smith River Athapascan"', add
cap label define languaged_lbl 7413 `"Hupa"', add
cap label define languaged_lbl 7420 `"Apache"', add
cap label define languaged_lbl 7421 `"Jicarilla, Lipan"', add
cap label define languaged_lbl 7422 `"Chiricahua, Mescalero"', add
cap label define languaged_lbl 7423 `"San Carlos, Cibecue, White Mountain"', add
cap label define languaged_lbl 7424 `"Kiowa-Apache"', add
cap label define languaged_lbl 7430 `"Kiowa"', add
cap label define languaged_lbl 7440 `"Eyak"', add
cap label define languaged_lbl 7450 `"Other Athapascan-Eyak, Cahto, Mattole, Wailaki"', add
cap label define languaged_lbl 7490 `"Other Algonquin languages"', add
cap label define languaged_lbl 7500 `"Navajo"', add
cap label define languaged_lbl 7600 `"Penutian-Sahaptin"', add
cap label define languaged_lbl 7610 `"Klamath, Modoc"', add
cap label define languaged_lbl 7620 `"Nez Perce"', add
cap label define languaged_lbl 7630 `"Sahaptian, Celilo, Klikitat, Palouse, Tenino, Umatilla, Warm"', add
cap label define languaged_lbl 7700 `"Mountain Maidu, Maidu"', add
cap label define languaged_lbl 7701 `"Northwest Maidu, Concow"', add
cap label define languaged_lbl 7702 `"Southern Maidu, Nisenan"', add
cap label define languaged_lbl 7703 `"Coast Miwok, Bodega, Marin"', add
cap label define languaged_lbl 7704 `"Plains Miwok"', add
cap label define languaged_lbl 7705 `"Sierra Miwok, Miwok"', add
cap label define languaged_lbl 7706 `"Nomlaki, Tehama"', add
cap label define languaged_lbl 7707 `"Patwin, Colouse, Suisun"', add
cap label define languaged_lbl 7708 `"Wintun"', add
cap label define languaged_lbl 7709 `"Foothill North Yokuts"', add
cap label define languaged_lbl 7710 `"Tachi"', add
cap label define languaged_lbl 7711 `"Santiam, Calapooya, Wapatu"', add
cap label define languaged_lbl 7712 `"Siuslaw, Coos, Lower Umpqua"', add
cap label define languaged_lbl 7713 `"Tsimshian"', add
cap label define languaged_lbl 7714 `"Upper Chinook, Clackamas, Multnomah, Wasco, Wishram"', add
cap label define languaged_lbl 7715 `"Chinook Jargon"', add
cap label define languaged_lbl 7800 `"Zuni"', add
cap label define languaged_lbl 7900 `"Yuman"', add
cap label define languaged_lbl 7910 `"Upriver Yuman"', add
cap label define languaged_lbl 7920 `"Cocomaricopa"', add
cap label define languaged_lbl 7930 `"Mohave"', add
cap label define languaged_lbl 7940 `"Diegueno"', add
cap label define languaged_lbl 7950 `"Delta River Yuman"', add
cap label define languaged_lbl 7960 `"Upland Yuman"', add
cap label define languaged_lbl 7970 `"Havasupai"', add
cap label define languaged_lbl 7980 `"Walapai"', add
cap label define languaged_lbl 7990 `"Yavapai"', add
cap label define languaged_lbl 8000 `"Achumawi"', add
cap label define languaged_lbl 8010 `"Atsugewi"', add
cap label define languaged_lbl 8020 `"Karok"', add
cap label define languaged_lbl 8030 `"Pomo"', add
cap label define languaged_lbl 8040 `"Shastan"', add
cap label define languaged_lbl 8050 `"Washo"', add
cap label define languaged_lbl 8060 `"Chumash"', add
cap label define languaged_lbl 8100 `"Siouan languages"', add
cap label define languaged_lbl 8101 `"Crow, Absaroke"', add
cap label define languaged_lbl 8102 `"Hidatsa"', add
cap label define languaged_lbl 8103 `"Mandan"', add
cap label define languaged_lbl 8104 `"Dakota, Lakota, Nakota, Sioux"', add
cap label define languaged_lbl 8105 `"Chiwere"', add
cap label define languaged_lbl 8106 `"Winnebago"', add
cap label define languaged_lbl 8107 `"Kansa, Kaw"', add
cap label define languaged_lbl 8108 `"Omaha"', add
cap label define languaged_lbl 8109 `"Osage"', add
cap label define languaged_lbl 8110 `"Ponca"', add
cap label define languaged_lbl 8111 `"Quapaw, Arkansas"', add
cap label define languaged_lbl 8120 `"Iowa"', add
cap label define languaged_lbl 8200 `"Muskogean"', add
cap label define languaged_lbl 8210 `"Alabama"', add
cap label define languaged_lbl 8220 `"Choctaw, Chickasaw"', add
cap label define languaged_lbl 8230 `"Mikasuki"', add
cap label define languaged_lbl 8240 `"Hichita, Apalachicola"', add
cap label define languaged_lbl 8250 `"Koasati"', add
cap label define languaged_lbl 8260 `"Muskogee, Creek, Seminole"', add
cap label define languaged_lbl 8300 `"Keres"', add
cap label define languaged_lbl 8400 `"Iroquoian"', add
cap label define languaged_lbl 8410 `"Mohawk"', add
cap label define languaged_lbl 8420 `"Oneida"', add
cap label define languaged_lbl 8430 `"Onondaga"', add
cap label define languaged_lbl 8440 `"Cayuga"', add
cap label define languaged_lbl 8450 `"Seneca"', add
cap label define languaged_lbl 8460 `"Tuscarora"', add
cap label define languaged_lbl 8470 `"Wyandot, Huron"', add
cap label define languaged_lbl 8480 `"Cherokee"', add
cap label define languaged_lbl 8500 `"Caddoan"', add
cap label define languaged_lbl 8510 `"Arikara"', add
cap label define languaged_lbl 8520 `"Pawnee"', add
cap label define languaged_lbl 8530 `"Wichita"', add
cap label define languaged_lbl 8600 `"Shoshonean/Hopi"', add
cap label define languaged_lbl 8601 `"Comanche"', add
cap label define languaged_lbl 8602 `"Mono, Owens Valley Paiute"', add
cap label define languaged_lbl 8603 `"Paiute"', add
cap label define languaged_lbl 8604 `"Northern Paiute, Bannock, Num, Snake"', add
cap label define languaged_lbl 8605 `"Southern Paiute"', add
cap label define languaged_lbl 8606 `"Chemehuevi"', add
cap label define languaged_lbl 8607 `"Kawaiisu"', add
cap label define languaged_lbl 8608 `"Ute"', add
cap label define languaged_lbl 8609 `"Shoshoni"', add
cap label define languaged_lbl 8610 `"Panamint"', add
cap label define languaged_lbl 8620 `"Hopi"', add
cap label define languaged_lbl 8630 `"Cahuilla"', add
cap label define languaged_lbl 8631 `"Cupeno"', add
cap label define languaged_lbl 8632 `"Luiseno"', add
cap label define languaged_lbl 8633 `"Serrano"', add
cap label define languaged_lbl 8640 `"Tubatulabal"', add
cap label define languaged_lbl 8700 `"Pima, Papago"', add
cap label define languaged_lbl 8800 `"Yaqui"', add
cap label define languaged_lbl 8810 `"Sonoran n.e.c., Cahita, Guasave, Huichole, Nayit, Tarahumar"', add
cap label define languaged_lbl 8820 `"Tarahumara"', add
cap label define languaged_lbl 8900 `"Aztecan, Nahuatl, Uto-Aztecan"', add
cap label define languaged_lbl 8910 `"Aztecan, Mexicano, Nahua"', add
cap label define languaged_lbl 9000 `"Tanoan languages"', add
cap label define languaged_lbl 9010 `"Picuris, Northern Tiwa, Taos"', add
cap label define languaged_lbl 9020 `"Tiwa, Isleta"', add
cap label define languaged_lbl 9030 `"Sandia"', add
cap label define languaged_lbl 9040 `"Tewa, Hano, Hopi-Tewa, San Ildefonso, San Juan, Santa Clara"', add
cap label define languaged_lbl 9050 `"Towa"', add
cap label define languaged_lbl 9100 `"Wiyot"', add
cap label define languaged_lbl 9101 `"Yurok"', add
cap label define languaged_lbl 9110 `"Kwakiutl"', add
cap label define languaged_lbl 9111 `"Nootka"', add
cap label define languaged_lbl 9112 `"Makah"', add
cap label define languaged_lbl 9120 `"Kutenai"', add
cap label define languaged_lbl 9130 `"Haida"', add
cap label define languaged_lbl 9131 `"Tlingit, Chilkat, Sitka, Tongass, Yakutat"', add
cap label define languaged_lbl 9140 `"Tonkawa"', add
cap label define languaged_lbl 9150 `"Yuchi"', add
cap label define languaged_lbl 9160 `"Chetemacha"', add
cap label define languaged_lbl 9170 `"Yuki"', add
cap label define languaged_lbl 9171 `"Wappo"', add
cap label define languaged_lbl 9200 `"Mayan languages"', add
cap label define languaged_lbl 9210 `"Misumalpan"', add
cap label define languaged_lbl 9211 `"Cakchiquel"', add
cap label define languaged_lbl 9212 `"Mam"', add
cap label define languaged_lbl 9213 `"Maya"', add
cap label define languaged_lbl 9214 `"Quekchi"', add
cap label define languaged_lbl 9215 `"Quiche"', add
cap label define languaged_lbl 9220 `"Tarascan"', add
cap label define languaged_lbl 9230 `"Mapuche"', add
cap label define languaged_lbl 9231 `"Araucanian"', add
cap label define languaged_lbl 9240 `"Oto-Manguen"', add
cap label define languaged_lbl 9241 `"Mixtec"', add
cap label define languaged_lbl 9242 `"Zapotec"', add
cap label define languaged_lbl 9250 `"Quechua"', add
cap label define languaged_lbl 9260 `"Aymara"', add
cap label define languaged_lbl 9270 `"Arawakian"', add
cap label define languaged_lbl 9271 `"Island Caribs"', add
cap label define languaged_lbl 9280 `"Chibchan"', add
cap label define languaged_lbl 9281 `"Cuna"', add
cap label define languaged_lbl 9282 `"Guaymi"', add
cap label define languaged_lbl 9290 `"Tupi-Guarani"', add
cap label define languaged_lbl 9291 `"Tupi"', add
cap label define languaged_lbl 9292 `"Guarani"', add
cap label define languaged_lbl 9300 `"American Indian, n.s."', add
cap label define languaged_lbl 9400 `"Native"', add
cap label define languaged_lbl 9410 `"Other specified American Indian languages"', add
cap label define languaged_lbl 9420 `"South/Central American Indian"', add
cap label define languaged_lbl 9500 `"No language"', add
cap label define languaged_lbl 9600 `"Other or not reported"', add
cap label define languaged_lbl 9601 `"Other n.e.c."', add
cap label define languaged_lbl 9602 `"Other n.s."', add
cap label define languaged_lbl 9999 `"9999"', add
cap label values languaged languaged_lbl

cap label define speakeng_lbl 0 `"N/A (Blank)"'
cap label define speakeng_lbl 1 `"Does not speak English"', add
cap label define speakeng_lbl 2 `"Yes, speaks English..."', add
cap label define speakeng_lbl 3 `"Yes, speaks only English"', add
cap label define speakeng_lbl 4 `"Yes, speaks very well"', add
cap label define speakeng_lbl 5 `"Yes, speaks well"', add
cap label define speakeng_lbl 6 `"Yes, but not well"', add
cap label define speakeng_lbl 7 `"Unknown"', add
cap label define speakeng_lbl 8 `"Illegible"', add
cap label values speakeng speakeng_lbl

cap label define school_lbl 0 `"N/A"'
cap label define school_lbl 1 `"No, not in school"', add
cap label define school_lbl 2 `"Yes, in school"', add
cap label define school_lbl 9 `"Missing"', add
cap label values school school_lbl

cap label define educ_lbl 00 `"N/A or no schooling"'
cap label define educ_lbl 01 `"Nursery school to grade 4"', add
cap label define educ_lbl 02 `"Grade 5, 6, 7, or 8"', add
cap label define educ_lbl 03 `"Grade 9"', add
cap label define educ_lbl 04 `"Grade 10"', add
cap label define educ_lbl 05 `"Grade 11"', add
cap label define educ_lbl 06 `"Grade 12"', add
cap label define educ_lbl 07 `"1 year of college"', add
cap label define educ_lbl 08 `"2 years of college"', add
cap label define educ_lbl 09 `"3 years of college"', add
cap label define educ_lbl 10 `"4 years of college"', add
cap label define educ_lbl 11 `"5+ years of college"', add
cap label values educ educ_lbl

cap label define educd_lbl 000 `"N/A or no schooling"'
cap label define educd_lbl 001 `"N/A"', add
cap label define educd_lbl 002 `"No schooling completed"', add
cap label define educd_lbl 010 `"Nursery school to grade 4"', add
cap label define educd_lbl 011 `"Nursery school, preschool"', add
cap label define educd_lbl 012 `"Kindergarten"', add
cap label define educd_lbl 013 `"Grade 1, 2, 3, or 4"', add
cap label define educd_lbl 014 `"Grade 1"', add
cap label define educd_lbl 015 `"Grade 2"', add
cap label define educd_lbl 016 `"Grade 3"', add
cap label define educd_lbl 017 `"Grade 4"', add
cap label define educd_lbl 020 `"Grade 5, 6, 7, or 8"', add
cap label define educd_lbl 021 `"Grade 5 or 6"', add
cap label define educd_lbl 022 `"Grade 5"', add
cap label define educd_lbl 023 `"Grade 6"', add
cap label define educd_lbl 024 `"Grade 7 or 8"', add
cap label define educd_lbl 025 `"Grade 7"', add
cap label define educd_lbl 026 `"Grade 8"', add
cap label define educd_lbl 030 `"Grade 9"', add
cap label define educd_lbl 040 `"Grade 10"', add
cap label define educd_lbl 050 `"Grade 11"', add
cap label define educd_lbl 060 `"Grade 12"', add
cap label define educd_lbl 061 `"12th grade, no diploma"', add
cap label define educd_lbl 062 `"High school graduate or GED"', add
cap label define educd_lbl 063 `"Regular high school diploma"', add
cap label define educd_lbl 064 `"GED or alternative credential"', add
cap label define educd_lbl 065 `"Some college, but less than 1 year"', add
cap label define educd_lbl 070 `"1 year of college"', add
cap label define educd_lbl 071 `"1 or more years of college credit, no degree"', add
cap label define educd_lbl 080 `"2 years of college"', add
cap label define educd_lbl 081 `"Associate's degree, type not specified"', add
cap label define educd_lbl 082 `"Associate's degree, occupational program"', add
cap label define educd_lbl 083 `"Associate's degree, academic program"', add
cap label define educd_lbl 090 `"3 years of college"', add
cap label define educd_lbl 100 `"4 years of college"', add
cap label define educd_lbl 101 `"Bachelor's degree"', add
cap label define educd_lbl 110 `"5+ years of college"', add
cap label define educd_lbl 111 `"6 years of college (6+ in 1960-1970)"', add
cap label define educd_lbl 112 `"7 years of college"', add
cap label define educd_lbl 113 `"8+ years of college"', add
cap label define educd_lbl 114 `"Master's degree"', add
cap label define educd_lbl 115 `"Professional degree beyond a bachelor's degree"', add
cap label define educd_lbl 116 `"Doctoral degree"', add
cap label define educd_lbl 999 `"Missing"', add
cap label values educd educd_lbl

cap label define empstat_lbl 0 `"N/A"'
cap label define empstat_lbl 1 `"Employed"', add
cap label define empstat_lbl 2 `"Unemployed"', add
cap label define empstat_lbl 3 `"Not in labor force"', add
cap label values empstat empstat_lbl

cap label define empstatd_lbl 00 `"N/A"'
cap label define empstatd_lbl 10 `"At work"', add
cap label define empstatd_lbl 11 `"At work, public emerg"', add
cap label define empstatd_lbl 12 `"Has job, not working"', add
cap label define empstatd_lbl 13 `"Armed forces"', add
cap label define empstatd_lbl 14 `"Armed forces--at work"', add
cap label define empstatd_lbl 15 `"Armed forces--not at work but with job"', add
cap label define empstatd_lbl 20 `"Unemployed"', add
cap label define empstatd_lbl 21 `"Unemp, exper worker"', add
cap label define empstatd_lbl 22 `"Unemp, new worker"', add
cap label define empstatd_lbl 30 `"Not in Labor Force"', add
cap label define empstatd_lbl 31 `"NILF, housework"', add
cap label define empstatd_lbl 32 `"NILF, unable to work"', add
cap label define empstatd_lbl 33 `"NILF, school"', add
cap label define empstatd_lbl 34 `"NILF, other"', add
cap label values empstatd empstatd_lbl

cap label define labforce_lbl 0 `"N/A"'
cap label define labforce_lbl 1 `"No, not in the labor force"', add
cap label define labforce_lbl 2 `"Yes, in the labor force"', add
cap label values labforce labforce_lbl

cap label define occ1990_lbl 003 `"Legislators"'
cap label define occ1990_lbl 004 `"Chief executives and public administrators"', add
cap label define occ1990_lbl 007 `"Financial managers"', add
cap label define occ1990_lbl 008 `"Human resources and labor relations managers"', add
cap label define occ1990_lbl 013 `"Managers and specialists in marketing, advertising, and public relations"', add
cap label define occ1990_lbl 014 `"Managers in education and related fields"', add
cap label define occ1990_lbl 015 `"Managers of medicine and health occupations"', add
cap label define occ1990_lbl 016 `"Postmasters and mail superintendents"', add
cap label define occ1990_lbl 017 `"Managers of food-serving and lodging establishments"', add
cap label define occ1990_lbl 018 `"Managers of properties and real estate"', add
cap label define occ1990_lbl 019 `"Funeral directors"', add
cap label define occ1990_lbl 021 `"Managers of service organizations, n.e.c."', add
cap label define occ1990_lbl 022 `"Managers and administrators, n.e.c."', add
cap label define occ1990_lbl 023 `"Accountants and auditors"', add
cap label define occ1990_lbl 024 `"Insurance underwriters"', add
cap label define occ1990_lbl 025 `"Other financial specialists"', add
cap label define occ1990_lbl 026 `"Management analysts"', add
cap label define occ1990_lbl 027 `"Personnel, HR, training, and labor relations specialists"', add
cap label define occ1990_lbl 028 `"Purchasing agents and buyers, of farm products"', add
cap label define occ1990_lbl 029 `"Buyers, wholesale and retail trade"', add
cap label define occ1990_lbl 033 `"Purchasing managers, agents and buyers, n.e.c."', add
cap label define occ1990_lbl 034 `"Business and promotion agents"', add
cap label define occ1990_lbl 035 `"Construction inspectors"', add
cap label define occ1990_lbl 036 `"Inspectors and compliance officers, outside construction"', add
cap label define occ1990_lbl 037 `"Management support occupations"', add
cap label define occ1990_lbl 043 `"Architects"', add
cap label define occ1990_lbl 044 `"Aerospace engineer"', add
cap label define occ1990_lbl 045 `"Metallurgical and materials engineers, variously phrased"', add
cap label define occ1990_lbl 047 `"Petroleum, mining, and geological engineers"', add
cap label define occ1990_lbl 048 `"Chemical engineers"', add
cap label define occ1990_lbl 053 `"Civil engineers"', add
cap label define occ1990_lbl 055 `"Electrical engineer"', add
cap label define occ1990_lbl 056 `"Industrial engineers"', add
cap label define occ1990_lbl 057 `"Mechanical engineers"', add
cap label define occ1990_lbl 059 `"Not-elsewhere-classified engineers"', add
cap label define occ1990_lbl 064 `"Computer systems analysts and computer scientists"', add
cap label define occ1990_lbl 065 `"Operations and systems researchers and analysts"', add
cap label define occ1990_lbl 066 `"Actuaries"', add
cap label define occ1990_lbl 067 `"Statisticians"', add
cap label define occ1990_lbl 068 `"Mathematicians and mathematical scientists"', add
cap label define occ1990_lbl 069 `"Physicists and astronomers"', add
cap label define occ1990_lbl 073 `"Chemists"', add
cap label define occ1990_lbl 074 `"Atmospheric and space scientists"', add
cap label define occ1990_lbl 075 `"Geologists"', add
cap label define occ1990_lbl 076 `"Physical scientists, n.e.c."', add
cap label define occ1990_lbl 077 `"Agricultural and food scientists"', add
cap label define occ1990_lbl 078 `"Biological scientists"', add
cap label define occ1990_lbl 079 `"Foresters and conservation scientists"', add
cap label define occ1990_lbl 083 `"Medical scientists"', add
cap label define occ1990_lbl 084 `"Physicians"', add
cap label define occ1990_lbl 085 `"Dentists"', add
cap label define occ1990_lbl 086 `"Veterinarians"', add
cap label define occ1990_lbl 087 `"Optometrists"', add
cap label define occ1990_lbl 088 `"Podiatrists"', add
cap label define occ1990_lbl 089 `"Other health and therapy"', add
cap label define occ1990_lbl 095 `"Registered nurses"', add
cap label define occ1990_lbl 096 `"Pharmacists"', add
cap label define occ1990_lbl 097 `"Dietitians and nutritionists"', add
cap label define occ1990_lbl 098 `"Respiratory therapists"', add
cap label define occ1990_lbl 099 `"Occupational therapists"', add
cap label define occ1990_lbl 103 `"Physical therapists"', add
cap label define occ1990_lbl 104 `"Speech therapists"', add
cap label define occ1990_lbl 105 `"Therapists, n.e.c."', add
cap label define occ1990_lbl 106 `"Physicians' assistants"', add
cap label define occ1990_lbl 113 `"Earth, environmental, and marine science instructors"', add
cap label define occ1990_lbl 114 `"Biological science instructors"', add
cap label define occ1990_lbl 115 `"Chemistry instructors"', add
cap label define occ1990_lbl 116 `"Physics instructors"', add
cap label define occ1990_lbl 118 `"Psychology instructors"', add
cap label define occ1990_lbl 119 `"Economics instructors"', add
cap label define occ1990_lbl 123 `"History instructors"', add
cap label define occ1990_lbl 125 `"Sociology instructors"', add
cap label define occ1990_lbl 127 `"Engineering instructors"', add
cap label define occ1990_lbl 128 `"Math instructors"', add
cap label define occ1990_lbl 139 `"Education instructors"', add
cap label define occ1990_lbl 145 `"Law instructors"', add
cap label define occ1990_lbl 147 `"Theology instructors"', add
cap label define occ1990_lbl 149 `"Home economics instructors"', add
cap label define occ1990_lbl 150 `"Humanities profs/instructors, college, nec"', add
cap label define occ1990_lbl 154 `"Subject instructors (HS/college)"', add
cap label define occ1990_lbl 155 `"Kindergarten and earlier school teachers"', add
cap label define occ1990_lbl 156 `"Primary school teachers"', add
cap label define occ1990_lbl 157 `"Secondary school teachers"', add
cap label define occ1990_lbl 158 `"Special education teachers"', add
cap label define occ1990_lbl 159 `"Teachers , n.e.c."', add
cap label define occ1990_lbl 163 `"Vocational and educational counselors"', add
cap label define occ1990_lbl 164 `"Librarians"', add
cap label define occ1990_lbl 165 `"Archivists and curators"', add
cap label define occ1990_lbl 166 `"Economists, market researchers, and survey researchers"', add
cap label define occ1990_lbl 167 `"Psychologists"', add
cap label define occ1990_lbl 168 `"Sociologists"', add
cap label define occ1990_lbl 169 `"Social scientists, n.e.c."', add
cap label define occ1990_lbl 173 `"Urban and regional planners"', add
cap label define occ1990_lbl 174 `"Social workers"', add
cap label define occ1990_lbl 175 `"Recreation workers"', add
cap label define occ1990_lbl 176 `"Clergy and religious workers"', add
cap label define occ1990_lbl 178 `"Lawyers"', add
cap label define occ1990_lbl 179 `"Judges"', add
cap label define occ1990_lbl 183 `"Writers and authors"', add
cap label define occ1990_lbl 184 `"Technical writers"', add
cap label define occ1990_lbl 185 `"Designers"', add
cap label define occ1990_lbl 186 `"Musician or composer"', add
cap label define occ1990_lbl 187 `"Actors, directors, producers"', add
cap label define occ1990_lbl 188 `"Art makers: painters, sculptors, craft-artists, and print-makers"', add
cap label define occ1990_lbl 189 `"Photographers"', add
cap label define occ1990_lbl 193 `"Dancers"', add
cap label define occ1990_lbl 194 `"Art/entertainment performers and related"', add
cap label define occ1990_lbl 195 `"Editors and reporters"', add
cap label define occ1990_lbl 198 `"Announcers"', add
cap label define occ1990_lbl 199 `"Athletes, sports instructors, and officials"', add
cap label define occ1990_lbl 200 `"Professionals, n.e.c."', add
cap label define occ1990_lbl 203 `"Clinical laboratory technologies and technicians"', add
cap label define occ1990_lbl 204 `"Dental hygenists"', add
cap label define occ1990_lbl 205 `"Health record tech specialists"', add
cap label define occ1990_lbl 206 `"Radiologic tech specialists"', add
cap label define occ1990_lbl 207 `"Licensed practical nurses"', add
cap label define occ1990_lbl 208 `"Health technologists and technicians, n.e.c."', add
cap label define occ1990_lbl 213 `"Electrical and electronic (engineering) technicians"', add
cap label define occ1990_lbl 214 `"Engineering technicians, n.e.c."', add
cap label define occ1990_lbl 215 `"Mechanical engineering technicians"', add
cap label define occ1990_lbl 217 `"Drafters"', add
cap label define occ1990_lbl 218 `"Surveyors, cartographers, mapping scientists and technicians"', add
cap label define occ1990_lbl 223 `"Biological technicians"', add
cap label define occ1990_lbl 224 `"Chemical technicians"', add
cap label define occ1990_lbl 225 `"Other science technicians"', add
cap label define occ1990_lbl 226 `"Airplane pilots and navigators"', add
cap label define occ1990_lbl 227 `"Air traffic controllers"', add
cap label define occ1990_lbl 228 `"Broadcast equipment operators"', add
cap label define occ1990_lbl 229 `"Computer software developers"', add
cap label define occ1990_lbl 233 `"Programmers of numerically controlled machine tools"', add
cap label define occ1990_lbl 234 `"Legal assistants, paralegals, legal support, etc"', add
cap label define occ1990_lbl 235 `"Technicians, n.e.c."', add
cap label define occ1990_lbl 243 `"Supervisors and proprietors of sales jobs"', add
cap label define occ1990_lbl 253 `"Insurance sales occupations"', add
cap label define occ1990_lbl 254 `"Real estate sales occupations"', add
cap label define occ1990_lbl 255 `"Financial services sales occupations"', add
cap label define occ1990_lbl 256 `"Advertising and related sales jobs"', add
cap label define occ1990_lbl 258 `"Sales engineers"', add
cap label define occ1990_lbl 274 `"Salespersons, n.e.c."', add
cap label define occ1990_lbl 275 `"Retail sales clerks"', add
cap label define occ1990_lbl 276 `"Cashiers"', add
cap label define occ1990_lbl 277 `"Door-to-door sales, street sales, and news vendors"', add
cap label define occ1990_lbl 283 `"Sales demonstrators / promoters / models"', add
cap label define occ1990_lbl 303 `"Office supervisors"', add
cap label define occ1990_lbl 308 `"Computer and peripheral equipment operators"', add
cap label define occ1990_lbl 313 `"Secretaries"', add
cap label define occ1990_lbl 314 `"Stenographers"', add
cap label define occ1990_lbl 315 `"Typists"', add
cap label define occ1990_lbl 316 `"Interviewers, enumerators, and surveyors"', add
cap label define occ1990_lbl 317 `"Hotel clerks"', add
cap label define occ1990_lbl 318 `"Transportation ticket and reservation agents"', add
cap label define occ1990_lbl 319 `"Receptionists"', add
cap label define occ1990_lbl 323 `"Information clerks, nec"', add
cap label define occ1990_lbl 326 `"Correspondence and order clerks"', add
cap label define occ1990_lbl 328 `"Human resources clerks, except payroll and timekeeping"', add
cap label define occ1990_lbl 329 `"Library assistants"', add
cap label define occ1990_lbl 335 `"File clerks"', add
cap label define occ1990_lbl 336 `"Records clerks"', add
cap label define occ1990_lbl 337 `"Bookkeepers and accounting and auditing clerks"', add
cap label define occ1990_lbl 338 `"Payroll and timekeeping clerks"', add
cap label define occ1990_lbl 343 `"Cost and rate clerks (financial records processing)"', add
cap label define occ1990_lbl 344 `"Billing clerks and related financial records processing"', add
cap label define occ1990_lbl 345 `"Duplication machine operators / office machine operators"', add
cap label define occ1990_lbl 346 `"Mail and paper handlers"', add
cap label define occ1990_lbl 347 `"Office machine operators, n.e.c."', add
cap label define occ1990_lbl 348 `"Telephone operators"', add
cap label define occ1990_lbl 349 `"Other telecom operators"', add
cap label define occ1990_lbl 354 `"Postal clerks, excluding mail carriers"', add
cap label define occ1990_lbl 355 `"Mail carriers for postal service"', add
cap label define occ1990_lbl 356 `"Mail clerks, outside of post office"', add
cap label define occ1990_lbl 357 `"Messengers"', add
cap label define occ1990_lbl 359 `"Dispatchers"', add
cap label define occ1990_lbl 361 `"Inspectors, n.e.c."', add
cap label define occ1990_lbl 364 `"Shipping and receiving clerks"', add
cap label define occ1990_lbl 365 `"Stock and inventory clerks"', add
cap label define occ1990_lbl 366 `"Meter readers"', add
cap label define occ1990_lbl 368 `"Weighers, measurers, and checkers"', add
cap label define occ1990_lbl 373 `"Material recording, scheduling, production, planning, and expediting clerks"', add
cap label define occ1990_lbl 375 `"Insurance adjusters, examiners, and investigators"', add
cap label define occ1990_lbl 376 `"Customer service reps, investigators and adjusters, except insurance"', add
cap label define occ1990_lbl 377 `"Eligibility clerks for government programs; social welfare"', add
cap label define occ1990_lbl 378 `"Bill and account collectors"', add
cap label define occ1990_lbl 379 `"General office clerks"', add
cap label define occ1990_lbl 383 `"Bank tellers"', add
cap label define occ1990_lbl 384 `"Proofreaders"', add
cap label define occ1990_lbl 385 `"Data entry keyers"', add
cap label define occ1990_lbl 386 `"Statistical clerks"', add
cap label define occ1990_lbl 387 `"Teacher's aides"', add
cap label define occ1990_lbl 389 `"Administrative support jobs, n.e.c."', add
cap label define occ1990_lbl 405 `"Housekeepers, maids, butlers, stewards, and lodging quarters cleaners"', add
cap label define occ1990_lbl 407 `"Private household cleaners and servants"', add
cap label define occ1990_lbl 415 `"Supervisors of guards"', add
cap label define occ1990_lbl 417 `"Fire fighting, prevention, and inspection"', add
cap label define occ1990_lbl 418 `"Police, detectives, and private investigators"', add
cap label define occ1990_lbl 423 `"Other law enforcement: sheriffs, bailiffs, correctional institution officers"', add
cap label define occ1990_lbl 425 `"Crossing guards and bridge tenders"', add
cap label define occ1990_lbl 426 `"Guards, watchmen, doorkeepers"', add
cap label define occ1990_lbl 427 `"Protective services, n.e.c."', add
cap label define occ1990_lbl 434 `"Bartenders"', add
cap label define occ1990_lbl 435 `"Waiter/waitress"', add
cap label define occ1990_lbl 436 `"Cooks, variously defined"', add
cap label define occ1990_lbl 438 `"Food counter and fountain workers"', add
cap label define occ1990_lbl 439 `"Kitchen workers"', add
cap label define occ1990_lbl 443 `"Waiter's assistant"', add
cap label define occ1990_lbl 444 `"Misc food prep workers"', add
cap label define occ1990_lbl 445 `"Dental assistants"', add
cap label define occ1990_lbl 446 `"Health aides, except nursing"', add
cap label define occ1990_lbl 447 `"Nursing aides, orderlies, and attendants"', add
cap label define occ1990_lbl 448 `"Supervisors of cleaning and building service"', add
cap label define occ1990_lbl 453 `"Janitors"', add
cap label define occ1990_lbl 454 `"Elevator operators"', add
cap label define occ1990_lbl 455 `"Pest control occupations"', add
cap label define occ1990_lbl 456 `"Supervisors of personal service jobs, n.e.c."', add
cap label define occ1990_lbl 457 `"Barbers"', add
cap label define occ1990_lbl 458 `"Hairdressers and cosmetologists"', add
cap label define occ1990_lbl 459 `"Recreation facility attendants"', add
cap label define occ1990_lbl 461 `"Guides"', add
cap label define occ1990_lbl 462 `"Ushers"', add
cap label define occ1990_lbl 463 `"Public transportation attendants and inspectors"', add
cap label define occ1990_lbl 464 `"Baggage porters"', add
cap label define occ1990_lbl 465 `"Welfare service aides"', add
cap label define occ1990_lbl 468 `"Child care workers"', add
cap label define occ1990_lbl 469 `"Personal service occupations, nec"', add
cap label define occ1990_lbl 473 `"Farmers (owners and tenants)"', add
cap label define occ1990_lbl 474 `"Horticultural specialty farmers"', add
cap label define occ1990_lbl 475 `"Farm managers, except for horticultural farms"', add
cap label define occ1990_lbl 476 `"Managers of horticultural specialty farms"', add
cap label define occ1990_lbl 479 `"Farm workers"', add
cap label define occ1990_lbl 483 `"Marine life cultivation workers"', add
cap label define occ1990_lbl 484 `"Nursery farming workers"', add
cap label define occ1990_lbl 485 `"Supervisors of agricultural occupations"', add
cap label define occ1990_lbl 486 `"Gardeners and groundskeepers"', add
cap label define occ1990_lbl 487 `"Animal caretakers except on farms"', add
cap label define occ1990_lbl 488 `"Graders and sorters of agricultural products"', add
cap label define occ1990_lbl 489 `"Inspectors of agricultural products"', add
cap label define occ1990_lbl 496 `"Timber, logging, and forestry workers"', add
cap label define occ1990_lbl 498 `"Fishers, hunters, and kindred"', add
cap label define occ1990_lbl 503 `"Supervisors of mechanics and repairers"', add
cap label define occ1990_lbl 505 `"Automobile mechanics"', add
cap label define occ1990_lbl 507 `"Bus, truck, and stationary engine mechanics"', add
cap label define occ1990_lbl 508 `"Aircraft mechanics"', add
cap label define occ1990_lbl 509 `"Small engine repairers"', add
cap label define occ1990_lbl 514 `"Auto body repairers"', add
cap label define occ1990_lbl 516 `"Heavy equipment and farm equipment mechanics"', add
cap label define occ1990_lbl 518 `"Industrial machinery repairers"', add
cap label define occ1990_lbl 519 `"Machinery maintenance occupations"', add
cap label define occ1990_lbl 523 `"Repairers of industrial electrical equipment"', add
cap label define occ1990_lbl 525 `"Repairers of data processing equipment"', add
cap label define occ1990_lbl 526 `"Repairers of household appliances and power tools"', add
cap label define occ1990_lbl 527 `"Telecom and line installers and repairers"', add
cap label define occ1990_lbl 533 `"Repairers of electrical equipment, n.e.c."', add
cap label define occ1990_lbl 534 `"Heating, air conditioning, and refigeration mechanics"', add
cap label define occ1990_lbl 535 `"Precision makers, repairers, and smiths"', add
cap label define occ1990_lbl 536 `"Locksmiths and safe repairers"', add
cap label define occ1990_lbl 538 `"Office machine repairers and mechanics"', add
cap label define occ1990_lbl 539 `"Repairers of mechanical controls and valves"', add
cap label define occ1990_lbl 543 `"Elevator installers and repairers"', add
cap label define occ1990_lbl 544 `"Millwrights"', add
cap label define occ1990_lbl 549 `"Mechanics and repairers, n.e.c."', add
cap label define occ1990_lbl 558 `"Supervisors of construction work"', add
cap label define occ1990_lbl 563 `"Masons, tilers, and carpet installers"', add
cap label define occ1990_lbl 567 `"Carpenters"', add
cap label define occ1990_lbl 573 `"Drywall installers"', add
cap label define occ1990_lbl 575 `"Electricians"', add
cap label define occ1990_lbl 577 `"Electric power installers and repairers"', add
cap label define occ1990_lbl 579 `"Painters, construction and maintenance"', add
cap label define occ1990_lbl 583 `"Paperhangers"', add
cap label define occ1990_lbl 584 `"Plasterers"', add
cap label define occ1990_lbl 585 `"Plumbers, pipe fitters, and steamfitters"', add
cap label define occ1990_lbl 588 `"Concrete and cement workers"', add
cap label define occ1990_lbl 589 `"Glaziers"', add
cap label define occ1990_lbl 593 `"Insulation workers"', add
cap label define occ1990_lbl 594 `"Paving, surfacing, and tamping equipment operators"', add
cap label define occ1990_lbl 595 `"Roofers and slaters"', add
cap label define occ1990_lbl 596 `"Sheet metal duct installers"', add
cap label define occ1990_lbl 597 `"Structural metal workers"', add
cap label define occ1990_lbl 598 `"Drillers of earth"', add
cap label define occ1990_lbl 599 `"Construction trades, n.e.c."', add
cap label define occ1990_lbl 614 `"Drillers of oil wells"', add
cap label define occ1990_lbl 615 `"Explosives workers"', add
cap label define occ1990_lbl 616 `"Miners"', add
cap label define occ1990_lbl 617 `"Other mining occupations"', add
cap label define occ1990_lbl 628 `"Production supervisors or foremen"', add
cap label define occ1990_lbl 634 `"Tool and die makers and die setters"', add
cap label define occ1990_lbl 637 `"Machinists"', add
cap label define occ1990_lbl 643 `"Boilermakers"', add
cap label define occ1990_lbl 644 `"Precision grinders and filers"', add
cap label define occ1990_lbl 645 `"Patternmakers and model makers"', add
cap label define occ1990_lbl 646 `"Lay-out workers"', add
cap label define occ1990_lbl 649 `"Engravers"', add
cap label define occ1990_lbl 653 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
cap label define occ1990_lbl 657 `"Cabinetmakers and bench carpenters"', add
cap label define occ1990_lbl 658 `"Furniture and wood finishers"', add
cap label define occ1990_lbl 659 `"Other precision woodworkers"', add
cap label define occ1990_lbl 666 `"Dressmakers and seamstresses"', add
cap label define occ1990_lbl 667 `"Tailors"', add
cap label define occ1990_lbl 668 `"Upholsterers"', add
cap label define occ1990_lbl 669 `"Shoe repairers"', add
cap label define occ1990_lbl 674 `"Other precision apparel and fabric workers"', add
cap label define occ1990_lbl 675 `"Hand molders and shapers, except jewelers"', add
cap label define occ1990_lbl 677 `"Optical goods workers"', add
cap label define occ1990_lbl 678 `"Dental laboratory and medical appliance technicians"', add
cap label define occ1990_lbl 679 `"Bookbinders"', add
cap label define occ1990_lbl 684 `"Other precision and craft workers"', add
cap label define occ1990_lbl 686 `"Butchers and meat cutters"', add
cap label define occ1990_lbl 687 `"Bakers"', add
cap label define occ1990_lbl 688 `"Batch food makers"', add
cap label define occ1990_lbl 693 `"Adjusters and calibrators"', add
cap label define occ1990_lbl 694 `"Water and sewage treatment plant operators"', add
cap label define occ1990_lbl 695 `"Power plant operators"', add
cap label define occ1990_lbl 696 `"Plant and system operators, stationary engineers"', add
cap label define occ1990_lbl 699 `"Other plant and system operators"', add
cap label define occ1990_lbl 703 `"Lathe, milling, and turning machine operatives"', add
cap label define occ1990_lbl 706 `"Punching and stamping press operatives"', add
cap label define occ1990_lbl 707 `"Rollers, roll hands, and finishers of metal"', add
cap label define occ1990_lbl 708 `"Drilling and boring machine operators"', add
cap label define occ1990_lbl 709 `"Grinding, abrading, buffing, and polishing workers"', add
cap label define occ1990_lbl 713 `"Forge and hammer operators"', add
cap label define occ1990_lbl 717 `"Fabricating machine operators, n.e.c."', add
cap label define occ1990_lbl 719 `"Molders, and casting machine operators"', add
cap label define occ1990_lbl 723 `"Metal platers"', add
cap label define occ1990_lbl 724 `"Heat treating equipment operators"', add
cap label define occ1990_lbl 726 `"Wood lathe, routing, and planing machine operators"', add
cap label define occ1990_lbl 727 `"Sawing machine operators and sawyers"', add
cap label define occ1990_lbl 728 `"Shaping and joining machine operator (woodworking)"', add
cap label define occ1990_lbl 729 `"Nail and tacking machine operators  (woodworking)"', add
cap label define occ1990_lbl 733 `"Other woodworking machine operators"', add
cap label define occ1990_lbl 734 `"Printing machine operators, n.e.c."', add
cap label define occ1990_lbl 735 `"Photoengravers and lithographers"', add
cap label define occ1990_lbl 736 `"Typesetters and compositors"', add
cap label define occ1990_lbl 738 `"Winding and twisting textile/apparel operatives"', add
cap label define occ1990_lbl 739 `"Knitters, loopers, and toppers textile operatives"', add
cap label define occ1990_lbl 743 `"Textile cutting machine operators"', add
cap label define occ1990_lbl 744 `"Textile sewing machine operators"', add
cap label define occ1990_lbl 745 `"Shoemaking machine operators"', add
cap label define occ1990_lbl 747 `"Pressing machine operators (clothing)"', add
cap label define occ1990_lbl 748 `"Laundry workers"', add
cap label define occ1990_lbl 749 `"Misc textile machine operators"', add
cap label define occ1990_lbl 753 `"Cementing and gluing maching operators"', add
cap label define occ1990_lbl 754 `"Packers, fillers, and wrappers"', add
cap label define occ1990_lbl 755 `"Extruding and forming machine operators"', add
cap label define occ1990_lbl 756 `"Mixing and blending machine operatives"', add
cap label define occ1990_lbl 757 `"Separating, filtering, and clarifying machine operators"', add
cap label define occ1990_lbl 759 `"Painting machine operators"', add
cap label define occ1990_lbl 763 `"Roasting and baking machine operators (food)"', add
cap label define occ1990_lbl 764 `"Washing, cleaning, and pickling machine operators"', add
cap label define occ1990_lbl 765 `"Paper folding machine operators"', add
cap label define occ1990_lbl 766 `"Furnace, kiln, and oven operators, apart from food"', add
cap label define occ1990_lbl 768 `"Crushing and grinding machine operators"', add
cap label define occ1990_lbl 769 `"Slicing and cutting machine operators"', add
cap label define occ1990_lbl 773 `"Motion picture projectionists"', add
cap label define occ1990_lbl 774 `"Photographic process workers"', add
cap label define occ1990_lbl 779 `"Machine operators, n.e.c."', add
cap label define occ1990_lbl 783 `"Welders and metal cutters"', add
cap label define occ1990_lbl 784 `"Solderers"', add
cap label define occ1990_lbl 785 `"Assemblers of electrical equipment"', add
cap label define occ1990_lbl 789 `"Hand painting, coating, and decorating occupations"', add
cap label define occ1990_lbl 796 `"Production checkers and inspectors"', add
cap label define occ1990_lbl 799 `"Graders and sorters in manufacturing"', add
cap label define occ1990_lbl 803 `"Supervisors of motor vehicle transportation"', add
cap label define occ1990_lbl 804 `"Truck, delivery, and tractor drivers"', add
cap label define occ1990_lbl 808 `"Bus drivers"', add
cap label define occ1990_lbl 809 `"Taxi cab drivers and chauffeurs"', add
cap label define occ1990_lbl 813 `"Parking lot attendants"', add
cap label define occ1990_lbl 823 `"Railroad conductors and yardmasters"', add
cap label define occ1990_lbl 824 `"Locomotive operators (engineers and firemen)"', add
cap label define occ1990_lbl 825 `"Railroad brake, coupler, and switch operators"', add
cap label define occ1990_lbl 829 `"Ship crews and marine engineers"', add
cap label define occ1990_lbl 834 `"Water transport infrastructure tenders and crossing guards"', add
cap label define occ1990_lbl 844 `"Operating engineers of construction equipment"', add
cap label define occ1990_lbl 848 `"Crane, derrick, winch, and hoist operators"', add
cap label define occ1990_lbl 853 `"Excavating and loading machine operators"', add
cap label define occ1990_lbl 859 `"Misc material moving occupations"', add
cap label define occ1990_lbl 865 `"Helpers, constructions"', add
cap label define occ1990_lbl 866 `"Helpers, surveyors"', add
cap label define occ1990_lbl 869 `"Construction laborers"', add
cap label define occ1990_lbl 874 `"Production helpers"', add
cap label define occ1990_lbl 875 `"Garbage and recyclable material collectors"', add
cap label define occ1990_lbl 876 `"Materials movers: stevedores and longshore workers"', add
cap label define occ1990_lbl 877 `"Stock handlers"', add
cap label define occ1990_lbl 878 `"Machine feeders and offbearers"', add
cap label define occ1990_lbl 883 `"Freight, stock, and materials handlers"', add
cap label define occ1990_lbl 885 `"Garage and service station related occupations"', add
cap label define occ1990_lbl 887 `"Vehicle washers and equipment cleaners"', add
cap label define occ1990_lbl 888 `"Packers and packagers by hand"', add
cap label define occ1990_lbl 889 `"Laborers outside construction"', add
cap label define occ1990_lbl 905 `"Military"', add
cap label define occ1990_lbl 991 `"Unemployed"', add
cap label define occ1990_lbl 999 `"Unknown"', add
cap label values occ1990 occ1990_lbl

cap label define ind1990_lbl 000 `"N/A (not applicable)"'
cap label define ind1990_lbl 010 `"Agricultural production, crops"', add
cap label define ind1990_lbl 011 `"Agricultural production, livestock"', add
cap label define ind1990_lbl 012 `"Veterinary services"', add
cap label define ind1990_lbl 020 `"Landscape and horticultural services"', add
cap label define ind1990_lbl 030 `"Agricultural services, n.e.c."', add
cap label define ind1990_lbl 031 `"Forestry"', add
cap label define ind1990_lbl 032 `"Fishing, hunting, and trapping"', add
cap label define ind1990_lbl 040 `"Metal mining"', add
cap label define ind1990_lbl 041 `"Coal mining"', add
cap label define ind1990_lbl 042 `"Oil and gas extraction"', add
cap label define ind1990_lbl 050 `"Nonmetallic mining and quarrying, except fuels"', add
cap label define ind1990_lbl 060 `"All construction"', add
cap label define ind1990_lbl 100 `"Meat products"', add
cap label define ind1990_lbl 101 `"Dairy products"', add
cap label define ind1990_lbl 102 `"Canned, frozen, and preserved fruits and vegetables"', add
cap label define ind1990_lbl 110 `"Grain mill products"', add
cap label define ind1990_lbl 111 `"Bakery products"', add
cap label define ind1990_lbl 112 `"Sugar and confectionery products"', add
cap label define ind1990_lbl 120 `"Beverage industries"', add
cap label define ind1990_lbl 121 `"Misc. food preparations and kindred products"', add
cap label define ind1990_lbl 122 `"Food industries, n.s."', add
cap label define ind1990_lbl 130 `"Tobacco manufactures"', add
cap label define ind1990_lbl 132 `"Knitting mills"', add
cap label define ind1990_lbl 140 `"Dyeing and finishing textiles, except wool and knit goods"', add
cap label define ind1990_lbl 141 `"Carpets and rugs"', add
cap label define ind1990_lbl 142 `"Yarn, thread, and fabric mills"', add
cap label define ind1990_lbl 150 `"Miscellaneous textile mill products"', add
cap label define ind1990_lbl 151 `"Apparel and accessories, except knit"', add
cap label define ind1990_lbl 152 `"Miscellaneous fabricated textile products"', add
cap label define ind1990_lbl 160 `"Pulp, paper, and paperboard mills"', add
cap label define ind1990_lbl 161 `"Miscellaneous paper and pulp products"', add
cap label define ind1990_lbl 162 `"Paperboard containers and boxes"', add
cap label define ind1990_lbl 171 `"Newspaper publishing and printing"', add
cap label define ind1990_lbl 172 `"Printing, publishing, and allied industries, except newspapers"', add
cap label define ind1990_lbl 180 `"Plastics, synthetics, and resins"', add
cap label define ind1990_lbl 181 `"Drugs"', add
cap label define ind1990_lbl 182 `"Soaps and cosmetics"', add
cap label define ind1990_lbl 190 `"Paints, varnishes, and related products"', add
cap label define ind1990_lbl 191 `"Agricultural chemicals"', add
cap label define ind1990_lbl 192 `"Industrial and miscellaneous chemicals"', add
cap label define ind1990_lbl 200 `"Petroleum refining"', add
cap label define ind1990_lbl 201 `"Miscellaneous petroleum and coal products"', add
cap label define ind1990_lbl 210 `"Tires and inner tubes"', add
cap label define ind1990_lbl 211 `"Other rubber products, and plastics footwear and belting"', add
cap label define ind1990_lbl 212 `"Miscellaneous plastics products"', add
cap label define ind1990_lbl 220 `"Leather tanning and finishing"', add
cap label define ind1990_lbl 221 `"Footwear, except rubber and plastic"', add
cap label define ind1990_lbl 222 `"Leather products, except footwear"', add
cap label define ind1990_lbl 230 `"Logging"', add
cap label define ind1990_lbl 231 `"Sawmills, planing mills, and millwork"', add
cap label define ind1990_lbl 232 `"Wood buildings and mobile homes"', add
cap label define ind1990_lbl 241 `"Miscellaneous wood products"', add
cap label define ind1990_lbl 242 `"Furniture and fixtures"', add
cap label define ind1990_lbl 250 `"Glass and glass products"', add
cap label define ind1990_lbl 251 `"Cement, concrete, gypsum, and plaster products"', add
cap label define ind1990_lbl 252 `"Structural clay products"', add
cap label define ind1990_lbl 261 `"Pottery and related products"', add
cap label define ind1990_lbl 262 `"Misc. nonmetallic mineral and stone products"', add
cap label define ind1990_lbl 270 `"Blast furnaces, steelworks, rolling and finishing mills"', add
cap label define ind1990_lbl 271 `"Iron and steel foundries"', add
cap label define ind1990_lbl 272 `"Primary aluminum industries"', add
cap label define ind1990_lbl 280 `"Other primary metal industries"', add
cap label define ind1990_lbl 281 `"Cutlery, handtools, and general hardware"', add
cap label define ind1990_lbl 282 `"Fabricated structural metal products"', add
cap label define ind1990_lbl 290 `"Screw machine products"', add
cap label define ind1990_lbl 291 `"Metal forgings and stampings"', add
cap label define ind1990_lbl 292 `"Ordnance"', add
cap label define ind1990_lbl 300 `"Miscellaneous fabricated metal products"', add
cap label define ind1990_lbl 301 `"Metal industries, n.s."', add
cap label define ind1990_lbl 310 `"Engines and turbines"', add
cap label define ind1990_lbl 311 `"Farm machinery and equipment"', add
cap label define ind1990_lbl 312 `"Construction and material handling machines"', add
cap label define ind1990_lbl 320 `"Metalworking machinery"', add
cap label define ind1990_lbl 321 `"Office and accounting machines"', add
cap label define ind1990_lbl 322 `"Computers and related equipment"', add
cap label define ind1990_lbl 331 `"Machinery, except electrical, n.e.c."', add
cap label define ind1990_lbl 332 `"Machinery, n.s."', add
cap label define ind1990_lbl 340 `"Household appliances"', add
cap label define ind1990_lbl 341 `"Radio, TV, and communication equipment"', add
cap label define ind1990_lbl 342 `"Electrical machinery, equipment, and supplies, n.e.c."', add
cap label define ind1990_lbl 350 `"Electrical machinery, equipment, and supplies, n.s."', add
cap label define ind1990_lbl 351 `"Motor vehicles and motor vehicle equipment"', add
cap label define ind1990_lbl 352 `"Aircraft and parts"', add
cap label define ind1990_lbl 360 `"Ship and boat building and repairing"', add
cap label define ind1990_lbl 361 `"Railroad locomotives and equipment"', add
cap label define ind1990_lbl 362 `"Guided missiles, space vehicles, and parts"', add
cap label define ind1990_lbl 370 `"Cycles and miscellaneous transportation equipment"', add
cap label define ind1990_lbl 371 `"Scientific and controlling instruments"', add
cap label define ind1990_lbl 372 `"Medical, dental, and optical instruments and supplies"', add
cap label define ind1990_lbl 380 `"Photographic equipment and supplies"', add
cap label define ind1990_lbl 381 `"Watches, clocks, and clockwork operated devices"', add
cap label define ind1990_lbl 390 `"Toys, amusement, and sporting goods"', add
cap label define ind1990_lbl 391 `"Miscellaneous manufacturing industries"', add
cap label define ind1990_lbl 392 `"Manufacturing industries, n.s."', add
cap label define ind1990_lbl 400 `"Railroads"', add
cap label define ind1990_lbl 401 `"Bus service and urban transit"', add
cap label define ind1990_lbl 402 `"Taxicab service"', add
cap label define ind1990_lbl 410 `"Trucking service"', add
cap label define ind1990_lbl 411 `"Warehousing and storage"', add
cap label define ind1990_lbl 412 `"U.S. Postal Service"', add
cap label define ind1990_lbl 420 `"Water transportation"', add
cap label define ind1990_lbl 421 `"Air transportation"', add
cap label define ind1990_lbl 422 `"Pipe lines, except natural gas"', add
cap label define ind1990_lbl 432 `"Services incidental to transportation"', add
cap label define ind1990_lbl 440 `"Radio and television broadcasting and cable"', add
cap label define ind1990_lbl 441 `"Telephone communications"', add
cap label define ind1990_lbl 442 `"Telegraph and miscellaneous communications services"', add
cap label define ind1990_lbl 450 `"Electric light and power"', add
cap label define ind1990_lbl 451 `"Gas and steam supply systems"', add
cap label define ind1990_lbl 452 `"Electric and gas, and other combinations"', add
cap label define ind1990_lbl 470 `"Water supply and irrigation"', add
cap label define ind1990_lbl 471 `"Sanitary services"', add
cap label define ind1990_lbl 472 `"Utilities, n.s."', add
cap label define ind1990_lbl 500 `"Motor vehicles and equipment"', add
cap label define ind1990_lbl 501 `"Furniture and home furnishings"', add
cap label define ind1990_lbl 502 `"Lumber and construction materials"', add
cap label define ind1990_lbl 510 `"Professional and commercial equipment and supplies"', add
cap label define ind1990_lbl 511 `"Metals and minerals, except petroleum"', add
cap label define ind1990_lbl 512 `"Electrical goods"', add
cap label define ind1990_lbl 521 `"Hardware, plumbing and heating supplies"', add
cap label define ind1990_lbl 530 `"Machinery, equipment, and supplies"', add
cap label define ind1990_lbl 531 `"Scrap and waste materials"', add
cap label define ind1990_lbl 532 `"Miscellaneous wholesale, durable goods"', add
cap label define ind1990_lbl 540 `"Paper and paper products"', add
cap label define ind1990_lbl 541 `"Drugs, chemicals, and allied products"', add
cap label define ind1990_lbl 542 `"Apparel, fabrics, and notions"', add
cap label define ind1990_lbl 550 `"Groceries and related products"', add
cap label define ind1990_lbl 551 `"Farm-product raw materials"', add
cap label define ind1990_lbl 552 `"Petroleum products"', add
cap label define ind1990_lbl 560 `"Alcoholic beverages"', add
cap label define ind1990_lbl 561 `"Farm supplies"', add
cap label define ind1990_lbl 562 `"Miscellaneous wholesale, nondurable goods"', add
cap label define ind1990_lbl 571 `"Wholesale trade, n.s."', add
cap label define ind1990_lbl 580 `"Lumber and building material retailing"', add
cap label define ind1990_lbl 581 `"Hardware stores"', add
cap label define ind1990_lbl 582 `"Retail nurseries and garden stores"', add
cap label define ind1990_lbl 590 `"Mobile home dealers"', add
cap label define ind1990_lbl 591 `"Department stores"', add
cap label define ind1990_lbl 592 `"Variety stores"', add
cap label define ind1990_lbl 600 `"Miscellaneous general merchandise stores"', add
cap label define ind1990_lbl 601 `"Grocery stores"', add
cap label define ind1990_lbl 602 `"Dairy products stores"', add
cap label define ind1990_lbl 610 `"Retail bakeries"', add
cap label define ind1990_lbl 611 `"Food stores, n.e.c."', add
cap label define ind1990_lbl 612 `"Motor vehicle dealers"', add
cap label define ind1990_lbl 620 `"Auto and home supply stores"', add
cap label define ind1990_lbl 621 `"Gasoline service stations"', add
cap label define ind1990_lbl 622 `"Miscellaneous vehicle dealers"', add
cap label define ind1990_lbl 623 `"Apparel and accessory stores, except shoe"', add
cap label define ind1990_lbl 630 `"Shoe stores"', add
cap label define ind1990_lbl 631 `"Furniture and home furnishings stores"', add
cap label define ind1990_lbl 632 `"Household appliance stores"', add
cap label define ind1990_lbl 633 `"Radio, TV, and computer stores"', add
cap label define ind1990_lbl 640 `"Music stores"', add
cap label define ind1990_lbl 641 `"Eating and drinking places"', add
cap label define ind1990_lbl 642 `"Drug stores"', add
cap label define ind1990_lbl 650 `"Liquor stores"', add
cap label define ind1990_lbl 651 `"Sporting goods, bicycles, and hobby stores"', add
cap label define ind1990_lbl 652 `"Book and stationery stores"', add
cap label define ind1990_lbl 660 `"Jewelry stores"', add
cap label define ind1990_lbl 661 `"Gift, novelty, and souvenir shops"', add
cap label define ind1990_lbl 662 `"Sewing, needlework, and piece goods stores"', add
cap label define ind1990_lbl 663 `"Catalog and mail order houses"', add
cap label define ind1990_lbl 670 `"Vending machine operators"', add
cap label define ind1990_lbl 671 `"Direct selling establishments"', add
cap label define ind1990_lbl 672 `"Fuel dealers"', add
cap label define ind1990_lbl 681 `"Retail florists"', add
cap label define ind1990_lbl 682 `"Miscellaneous retail stores"', add
cap label define ind1990_lbl 691 `"Retail trade, n.s."', add
cap label define ind1990_lbl 700 `"Banking"', add
cap label define ind1990_lbl 701 `"Savings institutions, including credit unions"', add
cap label define ind1990_lbl 702 `"Credit agencies, n.e.c."', add
cap label define ind1990_lbl 710 `"Security, commodity brokerage, and investment companies"', add
cap label define ind1990_lbl 711 `"Insurance"', add
cap label define ind1990_lbl 712 `"Real estate, including real estate-insurance offices"', add
cap label define ind1990_lbl 721 `"Advertising"', add
cap label define ind1990_lbl 722 `"Services to dwellings and other buildings"', add
cap label define ind1990_lbl 731 `"Personnel supply services"', add
cap label define ind1990_lbl 732 `"Computer and data processing services"', add
cap label define ind1990_lbl 740 `"Detective and protective services"', add
cap label define ind1990_lbl 741 `"Business services, n.e.c."', add
cap label define ind1990_lbl 742 `"Automotive rental and leasing, without drivers"', add
cap label define ind1990_lbl 750 `"Automobile parking and carwashes"', add
cap label define ind1990_lbl 751 `"Automotive repair and related services"', add
cap label define ind1990_lbl 752 `"Electrical repair shops"', add
cap label define ind1990_lbl 760 `"Miscellaneous repair services"', add
cap label define ind1990_lbl 761 `"Private households"', add
cap label define ind1990_lbl 762 `"Hotels and motels"', add
cap label define ind1990_lbl 770 `"Lodging places, except hotels and motels"', add
cap label define ind1990_lbl 771 `"Laundry, cleaning, and garment services"', add
cap label define ind1990_lbl 772 `"Beauty shops"', add
cap label define ind1990_lbl 780 `"Barber shops"', add
cap label define ind1990_lbl 781 `"Funeral service and crematories"', add
cap label define ind1990_lbl 782 `"Shoe repair shops"', add
cap label define ind1990_lbl 790 `"Dressmaking shops"', add
cap label define ind1990_lbl 791 `"Miscellaneous personal services"', add
cap label define ind1990_lbl 800 `"Theaters and motion pictures"', add
cap label define ind1990_lbl 801 `"Video tape rental"', add
cap label define ind1990_lbl 802 `"Bowling centers"', add
cap label define ind1990_lbl 810 `"Miscellaneous entertainment and recreation services"', add
cap label define ind1990_lbl 812 `"Offices and clinics of physicians"', add
cap label define ind1990_lbl 820 `"Offices and clinics of dentists"', add
cap label define ind1990_lbl 821 `"Offices and clinics of chiropractors"', add
cap label define ind1990_lbl 822 `"Offices and clinics of optometrists"', add
cap label define ind1990_lbl 830 `"Offices and clinics of health practitioners, n.e.c."', add
cap label define ind1990_lbl 831 `"Hospitals"', add
cap label define ind1990_lbl 832 `"Nursing and personal care facilities"', add
cap label define ind1990_lbl 840 `"Health services, n.e.c."', add
cap label define ind1990_lbl 841 `"Legal services"', add
cap label define ind1990_lbl 842 `"Elementary and secondary schools"', add
cap label define ind1990_lbl 850 `"Colleges and universities"', add
cap label define ind1990_lbl 851 `"Vocational schools"', add
cap label define ind1990_lbl 852 `"Libraries"', add
cap label define ind1990_lbl 860 `"Educational services, n.e.c."', add
cap label define ind1990_lbl 861 `"Job training and vocational rehabilitation services"', add
cap label define ind1990_lbl 862 `"Child day care services"', add
cap label define ind1990_lbl 863 `"Family child care homes"', add
cap label define ind1990_lbl 870 `"Residential care facilities, without nursing"', add
cap label define ind1990_lbl 871 `"Social services, n.e.c."', add
cap label define ind1990_lbl 872 `"Museums, art galleries, and zoos"', add
cap label define ind1990_lbl 873 `"Labor unions"', add
cap label define ind1990_lbl 880 `"Religious organizations"', add
cap label define ind1990_lbl 881 `"Membership organizations, n.e.c."', add
cap label define ind1990_lbl 882 `"Engineering, architectural, and surveying services"', add
cap label define ind1990_lbl 890 `"Accounting, auditing, and bookkeeping services"', add
cap label define ind1990_lbl 891 `"Research, development, and testing services"', add
cap label define ind1990_lbl 892 `"Management and public relations services"', add
cap label define ind1990_lbl 893 `"Miscellaneous professional and related services"', add
cap label define ind1990_lbl 900 `"Executive and legislative offices"', add
cap label define ind1990_lbl 901 `"General government, n.e.c."', add
cap label define ind1990_lbl 910 `"Justice, public order, and safety"', add
cap label define ind1990_lbl 921 `"Public finance, taxation, and monetary policy"', add
cap label define ind1990_lbl 922 `"Administration of human resources programs"', add
cap label define ind1990_lbl 930 `"Administration of environmental quality and housing programs"', add
cap label define ind1990_lbl 931 `"Administration of economic programs"', add
cap label define ind1990_lbl 932 `"National security and international affairs"', add
cap label define ind1990_lbl 940 `"Army"', add
cap label define ind1990_lbl 941 `"Air Force"', add
cap label define ind1990_lbl 942 `"Navy"', add
cap label define ind1990_lbl 950 `"Marines"', add
cap label define ind1990_lbl 951 `"Coast Guard"', add
cap label define ind1990_lbl 952 `"Armed Forces, branch not specified"', add
cap label define ind1990_lbl 960 `"Military Reserves or National Guard"', add
cap label define ind1990_lbl 992 `"Last worked 1984 or earlier"', add
cap label define ind1990_lbl 999 `"DID NOT RESPOND"', add
cap label values ind1990 ind1990_lbl

cap label define classwkr_lbl 0 `"N/A"'
cap label define classwkr_lbl 1 `"Self-employed"', add
cap label define classwkr_lbl 2 `"Works for wages"', add
cap label values classwkr classwkr_lbl

cap label define classwkrd_lbl 00 `"N/A"'
cap label define classwkrd_lbl 10 `"Self-employed"', add
cap label define classwkrd_lbl 11 `"Employer"', add
cap label define classwkrd_lbl 12 `"Working on own account"', add
cap label define classwkrd_lbl 13 `"Self-employed, not incorporated"', add
cap label define classwkrd_lbl 14 `"Self-employed, incorporated"', add
cap label define classwkrd_lbl 20 `"Works for wages"', add
cap label define classwkrd_lbl 21 `"Works on salary (1920)"', add
cap label define classwkrd_lbl 22 `"Wage/salary, private"', add
cap label define classwkrd_lbl 23 `"Wage/salary at non-profit"', add
cap label define classwkrd_lbl 24 `"Wage/salary, government"', add
cap label define classwkrd_lbl 25 `"Federal govt employee"', add
cap label define classwkrd_lbl 26 `"Armed forces"', add
cap label define classwkrd_lbl 27 `"State govt employee"', add
cap label define classwkrd_lbl 28 `"Local govt employee"', add
cap label define classwkrd_lbl 29 `"Unpaid family worker"', add
cap label values classwkrd classwkrd_lbl

cap label define wkswork2_lbl 0 `"N/A"'
cap label define wkswork2_lbl 1 `"1-13 weeks"', add
cap label define wkswork2_lbl 2 `"14-26 weeks"', add
cap label define wkswork2_lbl 3 `"27-39 weeks"', add
cap label define wkswork2_lbl 4 `"40-47 weeks"', add
cap label define wkswork2_lbl 5 `"48-49 weeks"', add
cap label define wkswork2_lbl 6 `"50-52 weeks"', add
cap label values wkswork2 wkswork2_lbl

cap label define uhrswork_lbl 00 `"N/A"'
cap label define uhrswork_lbl 01 `"1"', add
cap label define uhrswork_lbl 02 `"2"', add
cap label define uhrswork_lbl 03 `"3"', add
cap label define uhrswork_lbl 04 `"4"', add
cap label define uhrswork_lbl 05 `"5"', add
cap label define uhrswork_lbl 06 `"6"', add
cap label define uhrswork_lbl 07 `"7"', add
cap label define uhrswork_lbl 08 `"8"', add
cap label define uhrswork_lbl 09 `"9"', add
cap label define uhrswork_lbl 10 `"10"', add
cap label define uhrswork_lbl 11 `"11"', add
cap label define uhrswork_lbl 12 `"12"', add
cap label define uhrswork_lbl 13 `"13"', add
cap label define uhrswork_lbl 14 `"14"', add
cap label define uhrswork_lbl 15 `"15"', add
cap label define uhrswork_lbl 16 `"16"', add
cap label define uhrswork_lbl 17 `"17"', add
cap label define uhrswork_lbl 18 `"18"', add
cap label define uhrswork_lbl 19 `"19"', add
cap label define uhrswork_lbl 20 `"20"', add
cap label define uhrswork_lbl 21 `"21"', add
cap label define uhrswork_lbl 22 `"22"', add
cap label define uhrswork_lbl 23 `"23"', add
cap label define uhrswork_lbl 24 `"24"', add
cap label define uhrswork_lbl 25 `"25"', add
cap label define uhrswork_lbl 26 `"26"', add
cap label define uhrswork_lbl 27 `"27"', add
cap label define uhrswork_lbl 28 `"28"', add
cap label define uhrswork_lbl 29 `"29"', add
cap label define uhrswork_lbl 30 `"30"', add
cap label define uhrswork_lbl 31 `"31"', add
cap label define uhrswork_lbl 32 `"32"', add
cap label define uhrswork_lbl 33 `"33"', add
cap label define uhrswork_lbl 34 `"34"', add
cap label define uhrswork_lbl 35 `"35"', add
cap label define uhrswork_lbl 36 `"36"', add
cap label define uhrswork_lbl 37 `"37"', add
cap label define uhrswork_lbl 38 `"38"', add
cap label define uhrswork_lbl 39 `"39"', add
cap label define uhrswork_lbl 40 `"40"', add
cap label define uhrswork_lbl 41 `"41"', add
cap label define uhrswork_lbl 42 `"42"', add
cap label define uhrswork_lbl 43 `"43"', add
cap label define uhrswork_lbl 44 `"44"', add
cap label define uhrswork_lbl 45 `"45"', add
cap label define uhrswork_lbl 46 `"46"', add
cap label define uhrswork_lbl 47 `"47"', add
cap label define uhrswork_lbl 48 `"48"', add
cap label define uhrswork_lbl 49 `"49"', add
cap label define uhrswork_lbl 50 `"50"', add
cap label define uhrswork_lbl 51 `"51"', add
cap label define uhrswork_lbl 52 `"52"', add
cap label define uhrswork_lbl 53 `"53"', add
cap label define uhrswork_lbl 54 `"54"', add
cap label define uhrswork_lbl 55 `"55"', add
cap label define uhrswork_lbl 56 `"56"', add
cap label define uhrswork_lbl 57 `"57"', add
cap label define uhrswork_lbl 58 `"58"', add
cap label define uhrswork_lbl 59 `"59"', add
cap label define uhrswork_lbl 60 `"60"', add
cap label define uhrswork_lbl 61 `"61"', add
cap label define uhrswork_lbl 62 `"62"', add
cap label define uhrswork_lbl 63 `"63"', add
cap label define uhrswork_lbl 64 `"64"', add
cap label define uhrswork_lbl 65 `"65"', add
cap label define uhrswork_lbl 66 `"66"', add
cap label define uhrswork_lbl 67 `"67"', add
cap label define uhrswork_lbl 68 `"68"', add
cap label define uhrswork_lbl 69 `"69"', add
cap label define uhrswork_lbl 70 `"70"', add
cap label define uhrswork_lbl 71 `"71"', add
cap label define uhrswork_lbl 72 `"72"', add
cap label define uhrswork_lbl 73 `"73"', add
cap label define uhrswork_lbl 74 `"74"', add
cap label define uhrswork_lbl 75 `"75"', add
cap label define uhrswork_lbl 76 `"76"', add
cap label define uhrswork_lbl 77 `"77"', add
cap label define uhrswork_lbl 78 `"78"', add
cap label define uhrswork_lbl 79 `"79"', add
cap label define uhrswork_lbl 80 `"80"', add
cap label define uhrswork_lbl 81 `"81"', add
cap label define uhrswork_lbl 82 `"82"', add
cap label define uhrswork_lbl 83 `"83"', add
cap label define uhrswork_lbl 84 `"84"', add
cap label define uhrswork_lbl 85 `"85"', add
cap label define uhrswork_lbl 86 `"86"', add
cap label define uhrswork_lbl 87 `"87"', add
cap label define uhrswork_lbl 88 `"88"', add
cap label define uhrswork_lbl 89 `"89"', add
cap label define uhrswork_lbl 90 `"90"', add
cap label define uhrswork_lbl 91 `"91"', add
cap label define uhrswork_lbl 92 `"92"', add
cap label define uhrswork_lbl 93 `"93"', add
cap label define uhrswork_lbl 94 `"94"', add
cap label define uhrswork_lbl 95 `"95"', add
cap label define uhrswork_lbl 96 `"96"', add
cap label define uhrswork_lbl 97 `"97"', add
cap label define uhrswork_lbl 98 `"98"', add
cap label define uhrswork_lbl 99 `"99 (Topcode)"', add
cap label values uhrswork uhrswork_lbl

cap label define wrklstwk_lbl 0 `"N/A"'
cap label define wrklstwk_lbl 1 `"Did not work"', add
cap label define wrklstwk_lbl 2 `"Worked"', add
cap label define wrklstwk_lbl 3 `"Not Reported"', add
cap label values wrklstwk wrklstwk_lbl

cap label define looking_lbl 0 `"N/A"'
cap label define looking_lbl 1 `"No, did not look for work"', add
cap label define looking_lbl 2 `"Yes, looked for work"', add
cap label define looking_lbl 3 `"Not reported"', add
cap label values looking looking_lbl

cap label define availble_lbl 0 `"N/A"'
cap label define availble_lbl 1 `"No, already has job"', add
cap label define availble_lbl 2 `"No, temporarily ill"', add
cap label define availble_lbl 3 `"No, other reason(s)"', add
cap label define availble_lbl 4 `"Yes, available for work"', add
cap label define availble_lbl 5 `"Not reported"', add
cap label values availble availble_lbl

cap label define migrate1_lbl 0 `"N/A"'
cap label define migrate1_lbl 1 `"Same house"', add
cap label define migrate1_lbl 2 `"Moved within state"', add
cap label define migrate1_lbl 3 `"Moved between states"', add
cap label define migrate1_lbl 4 `"Abroad one year ago"', add
cap label define migrate1_lbl 9 `"Unknown"', add
cap label values migrate1 migrate1_lbl

cap label define migrate1d_lbl 00 `"N/A"'
cap label define migrate1d_lbl 10 `"Same house"', add
cap label define migrate1d_lbl 20 `"Same state (migration status within state unknown)"', add
cap label define migrate1d_lbl 21 `"Different house, moved within county"', add
cap label define migrate1d_lbl 22 `"Different house, moved within state, between counties"', add
cap label define migrate1d_lbl 23 `"Different house, moved within state, within PUMA"', add
cap label define migrate1d_lbl 24 `"Different house, moved within state, between PUMAs"', add
cap label define migrate1d_lbl 25 `"Different house, unknown within state"', add
cap label define migrate1d_lbl 30 `"Different state (general)"', add
cap label define migrate1d_lbl 31 `"Moved between contigious states"', add
cap label define migrate1d_lbl 32 `"Moved between non-contiguous states"', add
cap label define migrate1d_lbl 40 `"Abroad one year ago"', add
cap label define migrate1d_lbl 90 `"Unknown"', add
cap label values migrate1d migrate1d_lbl

cap label define migplac1_lbl 000 `"N/A"'
cap label define migplac1_lbl 001 `"Alabama"', add
cap label define migplac1_lbl 002 `"Alaska"', add
cap label define migplac1_lbl 004 `"Arizona"', add
cap label define migplac1_lbl 005 `"Arkansas"', add
cap label define migplac1_lbl 006 `"California"', add
cap label define migplac1_lbl 008 `"Colorado"', add
cap label define migplac1_lbl 009 `"Connecticut"', add
cap label define migplac1_lbl 010 `"Delaware"', add
cap label define migplac1_lbl 011 `"District of Columbia"', add
cap label define migplac1_lbl 012 `"Florida"', add
cap label define migplac1_lbl 013 `"Georgia"', add
cap label define migplac1_lbl 015 `"Hawaii"', add
cap label define migplac1_lbl 016 `"Idaho"', add
cap label define migplac1_lbl 017 `"Illinois"', add
cap label define migplac1_lbl 018 `"Indiana"', add
cap label define migplac1_lbl 019 `"Iowa"', add
cap label define migplac1_lbl 020 `"Kansas"', add
cap label define migplac1_lbl 021 `"Kentucky"', add
cap label define migplac1_lbl 022 `"Louisiana"', add
cap label define migplac1_lbl 023 `"Maine"', add
cap label define migplac1_lbl 024 `"Maryland"', add
cap label define migplac1_lbl 025 `"Massachusetts"', add
cap label define migplac1_lbl 026 `"Michigan"', add
cap label define migplac1_lbl 027 `"Minnesota"', add
cap label define migplac1_lbl 028 `"Mississippi"', add
cap label define migplac1_lbl 029 `"Missouri"', add
cap label define migplac1_lbl 030 `"Montana"', add
cap label define migplac1_lbl 031 `"Nebraska"', add
cap label define migplac1_lbl 032 `"Nevada"', add
cap label define migplac1_lbl 033 `"New Hampshire"', add
cap label define migplac1_lbl 034 `"New Jersey"', add
cap label define migplac1_lbl 035 `"New Mexico"', add
cap label define migplac1_lbl 036 `"New York"', add
cap label define migplac1_lbl 037 `"North Carolina"', add
cap label define migplac1_lbl 038 `"North Dakota"', add
cap label define migplac1_lbl 039 `"Ohio"', add
cap label define migplac1_lbl 040 `"Oklahoma"', add
cap label define migplac1_lbl 041 `"Oregon"', add
cap label define migplac1_lbl 042 `"Pennsylvania"', add
cap label define migplac1_lbl 044 `"Rhode Island"', add
cap label define migplac1_lbl 045 `"South Carolina"', add
cap label define migplac1_lbl 046 `"South Dakota"', add
cap label define migplac1_lbl 047 `"Tennessee"', add
cap label define migplac1_lbl 048 `"Texas"', add
cap label define migplac1_lbl 049 `"Utah"', add
cap label define migplac1_lbl 050 `"Vermont"', add
cap label define migplac1_lbl 051 `"Virginia"', add
cap label define migplac1_lbl 053 `"Washington"', add
cap label define migplac1_lbl 054 `"West Virginia"', add
cap label define migplac1_lbl 055 `"Wisconsin"', add
cap label define migplac1_lbl 056 `"Wyoming"', add
cap label define migplac1_lbl 099 `"United States, ns"', add
cap label define migplac1_lbl 100 `"Samoa, 1950"', add
cap label define migplac1_lbl 105 `"Guam"', add
cap label define migplac1_lbl 110 `"Puerto Rico"', add
cap label define migplac1_lbl 115 `"Virgin Islands"', add
cap label define migplac1_lbl 120 `"Other US Possessions"', add
cap label define migplac1_lbl 150 `"Canada"', add
cap label define migplac1_lbl 151 `"English Canada"', add
cap label define migplac1_lbl 152 `"French Canada"', add
cap label define migplac1_lbl 160 `"Atlantic Islands"', add
cap label define migplac1_lbl 200 `"Mexico"', add
cap label define migplac1_lbl 211 `"Belize/British Honduras"', add
cap label define migplac1_lbl 212 `"Costa Rica"', add
cap label define migplac1_lbl 213 `"El Salvador"', add
cap label define migplac1_lbl 214 `"Guatemala"', add
cap label define migplac1_lbl 215 `"Honduras"', add
cap label define migplac1_lbl 216 `"Nicaragua"', add
cap label define migplac1_lbl 217 `"Panama"', add
cap label define migplac1_lbl 218 `"Canal Zone"', add
cap label define migplac1_lbl 219 `"Central America, nec"', add
cap label define migplac1_lbl 250 `"Cuba"', add
cap label define migplac1_lbl 261 `"Dominican Republic"', add
cap label define migplac1_lbl 262 `"Haita"', add
cap label define migplac1_lbl 263 `"Jamaica"', add
cap label define migplac1_lbl 264 `"British West Indies"', add
cap label define migplac1_lbl 267 `"Other West Indies"', add
cap label define migplac1_lbl 290 `"Other Caribbean and North America"', add
cap label define migplac1_lbl 305 `"Argentina"', add
cap label define migplac1_lbl 310 `"Bolivia"', add
cap label define migplac1_lbl 315 `"Brazil"', add
cap label define migplac1_lbl 320 `"Chile"', add
cap label define migplac1_lbl 325 `"Colombia"', add
cap label define migplac1_lbl 330 `"Ecuador"', add
cap label define migplac1_lbl 345 `"Paraguay"', add
cap label define migplac1_lbl 350 `"Peru"', add
cap label define migplac1_lbl 360 `"Uruguay"', add
cap label define migplac1_lbl 365 `"Venezuela"', add
cap label define migplac1_lbl 390 `"South America, nec"', add
cap label define migplac1_lbl 400 `"Denmark"', add
cap label define migplac1_lbl 401 `"Finland"', add
cap label define migplac1_lbl 402 `"Iceland"', add
cap label define migplac1_lbl 404 `"Norway"', add
cap label define migplac1_lbl 405 `"Sweden"', add
cap label define migplac1_lbl 410 `"England"', add
cap label define migplac1_lbl 411 `"Scotland"', add
cap label define migplac1_lbl 412 `"Wales"', add
cap label define migplac1_lbl 413 `"United Kingdom (excluding England: 2005ACS)"', add
cap label define migplac1_lbl 414 `"Ireland"', add
cap label define migplac1_lbl 415 `"Northern Ireland"', add
cap label define migplac1_lbl 419 `"Other Northern Europe"', add
cap label define migplac1_lbl 420 `"Belgium"', add
cap label define migplac1_lbl 421 `"France"', add
cap label define migplac1_lbl 422 `"Luxembourg"', add
cap label define migplac1_lbl 425 `"Netherlands"', add
cap label define migplac1_lbl 426 `"Switzerland"', add
cap label define migplac1_lbl 429 `"Other Western Europe"', add
cap label define migplac1_lbl 430 `"Albania"', add
cap label define migplac1_lbl 433 `"Greece"', add
cap label define migplac1_lbl 434 `"Dodecanese Islands"', add
cap label define migplac1_lbl 435 `"Italy"', add
cap label define migplac1_lbl 436 `"Portugal"', add
cap label define migplac1_lbl 437 `"Azores"', add
cap label define migplac1_lbl 438 `"Spain"', add
cap label define migplac1_lbl 450 `"Austria"', add
cap label define migplac1_lbl 451 `"Bulgaria"', add
cap label define migplac1_lbl 452 `"Czechoslovakia"', add
cap label define migplac1_lbl 453 `"Germany"', add
cap label define migplac1_lbl 454 `"Hungary"', add
cap label define migplac1_lbl 455 `"Poland"', add
cap label define migplac1_lbl 456 `"Romania"', add
cap label define migplac1_lbl 457 `"Yugoslavia"', add
cap label define migplac1_lbl 458 `"Bosnia and Herzegovinia"', add
cap label define migplac1_lbl 459 `"Other Eastern Europe"', add
cap label define migplac1_lbl 460 `"Estonia"', add
cap label define migplac1_lbl 461 `"Latvia"', add
cap label define migplac1_lbl 462 `"Lithuania"', add
cap label define migplac1_lbl 463 `"Other Northern or Eastern Europe"', add
cap label define migplac1_lbl 465 `"USSR"', add
cap label define migplac1_lbl 498 `"Ukraine"', add
cap label define migplac1_lbl 499 `"Europe, ns"', add
cap label define migplac1_lbl 500 `"China"', add
cap label define migplac1_lbl 501 `"Japan"', add
cap label define migplac1_lbl 502 `"Korea"', add
cap label define migplac1_lbl 503 `"Taiwan"', add
cap label define migplac1_lbl 515 `"Philippines"', add
cap label define migplac1_lbl 517 `"Thailand"', add
cap label define migplac1_lbl 518 `"Vietnam"', add
cap label define migplac1_lbl 519 `"Other South East Asia"', add
cap label define migplac1_lbl 520 `"Nepal"', add
cap label define migplac1_lbl 521 `"India"', add
cap label define migplac1_lbl 522 `"Iran"', add
cap label define migplac1_lbl 523 `"Iraq"', add
cap label define migplac1_lbl 525 `"Pakistan"', add
cap label define migplac1_lbl 534 `"Israel/Palestine"', add
cap label define migplac1_lbl 535 `"Jordan"', add
cap label define migplac1_lbl 537 `"Lebanon"', add
cap label define migplac1_lbl 539 `"United Arab Emirates"', add
cap label define migplac1_lbl 540 `"Saudi Arabia"', add
cap label define migplac1_lbl 541 `"Syria"', add
cap label define migplac1_lbl 542 `"Turkey"', add
cap label define migplac1_lbl 543 `"Afghanistan"', add
cap label define migplac1_lbl 551 `"Other Western Asia"', add
cap label define migplac1_lbl 599 `"Asia, nec"', add
cap label define migplac1_lbl 600 `"Africa"', add
cap label define migplac1_lbl 610 `"Northern Africa"', add
cap label define migplac1_lbl 611 `"Egypt"', add
cap label define migplac1_lbl 619 `"Nigeria"', add
cap label define migplac1_lbl 620 `"Western Africa"', add
cap label define migplac1_lbl 621 `"Eastern Africa"', add
cap label define migplac1_lbl 622 `"Ethiopia"', add
cap label define migplac1_lbl 623 `"Kenya"', add
cap label define migplac1_lbl 694 `"South Africa (Union of)"', add
cap label define migplac1_lbl 699 `"Africa, nec"', add
cap label define migplac1_lbl 701 `"Australia"', add
cap label define migplac1_lbl 702 `"New Zealand"', add
cap label define migplac1_lbl 710 `"Pacific Islands (Australia and New Zealand Subregions, not specified, Oceania and at Sea: ACS)"', add
cap label define migplac1_lbl 900 `"Abroad (unknown) or at sea"', add
cap label define migplac1_lbl 997 `"Unknown value"', add
cap label define migplac1_lbl 999 `"Missing"', add
cap label values migplac1 migplac1_lbl


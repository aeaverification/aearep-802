clear
set more off

*********************************************************
*** Clean David Dorn's CZ crosswalks
*********************************************************
cd ${data}/ancillary
foreach j of numlist 1990(10)2010 { 
	cap unzipfile cw_puma`j'_czone.zip
}

foreach j of numlist 1990(10)2010 { 
	use cw_puma`j'_czone.dta, clear
	rename czone cz
	merge m:1 cz using ${data}/ancillary/CZs_state_region_Xwalk.dta, gen(m_1)
	drop m_1
	
	compress
	saveold ${data}/ancillary/cw_puma`j'_czone_state_region.dta, replace
}


* Clean folder
cd ${data}/ancillary
foreach j of numlist 1990(10)2010 { 
	zipfile cw_puma`j'_czone.dta, saving(cw_puma`j'_czone, replace)
	!rm cw_puma`j'_czone.dta
}


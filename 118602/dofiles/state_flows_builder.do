*-------------------------------------------------------------------------------
* Description: Build the migration data through 2000 from Andrew Foote 
* Heavily modified from Foote's county compilation code
* 
* Authors: Gaetano Basso & Justin Wiltshire
* Updated: November 10, 2019
*-------------------------------------------------------------------------------

*-------------------------------------------------------------------------------
* Preliminaries
*-------------------------------------------------------------------------------
clear
set more off


*-------------------------------------------------------------------------------
* Import and clean the county flows data to allow us to back out the state flows
*-------------------------------------------------------------------------------
* 1983 - 1984
local x 8384

* Inflows
tempfile temp`x'in
local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
foreach fl in `filelist' {
	qui infile using "${dofiles}/foote/migration8385_in.dct", using( `fl') clear
	
	* Identify the receiving state
	qui g sending = in_returns_pct==100
	qui g receiving_county_s = ""
	qui g receiving_state_s = ""
	qui sum in_returns
	local n = r(N)
	forvalues i = 1/`n' {
		if sending[`i'] == 1 {
			local county = county[`i']
			local state = state[`i']
		}
		qui replace receiving_county_s = "`county'" if _n==`i'
		qui replace receiving_state_s = "`state'" if _n==`i'
	}
	rename county sending_county_s
	rename state sending_state_s
	order  receiving_county_s receiving_state_s sending_county_s sending_state_s
	drop sending
		
	* Clean the data
	gen year = `x'
	qui replace sending_state_s = "ZZ" if substr(sending_county_s, 1, 6) == "REGION" | substr(sending_county_s, 1, 11) == "SAME REGION" | substr(sending_county_s, 1, 9) == "DIFFERENT"
	qui drop if sending_state_s == "XX" | receiving_state_s == "XX"
	qui replace sending_state_s = "..." if sending_state_s == "."
	qui replace sending_county_s = "..." if sending_state_s == "..."
	assert sending_state != "."
	qui drop if sending_state_s == receiving_state_s & sending_county_s == receiving_county_s
	qui replace sending_state_s = receiving_state_s if sending_state_s == "..."
		
	* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
	qui drop if receiving_state_s == "."

	* Collect the out-of-state flows together
	qui replace sending_state_s = "ZZ" if sending_state_s != receiving_state_s
	
	* Make the state observations upper-case and keep values of interest
	qui replace sending_state_s = upper(sending_state_s)
	qui replace receiving_state_s = upper(receiving_state_s)
	
	
	* Keep only variables of interest
	qui gen in_exemptions = in_exemptions_yr2
	qui drop in_exemptions_yr1 in_exemptions_yr2 in_returns_pct in_exemptions_yr1_pct in_exemptions_yr2_pct
	keep *_state_s in_ex year
	
	* Collapse the data by sending state (same as receiving, or different)
	capture collapse (sum) in_ex, by(sending_state receiving_state year)
	
	* Append and save
	capture append using `temp`x'in'
	qui save `temp`x'in', replace
}
save `temp`x'in', replace

* Outflows		
tempfile temp`x'out
local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
foreach fl in `filelist' {
	qui infile using "${dofiles}/foote/migration8385_out.dct", using( `fl') clear	
	
	* Identify the sending state
	qui g sending = out_returns_pct==100
	qui g sending_county_s = ""
	qui g sending_state_s = ""
	qui sum out_returns
	local n = r(N)
	forvalues i = 1/`n' {
		if sending[`i'] == 1 {
			local county = county[`i']
			local state = state[`i']
		}
		qui replace sending_county_s = "`county'" if _n==`i'
		qui replace sending_state_s = "`state'" if _n==`i'
	}
	rename county receiving_county_s
	rename state receiving_state_s
	order sending_county_s sending_state_s receiving_county_s receiving_state_s
	drop sending
	
	* Clean the data
	gen year = `x'
	qui replace receiving_state_s = "ZZ" if substr(receiving_state_s, 1, 6) == "REGION" | substr(receiving_state_s, 1, 11) == "SAME REGION" | substr(receiving_state_s, 1, 9) == "DIFFERENT"
	qui drop if receiving_state_s == "XX" | sending_state_s == "XX"
	qui replace receiving_state_s = "..." if receiving_state_s == "."
	qui replace receiving_county_s = "..." if receiving_state_s == "..."
	assert receiving_state != "."
	qui drop if receiving_state_s == sending_state_s & receiving_county_s == sending_county_s
	qui replace receiving_state_s = sending_state_s if receiving_state_s == "..."
		
	* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
	qui drop if sending_state_s == "."

	* Collect the out-of-state flows together
	qui replace receiving_state_s = "ZZ" if receiving_state_s != sending_state_s
	
	* Make the state observations upper-case and keep values of interest
	qui replace sending_state_s = upper(sending_state_s)
	qui replace receiving_state_s = upper(receiving_state_s)
	
	* Keep only variables of interest
	qui gen out_exemptions = out_exemptions_yr2
	qui drop out_exemptions_yr1 out_exemptions_yr2 out_returns_pct out_exemptions_yr1_pct out_exemptions_yr2_pct
	keep *_state_s out_ex year
	
	* Collapse the data by sending state (same as receiving, or different)
	capture collapse (sum) out_ex, by(sending_state receiving_state year)
	
	* Append and save
	capture append using `temp`x'out'
	qui save `temp`x'out', replace
	}
save `temp`x'out', replace	
	
* 1984 - 1985	
local x 8485

* Inflows
tempfile temp`x'in
local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
foreach fl in `filelist' {
	qui infile using "${dofiles}/foote/migration8385_in.dct", using( `fl') clear

	* Identify the receiving state
	qui gen sending = in_returns_pct==. & state!= "."
	qui g receiving_county_s = ""
	qui g receiving_state_s = ""
	qui sum in_returns
	local n = r(N)
	forvalues i = 1/`n' {
		if sending[`i'] == 1 {
			local county = county[`i']
			local state = state[`i']
		}
		qui replace receiving_county_s = "`county'" if _n==`i'
		qui replace receiving_state_s = "`state'" if _n==`i'
	}
	rename county sending_county_s
	rename state sending_state_s
	order  receiving_county_s receiving_state_s sending_county_s sending_state_s
	drop sending

	* Clean the data
	gen year = `x'
	qui replace sending_state_s = "ZZ" if substr(sending_county_s, 1, 6) == "Region" | substr(sending_county_s, 1, 11) == "Same Region" | substr(sending_county_s, 1, 9) == "Different"
	qui drop if sending_state_s == "FR" | receiving_state_s == "FR"
	qui replace sending_state_s = "..." if sending_state_s == "."
	qui replace sending_county_s = "..." if sending_state_s == "..."
	assert sending_state != "."
	qui drop if sending_state_s == receiving_state_s & sending_county_s == receiving_county_s
	qui replace sending_state_s = receiving_state_s if sending_state_s == "..."
	
	* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
	qui drop if receiving_state_s == "."

	* Collect the out-of-state flows together
	qui replace sending_state_s = "ZZ" if sending_state_s != receiving_state_s
	
	* Make the state observations upper-case and keep values of interest
	qui replace sending_state_s = upper(sending_state_s)
	qui replace receiving_state_s = upper(receiving_state_s)
	
	* Keep only variables of interest
	qui gen in_exemptions = in_exemptions_yr2
	qui drop in_exemptions_yr1 in_exemptions_yr2 in_returns_pct  in_exemptions_yr1_pct in_exemptions_yr2_pct
	keep *_state_s in_ex year
	
	* Collapse the data by sending state (same as receiving, or different)
	capture collapse (sum) in_ex, by(sending_state receiving_state year)

	* Append and save
	capture append using `temp`x'in'
	qui save `temp`x'in', replace
}
save `temp`x'in', replace
	
* Outflows	
tempfile temp`x'out
local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
foreach fl in `filelist' {
	qui infile using "${dofiles}/foote/migration8385_out.dct", using( `fl') clear	

	* Identify the sending state
	qui g sending = out_returns_pct==. & state!= "."
	qui g sending_county_s = ""
	qui g sending_state_s = ""
	qui sum out_returns
	local n = r(N)
	forvalues i = 1/`n' {
		if sending[`i'] == 1 {
			local county = county[`i']
			local state = state[`i']
		}
		qui replace sending_county_s = "`county'" if _n==`i'
		qui replace sending_state_s = "`state'" if _n==`i'
	}
	rename county receiving_county_s
	rename state receiving_state_s
	order sending_county_s sending_state_s receiving_county_s receiving_state_s
	drop sending
		
	* Clean the data
	gen year = `x'
	qui replace receiving_state_s = "ZZ" if substr(receiving_state_s, 1, 6) == "Region" | substr(receiving_state_s, 1, 11) == "Same Region" | substr(receiving_state_s, 1, 9) == "Different"
	qui drop if receiving_state_s == "FR" | sending_state_s == "FR"
	qui replace receiving_state_s = "..." if receiving_state_s == "."
	qui replace receiving_county_s = "..." if receiving_state_s == "..."
	assert receiving_state_s != "."
	qui drop if receiving_state_s == sending_state_s & receiving_county_s == sending_county_s
	qui replace receiving_state_s = sending_state_s if receiving_state_s == "..."
		
	* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
	qui drop if sending_state_s == "."

	* Collect the out-of-state flows together
	qui replace receiving_state_s = "ZZ" if receiving_state_s != sending_state_s
	
	* Make the state observations upper-case and keep values of interest
	qui replace sending_state_s = upper(sending_state_s)
	qui replace receiving_state_s = upper(receiving_state_s)
	
	* Keep only variables of interest
	qui gen out_exemptions = out_exemptions_yr2
	qui drop out_exemptions_yr1 out_exemptions_yr2 out_returns_pct out_exemptions_yr1_pct out_exemptions_yr2_pct
	keep *_state_s out_ex year
	
	* Collapse the data by sending state (same as receiving, or different)
	capture collapse (sum) out_ex, by(sending_state receiving_state year)
		
	* Append and save
	capture append using `temp`x'out'
	qui save `temp`x'out', replace
}
save `temp`x'out', replace

* 1985 - 1988
foreach x in 8586 8687 8788 { 

	* Inflows
	tempfile temp`x'in

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
	
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote/migration8588_in.dct", using( `fl') clear

		* Identify the receiving state
		drop statefip countyfip
		qui gen sending = in_returns_pct==. & state!= "."
		qui g receiving_county_s = ""
		qui g receiving_state_s = ""
		qui sum in_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace receiving_county_s = "`county'" if _n==`i'
			qui replace receiving_state_s = "`state'" if _n==`i'
		}
		rename county sending_county_s
		rename state sending_state_s
		order  receiving_county_s receiving_state_s sending_county_s sending_state_s
		drop sending
		
		* Clean the data
		gen year = `x'
		qui replace sending_state_s = "ZZ" if substr(sending_county_s, 1, 2) == "1'" | substr(sending_county_s, 1, 2) == "2'" | substr(sending_county_s, 1, 2) == "3'" | substr(sending_county_s, 1, 2) == "4'" | substr(sending_county_s, 1, 11) == "egion, Diff" | substr(sending_county_s, 1, 10) == "ent Region"
		qui drop if sending_state_s == "FR" | receiving_state_s == "FR" | substr(sending_county_s, 1, 9) == "n . . . ."
		qui replace sending_state_s = "..." if substr(sending_county_s, 1, 4) == "tate" | substr(sending_county_s, 7, 14) == "County Non-Mig"
		qui drop if sending_state_s == receiving_state_s & sending_county_s == receiving_county_s
		qui drop if substr(sending_county_s, 1, 3) == "ows"
		qui replace sending_state_s = receiving_state_s if sending_state_s == "..."
		assert sending_state_s != "."
		
		* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
		qui drop if receiving_state_s == "."

		* Collect the out-of-state flows together
		qui replace sending_state_s = "ZZ" if sending_state_s != receiving_state_s
		
		* Make the state observations upper-case and keep values of interest
		qui replace sending_state_s = upper(sending_state_s)
		qui replace receiving_state_s = upper(receiving_state_s)
		
		* Keep only variables of interest
		qui gen in_exemptions = in_exemptions_yr2
		qui drop in_exemptions_yr1 in_exemptions_yr2 in_returns_pct  in_exemptions_yr1_pct in_exemptions_yr2_pct
		keep *_state_s in_ex year
		
		* Collapse the data by sending state (same as receiving, or different)
		capture collapse (sum) in_ex, by(sending_state receiving_state year)
		
		* Append and save
		capture append using `temp`x'in'
		qui save `temp`x'in',replace
	}
	save `temp`x'in', replace

	* Outflows
	tempfile temp`x'out
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote/migration8588_out.dct", using( `fl') clear	
	
		* Identify the sending state
		drop statefip countyfip
		qui g sending = out_returns_pct==. & state!= "."
		qui g sending_county_s = ""
		qui g sending_state_s = ""
		qui sum out_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace sending_county_s = "`county'" if _n==`i'
			qui replace sending_state_s = "`state'" if _n==`i'
		}
		rename county receiving_county_s
		rename state receiving_state_s
		order sending_county_s sending_state_s receiving_county_s receiving_state_s
		drop sending
		
		* Clean the data
		gen year = `x'
		qui replace receiving_state_s = "ZZ" if substr(receiving_county_s, 1, 2) == "1'" | substr(receiving_county_s, 1, 2) == "2'" | substr(receiving_county_s, 1, 2) == "3'" | substr(receiving_county_s, 1, 2) == "4'" | substr(receiving_county_s, 1, 11) == "egion, Diff" | substr(receiving_county_s, 1, 10) == "ent Region"
		qui drop if receiving_state_s == "FR" | sending_state_s == "FR" | substr(receiving_county_s, 1, 9) == "n . . . ."
		qui replace receiving_state_s = "..." if substr(receiving_county_s, 1, 4) == "tate" | substr(receiving_county_s, 7, 14) == "County Non-Mig"
		qui drop if receiving_state_s == sending_state_s & receiving_county_s == sending_county_s
		qui drop if substr(receiving_county_s, 1, 3) == "ows"
		qui replace receiving_state_s = sending_state_s if receiving_state_s == "..."
		assert receiving_state_s != "."
		
		* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
		qui drop if sending_state_s == "."

		* Collect the out-of-state flows together
		qui replace receiving_state_s = "ZZ" if receiving_state_s != sending_state_s
		
		* Make the state observations upper-case and keep values of interest
		qui replace sending_state_s = upper(sending_state_s)
		qui replace receiving_state_s = upper(receiving_state_s)
		
		* Keep only variables of interest
		qui gen out_exemptions = out_exemptions_yr2
		qui drop out_exemptions_yr1 out_exemptions_yr2 out_returns_pct out_exemptions_yr1_pct out_exemptions_yr2_pct
		keep *_state_s out_ex year
		
		* Collapse the data by sending state (same as receiving, or different)
		capture collapse (sum) out_ex, by(sending_state receiving_state year)
			
		* Append and save	
		capture append using `temp`x'out'
		qui save `temp`x'out',replace
	}
	save `temp`x'out', replace
}

* 1988 - 1990
foreach x in 8889 8990 {
	* Inflows
	tempfile temp`x'in
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote/countymigration88_in.dct", using( `fl') clear
		
		* Identify the receiving state
		qui drop if county == "" & state == "" & mi(in_returns) & mi(in_returns_pct) & mi(in_ex) & mi(countyfip) & statefip == 1
		drop statefip countyfip
		qui replace county = "000   County Non-Migrants" if `x' == 8990 & state == "1"
		qui replace in_returns = 10600 if `x' == 8990 & state == "1"
		qui replace in_returns_pct = . if `x' == 8990 & state == "1"
		qui replace in_ex = 24687 if `x' == 8990 & state == "1"
		qui replace state = "In" if `x' == 8990 & state == "1"
		qui gen sending = in_returns_pct==. & state!= ""
		qui g receiving_county_s = ""
		qui g receiving_state_s = ""
		qui sum in_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace receiving_county_s = "`county'" if _n==`i'
			qui replace receiving_state_s = "`state'" if _n==`i'
		}
		rename county sending_county_s
		rename state sending_state_s
		order  receiving_county_s receiving_state_s sending_county_s sending_state_s
		drop sending
		
		* Clean the data
		gen year = `x'
		qui replace sending_state_s = "ZZ" if substr(sending_county_s, 1, 2) == "1:" | substr(sending_county_s, 1, 2) == "2:" | substr(sending_county_s, 1, 2) == "3:" | substr(sending_county_s, 1, 2) == "4:" | substr(sending_county_s, 1, 11) == "egion, Diff" | substr(sending_county_s, 1, 10) == "ent Region"
		qui drop if sending_state_s == "FR" | receiving_state_s == "FR" | sending_county_s == "n"
		qui replace sending_state_s = "..." if substr(sending_county_s, 1, 4) == "tate" | substr(sending_county_s, 7, 14) == "County Non-Mig"
		qui drop if sending_state_s == receiving_state_s & sending_county_s == receiving_county_s
		qui drop if substr(sending_county_s, 1, 3) == "ows"
		qui replace sending_state_s = receiving_state_s if sending_state_s == "..."
		assert sending_state_s != ""
		
		* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
		qui drop if receiving_state_s == ""

		* Collect the out-of-state flows together
		qui replace sending_state_s = "ZZ" if sending_state_s != receiving_state_s
		
		* Make the state observations upper-case and keep values of interest
		qui replace sending_state_s = upper(sending_state_s)
		qui replace receiving_state_s = upper(receiving_state_s)
		
		* Keep only variables of interest
		keep *_state_s in_ex year
		
		* Collapse the data by sending state (same as receiving, or different)
		capture collapse (sum) in_ex, by(sending_state receiving_state year)
				
		* Append and save
		capture append using `temp`x'in'
		qui save `temp`x'in',replace
	}
	save `temp`x'in', replace
	
	* Outflows
	tempfile temp`x'out
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote/countymigration88_out.dct", using( `fl') clear

		* Identify the sending state
		qui drop if county == "" & state == "" & mi(out_returns) & mi(out_returns_pct) & mi(out_ex) & mi(countyfip) & mi(statefip)
		qui drop statefip countyfip
		qui g sending = out_returns_pct==. & state!= ""
		qui g sending_county_s = ""
		qui g sending_state_s = ""
		qui sum out_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace sending_county_s = "`county'" if _n==`i'
			qui replace sending_state_s = "`state'" if _n==`i'
		}
		rename county receiving_county_s
		rename state receiving_state_s
		order sending_county_s sending_state_s receiving_county_s receiving_state_s
		drop sending
		
		* Clean the data
		gen year = `x'
		qui replace receiving_state_s = "ZZ" if substr(receiving_county_s, 1, 2) == "1:" | substr(receiving_county_s, 1, 2) == "2:" | substr(receiving_county_s, 1, 2) == "3:" | substr(receiving_county_s, 1, 2) == "4:" | substr(receiving_county_s, 1, 11) == "egion, Diff" | substr(receiving_county_s, 1, 10) == "ent Region"
		qui drop if receiving_state_s == "FR" | sending_state_s == "FR" | receiving_county_s == "n"
		qui replace receiving_state_s = "..." if substr(receiving_county_s, 1, 4) == "tate" | substr(receiving_county_s, 7, 14) == "County Non-Mig"
		qui drop if receiving_state_s == sending_state_s & receiving_county_s == sending_county_s
		qui drop if substr(receiving_county_s, 1, 3) == "ows"
		qui replace receiving_state_s = sending_state_s if receiving_state_s == "..."
		assert receiving_state_s != ""
		
		* Impossible to tell if "ALL OTHER FLOWS" are in-state or out, so drop all
		qui drop if sending_state_s == ""

		* Collect the out-of-state flows together
		qui replace receiving_state_s = "ZZ" if receiving_state_s != sending_state_s
		
		* Make the state observations upper-case and keep values of interest
		qui replace sending_state_s = upper(sending_state_s)
		qui replace receiving_state_s = upper(receiving_state_s)
		
		* Keep only variables of interest
		keep *_state_s out_ex year
		
		* Collapse the data by sending state (same as receiving, or different)
		capture collapse (sum) out_ex, by(sending_state receiving_state year)
				
		* Append and save
		capture append using `temp`x'out'
		qui save `temp`x'out',replace
	}
	
	save `temp`x'out',replace
}

*-------------------------------------------------------------------------------
* Create a crosswalk for state abbreviations to statefips
*-------------------------------------------------------------------------------

* Load the data
local x 8990
tempfile state_cw
local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
foreach fl in `filelist' {
	qui infile using "${dofiles}/foote/countymigration88_in.dct", using( `fl') clear
		
	* Clean and keep state abbreviations and statefips
	qui keep in 1
	qui keep state*
	qui replace state = upper(state)
		
	* Append and save
	capture append using `state_cw'
	qui save `state_cw',replace
}

/*
* Drop Alaska and Hawaii
drop if inlist(statefip, 2, 15, 57)
*/
drop if inlist(statefip, 57)
save `state_cw',replace

*-------------------------------------------------------------------------------
* Compile and finish cleaning up the time series
*-------------------------------------------------------------------------------

* Inflows
use `temp8384in', clear
foreach x in 8485 8586 8687 8788 8889 8990 {
	append using `temp`x'in'
}

* Ensure foreign migration is excluded and drop Alaska & Hawaii
assert sending_state_s != "FR" & receiving_state_s != "FR" & receiving_state_s != "XX" & receiving_state_s != "XX"
*drop if inlist(receiving_state_s, "AK", "HI")

* Apply the year crosswalk
do "${dofiles}/foote/yearxwalk.do"

* Rename and restructure the data for our purposes
qui rename in_exemptions taxexemptions
bysort year: egen x = total(taxex) if sending_state == receiving_state
bysort year: egen y = total(taxex) if sending_state != receiving_state
bysort year: egen totInstayers = max(x)
bysort year: egen totInexemptions = max(y)
drop x y

* Crosswalk the receiving state abbreviation to state FIPS
qui rename receiving_state state
qui merge m:1 state using `state_cw', noreport nogen

* Finish cleaning up
qui rename statefip statefipdestination
qui drop state sending_state
bysort statefipdestination year: keep if _n == 1
replace year = year - 1
order statefipdestination year taxex totIns totInex
sort statefipdestination year

* Save
compress
save "${pdir}/JEPmobility/data/foote_state_inflows.dta", replace

* Outflows
use `temp8384out', clear
foreach x in 8485 8586 8687 8788 8889 8990 {
	append using `temp`x'out'
}

* Ensure foreign migration is excluded and drop Alaska and Hawaii
assert sending_state_s != "FR" & receiving_state_s != "FR" & receiving_state_s != "XX" & receiving_state_s != "XX"
*drop if inlist(sending_state_s, "AK", "HI")

* Apply the year crosswalk
do "${dofiles}/foote/yearxwalk.do"

* Rename and restructure the data for our purposes
qui rename out_exemptions taxexemptions
bysort year: egen x = total(taxex) if sending_state == receiving_state
bysort year: egen y = total(taxex) if sending_state != receiving_state
bysort year: egen totOutstayers = max(x)
bysort year: egen totOutexemptions = max(y)
drop x y

* Crosswalk the sending state abbreviation to state FIPS
qui rename sending_state state
qui merge m:1 state using `state_cw', noreport nogen

* Finish cleaning up
qui rename statefip statefiporigin
qui drop state receiving_state
bysort statefiporigin year: keep if _n == 1
replace year = year - 1
order statefiporigin year taxex totOuts totOutex
sort statefiporigin year

* Save
compress
save "${pdir}/JEPmobility/data/foote_state_outflows.dta", replace

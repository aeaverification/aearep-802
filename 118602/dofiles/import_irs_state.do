clear
set more off

*-------------------------------------------------------------------------------
* Description: Clean and compile the IRS state migration data
*
* Authors: Justin Wiltshire and Gaetano Basso
* Updated: October 19, 2019
*-------------------------------------------------------------------------------

cd ${data}/irsraw

************************************** 
*** Import and save IRS data
************************************** 
*** 1990-1991
* Set the primary locals
forval j = 90/91 {
	local k = `j'+1
	local m = `j' + 1900
	local n = `k' + 1900
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'statemigration.zip `m'to`n'statemigration.zip, replace
	unzipfile `m'to`n'statemigration.zip, replace
		
	* Set the secondary locals
	foreach p in In Out {
		if "`p'" == "In" {
			local q "in"
		}
		if "`p'" == "Out" {
			local q "Out"
		}
		local s "`m'to`n'statemigration/`m'to`n'StateMigration`p'flow"
		cd "${data}/irsraw/`s'"
		qui local myfiles: dir "${data}/irsraw/`s'" files "*.xls"

		foreach file of local myfiles {
			local subfile = subinstr("`file'"," ","",.)
			local subfile2 = lower(`subfile')
			!rename "`file'" "`subfile2'"
		}

		* Loop over each state of interest
		foreach st in alaska hawaii alabama arizona arkansas california colorado connecticut delaware districtofcolumbia florida georgia idaho illinois indiana iowa kansas kentucky louisiana maine maryland massachusetts michigan minnesota mississippi missouri montana nebraska nevada newhampshire newjersey newmexico newyork northcarolina northdakota ohio oklahoma oregon pennsylvania rhodeisland southcarolina southdakota tennessee texas utah vermont virginia washington westvirginia wisconsin wyoming {
			
			* Import the data
			clear
			if "`p'" == "In" {
				import excel using "`st'`k'`q'.xls", clear sheet("A") cellrange(A9:F61)
				erase "`st'`k'`q'.xls"	
			}
			if "`p'" == "Out" & "`st'" != "alabama" {
				import excel using "`st'`k'`q'.xls", clear sheet("A") cellrange(A9:F61)
				erase "`st'`k'`q'.xls"	
			}
			if "`p'" == "Out" & ("`st'" == "alabama" & "`j'"!="90") {
				import excel using "`st'`k'`q'.xls", clear sheet("A") cellrange(A9:F61)
				erase "`st'`k'`q'.xls"	
			}
			if "`p'" == "Out" & "`st'" == "alabama" & "`j'"=="90" {
				import excel using "`st'`k'`q'.xls", clear sheet("A") cellrange(A9:G61)
				erase "`st'`k'`q'.xls"	
			}
						
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring _all, replace
			if "`p'" == "Out" & "`st'" == "alabama" & "`j'"=="90" {
				rename A statefipdestination
				drop B C 
				rename E returns
				rename G exemptions
				drop F
				gen x = statefipdestination if D=="State Non-Migrant"
				egen statefiporigin = max(x)
				drop x
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdes == 96
				gen y = exemptions if statefipor == statefipdestination
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				cap drop C
				cap drop D
				gen x = returns if statefipdes == 57
				gen y = exemptions if statefipdes == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
				}			
			if "`p'" == "Out" & ("`st'" == "alabama" & "`j'"!="90") {
				rename A statefipdestination
				rename D returns
				rename F exemptions
				drop B E
				gen x = statefipdestination if C=="State Non-Migrant"
				egen statefiporigin = max(x)
				drop x
				gen x = returns if statefipdes == 96
				gen y = exemptions if statefipdes == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdes == 96
				gen y = exemptions if statefipor == statefipdestination
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				cap drop C
				cap drop D
				gen x = returns if statefipdes == 57
				gen y = exemptions if statefipdes == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
				
			}
			if "`p'" == "Out" & "`st'" != "alabama" {
				rename A statefipdestination
				rename D returns
				rename F exemptions
				drop B E
				gen x = statefipdestination if C=="State Non-Migrant"
				egen statefiporigin = max(x)
				drop x
				gen x = returns if statefipdes == 96
				gen y = exemptions if statefipdes == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdes == 96
				gen y = exemptions if statefipor == statefipdestination
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				cap drop C
				cap drop D
				gen x = returns if statefipdes == 57
				gen y = exemptions if statefipdes == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}			
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename F exemptions
				drop B E
				gen x = statefiporigin if C=="State Non-Migrant"
				egen statefipdestination = max(x)
				drop x
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdestination
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				cap drop C
				cap drop D
				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}
			
			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
			
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_alabama.dta, clear
		foreach st in alaska hawaii arizona arkansas california colorado connecticut delaware districtofcolumbia florida georgia idaho illinois indiana iowa kansas kentucky louisiana maine maryland massachusetts michigan minnesota mississippi missouri montana nebraska nevada newhampshire newjersey newmexico newyork northcarolina northdakota ohio oklahoma oregon pennsylvania rhodeisland southcarolina southdakota tennessee texas utah vermont virginia washington westvirginia wisconsin wyoming {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_alabama.dta
	}

	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'statemigration.zip
}

*** 1992
* Set the primary locals
forval j = 92/92 {
	local k = `j'+1
	local m = `j' + 1900
	local n = `k' + 1900
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'statemigration.zip `m'to`n'statemigration.zip, replace
	unzipfile `m'to`n'statemigration.zip, replace
		
	* Set the secondary locals
	foreach p in In Out {
		if "`p'" == "In" {
			local q "in"
		}
		if "`p'" == "Out" {
			local q "out"
		}
		local s "`m'to`n'statemigration/`m'to`n'StateMigration`p'flow"
		cd "${data}/irsraw/`s'"
		qui local myfiles: dir "${data}/irsraw/`s'" files "*.xls"

		foreach file of local myfiles {
			local subfile = subinstr("`file'"," ","",.)
			!rename "`file'" "`subfile'"
		}

		* Loop over each state of interest
		foreach st in alaska hawaii alabama arizona arkansas california colorado connecticut delaware districtofcolumbia florida georgia idaho illinois indiana iowa kansas kentucky louisiana maine maryland massachusetts michigan minnesota mississippi missouri montana nebraska nevada newhampshire newjersey newmexico newyork northcarolina northdakota ohio oklahoma oregon pennsylvania rhodeisland southcarolina southdakota tennessee texas utah vermont virginia washington westvirginia wisconsin wyoming {
			
			* Import the data
			import excel using "`st'`k'`q'.xls", clear sheet("shell") cellrange(A9:E61)
			erase "`st'`k'`q'.xls"		
			
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring _all, replace
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename E exemptions
				drop B
				gen x = statefiporigin if substr(C,-11,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				drop x 
				drop C
				
				gen x = returns if statefipor == 63
				gen y = exemptions if statefipor == 63
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 63
				gen y = exemptions if statefipor == statefipdestination
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				
				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}
			if "`p'" == "Out" {
				rename A statefipdestination
				rename D returns
				rename E exemptions
				drop B
				gen x = statefipdestination if substr(C,-11,11)=="Non-Migrant"
				egen statefiporigin = max(x)
				drop x 
				drop C
				
				gen x = returns if statefipdes == 63
				gen y = exemptions if statefipdes == 63
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdes == 63
				gen y = exemptions if statefipor == statefipdestination
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				
				gen x = returns if statefipdes == 57
				gen y = exemptions if statefipdes == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)

				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}
			
			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			compress
			drop if statefipor>56|statefipdes>56
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
			
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_alabama.dta, clear
		foreach st in  alaska hawaii arizona arkansas california colorado connecticut delaware districtofcolumbia florida georgia idaho illinois indiana iowa kansas kentucky louisiana maine maryland massachusetts michigan minnesota mississippi missouri montana nebraska nevada newhampshire newjersey newmexico newyork northcarolina northdakota ohio oklahoma oregon pennsylvania rhodeisland southcarolina southdakota tennessee texas utah vermont virginia washington westvirginia wisconsin wyoming {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		*erase ${data}/irsraw/`p'migration`j'`k'_alabama.dta
	}

	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'statemigration.zip
}


*** 1993-1994
* Set the primary locals
cd ${data}/irsraw
forval j = 93/94 { 
	* Set the primary locals
	local k = `j'+1
	local l = `k' - 90
	local m = `j' + 1900
	local n = `k' + 1900
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'statemigration.zip `m'to`n'statemigration.zip, replace
	unzipfile `m'to`n'statemigration.zip, replace

	* Set the secondary locals
	foreach p in In Out {
		if "`p'" == "In" {
			local q "in"
		}
		if "`p'" == "Out" {
			local q "ot"
		}		
		
		local s "`m'to`n'statemigration/`m'to`n'StateMigration`p'flow"
		cd "${data}/irsraw/`s'"
		qui local myfiles: dir "${data}/irsraw/`s'" files "*.xls"

		foreach file of local myfiles {
			local subfile = subinstr("`file'"," ","",.)
			local subfile = subinstr("`file'","..",".",.)
			!rename "`file'" "`subfile'"
		}

		* Loop over each state of interest
		if "`p'" == "In" | "`j'" == "94" {
			local statelist "alas hawa alab ariz arka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss nebr mont neva newh newm newj newy noca noda ohio okla oreg penn rhod soca soda tenn texa utah verm virg wash west wisc wyom"
		}
		if "`p'" == "Out" & "`j'" == "93" {
			local statelist "alas hawa alab az aka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miss miso mont nebr neva newh newm newj newy noca noda ohio okla oreg penn rhod soca soda tenn texa utah verm vrg wash west wisc wyom"
		}	
		foreach st of local statelist {
			* Import the data
			import excel using `st'`k'`q'.xls, clear cellrange(A9:E61)
			erase `st'`k'`q'.xls		
			
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring _all, replace 
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename E exemptions
				drop B 
				replace C = trim(C)
				gen x = statefiporigin if substr(C,-11,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				replace statefipor = 96 if C=="Total Inflow"
				drop x 
				drop C
								
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
				
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}
			if "`p'" == "Out" {
				rename A statefipdestination
				rename D returns
				rename E exemptions
				replace B = trim(B)
				replace C = trim(C)
				gen x = statefipdestination if substr(C,-11,11)=="Non-Migrant"
				replace x = 50 if statefipdes==63&B=="Vt"
				replace statefipdes = 50 if statefipdes==63
				egen statefiporigin = max(x)
				if "`st'`k'`q'"=="alas94ot" {
					replace statefiporigin = 2
				}
				replace statefipdestination = 96 if C=="Total Outflow"|C=="total Outflow"
				drop x 
				drop C
				drop B
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				
				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}	

			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}			
		* Combine the state-year files and erase unwanted .dta files
		local AL alab
		local somestates: list statelist-AL
		use ${data}/irsraw/`p'migration`j'`k'_alab.dta, clear
		foreach st of local somestates {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_alab.dta
	}
	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'statemigration.zip
}


*** 1995
* Set the primary locals
cd ${data}/irsraw
local j = 95
	* Set the primary locals
	local k = 96
	local l = 6
	local m = 1995
	local n = 1996
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'statemigration.zip `m'to`n'statemigration.zip, replace
	unzipfile `m'to`n'statemigration.zip, replace

	* Set the secondary locals
	foreach p in In Out {
		if "`p'" == "In" {
			local q "i"
		}
		if "`p'" == "Out" {
			local q "o"
		}		

		local s "`m'to`n'statemigration/`m'to`n'StateMigration`p'flow"
		cd "${data}/irsraw/`s'"
		qui local myfiles: dir "${data}/irsraw/`s'" files "*.xls"

		foreach file of local myfiles {
			local subfile = subinstr("`file'","nhio","ohio",.)
			!rename "`file'" "`subfile'"
		}

		* Loop over each state of interest
		local statelist "ak hi al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy" 
		foreach st of local statelist {
			* Import the data
			clear
			local sheet "s`j'`l'`st'`q'r"
			local raw "s`j'`l'`st'`q'r"
			local start A9
			local end E63
			if "`raw'" == "s956akor" {
				local start A16
				local end E70
			}
			import excel using `raw'.xls, clear sheet("`sheet'") cellrange(`start':`end')
			erase `raw'.xls		
				
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring _all, replace
			
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefiporigin if substr(C,4,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				drop x
				
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}
			if "`p'" == "Out" {
				cap replace A = trim(A)
				cap replace A = "96" if A=="TO:"
				destring _all, replace
				rename A statefipdestination
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefipdestination if substr(C,4,11)=="Non-Migrant"
				egen statefiporigin = max(x)
				drop x
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad				
			}	

			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}			
		* Combine the state-year files and erase unwanted .dta files
		local AL al
		local somestates: list statelist-AL
		use ${data}/irsraw/`p'migration`j'`k'_al.dta, clear
		foreach st of local somestates {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_al.dta
	}
	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'statemigration.zip


*** 1996-1998
* Set the primary locals
cd ${data}/irsraw
forval j = 96/98 { 
	* Set the primary locals
	local k = `j'+1
	local l = `k' - 90
	local m = `j' + 1900
	local n = `k' + 1900
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'statemigration.zip `m'to`n'statemigration.zip, replace
	unzipfile `m'to`n'statemigration.zip, replace

	* Set the secondary locals
	foreach p in In Out {
		if "`p'" == "In" {
			local q "in"
		}
		if "`p'" == "Out" {
			local q "ot"
		}		
		
		local s "`m'to`n'statemigration/`m'to`n'StateMigration`p'flow"
		cd "${data}/irsraw/`s'"
		qui local myfiles: dir "${data}/irsraw/`s'" files "*.xls"

		foreach file of local myfiles {
			local subfile = subinstr("`file'","nhio","ohio",.)
			!rename "`file'" "`subfile'"
		}

		* Loop over each state of interest
		if "`p'" == "In" {
			local statelist "alas hawa alab ariz arka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss nebr mont neva newh newm newj newy ncar ndak ohio okla oreg penn rhod scar sdak tenn texa utah verm virg wash west wisc wyom" 
		}
		if "`p'" == "Out" & "`j'" == "96" {
			local statelist "alas hawa alab ariz arka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss nebr mont neva newh newm newj newy ncar ndak ohio okla oreg penn rhod scar sdak tenn texa utah verm virg wash west wsc wyom" 
		}	
		if "`p'" == "Out" & "`j'" == "97" {
			local statelist "alas hawa alab ariz arka cali colo conn dela dist for geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss nebr mont neva newh newm newj newy ncar ndak ohio okla oreg penn rhod scar sdak tenn texa utah verm vrg wash west wsc wyom" 
		}	
		foreach st of local statelist {
			* Import the data
			clear
			local sheet "`st'`k'`q'"
			local raw "`st'`k'`q'"
			local end E63
			if "`raw'"=="miso98ot" {
				local raw "miso8ot"
				local sheet "miso8ot"
			}
			if "`raw'"=="mont98ot" {
				local raw "mont8ot"
				local sheet "mont8ot"
			}
			if "`raw'"=="nebr98ot" {
				local raw "nebr8ot"
				local sheet "nebr8ot"
			}
			if "`raw'"=="neva98ot" {
				local raw "neva8ot"
				local sheet "neva8ot"
			}
			if "`raw'"=="newh98ot" {
				local raw "newh8ot"
				local sheet "newh8ot"
			}
			if "`raw'"=="newj98ot" {
				local raw "newj8ot"
				local sheet "newj8ot"
			}
			if "`raw'"=="ohio97in" {
				local sheet "nhio97in"
			}
			if "`raw'"=="newy97ot" {
				local sheet "newY97ot"
			}			
			if "`raw'" == "ndak98in" {
				local end E61
			}
			if "`raw'" == "ndak99in" {
				local end E61
			}
			if "`raw'" == "sdak99in" {
				local end E61
			}
			if "`raw'" == "verm98ot" {
				local end E61
			}
			if "`raw'" == "verm99ot" {
				local end E61
			}
			if "`raw'" == "newh8ot" {
				local end E61
			}
			if "`raw'" == "newh99ot" {
				local end E61
			}
			if "`raw'" == "sdak98in" {
				local end E61
			}
			import excel using `raw'.xls, clear sheet("`sheet'") cellrange(A9:`end')
			erase `raw'.xls		
			
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring _all, replace
			
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefiporigin if substr(C,4,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				drop x
								
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}
			if "`p'" == "Out" {
				cap replace A = trim(A)
				cap replace A = "96" if A=="TO:"
				destring _all, replace
				rename A statefipdestination
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefipdestination if substr(C,4,11)=="Non-Migrant"
				egen statefiporigin  = max(x)
				drop x
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad				
			}	

			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}			
		* Combine the state-year files and erase unwanted .dta files
		local AL alab
		local somestates: list statelist-AL
		use ${data}/irsraw/`p'migration`j'`k'_alab.dta, clear
		foreach st of local somestates {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_alab.dta
	}
	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'statemigration.zip
}

*** 1999
* Set the primary locals
cd ${data}/irsraw

	* Set the primary locals
	local j = 99
	local k "00"
	local m = 1999
	local n = 2000
		
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'statemigration.zip `m'to`n'statemigration.zip, replace
	unzipfile `m'to`n'statemigration.zip, replace

	* Set the secondary locals
	foreach p in Out In {
		if "`p'" == "In" {
			local q "in"
		}
		if "`p'" == "Out" {
			local q "ot"
		}		
		
		local s "`m'to`n'statemigration/`m'to`n'StateMigration`p'flow"
		cd "${data}/irsraw/`s'"

		* Loop over each state of interest
		if "`p'" == "In" {
			local statelist "alas hawa alab ariz arka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss nebr mont neva newh newm newj newy ncar ndak ohio okla oreg penn rhod scar sdak tenn texa utah verm virg wash west wisc wyom" 
		}
		if "`p'" == "Out" {
			local statelist "alas hawa alab ariz arka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss nebr mont neva newh newm newj newy ncar ndak ohio okla oreg penn rhod scar sdak tenn texa utah verm vrg wash west wisc wyom"
		}	
		foreach st of local statelist {
			* Import the data
			local sheet "`st'`k'`q'"
			local raw "`st'`k'`q'"
			local end E63
			if "`st'" == "conn" & "`q'"=="in" {
				local end E61
			}
			if "`st'" == "rhod" & "`q'"=="in" {
				local end E61
			}
			if "`st'" == "ndak" & "`q'"=="ot" {
				local end E61
			}
			if "`st'" == "sdak" & "`q'"=="ot" {
				local end E61
			}
			if "`st'" == "loui" & "`q'"=="in" {
				local sheet "loui99in"
			}
			if "`st'" == "newj" & "`q'"=="in" {
				local sheet "newj99in"
			}
			if "`st'" == "texa" & "`q'"=="in" {
				local sheet "texa99in"
			}

			import excel using `raw'.xls, clear sheet("`sheet'") cellrange(A9:`end')
			erase `raw'.xls		
			
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring _all, replace
			
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefiporigin if substr(C,4,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				drop x
				
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad	
			}
			if "`p'" == "Out" {
				rename A statefipdestination
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefipdestination if substr(C,4,11)=="Non-Migrant"
				egen statefiporigin  = max(x)
				drop x
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
				
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad				
			}	

			* Record the year, run the clean file, and save
			qui gen year = 19`j'
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}			
		* Combine the state-year files and erase unwanted .dta files
		local AL alab
		local somestates: list statelist-AL
		use ${data}/irsraw/`p'migration`j'`k'_alab.dta, clear
		foreach st of local somestates {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_alab.dta
	}
	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'statemigration.zip

*** 2000-2003

foreach j of numlist 0(1)3 {

	* Set the primary locals
	local k = `j'+1
	local m = `j' + 2000
	local n = `k' + 2000
	local r ""
	
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/`m'to`n'statemigration.zip `m'to`n'statemigration.zip, replace
	unzipfile `m'to`n'statemigration.zip, replace
	
	* Set the secondary locals
	foreach p in Out In {
		if "`p'" == "In" {
			local q "in"
		}
		else if "`p'" == "Out" {
			local q "ot"
		}		
		
		local s "`m'to`n'statemigration/`m'to`n'StateMigration`p'flow"
		cd "${data}/irsraw/`s'"

		* Loop over each state of interest
		if "`p'" == "In" {
			local oreg "oreg"
			if `j' == 0 {
				local oreg "oeg"
			}
			local statelist "alas hawa alab ariz arka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss mont nebr neva newh newm newj newy ncar ndak ohio okla `oreg' penn rhod scar sdak tenn texa utah verm virg wash west wiso wyom" 
			if `j' == 3 {
				local statelist "Alas Hawa Alab Ariz Arka Cali Colo Conn Dela DiCo Flor Geor Idah Illi Indi Iowa Kans Kent Loui Main Mary Mass Mich Minn Miso Misi Mont Nebr Neva NeHa NeMe NeJe NeYo NoCa NoDa Ohio Okla Oreg Penn RhIs SoCa SoDa Tenn Texa Utah Verm Virg Wash wevi wisc Wyom"
			}
		}
		if "`p'" == "Out" {
			local virg "virg"
			if `j' == 0 {
				local virg "vrg"
			}
			local statelist "alas hawa alab ariz arka cali colo conn dela dist flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss nebr mont neva newh newm newj newy ncar ndak ohio okla oreg penn rhod scar sdak tenn texa utah verm `virg' wash west wisc wyom"
			if `j' == 3 {
				local statelist "alas hawa alab ariz arkas cali colo conn dela dico flor geor idah illi indi iowa kans kent loui main mary mass mich minn miso miss mont nrbt neva neha neme neje neyo noca noda ohio okla oreg penn rhod soca soda tenn texa utah verm virg wash wvir wisc Wyom"
			} 
		}
		foreach st of local statelist {
			
			* Import the data
			if `j' == 0 {
				local r "r"
			}
			local raw "`st'0`k'`q'`r'"
			local end E63
			local last = 54
			if inlist("`st'", "newh", "verm") & "`q'" == "ot" & `j' == 0 {
				local end E61
				local last = 53
			}
			if "`st'" == "newh" & "`q'" == "in" & `j' == 1 {
				local end E61
				local last = 53
			}
			if "`st'" == "ndak" & "`q'" == "in" & inlist(`j', 0, 1) {
				local end E61
				local last = 53
			}
			if inlist("`st'", "rhod", "verm") & "`q'" == "ot" & `j' == 1 {
				local end E61
				local last = 53
			}
			if "`st'" == "sdak" & "`q'" == "in" & inlist(`j', 0, 1) {
				local end E61
				local last = 53
			}
			if inlist("`st'", "kent", "newm", "newj", "oreg", "west", "wiso") & "`q'" == "in" & `j' == 2 {
				local raw "`st'0`k'n`r'"
			}
			import excel using `raw'.xls, cellrange(A9:`end') clear
			erase `raw'.xls		

			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring _all, replace
			assert inrange(A[1], 1, 100)
			assert inrange(A[`last'], 1, 100)
			
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefiporigin if substr(C,4,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				drop x
				
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad	
			}
			if "`p'" == "Out" {
				rename A statefipdestination
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefipdestination if substr(C,4,11)=="Non-Migrant"
				egen statefiporigin  = max(x)
				drop x
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
				
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad				
			}	

			* Record the year, run the clean file, and save
			qui gen year = 200`j'
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
		
		* Combine the state-year files and erase unwanted .dta files
		local alab "alab"
		if `j' == 3 & "`p'" == "In" {
			local alab "Alab"
		}
		local AL `alab'
		local somestates: list statelist-AL
		use ${data}/irsraw/`p'migration`j'`k'_`alab'.dta, clear
		foreach st of local somestates {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration0`j'0`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_`alab'.dta
	}
	
	* Erase the unwanted files
	cd ${data}/irsraw
	erase `m'to`n'statemigration.zip
}


*** 2004-2008
foreach j of numlist 4(1)8 {

	* Set the primary locals
	local k = `j'+1
	local m = 200`j'
	local n = 200`k'
	
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/state0`j'0`k'.zip state0`j'0`k'.zip, replace
	unzipfile state0`j'0`k'.zip, replace
	
	* Set the secondary locals
	foreach p in Out In {
		if "`p'" == "In" {
			local q "in"
		}
		else if "`p'" == "Out" {
			local q "out"
		}		
		
		cd "${data}/irsraw

		* Loop over each state of interest
		local statelist "Alaska Hawaii Alabama Arizona Arkansas California Colorado Connecticut Delaware DistrictofColumbia Florida Georgia Idaho Illinois Indiana Iowa Kansas Kentucky Louisiana Maine Maryland Massachusetts Michigan Minnesota Mississippi Missouri Montana Nebraska Nevada NewHampshire NewMexico NewJersey NewYork NorthCarolina NorthDakota Ohio Oklahoma Oregon Pennsylvania RhodeIsland SouthCarolina SouthDakota Tennessee Texas Utah Vermont Virginia Washington WestVirginia Wisconsin Wyoming"
		local chg_flag = 0
		foreach st of local statelist {
			if "`st'" == "Nebraska" {
				local chg_flag = 1
			}
			
			* Import the data
			local raw "`st'0`j'0`k'`q'"
			local begin = 8
			local end E62
			if inlist(`j', 7, 8) | (`j' == 6 & "`p'" == "Out" & `chg_flag' == 1) {
				local begin = 9
				local end E63
			}
			di "Year: `m'; State: `st'; P: `p'"
			import excel using `raw'.xls, cellrange(A`begin':`end') clear			
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring A D E, replace force
			assert inrange(A[1], 1, 100)
			assert inrange(A[54], 1, 100)
			
			if "`p'" == "In" {
				rename A statefiporigin
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefiporigin if substr(C,4,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				drop x
				assert inrange(statefipdestination, 1, 100)
				
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad	
			}
			if "`p'" == "Out" {
				rename A statefipdestination
				rename D returns
				rename E exemptions
				drop B 
				
				gen x = statefipdestination if substr(C,4,11)=="Non-Migrant"
				egen statefiporigin  = max(x)
				drop x
				assert inrange(statefiporigin, 1, 100)
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop C

				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
				
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad				
			}	

			* Record the year, run the clean file, and save
			erase `raw'.xls	
			qui gen year = 200`j'
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
		
		* Combine the state-year files and erase unwanted .dta files
		local AL Alabama
		local somestates: list statelist-AL
		use ${data}/irsraw/`p'migration`j'`k'_Alabama.dta, clear
		foreach st of local somestates {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration0`j'0`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_Alabama.dta
	}
	
	* Erase the unwanted files
	cd ${data}/irsraw
	erase state0`j'0`k'.zip
}

*** 2009-2010
foreach j of numlist 9(1)10 {

	* Set the primary locals
	local k = `j'+1
	local zj "0"
	local zk "0"
	if `j' > 8 {
		local zk ""
	}
	if `j' == 10 {
		local zj ""
	}
	local m "20`zj'`j'"
	local n "20`zk'`k'"
	
	* Download and unzip the data
	copy https://www.irs.gov/pub/irs-soi/state`zj'`j'`zk'`k'.zip state`zj'`j'`zk'`k'.zip, replace
	unzipfile state`zj'`j'`zk'`k'.zip, replace
	
	* Set the secondary locals
	foreach p in Out In {
		if "`p'" == "In" {
			local q "in"
		}
		else if "`p'" == "Out" {
			local q "out"
		}		
		
		cd "${data}/irsraw

		* Loop over each state of interest
		if `j' == 9 {
			local statelist "AK HI AL AZ AR CA CO CT DE DC FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY"
		}
		if `j' == 10 {
			local statelist "ak hi al az ar ca co ct de dc fl ga id il in ia ks ky la me md ma mi mn ms mo mt ne nv nh nj nm ny nc nd oh ok or pa ri sc sd tn tx ut vt va wa wv wi wy"
		}
		foreach st of local statelist {
			
			* Import the data
			local raw "`zj'`j'`zk'`k'inmig`q'`st'"
			local end F61
			di "Year: `m'; State: `st'; P: `p'"
			import excel using `raw'.xls, cellrange(B7:`end') clear			
			
			*-------------------------------------------------------------------
			* Clean up the data
			*-------------------------------------------------------------------
			
			* Basic cleanup and observation of same/other-state & total migration
			destring B E F, replace force
			
			if "`p'" == "In" {
				rename B statefiporigin
				rename E returns
				rename F exemptions
				drop C 
				
				gen x = statefiporigin if substr(D,4,11)=="Non-Migrant"
				egen statefipdestination = max(x)
				drop x
				
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop D

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad	
			}
			if "`p'" == "Out" {
				rename B statefipdestination
				rename E returns
				rename F exemptions
				drop C 
				
				gen x = statefipdestination if substr(D,4,11)=="Non-Migrant"
				egen statefiporigin  = max(x)
				drop x
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop D

				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
				
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad				
			}	

			* Record the year, run the clean file, and save
			erase `raw'.xls	
			qui gen year = 200`j'
			if `j' == 10 {
				qui replace year = 20`j'
			}
			drop if statefipor>56|statefipdes>56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}
		
		* Combine the state-year files and erase unwanted .dta files
		local AL AL
		if `j' == 10 {
			local AL al
		}
		local somestates: list statelist-AL
		use ${data}/irsraw/`p'migration`j'`k'_`AL'.dta, clear
		foreach st of local somestates {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		if `j' == 9 {
			saveold ${data}/irsraw/state_`p'migration0`j'`k'.dta, replace
		}
		else if `j' == 10 {
			saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		}
		erase ${data}/irsraw/`p'migration`j'`k'_`AL'.dta
	}
	
	* Erase the unwanted files
	cd ${data}/irsraw
	erase state`zj'`j'`zk'`k'.zip
}

*** 2011-2016
foreach j of numlist 11(1)15 {
	
	* Set the primary locals
	local k = `j'+1
	
	* Set the secondary locals
	foreach p in Out In {
		if "`p'" == "In" {
			local q "in"
		}
		else if "`p'" == "Out" {
			local q "out"
		}		
		
		* Download and unzip the data
		cd "${data}/irsraw
		copy https://www.irs.gov/pub/irs-soi/state`q'flow`j'`k'.csv state`q'flow`j'`k'.csv, replace
	
		* Loop over each state FIPS of interest
		foreach st of numlist 2 15 1 4 5 6 8 9 10 11 12 13 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 44 45 46 47 48 49 50 51 53 54 55 56 {
			
			* Import and clean up the data
			di "Year: 20`j'; State: `st'; P: `p'"
			import delimited using state`q'flow`j'`k'.csv, varnames(1) clear
			if "`q'" == "in" {
				keep if y2_statefips == `st'
				drop y2_statefips y1_state agi
				rename y1_statefips statefiporigin
				rename y1_state_name D
				rename n1 returns
				rename n2 exemptions
				gen x = statefiporigin if substr(D,4,11)=="Non-migrant"
				egen statefipdestination = max(x)
				drop x
				
				gen x = returns if statefipor == 96
				gen y = exemptions if statefipor == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipor == 96
				gen y = exemptions if statefipor == statefipdes
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop D

				gen x = returns if statefipor == 57
				gen y = exemptions if statefipor == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
			
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}
			else if "`q'" == "out" {
				keep if y1_statefips == `st'
				drop y1_statefips y2_state agi
				rename y2_statefips statefipdestination
				rename y2_state_name D
				rename n1 returns
				rename n2 exemptions
				gen x = statefipdestination if substr(D,4,11)=="Non-migrant"
				egen statefiporigin  = max(x)
				drop x
				
				gen x = returns if statefipdestination == 96
				gen y = exemptions if statefipdestination == 96
				egen tot`p'returns = max(x)
				egen tot`p'exemptions = max(y)
				drop x y
				drop if statefipdestination == 96
				gen y = exemptions if statefipdestination == statefipor
				egen tot`p'stayers = max(y)
				drop y
				drop if statefipor == statefipdes
				drop D

				gen x = returns if statefipdestination == 57
				gen y = exemptions if statefipdestination == 57
				egen tot`p'returnsabroad = max(x)
				egen tot`p'exemptionsabroad = max(y)
				drop x y
				
				gen tot`p'returnsUS = tot`p'returns-tot`p'returnsabroad
				gen tot`p'exemptionsUS = tot`p'exemptions-tot`p'exemptionsabroad
			}

			* Record the year, run the clean file, and save
			qui gen year = 20`j'
			drop if statefipor > 56 | statefipdes > 56
			compress
			save "${data}/irsraw/`p'migration`j'`k'_`st'.dta", replace
		}	
	
		* Combine the state-year files and erase unwanted .dta files
		use ${data}/irsraw/`p'migration`j'`k'_1.dta, clear
		foreach st of numlist 2 15 4 5 6 8 9 10 11 12 13 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 44 45 46 47 48 49 50 51 53 54 55 56 {
			append using ${data}/irsraw/`p'migration`j'`k'_`st'.dta
			erase ${data}/irsraw/`p'migration`j'`k'_`st'.dta
		}
		saveold ${data}/irsraw/state_`p'migration`j'`k'.dta, replace
		erase ${data}/irsraw/`p'migration`j'`k'_1.dta
		
		* Erase the unwanted files
		cd ${data}/irsraw
		erase state`q'flow`j'`k'.csv
	}
}

************************************** 
*** Create panels for each data series
************************************** 
foreach p in In Out {
qui use "${data}/irsraw/state_`p'migration9091.dta", clear
	forval j = 1(1)25 {
		local k = `j' + 1
		if `k' < 10 {
			append using "${data}/irsraw/state_`p'migration9`j'9`k'.dta"
		}
		else if `k' == 10 {
			append using "${data}/irsraw/state_`p'migration9`j'00.dta"
		}
		else if inrange(`k', 11, 19) {
			local j = `j' - 10
			local k = `k' - 10
			append using "${data}/irsraw/state_`p'migration0`j'0`k'.dta"
		}
		else if `k' == 20 {
			local j = `j' - 10
			local k = `k' - 10
			append using "${data}/irsraw/state_`p'migration0`j'`k'.dta"
		}
		else if `k' > 20   {
			local j = `j' - 10
			local k = `k' - 10
			append using "${data}/irsraw/state_`p'migration`j'`k'.dta"
		}
	}
	capture drop j k 
	capture drop J K 
	capture drop B 
	capture drop C
	capture drop D
	capture drop x y
	save "${data}/state_`p'migration_9016.dta", replace
}

************************************** 
*** Clean folder
************************************** 

cd ${data}/irsraw
!rm /s /q *.xls 
!rm /s /q *.zip 
!rm /s /q *.dat 
!rm /s /q *.doc 

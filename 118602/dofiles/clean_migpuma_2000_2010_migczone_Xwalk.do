clear 
set more off


/*****************************************
 Crosswalk between MIGPUMA 2000 and MIGCZ 
 based on PUMA-MIGPUMA 2000 and PUMA 2000
 to CZ 1990 DORN's CROSSWALK
******************************************/

use ${data}/ancillary/cw_puma2000_czone_state_region.dta, clear
merge m:1 puma2000 using ${data}/ancillary/puma2000_migpuma1.dta, gen(m_2)
drop m_2
rename aggregatedpuma2000 migpuma2000
preserve
	keep puma2000 migpuma2000 poppuma2000
	duplicates drop
	collapse (sum) poppuma2000, by(migpuma)
	tempfile migpumapop
	save `migpumapop'
restore
rename cz migcz
gen wafactor=afactor*poppuma2000

collapse (sum) wafactor afactor, by(mig*)
merge m:1 migpuma using `migpumapop', nogen
replace afactor = wafactor/poppuma2000
keep afactor migpuma2000 migcz

rename migcz cz
preserve
	use ${data}/ancillary/cw_puma2000_czone_state_region.dta, clear
	keep cz statefip division region czname statename stateabbrv
	duplicates drop
	tempfile cz_state
	save `cz_state'
restore
merge m:1 cz using `cz_state', nogen keepusing(statefip division region czname statename stateabbrv)
rename cz migcz
cap drop _m
compress 
saveold ${data}/ancillary/cw_migpuma2000_migczone_state_region.dta, replace


/*****************************************
 Crosswalk between MIGPUMA 2010 and MIGCZ 
 based on PUMA-MIGPUMA 2010 and PUMA 2010
 to CZ 1990 DORN's CROSSWALK
******************************************/
use ${data}/ancillary/cw_puma2010_czone_state_region.dta, clear
merge m:1 puma2010 using ${data}/ancillary/puma2010_migpuma1.dta, gen(m_2)
drop m_2
rename aggregatedpuma2010 migpuma2010
preserve
	keep puma201 migpuma2010 poppuma2010
	duplicates drop
	collapse (sum) poppuma2010, by(migpuma)
	tempfile migpumapop
	save `migpumapop'
restore
rename cz migcz
gen wafactor=afactor*poppuma2010

collapse (sum) wafactor afactor, by(mig*)
merge m:1 migpuma using `migpumapop', nogen
replace afactor = wafactor/poppuma2010
keep afactor migpuma2010 migcz

rename migcz cz
preserve
	use ${data}/ancillary/cw_puma2010_czone_state_region.dta, clear
	keep cz statefip division region czname statename stateabbrv
	duplicates drop
	tempfile cz_state
	save `cz_state'
restore
merge m:1 cz using `cz_state',  keepusing(statefip division region czname statename stateabbrv)
rename cz migcz
cap drop _m
compress 
saveold ${data}/ancillary/cw_migpuma2010_migczone_state_region.dta, replace


clear all
set more off

*******************************************************************
*** Master dofile for Basso and Peri's JEP migration project
*** For any question, please email: gaetano.basso@bancaditalia.it
*** May 28, 2020
*******************************************************************

* Attention! This dofile is a configuration dofile and it is in the program folder. First you need to set up the directory 
* where you save it (i) it imports and installs Stata user-written packages used in the other dofiles below 
* (it needs a working internet connection) (ii) set up the directories (first you need to set your current directory!)

*** Set your own personal working directory HERE (i.e., where you downloaded the replication package)

include config.do

global pdir ${rootdir}
do ${pdir}/dofiles/setup_dofile.do

/* All the final datasets are already provided in the replication package
*** Clean Census and ACS data (downloaded from IPUMS US as indicated in the README) ***
* This dofile clean and aggregates Census data to commuting zone- and state-level cells for different analyses in the paper (see "*** Main analysis..." below)
do $dofiles/clean_census.do 


*** Import and clean IRS data ***

* This dofile imports IRS raw data post 1990 (it needs a working internet connection): these data are not included in the package 
* (but are downloaded directly from the IRS website, as indicated in the program); some additional dofiles are also included (see at the bottom)
do $dofiles/import_irs.do

* This dofile imports IRS state-level raw data post 1990: these data are not included in the package (but can be downloaded as indicated in the program); the underlying dofiles are included (see below in this dofile)
do $dofiles/import_irs_state.do 

* This dofile maps counties to commuting zones for IRS data
do $dofiles/IRS_cty_to_cz.do 
*/

*** Main analysis, tables and graphs in the paper ***

* This dofile generates Table 1 (summstats_tab1.xls in ./tables) and Figure 4 
do $dofiles/summstats_demogr_decomp.do

* This dofile generates Figures 1, 2 and A1
do $dofiles/summstats_census_irs_19802017_baselinegraphs.do

* This dofile generates Figures 3a and 3b
do $dofiles/summstats_census_19802017_baselinegraphs.do

* This dofile generates Figures 5a and 5b
do $dofiles/directed_census_19802017.do

* This dofile generates the results in Table A2 (it saves the logfiles regressions_1980_2000.log and regressions_2000_2017.log in ./tables)
do $dofiles/tableA2.do

* These dofiles generate the graphs in Figure 6a and 6b respectively
do $dofiles/directed_census_fromabroad_20002017.do
do $dofiles/directed_cz_census_fromabroad_20052017.do

* This dofile just replicates figure A2 from data obtained from Joshua Grelewicz in October 2019 (see README)
do $dofiles/graph_licensing_fborn.do


*** Note: Additional dofiles provided, but that do not need to be run
/* Additional dofiles to read and save Census and ACS raw-data downloaded from IPUMS:
$dofiles/import_census.do // This dofile reads IPUMS-downloaded Census and ACS raw data files in *.dat format and save them in *.dta format.
$dofiles/censusraw/import_00`j'.do (for j = 152/159 166/167 171/179 181 183) // *.dat data are not included in the package, but these dofiles indicate all the imported variables
*/

/* Additional dofiles used to read, clean and save county and state-level IRS migration data obtained upon private agreement from Andrew Foote (see README):
$dofiles/county_flows_builder.do // This dofile cleans IRS county-level raw data pre-1990. However, Foote's dofiles and original raw datasets are not included in the package and the dofile cannot be re-run (data obtained in October 2019; see README)
$dofiles/state_flows_builder.do  // This dofile cleans IRS state-level raw data pre 1990. However, Foote's dofiles and original raw datasets are not included in the package and the dofile cannot be re-run (obtained in October 2019; see README) 
*/

/* Additional ancillary dofiles used to label variables, clean geographic identifiers and as crosswalk from PUMAs to commuting zones:
$dofiles/labelvar_census.do
$dofiles/state_region_additionalXwalk.do
$dofiles/clean_DD_CZs_Xwalk.do
$dofiles/clean_puma2000_migpuma2000_Xwalk.do
$dofiles/clean_puma2010_migpuma2010_Xwalk.do
$dofiles/clean_migpuma_2000_2010_migczone_Xwalk.do
*/

/* Additional dofiles used in "import_irs.do" to clean IRS raw-data (recalled in the dofiles described above):
$dofiles/irsraw/clean_inmigration_19902009.do
$dofiles/irsraw/clean_outmigration_19902009.do
$dofiles/irsraw/clean_inmigration_20092011.do
$dofiles/irsraw/clean_outmigration_20092011.do
$dofiles/irsraw/clean_inmigration_2011on.do
$dofiles/irsraw/clean_outmigration_2011on.do
*/


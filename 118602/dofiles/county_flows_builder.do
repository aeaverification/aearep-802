*-------------------------------------------------------------------------------
* Description: Build the migration data through 2000 from Andrew Foote 
* (lightly modified)
*
* Original author: Andrew Foote
* Modifying authors: Gaetano Basso & Justin Wiltshire
* Updated: October 31, 2019
*-------------------------------------------------------------------------------


/* 
Original from Andrew Foote:
This do-file takes the raw data and builds two data sets:
 1. county_inflows.dta
 2. county_outflows.dta 
 
 The end format for the inflows data set is the following:
	receiving sending returns exemptions
 The end format for the outflows data set is the following:
	sending receiving returns exemptions
In both datasets, in addition to the county pairs, there are two extra rows for
each county.
 1. total migrants: in this case, the second fips code columnn == 96000
 2. County non-migrants: in this case, the fips codes match in sending & receiving
 
 This is the first do-file in the data build. The next do-file is county_nonmigration_build
 */

clear
set more off

cd ${data}

tempfile mergefilereceive
tempfile mergefilesend
use ${dofiles}/foote//crosswalk.dta, clear

drop if state == "."
rename areaname receiving_county_s
rename statename receiving_state_s
gen receiving_county = stfips*1000+ctyfips
keep receiving_county_s receiving_state_s year receiving_county
sort receiving_county_s receiving_state_s year
save `mergefilereceive', replace

rename receiving_county_s sending_county_s
rename receiving_state_s sending_state_s
rename receiving_county sending_county
sort sending_county_s sending_state_s year
save `mergefilesend', replace

********************************************************

local x 8384
	tempfile temp`x'in

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt" 
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//migration8385_in.dct", using( `fl') clear
		qui g sending = in_returns_pct==100
		qui g receiving_county_s = ""
		qui g receiving_state_s = ""
		qui sum in_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace receiving_county_s = "`county'" if _n==`i'
			qui replace receiving_state_s = "`state'" if _n==`i'
		}
		rename county sending_county_s
		rename state sending_state_s
		order  receiving_county_s receiving_state_s sending_county_s sending_state_s
		drop sending
		
		gen year = `x'
		qui drop if sending_county_s== "REGION 1'   NORTH EAST." | sending_county_s == "REGION 2'   NORTH CENTR" | sending_county_s == "REGION 3'   SOUTH . . ." | sending_county_s == "REGION 4'   WEST. . . ."
		qui replace sending_state_s = "..." if sending_state_s == "." & sending_county_s == "SAME STATE. . . . . . ."
		qui replace sending_state_s = ".." if sending_state_s == "." & sending_county_s == "COUNTY NON-MIGRANTS."
		qui drop if sending_state_s == "." 
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
}
save `temp`x'in', replace

local x 8384		
tempfile temp`x'out

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//migration8385_out.dct", using( `fl') clear	
		qui g sending = out_returns_pct==100
		qui g sending_county_s = ""
		qui g sending_state_s = ""
		qui sum out_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace sending_county_s = "`county'" if _n==`i'
			qui replace sending_state_s = "`state'" if _n==`i'
		}
		rename county receiving_county_s
		rename state receiving_state_s
		order sending_county_s sending_state_s receiving_county_s receiving_state_s
		drop sending
		
		gen year = `x'
		qui drop if receiving_county_s== "REGION 1'   NORTH EAST." | receiving_county_s == "REGION 2'   NORTH CENTR" | receiving_county_s == "REGION 3'   SOUTH . . ." | receiving_county_s == "REGION 4'   WEST. . . ."
		qui replace receiving_state_s = "..." if receiving_state_s == "." & receiving_county_s == "SAME STATE. . . . . . ."
		qui replace receiving_state_s = ".." if receiving_state_s == "." & receiving_county_s == "COUNTY NON-MIGRANTS."
		qui drop if receiving_state_s == "." 
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
save `temp`x'out', replace	
	
	
local x 8485

	tempfile temp`x'in

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//migration8385_in.dct", using( `fl') clear
		qui gen sending = in_returns_pct==. & state!= "."
		qui g receiving_county_s = ""
		qui g receiving_state_s = ""
		qui sum in_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace receiving_county_s = "`county'" if _n==`i'
			qui replace receiving_state_s = "`state'" if _n==`i'
		}
		rename county sending_county_s
		rename state sending_state_s
		order  receiving_county_s receiving_state_s sending_county_s sending_state_s
		drop sending
		
		gen year = `x'
		qui drop if sending_county_s== "Same Region, Diff. Stat" | sending_county_s== "Region 1'   Northeast ." | sending_county_s == "Region 2'   North Centr" | sending_county_s == "Region 3'   South . . ." | sending_county_s == "Region 4'   West. . . ."
		qui replace sending_state_s = "..." if sending_state_s == "." & sending_county_s == "Same State. . . . . . ."
		qui replace sending_state_s = ".." if sending_state_s == "." & sending_county_s == "County Non-Migrants."
		qui drop if sending_state_s == "." 
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
	}
save `temp`x'in', replace
	
tempfile temp`x'out

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//migration8385_out.dct", using( `fl') clear	
		qui g sending = out_returns_pct==. & state!= "."
		qui g sending_county_s = ""
		qui g sending_state_s = ""
		qui sum out_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = county[`i']
				local state = state[`i']
			}
			qui replace sending_county_s = "`county'" if _n==`i'
			qui replace sending_state_s = "`state'" if _n==`i'
		}
		rename county receiving_county_s
		rename state receiving_state_s
		order sending_county_s sending_state_s receiving_county_s receiving_state_s
		drop sending
		
		gen year = `x'		
		qui drop if receiving_county_s== "Same Region, Diff. Stat" | receiving_county_s== "Region 1'   Northeast ." | receiving_county_s == "Region 2'   North Centr" | receiving_county_s == "Region 3'   South . . ." | receiving_county_s == "Region 4'   West. . . ."
				qui replace receiving_state_s = "..." if receiving_state_s == "." & receiving_county_s == "Same State. . . . . . ."
				qui replace receiving_state_s = ".." if receiving_state_s == "." & receiving_county_s == "County Non-Migrants."
		qui drop if receiving_state_s == "."
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
save `temp`x'out', replace

************************************************************************

foreach x in 8586 8687 8788 { 

	tempfile temp`x'in

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
	
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//migration8588_in.dct", using( `fl') clear
		qui replace countyfip = statefip*1000 + countyfip if statefip!=.

		g year = `x'
		g sending = statefip!=.
		qui g receiving_county = .
		qui sum in_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = countyfip[`i']

			}
			qui replace receiving_county = `county' if _n==`i'
		}
		rename countyfip sending_county
		qui replace sending_county = 97001 if sending_county==.&substr(county,1,4)=="tate"
		qui drop if sending_county==.
		qui replace sending_county = 96000 if sending == 1
		qui replace sending_county = receiving_county if state== "."&sending_county!=97001
		qui drop sending
		order receiving_county sending_county
		capture append using `temp`x'in'
		qui save `temp`x'in',replace
	}

	save `temp`x'in', replace

	tempfile temp`x'out

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
	
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//migration8588_out.dct", using( `fl') clear	
		qui replace countyfip = statefip*1000 + countyfip if statefip!=.
		g year = `x'
		g sending = statefip!=.
		qui g sending_county = .
		qui sum out_returns
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = countyfip[`i']

			}
			qui replace sending_county = `county' if _n==`i'
		}
		rename countyfip receiving_county
		qui replace receiving_county = 97001 if receiving_county==.&substr(county,1,4)=="tate"
		qui drop if receiving_county==.
		qui replace receiving_county = 96000 if sending == 1
		qui replace receiving_county = receiving_county if state== "."&receiving_county!=97001
		qui drop sending
		order sending_county receiving_county
		capture append using `temp`x'out'
		qui save `temp`x'out',replace
	}

	save `temp`x'out', replace
}

****************************************************************************

foreach x in 8889 8990 9091 9192 {

	tempfile temp`x'in

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
	
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//countymigration88_in.dct", using( `fl') clear
		qui replace countyfip = statefip*1000 + countyfip if statefip!=.
		g year = `x'
		g sending = statefip!=.
		qui g receiving_county = .
		qui des countyfip
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = countyfip[`i']

			}
			qui replace receiving_county = `county' if _n==`i'
		}
		rename countyfip sending_county
		qui replace sending_county = 97001 if sending_county==.&substr(county,1,4)=="tate"
		qui drop if sending_county==.
		qui replace sending_county = 96000 if sending_county==receiving_county
		qui replace sending_county = receiving_county if state== ""&sending_county!=97001
		qui drop sending
		order receiving_county sending_county
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
	}
	
	save `temp`x'in', replace
	
	tempfile temp`x'out

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
	
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//countymigration88_out.dct", using( `fl') clear
		qui replace countyfip = statefip*1000 + countyfip if statefip!=.
		g year = `x'
		g sending = statefip!=.
		qui g sending_county = .
		qui des countyfip
		local n = r(N)
		forvalues i = 1/`n' {
			if sending[`i'] == 1 {
				local county = countyfip[`i']

			}
			qui replace sending_county = `county' if _n==`i'
		}
		rename countyfip receiving_county
		qui replace receiving_county = 97001 if receiving_county==.&substr(county,1,4)=="tate"
		qui drop if receiving_county==.
		qui replace receiving_county = 96000 if sending_county==receiving_county
		qui replace receiving_county = receiving_county if state== ""&receiving_county!=97001
		qui drop sending
		order sending_county receiving_county
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
	
	save `temp`x'out',replace
}

********************************************************


local x 9293

	tempfile temp`x'in

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'in_ascii/"
	
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//countymigration92_in.dct", using( `fl') clear
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
	}
	g sending_county = otherstatefip*1000 + othercountyfip
	g receiving_county = statefip*1000 + countyfip
	g year = `x'
	qui replace sending_county = receiving_county if sending_county == 63050
	qui replace sending_county = 96000 if sending_county == 1
	qui replace state = "SS" if state == "XX"
	qui replace sending_county = 97001 if state == "SS"
	qui drop if state == "XX" | state == "FR"
	order year receiving_county sending_county in_returns in_exemptions 
	
	qui save `temp`x'in', replace
	
	tempfile temp`x'out

	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/" files "*.txt"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_ascii/Co`x'out_ascii/"
	
	foreach fl in `filelist' {
		qui infile using "${dofiles}/foote//countymigration92_out.dct", using( `fl') clear
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
	g receiving_county = otherstatefip*1000 + othercountyfip
	g sending_county = statefip*1000 + countyfip
	g year = `x'
	qui replace receiving_county = sending_county if receiving_county == 63050
	qui replace receiving_county = 96000 if receiving_county == 1
	qui replace state = "SS" if state == "XX"
	qui replace receiving_county = 97001 if state == "SS"	
	qui drop if state == "XX" | state == "FR"
	order year  sending_county receiving_county out_returns out_exemptions 
	
	qui save `temp`x'out', replace
	
**********************************************************************
/** Create excel files for 1993 on
foreach x in 9394 9495 {
local filelist: dir "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'in_excel\" files "*.xls"
local directory "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'in_excel"
cd "`directory'"
foreach fl in `filelist' {
shell XlsToCsv.vbs `fl' `fl'.csv
local directory "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'out_excel"
cd "`directory'"
shell XlsToCsv.vbs `fl' `fl'.csv
}
}

local x 9596
local filelist: dir "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'in_excel\" files "*.xls"
local directory "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'in_excel"
cd "`directory'"
foreach fl in `filelist' {
shell XlsToCsv.vbs `fl' `fl'.csv
local directory "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'out_excel"
cd "`directory'"
shell XlsToCsv.vbs `fl' `fl'.csv
}

foreach x in 9697 9798 9899 9900 {
local filelist: dir "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'in_excel\" files "*.xls"
local directory "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'in_excel"
cd "`directory'"
foreach fl in `filelist' {
shell XlsToCsv.vbs `fl' `fl'.csv
local directory "${data}\irsraw\COUNTY Migration\CO`x'\Co`x'_excel\Co`x'out_excel"
cd "`directory'"
shell XlsToCsv.vbs `fl' `fl'.csv
}
}
*/
***************************************************************

local x 9394

tempfile temp`x'in
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'in_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'in_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<=3
		forvalues i = 1/8 {
			qui destring v`i', generate(v`i'_b)
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstatefip
		qui rename v4_b othercountyfip
		qui rename v5_b in_returns
		qui rename v6_b in_exemptions
		qui rename v7_b in_agg_income
		qui rename v8_b in_med_income
		qui rename v9 state
		qui rename v10 county
		
		gen receiving_county = statefip*1000 + countyfip
		gen sending_county = otherstatefip*1000 + othercountyfip
		qui drop v* 
		g year = `x'
		qui replace sending_county = receiving_county if sending_county == 63050
		qui replace sending_county = 96000 if sending_county == 1
		qui replace state = "SS" if state == "XX"
		qui replace sending_county = 97001 if state == "SS"		
		qui drop if state == "XX" | state == "FR"
		order year receiving_county sending_county in_returns in_exemptions
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
	}
save `temp`x'in', replace
	
tempfile temp`x'out
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'out_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'out_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<=3
		forvalues i = 1/8 {
			qui destring v`i', generate(v`i'_b)
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstatefip
		qui rename v4_b othercountyfip
		qui rename v5_b out_returns
		qui rename v6_b out_exemptions
		qui rename v7_b out_agg_income
		qui rename v8_b out_med_income
		qui rename v9 state
		qui rename v10 county
		
		gen sending_county = statefip*1000 + countyfip
		gen receiving_county = otherstatefip*1000 + othercountyfip
		qui drop v* 
		g year = `x'
		qui replace receiving_county = sending_county if receiving_county == 63050
		qui replace receiving_county = 96000 if receiving_county == 1
		qui replace state = "SS" if state == "XX"
		qui replace receiving_county = 97001 if state == "SS"		
		qui drop if state == "XX" | state == "FR"
		order year receiving_county sending_county out_returns out_exemptions
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
save `temp`x'out', replace
	
***************************************************************
 

foreach x in 9495 9697 9798 {
tempfile temp`x'in
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'in_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'in_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<9
		forvalues i = 1/4 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		forvalues i = 7/9 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstate
		qui rename v4_b othercounty
		qui rename v5 state
		qui rename v6 county
		qui rename v7_b in_returns
		qui rename v8_b in_exemptions
		qui rename v9_b in_agg_income
		
		gen receiving_county = statefip*1000 + countyfip
		gen sending_county = otherstate*1000 + othercounty
		
		qui drop v*
		g year = `x'
		qui replace sending_county = receiving_county if sending_county == 63050
		qui replace sending_county = 96000 if sending_county == 1
		qui replace state = "SS" if state == "XX"
		qui replace sending_county = 97001 if state == "SS"
		qui drop if state == "XX" | state == "FR"		
		order year receiving_county sending_county
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
	}
	save `temp`x'in', replace
	
tempfile temp`x'out
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'out_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'out_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<9
		forvalues i = 1/4 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		forvalues i = 7/9 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstate
		qui rename v4_b othercounty
		qui rename v5 state
		qui rename v6 county
		qui rename v7_b out_returns
		qui rename v8_b out_exemptions
		qui rename v9_b out_agg_income
		
		gen sending_county = statefip*1000 + countyfip
		gen receiving_county = otherstate*1000 + othercounty
		
		qui drop v*
		g year = `x'
		qui replace receiving_county = sending_county if receiving_county == 63050
		qui replace receiving_county = 96000 if receiving_county == 1
		qui replace state = "SS" if state == "XX"
		qui replace receiving_county = 97001 if state == "SS"	
		qui drop if state == "XX" | state == "FR"	
		* order year receiving_county sending_county
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
	save `temp`x'out', replace
}

local x 9596

tempfile temp`x'in
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/Co9596_r/Co9596r_excel/Co9596inr_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/Co9596_r/Co9596r_excel/Co9596inr_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<9
		forvalues i = 1/4 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		forvalues i = 7/9 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstate
		qui rename v4_b othercounty
		qui rename v5 state
		qui rename v6 county
		qui rename v7_b in_returns
		qui rename v8_b in_exemptions
		qui rename v9_b in_agg_income
		
		gen receiving_county = statefip*1000 + countyfip
		gen sending_county = otherstate*1000 + othercounty
		qui drop if statefip==.

		qui drop v*
		g year = `x'
		* qui replace sending_county = receiving_county if sending_county == 63050
		* qui replace sending_county = 96000 if sending_county == 1
		qui drop if sending_county == 97000 | sending_county == 97003 | sending_county == 98000
		qui replace state = "SS" if state == "XX"
		qui drop if state == "XX" | state == "FR"
		order year receiving_county sending_county
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
	}
save `temp`x'in', replace

local x 9596	

tempfile temp`x'out
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/Co9596_r/Co9596r_excel/Co9596outr_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/Co9596_r/Co9596r_excel/Co9596outr_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<9
		forvalues i = 1/4 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		forvalues i = 7/9 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstate
		qui rename v4_b othercounty
		qui rename v5 state
		qui rename v6 county
		qui rename v7_b out_returns
		qui rename v8_b out_exemptions
		qui rename v9_b out_agg_income
		
		gen sending_county = statefip*1000 + countyfip
		gen receiving_county = otherstate*1000 + othercounty
		qui drop if statefip==.
		qui drop v*
		g year = `x'
		* qui replace receiving_county = sending_county if receiving_county == 63050
		* qui replace receiving_county = 96000 if receiving_county == 1
		qui drop if receiving_county == 97000 |  receiving_county == 97003 | receiving_county == 98000
		qui replace state = "SS" if state == "XX"
		qui drop if state == "XX" | state == "FR"
		order year sending_county receiving_county
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
	save `temp`x'out', replace
	
*********************************************************************
set more off
 
foreach x in 9899 9900 {

	tempfile temp`x'in
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'in_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'in_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<6
		forvalues i = 1/4 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		forvalues i = 7/10 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstate
		qui rename v4_b othercounty
		qui rename v5 state
		qui rename v6 county
		qui rename v7_b in_returns
		qui rename v8_b in_exemptions
		qui rename v9_b in_agg_income
		qui rename v10_b in_med_income
		
		gen receiving_county = statefip*1000 + countyfip
		gen sending_county = otherstate*1000 + othercounty
		
		qui drop v* 
		g year = `x'
		order year receiving_county sending_county
		capture append using `temp`x'in'
		qui save `temp`x'in', replace
	}
	save `temp`x'in', replace
	
	tempfile temp`x'out
	local filelist: dir "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'out_excel/" files "*.csv"
	cd "${data}/irsraw/foote/COUNTY Migration/CO`x'/Co`x'_excel/Co`x'out_excel/"
	foreach fl in `filelist' {
		qui insheet using `fl', clear
		qui drop if _n<6
		forvalues i = 1/4 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		forvalues i = 7/10 {
			qui destring v`i', generate(v`i'_b) ignore(",") force
		}
		qui rename v1_b statefip
		qui rename v2_b countyfip
		qui rename v3_b otherstate
		qui rename v4_b othercounty
		qui rename v5 state
		qui rename v6 county
		qui rename v7_b out_returns
		qui rename v8_b out_exemptions
		qui rename v9_b out_agg_income
		qui rename v10_b out_med_income
		
		gen sending_county = statefip*1000 + countyfip
		gen receiving_county = otherstate*1000 + othercounty
		
		qui drop v* 
		g year = `x'
		order year sending_county receiving_county
		capture append using `temp`x'out'
		qui save `temp`x'out', replace
	}
	save `temp`x'out', replace
}



*******************************************************
/* This section gets fipscodes for the 1984 and 1985 data */
tempfile temp8385in temp8385out mergefile mergefile2
tempfile mergefilesend mergefilereceive

foreach x in in out {
/* Creating a dataset here for the 8384 and 8485 files  */
use `temp8384`x'',clear
append using `temp8485`x''


save `temp8385`x'', replace
}

use "${dofiles}/foote//crosswalk.dta", clear

drop if state == "."
keep areaname statename year stfips ctyfips
rename areaname receiving_county_s
rename statename receiving_state_s
gen receiving_county = stfips*1000+ctyfips
sort receiving_county_s receiving_state_s year
save `mergefilereceive', replace

rename receiving_county_s sending_county_s
rename receiving_state_s sending_state_s
rename receiving_county sending_county
sort sending_county_s sending_state_s year
save `mergefilesend', replace


clear
foreach x in in out {


	use `temp8385`x'', clear

	sort receiving_county_s receiving_state_s year

	joinby receiving_county_s receiving_state_s year using `mergefilereceive', unmatched(master)
	drop _merge

	sort sending_county_s sending_state_s year
	joinby sending_county_s sending_state_s year using `mergefilesend', unmatched(master)
	drop _merge
	
	drop if sending_state_s == "FR" | receiving_state_s == "FR" | receiving_state_s == "XX" | sending_state_s == "XX" | sending_state_s == "e." | receiving_state_s == "e."
	
	save `temp8385`x'', replace
}

use `temp8385in', clear

	replace sending_county = 96000 if sending_county_s==receiving_county_s & sending_state_s == receiving_state_s
	replace sending_county = receiving_county if sending_state_s== "." | sending_state_s== ".."
	replace sending_county = 97001 if sending_state_s== "..."
	drop if sending_county==. & receiving_county==.

save `temp8385in', replace

use `temp8385out', clear

	replace receiving_county = 96000 if sending_county_s==receiving_county_s & sending_state_s == receiving_state_s
	replace receiving_county = sending_county if receiving_state_s== "." | sending_state_s== ".."
	replace receiving_county = 97001 if sending_state_s== "..."
	drop if sending_county==. & receiving_county==.

	
save `temp8385out', replace
	

*********************************************************************

* Finish up the inmigration data

use `temp8385in', clear

foreach x in 8586 8687 8788 8889 8990 9091 9192 9293 9394 9495 9596 9697 9798 9899 9900 {
	append using `temp`x'in'
}

drop if sending_state_s == "FR" | receiving_state_s == "FR" | receiving_state_s == "XX" | receiving_state_s == "XX"
rename state statename
replace statename = upper(statename)
rename county areaname
sort year statename areaname
replace in_exemptions = in_exemptions_yr2 if in_exemptions==.
drop in_exemptions_yr1 in_exemptions_yr2 in_returns_pct  in_exemptions_yr1_pct in_exemptions_yr2_pct

foreach x in sending receiving {
	drop if `x'_county >=57000 & `x'_county<96000
	drop if `x'_county > 97001 & `x'_county!=.
}

do "${dofiles}/foote//yearxwalk.do"
drop  receiving_county_s receiving_state_s sending_county_s sending_state_s

order year receiving_county sending_county in_returns in_exemptions

drop if receiving_county==.

replace in_returns = 0 if  in_returns == . | in_returns<0
replace in_exemptions = 0 if in_exemptions == . | in_exemptions<0

* Rename and restructure the data for our purposes
qui keep year receiving_county sending_county in_returns in_exemptions
qui rename receiving_county countyfipdestination
qui rename sending_county countyfiporigin
qui gen statefipdestination = floor(countyfipdestination/1000)
qui gen statefiporigin = floor(countyfiporigin/1000)
qui rename in_returns taxreturns
qui rename in_exemptions taxexemptions

* Make consistent with the cleaned IRS data files
*replace taxreturns = . if taxreturns<20&year<2013 & (statefipor!=57|statefipor!=58|statefipor!=59|statefipor!=96|statefipor!=97|statefipor!=98)
*replace taxexemptions = . if taxreturns==.
		
drop if countyfipdes==0
		
gen x1 = taxreturns if statefipor==96
gen x2 = taxexemptions if statefipor==96
bys countyfipdes statefipdes year: egen tottaxreturns = total(x1) 
bys countyfipdes statefipdes year: egen tottaxexemptions = total(x2)
drop x1 x2	
		
gen x = taxreturns if statefipor==97
gen y = taxexemptions if statefipor==97
bys countyfipdes statefipdes year: egen taxreturnssamestate=max(x)
bys countyfipdes statefipdes year: egen taxexemptionssamestate=max(y)
drop x y

gen x = taxreturns if countyfipor==countyfipdes&statefipor==statefipdes
gen y = taxexemptions if countyfipor==countyfipdes&statefipor==statefipdes
bys countyfipdes statefipdes year: egen taxreturnsstayers=max(x)
bys countyfipdes statefipdes year: egen taxexemptionsstayers=max(y)
drop x y
drop if countyfipor==countyfipdes&statefipor==statefipdes
drop if statefipor==57|statefipor==58|statefipor==59|statefipor==96|statefipor==97

* Drop migrations terminting in Alaska or Hawaii
*qui drop if inlist(statefipdes, 2, 15)

* Save
replace year = year-1
compress
preserve
	keep if year<1990
	keep year countyfipor countyfipdes statefipor statefipdes taxexemptions tottaxexemptions taxexemptionssamestate taxexemptionsstayers
	save "${data}/foote_county_inflows_x_state.dta", replace
restore
drop *samestate
save "${data}/foote_county_inflows.dta", replace

* Finish up the outmigration data
use `temp8385out', clear

foreach x in 8586 8687 8788 8889 8990 9091 9192 9293 9394 9495 9596 9697 9798 9899 9900 {
	append using `temp`x'out'
}

drop if sending_state_s == "FR" | receiving_state_s == "FR" | receiving_state_s == "XX" | receiving_state_s == "XX"
rename state statename
replace statename = upper(statename)
rename county areaname
sort year statename areaname
replace out_exemptions = out_exemptions_yr2 if out_exemptions==.
drop out_exemptions_yr1 out_exemptions_yr2 out_returns_pct  out_exemptions_yr1_pct out_exemptions_yr2_pct
foreach x in sending receiving {
	drop if `x'_county >=57000 & `x'_county<96000
	drop if `x'_county > 97001 & `x'_county!=.
}

do "${dofiles}/foote//yearxwalk.do"

drop if sending_county == .

replace out_returns = 0 if  out_returns == . | out_returns<0
replace out_exemptions = 0 if out_exemptions == . | out_exemptions<0

drop  receiving_county_s receiving_state_s sending_county_s sending_state_s

order year receiving_county sending_county out_returns out_exemptions

* Rename and restructure the data for our purposes
qui keep year receiving_county sending_county out_returns out_exemptions
qui rename receiving_county countyfipdestination
qui rename sending_county countyfiporigin
qui gen statefipdestination = floor(countyfipdestination/1000)
qui gen statefiporigin = floor(countyfiporigin/1000)
qui rename out_returns taxreturns
qui rename out_exemptions taxexemptions

* Make consistent with the cleaned IRS data files
*replace taxreturns = . if taxreturns<20&year<2013 & (statefipor!=57|statefipor!=58|statefipor!=59|statefipor!=96|statefipor!=97|statefipor!=98)
*replace taxexemptions = . if taxreturns==.
		
drop if countyfipor==0
		
gen x1 = taxreturns if statefipdes==96
gen x2 = taxexemptions if statefipdes==96
bys countyfipor statefipor year: egen tottaxreturns = total(x1) 
bys countyfipor statefipor year: egen tottaxexemptions = total(x2)
drop x1 x2

gen x = taxreturns if statefipdes==97
gen y = taxexemptions if statefipdes==97
bys countyfipor statefipor year: egen taxreturnssamestate=max(x)
bys countyfipor statefipor year: egen taxexemptionssamestate=max(y)
drop x y

gen x = taxreturns if countyfipdes==countyfipor&statefipdes==statefipor
gen y = taxexemptions if countyfipdes==countyfipor&statefipdes==statefipor
bys countyfipor statefipor year: egen taxreturnsstayers=max(x)
bys countyfipor statefipor year: egen taxexemptionsstayers=max(y)
drop x y
drop if countyfipdes==countyfipor&statefipdes==statefipor
drop if statefipdes==57|statefipdes==58|statefipdes==59|statefipdes==96|statefipdes==97|statefipdes==98	

* Drop migrations originating in Alaska or Hawaii
*qui drop if inlist(statefipor, 2, 15)

*Save
replace year = year-1
compress
preserve
	keep if year<1990
	keep year countyfipor countyfipdes statefipor statefipdes taxexemptions tottaxexemptions taxexemptionssamestate taxexemptionsstayers
	save "${data}/foote_county_outflows_x_state.dta", replace
restore
drop *samestate
save "${data}/foote_county_outflows.dta", replace

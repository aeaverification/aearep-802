clear
set more off

*-------------------------------------------------------------------------------
* Description: Map the county-level migration data into 1990 CZs
* Author: Justin Wiltshire
*
* Updated: October 23, 2019
*-------------------------------------------------------------------------------

*-------------------------------------------------------------------------------
* Preliminaries
*-------------------------------------------------------------------------------

* Define the program to re-map 1990-onward counties into 1990 counties
*	Source: https://www.ddorn.net/data/FIPS_County_Code_Changes.pdf (09/14/19)
*	Notes:	Ignore Alaskan changes or changes with no action necessary to later
*			map counties into 1990 CZs

capture program drop cty_remap
program define cty_remap

	* Confirm cty_fips variable is in memory
	confirm variable cty_fips
	
	* Recode Miami-Dade County, FL
	replace cty_fips = 12025 if cty_fips == 12086

	* Combine Broomfield and Boulder Counties, CO
	replace cty_fips = 8013 if cty_fips == 8014

	* Combine Shannon Oglala Lakota Counties, SD
	replace cty_fips = 46113 if cty_fips == 46102
end


/*
* Download David Dorn's 1990 county-to-CZ crosswalk to new folder
*capture mkdir ${data}/ancillary, public
#delimit ;
	copy 
		https://www.ddorn.net/data/cw_cty_czone.zip 
		${data}/ancillary/cw_cty_czone.zip, replace;
	#delimit cr
cd ${data}/ancillary
unzipfile ${data}/ancillary/cw_cty_czone.zip, replace
qui erase ${data}/ancillary/cw_cty_czone.zip
cd ${pdir}
*/

*-------------------------------------------------------------------------------
* Crosswalk the counties into 1990 CZs
*-------------------------------------------------------------------------------

foreach p in out in {

	*** Loop over inmigration and outmigration files 1983-1999
	use ${data}/foote_county_`p'flows.dta, clear
	
	drop if statefipdestination==. | statefipdestination==0
	drop if statefipor==. | statefipor==0
	
	* Drop if statefiporigin > 57 
	drop if inlist(statefiporigin, 59, 97)
	drop if inlist(statefipdestination, 59, 97)

	* Drop Alaska and Hawaii
	drop if inlist(statefiporigin, 2, 15)
	drop if inlist(statefipdestination, 2, 15)

	* Rename variables of interest
	rename taxexemptions `p'migrants
	rename tottaxexemptions `p'migrants_total
	rename taxexemptionsstayers stayers_from`p'mig

	* Generate destination cty_fips variable, apply changes to map into 1990 CZs
	gen cty_fips_des = countyfipdestination

	* Generate origin cty_fips variable, apply changes to map into 1990 CZs
	gen cty_fips_or = countyfiporigin

	tempfile temp`p'
	save `temp`p''
	
	* County total migration fixed
	if "`p'"=="in" {
		use `temp`p'', clear
		keep year cty_fips_des inmigrants_total
		rename cty_fips_des cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) inmigrants_total, by(year cty_fips)
		tempfile total`p'
		save `total`p''
	}	
	if "`p'"=="out" {
		use `temp`p'', clear
		keep year cty_fips_or outmigrants_total
		rename cty_fips_or cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) outmigrants_total, by(year cty_fips)
		tempfile total`p'
		save `total`p''
	}
	
	* County migration flows
	if "`p'"=="in" {
		use `temp`p'', clear
		keep year cty_fips_des cty_fips_or inmigrants
		rename cty_fips_des cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) inmigrants, by(year cty_fips cty_fips_or)
		tempfile flows`p'
		save `flows`p''
	}	
	if "`p'"=="out" {
		use `temp`p'', clear
		keep year cty_fips_or cty_fips_des outmigrants
		rename cty_fips_or cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) outmigrants, by(year cty_fips cty_fips_des)
		tempfile flows`p'
		save `flows`p''
	}	
	
	* County stayers
	if "`p'"=="in" {
		use `temp`p'', clear
		keep year cty_fips_des stayers
		rename cty_fips_des cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) stayers, by(year cty_fips)
		tempfile stayers`p'
		save `stayers`p''
	}	
	if "`p'"=="out" {
		use `temp`p'', clear
		keep year cty_fips_or stayers
		rename cty_fips_or cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) stayers, by(year cty_fips)
		tempfile stayers`p'
		save `stayers`p''
	}	
	
	* Match the data
	use `total`p'', clear
	merge 1:1 cty_fips year using `stayers`p'', nogen keep(1 3)
	merge 1:m cty_fips year using `flows`p'', nogen keep(1 3)
	
	if "`p'"=="in" {
		bys year cty_fips: gen x = stayers if (cty_fips==8013&cty_fips_or==8014)|(cty_fips == 12025&cty_fips_or == 12086)|(cty_fips == 46113&cty_fips_or == 46102)
		bys year cty_fips: egen x1 = max(x)
		replace x1 = 0 if x1==.
		bys year cty_fips: gen y = inmigrants if (cty_fips==8013&cty_fips_or==8014)|(cty_fips == 12025&cty_fips_or == 12086)|(cty_fips == 46113&cty_fips_or == 46102)
		bys year cty_fips: egen y1 = max(y)
		replace y1 = 0 if y1==.
		drop x y
		replace stayers = stayers+x1
		replace inmigrants_total = inmigrants_total-y1
		drop x1 y1
		drop if (cty_fips==8013&cty_fips_or==8014)|(cty_fips == 12025&cty_fips_or == 12086)|(cty_fips == 46113&cty_fips_or == 46102)
		* Recode Miami-Dade County, FL
		replace cty_fips_or = 12025 if cty_fips_or == 12086
		* Combine Broomfield and Boulder Counties, CO
		replace cty_fips_or = 8013 if cty_fips_or == 8014
		* Combine Shannon Oglala Lakota Counties, SD
		replace cty_fips_or = 46113 if cty_fips_or == 46102
	}
	if "`p'"=="out" {
		bys year cty_fips: gen x = stayers if (cty_fips==8013&cty_fips_des==8014)|(cty_fips == 12025&cty_fips_des == 12086)|(cty_fips == 46113&cty_fips_des == 46102)
		bys year cty_fips: egen x1 = max(x)
		replace x1 = 0 if x1==.
		bys year cty_fips: gen y = outmigrants if (cty_fips==8013&cty_fips_des==8014)|(cty_fips == 12025&cty_fips_des == 12086)|(cty_fips == 46113&cty_fips_des == 46102)
		bys year cty_fips: egen y1 = max(y)
		replace y1 = 0 if y1==.
		drop x y
		replace stayers = stayers+x1
		replace outmigrants_total = outmigrants_total-y1
		drop x1 y1
		drop if (cty_fips==8013&cty_fips_des==8014)|(cty_fips == 12025&cty_fips_des == 12086)|(cty_fips == 46113&cty_fips_des == 46102)
		* Recode Miami-Dade County, FL
		replace cty_fips_des = 12025 if cty_fips_des == 12086
		* Combine Broomfield and Boulder Counties, CO
		replace cty_fips_des = 8013 if cty_fips_des == 8014
		* Combine Shannon Oglala Lakota Counties, SD
		replace cty_fips_des = 46113 if cty_fips_des == 46102
	}	
	tempfile `p'8399
	save ``p'8399'


	*** Loop over inmigration and outmigration files 2001 onwards

	* Load the `p'migration data
	use ${data}/`p'migration_9016.dta, clear

	drop if year==1994&statefipor==.
	drop if year==1994&statefipdes==.
	
	drop if year<2000
	
	* Drop if statefiporigin > 57 
	drop if inlist(statefiporigin, 59, 97)
	drop if inlist(statefipdestination, 59, 97)

	* Drop Alaska and Hawaii
	drop if inlist(statefiporigin, 2, 15)
	drop if inlist(statefipdestination, 2, 15)

	* Rename variables of interest
	rename taxexemptions `p'migrants
	rename tottaxexemptions `p'migrants_total
	rename taxexemptionsstayers stayers_from`p'mig

	* Generate destination cty_fips variable, apply changes to map into 1990 CZs
	gen cty_fips_des = 1000*statefipdestination + countyfipdestination

	* Generate origin cty_fips variable, apply changes to map into 1990 CZs
	gen cty_fips_or = 1000*statefiporigin + countyfiporigin

	tempfile temp`p'
	save `temp`p''
	
	* County total migration fixed
	if "`p'"=="in" {
		use `temp`p'', clear
		keep year cty_fips_des inmigrants_total
		rename cty_fips_des cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) inmigrants_total, by(year cty_fips)
		tempfile total`p'
		save `total`p''
	}	
	if "`p'"=="out" {
		use `temp`p'', clear
		keep year cty_fips_or outmigrants_total
		rename cty_fips_or cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) outmigrants_total, by(year cty_fips)
		tempfile total`p'
		save `total`p''
	}
	
	* County migration flows
	if "`p'"=="in" {
		use `temp`p'', clear
		keep year cty_fips_des cty_fips_or inmigrants
		rename cty_fips_des cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) inmigrants, by(year cty_fips cty_fips_or)
		tempfile flows`p'
		save `flows`p''
	}	
	if "`p'"=="out" {
		use `temp`p'', clear
		keep year cty_fips_or cty_fips_des outmigrants
		rename cty_fips_or cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) outmigrants, by(year cty_fips cty_fips_des)
		tempfile flows`p'
		save `flows`p''
	}	
	
	* County stayers
	if "`p'"=="in" {
		use `temp`p'', clear
		keep year cty_fips_des stayers
		rename cty_fips_des cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) stayers, by(year cty_fips)
		tempfile stayers`p'
		save `stayers`p''
	}	
	if "`p'"=="out" {
		use `temp`p'', clear
		keep year cty_fips_or stayers
		rename cty_fips_or cty_fips 
		duplicates drop 
		cty_remap
		collapse (sum) stayers, by(year cty_fips)
		tempfile stayers`p'
		save `stayers`p''
	}	
	
	* Match the data
	use `total`p'', clear
	merge 1:1 cty_fips year using `stayers`p'', nogen keep(1 3)
	merge 1:m cty_fips year using `flows`p'', nogen keep(1 3)
	
	if "`p'"=="in" {
		bys year cty_fips: gen x = stayers if (cty_fips==8013&cty_fips_or==8014)|(cty_fips == 12025&cty_fips_or == 12086)|(cty_fips == 46113&cty_fips_or == 46102)
		bys year cty_fips: egen x1 = max(x)
		replace x1 = 0 if x1==.
		bys year cty_fips: gen y = inmigrants if (cty_fips==8013&cty_fips_or==8014)|(cty_fips == 12025&cty_fips_or == 12086)|(cty_fips == 46113&cty_fips_or == 46102)
		bys year cty_fips: egen y1 = max(y)
		replace y1 = 0 if y1==.
		drop x y
		replace stayers = stayers+x1
		replace inmigrants_total = inmigrants_total-y1
		drop x1 y1
		drop if (cty_fips==8013&cty_fips_or==8014)|(cty_fips == 12025&cty_fips_or == 12086)|(cty_fips == 46113&cty_fips_or == 46102)
		* Recode Miami-Dade County, FL
		replace cty_fips_or = 12025 if cty_fips_or == 12086
		* Combine Broomfield and Boulder Counties, CO
		replace cty_fips_or = 8013 if cty_fips_or == 8014
		* Combine Shannon Oglala Lakota Counties, SD
		replace cty_fips_or = 46113 if cty_fips_or == 46102
	}
	if "`p'"=="out" {
		bys year cty_fips: gen x = stayers if (cty_fips==8013&cty_fips_des==8014)|(cty_fips == 12025&cty_fips_des == 12086)|(cty_fips == 46113&cty_fips_des == 46102)
		bys year cty_fips: egen x1 = max(x)
		replace x1 = 0 if x1==.
		bys year cty_fips: gen y = outmigrants if (cty_fips==8013&cty_fips_des==8014)|(cty_fips == 12025&cty_fips_des == 12086)|(cty_fips == 46113&cty_fips_des == 46102)
		bys year cty_fips: egen y1 = max(y)
		replace y1 = 0 if y1==.
		drop x y
		replace stayers = stayers+x1
		replace outmigrants_total = outmigrants_total-y1
		drop x1 y1
		drop if (cty_fips==8013&cty_fips_des==8014)|(cty_fips == 12025&cty_fips_des == 12086)|(cty_fips == 46113&cty_fips_des == 46102)
		* Recode Miami-Dade County, FL
		replace cty_fips_des = 12025 if cty_fips_des == 12086
		* Combine Broomfield and Boulder Counties, CO
		replace cty_fips_des = 8013 if cty_fips_des == 8014
		* Combine Shannon Oglala Lakota Counties, SD
		replace cty_fips_des = 46113 if cty_fips_des == 46102
	}
	
	*** Match 83-99 and 00-16 data
	append using ``p'8399'
		
	* CZ level cleaning
	preserve
		use "${data}/ancillary/cw_cty_czone.dta", clear
		rename cty_fips cty_fips_or
		rename czone czone_or
		tempfile cz_cw_or
		save `cz_cw_or'
		
		use "${data}/ancillary/cw_cty_czone.dta", clear
		rename cty_fips cty_fips_des
		rename czone czone_des
		tempfile cz_cw_des
		save `cz_cw_des'
	restore
	
	merge m:1 cty_fips using "${data}/ancillary/cw_cty_czone.dta", 
	tab _m
	drop if _m==2
	drop _m
	if "`p'"=="in" {
		merge m:1 cty_fips_or using `cz_cw_or', 
		drop if _m==2
		drop _m
	}
	if "`p'"=="out" {
		merge m:1 cty_fips_des using `cz_cw_des', 
		drop if _m==2
		drop _m
	}

	
	tempfile tempCZ`p'
	save `tempCZ`p''
	
	* CZ total migration fixed
	if "`p'"=="in" {
		use `tempCZ`p'', clear
		keep year czone inmigrants_total cty_fips
		duplicates drop 
		collapse (sum) inmigrants_total, by(year czone)
		tempfile totalCZ`p'
		save `totalCZ`p''
	}	
	if "`p'"=="out" {
		use `tempCZ`p'', clear
		keep year czone outmigrants_total cty_fips
		duplicates drop 
		collapse (sum) outmigrants_total, by(year czone)
		tempfile totalCZ`p'
		save `totalCZ`p''
	}
	
	* CZ migration flows
	if "`p'"=="in" {
		use `tempCZ`p'', clear
		keep year czone czone_or inmigrants cty_fips cty_fips_or
		duplicates drop 
		collapse (sum) inmigrants, by(year czone czone_or)
		tempfile flowsCZ`p'
		save `flowsCZ`p''
	}	
	if "`p'"=="out" {
		use `tempCZ`p'', clear
		keep year czone czone_des outmigrants cty_fips cty_fips_des
		duplicates drop 
		collapse (sum) outmigrants, by(year czone czone_des)
		tempfile flowsCZ`p'
		save `flowsCZ`p''
	}	
	
	* CZ stayers
	if "`p'"=="in" {
		use `tempCZ`p'', clear
		keep year czone stayers cty_fips
		duplicates drop 
		collapse (sum) stayers, by(year czone)
		tempfile stayersCZ`p'
		save `stayersCZ`p''
	}	
	if "`p'"=="out" {
		use `tempCZ`p'', clear
		keep year czone stayers cty_fips
		duplicates drop 
		collapse (sum) stayers, by(year czone)
		tempfile stayersCZ`p'
		save `stayersCZ`p''
	}	
	
	* Match the data
	use `totalCZ`p'', clear
	merge 1:1 czone year using `stayersCZ`p'', nogen keep(1 3)
	merge 1:m czone year using `flowsCZ`p'', nogen keep(1 3)

	if "`p'"=="in" {
		bys year czone: egen x = total(inmigrants)
		bys year czone: gen y = inmigrants if (czone==czone_or)
		bys year czone: egen y1 = max(y)
		replace y1 = 0 if y1==.
		gen z1 = y1/x
		drop x y
		gen stayers_upbound = stayers_from+y1
		replace stayers_from = stayers_from+(inmigrants_total*z1)
		gen inmigrants_total_upbound = inmigrants_total-y1
		replace inmigrants_total = inmigrants_total*(1-z1)
		drop y1 z1
		drop if czone==czone_or
	}
	if "`p'"=="out" {
		bys year czone: egen x = total(outmigrants)
		bys year czone: gen y = outmigrants if (czone==czone_des)
		bys year czone: egen y1 = max(y)
		replace y1 = 0 if y1==.
		gen z1 = y1/x
		drop x y
		gen stayers_upbound = stayers_from+y1
		replace stayers_from = stayers_from+(outmigrants_total*z1)
		gen outmigrants_total_upbound = outmigrants_total-y1
		replace outmigrants_total = outmigrants_total*(1-z1)

		drop y1 z1
		drop if czone==czone_des
	}
	compress
	* Save the data
	qui save "${data}/cz_`p'migration_8315.dta", replace
}	
	

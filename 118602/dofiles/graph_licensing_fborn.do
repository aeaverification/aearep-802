clear 
set more off
set scheme s2mono


use $data/license_graph.dta, clear // Original raw data not available as privately provided by Joshua Grelewicz at UC Davis (in October 2019)

* Plot figure 8 from from Joshua Grelewicz's data
binscatter foreignborn statecert if N_os > 19, ///
	xtitle("Fraction licensed") ytitle("Fraction foreign-born") ///
	replace ylabel(, nogrid angle(0)) lcol(gs3) mcol(black)
graph export $graphs/fig8_licensing_fborn.png, replace

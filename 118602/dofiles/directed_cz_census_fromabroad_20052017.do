clear 
set more off
set scheme s1color


*******************************************************
* 1. Graph (Figure 6b)
*******************************************************


********************************************************
*** 1. Graph (Figure 6b)
*********************************************************
use ${data}/czone_directed_2005.dta, clear
foreach j of numlist 2006/2017 { 
	append using ${data}/czone_directed_`j'.dta,
}
merge m:1 cz using ${data}/ancillary/CZs_state_region_Xwalk.dta,  keep(matched)
drop if statefip==2|statefip==15

preserve
	collapse (rawsum) totpop = pop totempl = empl [aw=pop], by(cz statefip year)
	tempfile czdata20052017
	save `czdata20052017'
restore
	
collapse (rawsum) pop empl mig1yrabroad [aw=pop], by(cz year)

merge m:1 cz year using `czdata20052017'

gen logtotempl = log(totempl)
gen logpop = log(pop)

drop if statefip==2|statefip==15
xtset cz year

foreach var of varlist totpop logtotempl totempl {
	gen x = `var' if yea==2005
	bys cz: egen `var'2005 = max(x)
	drop x
}
	
gen totemplgrowth = ((totempl-totempl2005)/totempl2005)
xtset 
bys cz (year): gen cumimmigration = sum(mig1yrabroad)
replace cumimmig = cumimmigration/totpop2005

gen logtotempl_z_state = .
gen totemplgrowth_z_state = .
qui levelsof statefip, local(st)
foreach s of local st { 
	qui summ logtotempl if statefip==`s'
	replace logtotempl_z_state = (logtotempl-`r(mean)')/`r(sd)' if statefip==`s'
	qui summ totemplgrowth if statefip==`s'
	replace totemplgrowth_z_state = (totemplgrowth-`r(mean)')/`r(sd)' if statefip==`s'
}

keep if yea==2017

reg cumimmigration totemplgrowth [aw=totpop2005], vce(cluster state)
xtile bin_totemplgrowth = totemplgrowth, nq(10)
xtset
collapse (mean) cumimmigration totemplgrowth (sum) totpop2005, by(bin_totemplgrowth)
tw scatter cumimmigration totemplgrowth [aw=totpop2005], m(Th) mcol(gs9) || ///
	lfit cumimmigration totemplgrowth [aw=totpop2005], lcol(gs12) ///
	ylabel(,angle(0)) ytit("Foreign immigration from abroad 2005-2017") ///
	xtit("Employment growth 2005-2017") ///
	legend(off)
graph export ${graphs}/cz_cumimmigration_emplgrowth_binned_2005_2017REPLICA.png, as(png) replace


clear 
set more off

*******************************************************
* 1. Unzip 1980/2000 files (needed only if files have been zipped, otherwise it doesn't cause error)
* 2. Clean CZ inmigration 1980/2000
* 2b. Clean state inmigration ACS 2000
* 3. Clean state inmigration 1980/2000
* 4. Clean state in-migration 1980/2000 for mobility elasticity (Table A2 and Figures 5 and 6)
* 5. Clean folder re: 1980/2000
* 6. Unzip 2000s files (needed only if files have been zipped, otherwise it doesn't cause error)
* 7. Clean state inmigration 2000s
* 8. Clean state in-migration 2008-2009-2017 for mobility elasticity (Table A2 and Figures 5 and 6)
* 9. Clean CZ inmigration 2000s
* 10. Clean CZ in-migration 2000s for mobility elasticity (data for Figures 6b)
* 11. Clean folder re: 2000s
*******************************************************


*******************************************************
*** 1. Unzip files (needed only if files have been zipped, otherwise it doesn't cause error)
*******************************************************
cd ${data}
foreach j of numlist 1980(10)2000 { 
	unzipfile census_micro_`j'
}

*******************************************************
*** 2. Clean CZs in-migration
*******************************************************
foreach j of numlist 1980(10)2000 { 

	*** Version 25-64
	use ${data}/census_micro_`j'.dta, clear
		
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.

	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migplac5 statefip bpld
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn

	if "`j'"=="1980" {
		recast double cntygp98 migcogr
		replace migplac5 = statefip if migplac5==0 & migsam==2
		replace migplac5 = statefip if migplac5==990 & migsam==2
		replace migplac5 = . if migsam==1
		replace migcogr = cntygp98 if migplac5==statefip & migcogr==999 & migsam==2
		replace migcogr = . if migsam==1
		rename cntygp98 ctygrp1980
		replace ctygrp1980 = statefip*1000+ctygrp1980
		replace migcogr = ctygrp1980 if migcogr==999 & migsam==2
		replace migcogr = ctygrp1980 if migcogr==0 & migsam==2
		replace migcogr = migplac5*1000+migcogr if migsam==2
		gen mig5yrpuma = 2 if (ctygrp1980!=migcogr) & migsam==2
		replace mig5yrpuma = . if ctygrp1980==migcogr & migsam==2
	}	
		
	if "`j'"=="1990" {
		recast double puma migpuma
		replace migplac5 = statefip if migplac5==0
		gen double puma1990 = statefip*10000+puma
		gen double aggregatedpuma1990=puma
		replace aggregatedpuma1990 = statefip*10000+puma
		replace aggregatedpuma1990 = floor(aggregatedpuma1990/100)*100
		gen double migpuma1990 = migpuma
		replace migpuma1990 = aggregatedpuma1990 if migpuma1990==0
		replace migpuma1990 = (migplac5*100+migpuma)*100 if migpuma!=0
		gen double mig5yrpuma = (aggregatedpuma1990!=migpuma1990)
		replace mig5yrpuma = . if migpuma1990==aggregatedpuma1990
		replace migpuma1990 = . if migpuma1990==aggregatedpuma1990
	}

	if "`j'"=="2000" {
		recast double puma migpuma
		replace migplac5 = statefip if migplac5==0
		gen puma2000 = statefip*10000+puma 
		gen migpuma2000 = migplac5*10000+100*migpuma
		merge m:1 puma2000 using ${data}/ancillary/puma2000_migpuma1.dta, gen(m_2)
		drop m_2
		replace migpuma2000 = aggregatedpuma2000 if migpuma==0 & migplac5==statefip
		gen mig5yrpuma = (migpuma2000!=aggregatedpuma2000)
		replace mig5yrpuma = . if migpuma2000==aggregatedpuma2000
		replace migpuma2000 = . if migpuma2000==aggregatedpuma2000
	}	

	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)
		
	if "`j'"=="1980" {
		* Merge with CZ info (obtained by Dorn's website on 1/3/2015,
		* "http://www.ddorn.net/data.htm") and create new weights
		joinby ctygrp1980 using ${data}/ancillary/cw_ctygrp1980_czone_corr.dta, unmatched(both)
	}

	if "`j'"=="1990" {
		* Merge with CZ info (obtained by Dorn's website on 1/3/2015,
		* "http://www.ddorn.net/data.htm") and create new weights
		joinby puma1990 using ${data}/ancillary/cw_puma1990_czone_state_region.dta, unmatched(both)
	}

	if "`j'"=="2000" {
		* Merge with CZ info (obtained by Dorn's website on 1/3/2015,
		* "http://www.ddorn.net/data.htm") and create new weights
		joinby puma2000 using ${data}/ancillary/cw_puma2000_czone_state_region.dta, unmatched(both)
	}
	
	cap drop _m
	replace perwt = perwt*afactor

	if "`j'"=="1980" {	
		gen mig5yrabroad = mig5yrpuma if migplac5>56 & migsam==2
		replace mig5yrpuma = . if migplac5>56 & migsam==2
	}
	else {
		gen mig5yrabroad = mig5yrpuma if migplac5>56 
		replace mig5yrpuma = . if migplac5>56 
	}

	*drop if statename=="Alaska"|statename=="Hawaii"
	cap rename czone cz
	collapse (sum) pop mig5yrpuma mig5yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_collapsed_`j'.dta, replace


	*** Version "full" 0-99+
	use ${data}/census_micro_`j'.dta, clear
		
	drop if gq==0 | gq==3 | gq==4 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.

	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migplac5 statefip bpld
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn

	if "`j'"=="1980" {
		recast double cntygp98 migcogr
		replace migplac5 = statefip if migplac5==0 & migsam==2
		replace migplac5 = statefip if migplac5==990 & migsam==2
		replace migplac5 = . if migsam==1
		replace migcogr = cntygp98 if migplac5==statefip & migcogr==999 & migsam==2
		replace migcogr = . if migsam==1
		rename cntygp98 ctygrp1980
		replace ctygrp1980 = statefip*1000+ctygrp1980
		replace migcogr = ctygrp1980 if migcogr==999 & migsam==2
		replace migcogr = ctygrp1980 if migcogr==0 & migsam==2
		replace migcogr = migplac5*1000+migcogr if migsam==2
		gen mig5yrpuma = 2 if (ctygrp1980!=migcogr) & migsam==2
		replace mig5yrpuma = . if ctygrp1980==migcogr & migsam==2
	}	
		
	if "`j'"=="1990" {
		recast double puma migpuma
		replace migplac5 = statefip if migplac5==0
		gen double puma1990 = statefip*10000+puma
		gen double aggregatedpuma1990=puma
		replace aggregatedpuma1990 = statefip*10000+puma
		replace aggregatedpuma1990 = floor(aggregatedpuma1990/100)*100
		gen double migpuma1990 = migpuma
		replace migpuma1990 = aggregatedpuma1990 if migpuma1990==0
		replace migpuma1990 = (migplac5*100+migpuma)*100 if migpuma!=0
		gen double mig5yrpuma = (aggregatedpuma1990!=migpuma1990)
		replace mig5yrpuma = . if migpuma1990==aggregatedpuma1990
		replace migpuma1990 = . if migpuma1990==aggregatedpuma1990
	}

	if "`j'"=="2000" {
		recast double puma migpuma
		replace migplac5 = statefip if migplac5==0
		gen puma2000 = statefip*10000+puma 
		gen migpuma2000 = migplac5*10000+100*migpuma
		merge m:1 puma2000 using ${data}/ancillary/puma2000_migpuma1.dta, gen(m_2)
		drop m_2
		replace migpuma2000 = aggregatedpuma2000 if migpuma==0 & migplac5==statefip
		gen mig5yrpuma = (migpuma2000!=aggregatedpuma2000)
		replace mig5yrpuma = . if migpuma2000==aggregatedpuma2000
		replace migpuma2000 = . if migpuma2000==aggregatedpuma2000
	}	

	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)
		
	if "`j'"=="1980" {
		* Merge with CZ info (obtained by Dorn's website on 1/3/2015,
		* "http://www.ddorn.net/data.htm") and create new weights
		joinby ctygrp1980 using ${data}/ancillary/cw_ctygrp1980_czone_corr.dta, unmatched(both)
	}

	if "`j'"=="1990" {
		* Merge with CZ info (obtained by Dorn's website on 1/3/2015,
		* "http://www.ddorn.net/data.htm") and create new weights
		joinby puma1990 using ${data}/ancillary/cw_puma1990_czone_state_region.dta, unmatched(both)
	}

	if "`j'"=="2000" {
		* Merge with CZ info (obtained by Dorn's website on 1/3/2015,
		* "http://www.ddorn.net/data.htm") and create new weights
		joinby puma2000 using ${data}/ancillary/cw_puma2000_czone_state_region.dta, unmatched(both)
	}
	
	cap drop _m
	replace perwt = perwt*afactor

	if "`j'"=="1980" {	
		gen mig5yrabroad = mig5yrpuma if migplac5>56 & migsam==2
		replace mig5yrpuma = . if migplac5>56 & migsam==2
	}
	else {
		gen mig5yrabroad = mig5yrpuma if migplac5>56 
		replace mig5yrpuma = . if migplac5>56 
	}

	*drop if statename=="Alaska"|statename=="Hawaii"
	cap rename czone cz
	collapse (sum) pop mig5yrpuma mig5yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_collapsed_`j'_full.dta, replace

}

*******************************************************
*** 2b. Clean state in-migration with ACS 2000
*******************************************************
cd ${data}
unzipfile census_micro_2000_vACS

*** Version 25-64
use ${data}/census_micro_2000_vACS.dta, clear

drop if gq==0 | gq==3 | gq==4 
drop if age<25 | age>64 // comment out for _full version 
drop if school==2 // comment out for _full version
gen lowskilled = educd<=64
replace lowskilled = . if educd<10|educd==.
	
recast double bpld
replace bpld = bpld/100 if bpld<15000
merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) 

recast double migplac1 statefip bpld

replace migplac1 = statefip if migplac1==0
replace migplac1 = statefip if migplac1==990
gen mig1yrstate = (migplac1!=statefip)
replace mig1yrstate = . if migplac1==statefip
	
gen stateborn = (statefip==bpld)	
gen miglifestate = 1-stateborn

gen byte fborn=(bpld>=15000)
replace fborn=0 if bpld==90011 | bpld==90021

gen pop = 1

rename region division
gen region = floor(division/10)

gen mig1yrabroad = mig1yrstate if migplac1>56
replace mig1yrstate = . if migplac1>56

collapse (sum) pop mig1yrstate mig1yrabroad miglifestate [pw=perwt], by(statefip fborn lowskilled year)
compress
saveold ${data}/state_collapsed_2000_vACS.dta, replace


*** Version "full" 0-99+
use ${data}/census_micro_2000_vACS.dta, clear

drop if gq==0 | gq==3 | gq==4 
gen lowskilled = educd<=64
replace lowskilled = . if educd<10|educd==.
	
recast double bpld
replace bpld = bpld/100 if bpld<15000
merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) 

recast double migplac1 statefip bpld

replace migplac1 = statefip if migplac1==0
replace migplac1 = statefip if migplac1==990
gen mig1yrstate = (migplac1!=statefip)
replace mig1yrstate = . if migplac1==statefip
	
gen stateborn = (statefip==bpld)	
gen miglifestate = 1-stateborn

gen byte fborn=(bpld>=15000)
replace fborn=0 if bpld==90011 | bpld==90021

gen pop = 1

rename region division
gen region = floor(division/10)

gen mig1yrabroad = mig1yrstate if migplac1>56
replace mig1yrstate = . if migplac1>56

collapse (sum) pop mig1yrstate mig1yrabroad miglifestate [pw=perwt], by(statefip fborn lowskilled year)
compress
saveold ${data}/state_collapsed_2000_vACS_full.dta, replace


*******************************************************
*** 3. Clean state in-migration
*******************************************************
foreach j of numlist 1980(10)2000 {  

	* Version 25-64
	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	recast double bpld
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) 

	recast double migplac5 statefip bpld
	
	if "`j'"=="1980" {
		replace migplac5 = statefip if migplac5==0 & migsam==2
		replace migplac5 = statefip if migplac5==990 & migsam==2
		gen mig5yrstate = 2 if migplac5!=statefip & migsam==2
		replace mig5yrstate = . if migplac5==statefip & migsam==2
	}
	else {
		replace migplac5 = statefip if migplac5==0
		replace migplac5 = statefip if migplac5==990
		gen mig5yrstate = (migplac5!=statefip)
		replace mig5yrstate = . if migplac5==statefip
	}
		
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)

	if "`j'"=="1980" {
		gen mig5yrabroad = mig5yrstate if migplac5>56 & migsam==2
		replace mig5yrstate = . if migplac5>56 & migsam==2
	}
	else {
		gen mig5yrabroad = mig5yrstate if migplac5>56
		replace mig5yrstate = . if migplac5>56
	}
	
	collapse (sum) pop mig5yrstate mig5yrabroad miglifestate [pw=perwt], by(statefip fborn lowskilled year)
	compress
	saveold ${data}/state_collapsed_`j'.dta, replace

	
	*** Version "full" 0-99+
	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	recast double bpld
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) 

	recast double migplac5 statefip bpld
	
	if "`j'"=="1980" {
		replace migplac5 = statefip if migplac5==0 & migsam==2
		replace migplac5 = statefip if migplac5==990 & migsam==2
		gen mig5yrstate = 2 if migplac5!=statefip & migsam==2
		replace mig5yrstate = . if migplac5==statefip & migsam==2
	}
	else {
		replace migplac5 = statefip if migplac5==0
		replace migplac5 = statefip if migplac5==990
		gen mig5yrstate = (migplac5!=statefip)
		replace mig5yrstate = . if migplac5==statefip
	}
		
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)

	if "`j'"=="1980" {
		gen mig5yrabroad = mig5yrstate if migplac5>56 & migsam==2
		replace mig5yrstate = . if migplac5>56 & migsam==2
	}
	else {
		gen mig5yrabroad = mig5yrstate if migplac5>56
		replace mig5yrstate = . if migplac5>56
	}
	
	collapse (sum) pop mig5yrstate mig5yrabroad miglifestate [pw=perwt], by(statefip fborn lowskilled year)
	compress
	saveold ${data}/state_collapsed_`j'_full.dta, replace

}


*********************************************************************************************
*** 4. Clean state in-migration for mobility elasticity (Table A2 and Figures 5 and 6)
*********************************************************************************************
use ${data}/census_micro_2000_vACS.dta, clear

drop if gq==0 | gq==3 | gq==4 
drop if age<25 | age>64 
drop if school==2 
gen lowskilled = educd<=64
replace lowskilled = 1 if educd<10|educd==.

replace bpld = bpld/100 if bpld<15000
merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen

recast double migplac1 statefip bpld

replace migplac1 = statefip if migplac1==0
gen mig1yrstate = (migplac1!=statefip)
replace mig1yrstate = . if migplac1==statefip
replace migplac1 = . if migplac1==statefip

gen stateborn = (statefip==bpld)	
gen miglifestate = 1-stateborn

gen byte fborn=(bpld>=15000)
replace fborn=0 if bpld==90011 | bpld==90021

gen pop = 1
gen empl = empstat==1

rename region division
gen region = floor(division/10)

gen mig1yrabroad = mig1yrstate if migplac1>56
replace mig1yrstate = . if migplac1>56

replace inctot = . if inctot==9999999
replace inctot = 1.5*-19998 if inctot==-19998&year>2001

replace inctot = inctot*cpi99 
replace inctot =  1.5072*inctot

collapse (sum) pop empl mig1yrabroad mig1yrstate (mean) inctot [pw=perwt], by(statefip fborn lowskilled year)
compress
saveold ${data}/state_directed_2000ACS.dta, replace


foreach j of numlist 1980 2000 {  // 1980(10)2000

	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = 1 if educd<10|educd==.
	
	recast double bpld
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) 

	recast double migplac5 statefip bpld
	
	if "`j'"=="1980" {
		replace migplac5 = statefip if migplac5==0 & migsam==2
		replace migplac5 = statefip if migplac5==990 & migsam==2
		gen mig5yrstate = 2 if migplac5!=statefip & migsam==2
		replace mig5yrstate = . if migplac5==statefip & migsam==2
	}
	else {
		replace migplac5 = statefip if migplac5==0
		replace migplac5 = statefip if migplac5==990
		gen mig5yrstate = (migplac5!=statefip)
		replace mig5yrstate = . if migplac5==statefip
	}
		
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	gen empl = empstat==1
	
	rename region division
	gen region = floor(division/10)

	if "`j'"=="1980" {
		gen mig5yrabroad = mig5yrstate if migplac5>56 & migsam==2
		replace mig5yrstate = . if migplac5>56 & migsam==2
	}
	else {
		gen mig5yrabroad = mig5yrstate if migplac5>56
		replace mig5yrstate = . if migplac5>56
	}
	
	replace inctot = . if inctot==9999999
	replace inctot = 1.5*75000 if inctot==75000&year==1980
	replace inctot = -9990*1.5 if inctot==-009995&year==1980
	replace inctot = -9990*1.5 if inctot==-9990&year==1980
	replace inctot = -20000*1.5 if inctot==-20000&year==2000
	replace inctot = 1.5*999998 if inctot==999998&year==2000
	
	replace inctot = inctot*cpi99 
	replace inctot =  1.5072*inctot
	
	collapse (sum) pop empl mig5yrabroad mig5yrstate (mean) inctot [pw=perwt], by(statefip fborn lowskilled year)
	compress
	saveold ${data}/state_directed_`j'.dta, replace
	
}


*********************************************************
*** 5. Clean folder
*********************************************************
cd ${data}
forvalues j = 1980(10)2000 { 
	cap zipfile census_micro_`j'.dta, saving(census_micro_`j', replace)
	cap !rm census_micro_`j'.dta
}
cap zipfile census_micro_2000_vACS.dta, saving(census_micro_2000_vACS, replace)
cap !rm census_micro_2000_vACS.dta

*******************************************************
*** 6. Unzip files (needed only if files have been zipped, otherwise it doesn't cause error)
*******************************************************
cd ${data}
foreach j of numlist 2001/2017 { 
	unzipfile census_micro_`j'
}

*******************************************************
*** 7. Clean state inmigration 2000s
*******************************************************

foreach j of numlist 2001/2017 { 

	*** Version 25-64
	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migplac1 statefip bpld
	
	replace migplac1 = statefip if migplac1==0
	gen mig1yrstate = (migplac1!=statefip)
	replace mig1yrstate = . if migplac1==statefip
	replace migplac1 = . if migplac1==statefip
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)

	gen mig1yrabroad = mig1yrstate if migplac1>56
	replace mig1yrstate = . if migplac1>56
	
	collapse (sum) pop mig1yrstate mig1yrabroad miglifestate [pw=perwt], by(statefip fborn lowskilled year)
	compress
	saveold ${data}/state_collapsed_`j'.dta, replace


	*** Version "full" 0-99+
	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migplac1 statefip bpld
	
	replace migplac1 = statefip if migplac1==0
	gen mig1yrstate = (migplac1!=statefip)
	replace mig1yrstate = . if migplac1==statefip
	replace migplac1 = . if migplac1==statefip
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)

	gen mig1yrabroad = mig1yrstate if migplac1>56
	replace mig1yrstate = . if migplac1>56
	
	collapse (sum) pop mig1yrstate mig1yrabroad miglifestate [pw=perwt], by(statefip fborn lowskilled year)
	compress
	saveold ${data}/state_collapsed_`j'_full.dta, replace

}	


*******************************************************
*** 8. Clean state in-migration 2008-2009-2017 for mobility elasticity (Table A2 and Figures 5 and 6)
*******************************************************

foreach j of numlist 2008 2009 2017 { 

	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = 1 if educd<10|educd==.
	
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migplac1 statefip bpld
	
	replace migplac1 = statefip if migplac1==0
	gen mig1yrstate = (migplac1!=statefip)
	replace mig1yrstate = . if migplac1==statefip
	replace migplac1 = . if migplac1==statefip
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	gen empl = empstat==1
	
	rename region division
	gen region = floor(division/10)

	gen mig1yrabroad = mig1yrstate if migplac1>56
	replace mig1yrstate = . if migplac1>56
	
	replace inctot = . if inctot==9999999
	replace inctot = 1.5*-19998 if inctot==-19998&year>2001
	
	replace inctot = inctot*cpi99 
	replace inctot =  1.5072*inctot
	
	collapse (sum) pop empl mig1yrabroad mig1yrstate (mean) inctot [pw=perwt], by(statefip fborn lowskilled year)
	compress
	saveold ${data}/state_directed_`j'.dta, replace

}


*******************************************************
*** 9. Clean CZs in-migration
*******************************************************

foreach j of numlist 2005/2011 { 
	
	*** Version 25-64
	use ${data}/census_micro_`j'.dta, clear
		
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	drop if statefip==2|statefip==15
	*drop if statename=="Alaska"|statename=="Hawaii"
	
	gen puma2000 = statefip*10000+puma 
	gen migpuma2000 = migplac1*10000+migpuma1
	
	merge m:1 puma2000 using ${data}/ancillary/puma2000_migpuma1.dta, gen(m_2)
	drop m_2
	
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migpuma1 migplac1 statefip bpld

	replace migpuma2000 = aggregatedpuma2000 if migpuma1==0
	gen mig1yrpuma = (migpuma2000!=aggregatedpuma2000)
	replace mig1yrpuma = . if migpuma2000==aggregatedpuma2000
	replace migpuma2000 = . if migpuma2000==aggregatedpuma2000
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)
	joinby puma2000 using ${data}/ancillary/cw_puma2000_czone_state_region.dta, unmatched(both)
	cap drop _m
	replace perwt = perwt*afactor

	gen mig1yrabroad = mig1yrpuma if migplac1>56
	replace mig1yrpuma = . if migplac1>56 
	
	cap rename czone cz
	collapse (sum) pop mig1yrpuma mig1yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_collapsed_`j'.dta, replace


	*** Version "full" 0-99+
	use ${data}/census_micro_`j'.dta, clear
		
	drop if gq==0 | gq==3 | gq==4 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	drop if statefip==2|statefip==15
	*drop if statename=="Alaska"|statename=="Hawaii"
	
	gen puma2000 = statefip*10000+puma 
	gen migpuma2000 = migplac1*10000+migpuma1
	
	merge m:1 puma2000 using ${data}/ancillary/puma2000_migpuma1.dta, gen(m_2)
	drop m_2
	
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migpuma1 migplac1 statefip bpld

	replace migpuma2000 = aggregatedpuma2000 if migpuma1==0
	gen mig1yrpuma = (migpuma2000!=aggregatedpuma2000)
	replace mig1yrpuma = . if migpuma2000==aggregatedpuma2000
	replace migpuma2000 = . if migpuma2000==aggregatedpuma2000
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)
	joinby puma2000 using ${data}/ancillary/cw_puma2000_czone_state_region.dta, unmatched(both)
	cap drop _m
	replace perwt = perwt*afactor

	gen mig1yrabroad = mig1yrpuma if migplac1>56
	replace mig1yrpuma = . if migplac1>56 
	
	cap rename czone cz
	collapse (sum) pop mig1yrpuma mig1yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_collapsed_`j'_full.dta, replace

}

foreach j of numlist 2012/2017 { 

	*** Version 25-64
	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	drop if statefip==2|statefip==15
	*drop if statename=="Alaska"|statename=="Hawaii"

	gen puma2010 = statefip*100000+puma 
	gen migpuma2010 = migplac1*100000+migpuma1
	
	merge m:1 puma2010 using ${data}/ancillary/puma2010_migpuma1.dta, gen(m_2)
	drop m_2

	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migpuma1 migplac1 statefip bpld

	replace migpuma2010 = aggregatedpuma2010 if migpuma1==0
	gen mig1yrpuma = (migpuma2010!=aggregatedpuma2010)
	replace mig1yrpuma = . if migpuma2010==aggregatedpuma2010
	replace migpuma2010 = . if migpuma2010==aggregatedpuma2010
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)
	joinby puma2010 using ${data}/ancillary/cw_puma2010_czone_state_region.dta, unmatched(both)
	cap drop _m
	replace perwt = perwt*afactor

	gen mig1yrabroad = mig1yrpuma if migplac1>56
	replace mig1yrpuma = . if migplac1>56 
	
	cap rename czone cz
	
	collapse (sum) pop mig1yrpuma mig1yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_collapsed_`j'.dta, replace


	*** Version "full" 0-99+
	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	drop if statefip==2|statefip==15
	*drop if statename=="Alaska"|statename=="Hawaii"

	gen puma2010 = statefip*100000+puma 
	gen migpuma2010 = migplac1*100000+migpuma1
	
	merge m:1 puma2010 using ${data}/ancillary/puma2010_migpuma1.dta, gen(m_2)
	drop m_2

	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migpuma1 migplac1 statefip bpld

	replace migpuma2010 = aggregatedpuma2010 if migpuma1==0
	gen mig1yrpuma = (migpuma2010!=aggregatedpuma2010)
	replace mig1yrpuma = . if migpuma2010==aggregatedpuma2010
	replace migpuma2010 = . if migpuma2010==aggregatedpuma2010
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	
	rename region division
	gen region = floor(division/10)
	joinby puma2010 using ${data}/ancillary/cw_puma2010_czone_state_region.dta, unmatched(both)
	cap drop _m
	replace perwt = perwt*afactor

	gen mig1yrabroad = mig1yrpuma if migplac1>56
	replace mig1yrpuma = . if migplac1>56 
	
	cap rename czone cz
	
	collapse (sum) pop mig1yrpuma mig1yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_collapsed_`j'_full.dta, replace

}

 
**************************************************************************************************
*** 10. Clean CZ in-migration 2000s for mobility elasticity (data for Figures 6b)
**************************************************************************************************
foreach j of numlist 2005/2011 { 
	
	use ${data}/census_micro_`j'.dta, clear
		
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	drop if statefip==2|statefip==15
	*drop if statename=="Alaska"|statename=="Hawaii"
	
	gen puma2000 = statefip*10000+puma 
	gen migpuma2000 = migplac1*10000+migpuma1
	
	merge m:1 puma2000 using ${data}/ancillary/puma2000_migpuma1.dta, gen(m_2)
	drop m_2
	
	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migpuma1 migplac1 statefip bpld

	replace migpuma2000 = aggregatedpuma2000 if migpuma1==0
	gen mig1yrpuma = (migpuma2000!=aggregatedpuma2000)
	replace mig1yrpuma = . if migpuma2000==aggregatedpuma2000
	replace migpuma2000 = . if migpuma2000==aggregatedpuma2000
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	gen empl = empstat==1
	
	rename region division
	gen region = floor(division/10)
	joinby puma2000 using ${data}/ancillary/cw_puma2000_czone_state_region.dta, unmatched(both)
	cap drop _m
	replace perwt = perwt*afactor

	gen mig1yrabroad = mig1yrpuma if migplac1>56
	replace mig1yrpuma = . if migplac1>56 
	
	cap rename czone cz
	collapse (sum) pop empl mig1yrpuma mig1yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_directed_`j'.dta, replace

}

foreach j of numlist 2012/2017 { 

	use ${data}/census_micro_`j'.dta, clear
	
	drop if gq==0 | gq==3 | gq==4 
	drop if age<25 | age>64 
	drop if school==2 
	gen lowskilled = educd<=64
	replace lowskilled = . if educd<10|educd==.
	
	drop if statefip==2|statefip==15
	*drop if statename=="Alaska"|statename=="Hawaii"

	gen puma2010 = statefip*100000+puma 
	gen migpuma2010 = migplac1*100000+migpuma1
	
	merge m:1 puma2010 using ${data}/ancillary/puma2010_migpuma1.dta, gen(m_2)
	drop m_2

	replace bpld = bpld/100 if bpld<15000
	merge m:1 bpld using ${data}/ancillary/state_region_birth_Xwalk.dta, keepusing(region* division*) nogen
	
	recast double migpuma1 migplac1 statefip bpld

	replace migpuma2010 = aggregatedpuma2010 if migpuma1==0
	gen mig1yrpuma = (migpuma2010!=aggregatedpuma2010)
	replace mig1yrpuma = . if migpuma2010==aggregatedpuma2010
	replace migpuma2010 = . if migpuma2010==aggregatedpuma2010
	
	gen stateborn = (statefip==bpld)	
	gen miglifestate = 1-stateborn
	
	gen byte fborn=(bpld>=15000)
	replace fborn=0 if bpld==90011 | bpld==90021
	
	gen pop = 1
	gen empl = empstat==1
	
	rename region division
	gen region = floor(division/10)
	joinby puma2010 using ${data}/ancillary/cw_puma2010_czone_state_region.dta, unmatched(both)
	cap drop _m
	replace perwt = perwt*afactor

	gen mig1yrabroad = mig1yrpuma if migplac1>56
	replace mig1yrpuma = . if migplac1>56 
	
	cap rename czone cz
	collapse (sum) pop empl mig1yrpuma mig1yrabroad miglifestate [pw=perwt], by(cz fborn lowskilled year)
	compress
	saveold ${data}/czone_directed_`j'.dta, replace

}

*************************************************
*** 11. Clean folder
*************************************************
cd ${data}
foreach j of numlist 2001/2017 { 
	cap zipfile census_micro_`j'.dta, saving(census_micro_`j', replace)
	cap !rm census_micro_`j'.dta
}









* Census decennial data 1980
clear
quietly infix                 ///
  int     year       1-4      ///
  long    sample     5-10     ///
  double  serial     11-18    ///
  double  hhwt       19-28    ///
  double  cpi99      29-33    ///
  byte    region     34-35    ///
  byte    statefip   36-37    ///
  int     countyfip  38-40    ///
  int     metarea    41-43    ///
  int     metaread   44-47    ///
  int     cntygp98   48-50    ///
  int     conspuma   51-53    ///
  int     cntry      54-56    ///
  byte    gq         57-57    ///
  byte    ownershp   58-58    ///
  byte    ownershpd  59-60    ///
  byte    mortgage   61-61    ///
  byte    mortgag2   62-62    ///
  long    owncost    63-67    ///
  int     rent       68-71    ///
  int     rentgrs    72-75    ///
  int     costelec   76-79    ///
  int     costgas    80-83    ///
  int     costwatr   84-87    ///
  int     costfuel   88-91    ///
  long    hhincome   92-98    ///
  long    valueh     99-105   ///
  byte    rooms      106-107  ///
  byte    bedrooms   108-109  ///
  int     pernum     110-113  ///
  double  perwt      114-123  ///
  byte    famsize    124-125  ///
  byte    nchild     126-126  ///
  byte    nchlt5     127-127  ///
  byte    eldch      128-129  ///
  byte    yngch      130-131  ///
  byte    relate     132-133  ///
  int     related    134-137  ///
  byte    sex        138-138  ///
  int     age        139-141  ///
  int     birthyr    142-145  ///
  byte    race       146-146  ///
  int     raced      147-149  ///
  byte    hispan     150-150  ///
  int     hispand    151-153  ///
  int     bpl        154-156  ///
  long    bpld       157-161  ///
  int     ancestr1   162-164  ///
  int     ancestr1d  165-168  ///
  int     ancestr2   169-171  ///
  int     ancestr2d  172-175  ///
  byte    citizen    176-176  ///
  int     yrimmig    177-180  ///
  byte    yrsusa2    181-181  ///
  byte    language   182-183  ///
  int     languaged  184-187  ///
  byte    speakeng   188-188  ///
  byte    school     189-189  ///
  byte    educ       190-191  ///
  int     educd      192-194  ///
  byte    empstat    195-195  ///
  byte    empstatd   196-197  ///
  byte    labforce   198-198  ///
  int     occ        199-202  ///
  int     occ1990    203-205  ///
  int     ind        206-209  ///
  int     ind1990    210-212  ///
  byte    classwkr   213-213  ///
  byte    classwkrd  214-215  ///
  byte    wkswork1   216-217  ///
  byte    wkswork2   218-218  ///
  byte    uhrswork   219-220  ///
  byte    looking    221-221  ///
  byte    availble   222-222  ///
  long    inctot     223-229  ///
  long    ftotinc    230-236  ///
  long    incwage    237-242  ///
  long    incss      243-247  ///
  long    incwelfr   248-252  ///
  long    incinvst   253-258  ///
  long    incother   259-263  ///
  int     poverty    264-266  ///
  byte    migrate5   267-267  ///
  byte    migrate5d  268-269  ///
  int     migplac5   270-272  ///
  int     migcogrp   273-275  ///
  byte    movedin    276-276  ///
  byte    migsamp    277-277  ///
  int     trantime   278-280  ///
  using usa_00181.dat

replace hhwt      = hhwt      / 100
replace cpi99     = cpi99     / 1000
replace perwt     = perwt     / 100

format serial    %8.0g
format hhwt      %10.2f
format cpi99     %5.3f
format perwt     %10.2f

run ${pdir}/JEPmobility/dofiles/labelvar_census.do

compress
qui summ year
saveold ${pdir}/JEPmobility/data/census_micro_`r(mean)'.dta, replace



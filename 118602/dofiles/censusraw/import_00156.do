
* Census ACS data 2013
clear
quietly infix                  ///
  int     year        1-4      ///
  long    sample      5-10     ///
  double  serial      11-18    ///
  double  cbserial    19-31    ///
  double  hhwt        32-41    ///
  double  cpi99       42-46    ///
  byte    region      47-48    ///
  byte    statefip    49-50    ///
  int     countyfip   51-53    ///
  byte    metro       54-54    ///
  long    puma        55-59    ///
  int     cpuma0010   60-63    ///
  int     cntry       64-66    ///
  byte    gq          67-67    ///
  byte    ownershp    68-68    ///
  byte    ownershpd   69-70    ///
  byte    mortgage    71-71    ///
  byte    mortgag2    72-72    ///
  long    owncost     73-77    ///
  int     rent        78-81    ///
  int     rentgrs     82-85    ///
  byte    rentmeal    86-86    ///
  int     costelec    87-90    ///
  int     costgas     91-94    ///
  int     costwatr    95-98    ///
  int     costfuel    99-102   ///
  long    hhincome    103-109  ///
  byte    foodstmp    110-110  ///
  long    valueh      111-117  ///
  byte    rooms       118-119  ///
  byte    builtyr2    120-121  ///
  byte    bedrooms    122-123  ///
  int     pernum      124-127  ///
  double  perwt       128-137  ///
  byte    famsize     138-139  ///
  byte    nchild      140-140  ///
  byte    nchlt5      141-141  ///
  byte    eldch       142-143  ///
  byte    yngch       144-145  ///
  byte    relate      146-147  ///
  int     related     148-151  ///
  byte    sex         152-152  ///
  int     age         153-155  ///
  int     birthyr     156-159  ///
  byte    fertyr      160-160  ///
  byte    race        161-161  ///
  int     raced       162-164  ///
  byte    hispan      165-165  ///
  int     hispand     166-168  ///
  int     bpl         169-171  ///
  long    bpld        172-176  ///
  int     ancestr1    177-179  ///
  int     ancestr1d   180-183  ///
  int     ancestr2    184-186  ///
  int     ancestr2d   187-190  ///
  byte    citizen     191-191  ///
  int     yrimmig     192-195  ///
  byte    yrsusa1     196-197  ///
  byte    yrsusa2     198-198  ///
  byte    language    199-200  ///
  int     languaged   201-204  ///
  byte    speakeng    205-205  ///
  byte    school      206-206  ///
  byte    educ        207-208  ///
  int     educd       209-211  ///
  byte    degfield    212-213  ///
  int     degfieldd   214-217  ///
  byte    empstat     218-218  ///
  byte    empstatd    219-220  ///
  byte    labforce    221-221  ///
  int     occ         222-225  ///
  int     occ1990     226-228  ///
  int     ind         229-232  ///
  int     ind1990     233-235  ///
  byte    classwkr    236-236  ///
  byte    classwkrd   237-238  ///
  str     occsoc      239-244  ///
  byte    wkswork2    245-245  ///
  byte    uhrswork    246-247  ///
  byte    wrklstwk    248-248  ///
  byte    looking     249-249  ///
  byte    availble    250-250  ///
  long    inctot      251-257  ///
  long    ftotinc     258-264  ///
  long    incwage     265-270  ///
  long    incbus00    271-276  ///
  long    incss       277-281  ///
  long    incwelfr    282-286  ///
  long    incinvst    287-292  ///
  long    incretir    293-298  ///
  long    incsupp     299-303  ///
  long    incother    304-308  ///
  long    incearn     309-315  ///
  int     poverty     316-318  ///
  byte    migrate1    319-319  ///
  byte    migrate1d   320-321  ///
  int     migplac1    322-324  ///
  int     migcounty1  325-327  ///
  long    migpuma1    328-332  ///
  byte    movedin     333-333  ///
  int     trantime    334-336  ///
  using usa_00156.dat

replace hhwt      = hhwt      / 100
replace cpi99     = cpi99     / 1000
replace perwt     = perwt     / 100

format serial    %8.0g
format hhwt      %10.2f
format cpi99     %5.3f
format perwt     %10.2f

run ${pdir}/JEPmobility/dofiles/labelvar_census.do

compress
qui summ year
saveold ${pdir}/JEPmobility/data/census_micro_`r(mean)'.dta, replace




* Census decennial data 1990
clear
quietly infix                 ///
  int     year       1-4      ///
  long    sample     5-10     ///
  double  serial     11-18    ///
  double  hhwt       19-28    ///
  double  cpi99      29-33    ///
  byte    region     34-35    ///
  byte    statefip   36-37    ///
  int     countyfip  38-40    ///
  int     metarea    41-43    ///
  int     metaread   44-47    ///
  long    puma       48-52    ///
  int     conspuma   53-55    ///
  int     cntry      56-58    ///
  byte    gq         59-59    ///
  byte    ownershp   60-60    ///
  byte    ownershpd  61-62    ///
  byte    mortgage   63-63    ///
  byte    mortgag2   64-64    ///
  long    owncost    65-69    ///
  int     rent       70-73    ///
  int     rentgrs    74-77    ///
  byte    rentmeal   78-78    ///
  int     costelec   79-82    ///
  int     costgas    83-86    ///
  int     costwatr   87-90    ///
  int     costfuel   91-94    ///
  long    hhincome   95-101   ///
  long    valueh     102-108  ///
  byte    rooms      109-110  ///
  byte    bedrooms   111-112  ///
  int     pernum     113-116  ///
  double  perwt      117-126  ///
  byte    famsize    127-128  ///
  byte    nchild     129-129  ///
  byte    nchlt5     130-130  ///
  byte    eldch      131-132  ///
  byte    yngch      133-134  ///
  byte    relate     135-136  ///
  int     related    137-140  ///
  byte    sex        141-141  ///
  int     age        142-144  ///
  int     birthyr    145-148  ///
  byte    race       149-149  ///
  int     raced      150-152  ///
  byte    hispan     153-153  ///
  int     hispand    154-156  ///
  int     bpl        157-159  ///
  long    bpld       160-164  ///
  int     ancestr1   165-167  ///
  int     ancestr1d  168-171  ///
  int     ancestr2   172-174  ///
  int     ancestr2d  175-178  ///
  byte    citizen    179-179  ///
  int     yrimmig    180-183  ///
  byte    yrsusa2    184-184  ///
  byte    language   185-186  ///
  int     languaged  187-190  ///
  byte    speakeng   191-191  ///
  byte    school     192-192  ///
  byte    educ       193-194  ///
  int     educd      195-197  ///
  byte    empstat    198-198  ///
  byte    empstatd   199-200  ///
  byte    labforce   201-201  ///
  int     occ        202-205  ///
  int     occ1990    206-208  ///
  int     ind        209-212  ///
  int     ind1990    213-215  ///
  byte    classwkr   216-216  ///
  byte    classwkrd  217-218  ///
  byte    wkswork1   219-220  ///
  byte    wkswork2   221-221  ///
  byte    uhrswork   222-223  ///
  byte    looking    224-224  ///
  byte    availble   225-225  ///
  long    inctot     226-232  ///
  long    ftotinc    233-239  ///
  long    incwage    240-245  ///
  long    incss      246-250  ///
  long    incwelfr   251-255  ///
  long    incinvst   256-261  ///
  long    incretir   262-267  ///
  long    incother   268-272  ///
  long    incearn    273-279  ///
  int     poverty    280-282  ///
  byte    migrate5   283-283  ///
  byte    migrate5d  284-285  ///
  int     migplac5   286-288  ///
  int     migpuma    289-291  ///
  byte    movedin    292-292  ///
  int     trantime   293-295  ///
  using usa_00167.dat

replace hhwt      = hhwt      / 100
replace cpi99     = cpi99     / 1000
replace perwt     = perwt     / 100

format serial    %8.0g
format hhwt      %10.2f
format cpi99     %5.3f
format perwt     %10.2f

run ${pdir}/JEPmobility/dofiles/labelvar_census.do

compress
qui summ year
saveold ${pdir}/JEPmobility/data/census_micro_`r(mean)'.dta, replace



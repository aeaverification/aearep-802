
* Census ACS data 2002
clear
quietly infix                 ///
  int     year       1-4      ///
  long    sample     5-10     ///
  double  serial     11-18    ///
  double  hhwt       19-28    ///
  double  cpi99      29-33    ///
  byte    region     34-35    ///
  byte    statefip   36-37    ///
  int     cntry      38-40    ///
  byte    gq         41-41    ///
  byte    ownershp   42-42    ///
  byte    ownershpd  43-44    ///
  byte    mortgage   45-45    ///
  byte    mortgag2   46-46    ///
  long    owncost    47-51    ///
  int     rent       52-55    ///
  int     rentgrs    56-59    ///
  byte    rentmeal   60-60    ///
  int     costelec   61-64    ///
  int     costgas    65-68    ///
  int     costwatr   69-72    ///
  int     costfuel   73-76    ///
  long    hhincome   77-83    ///
  byte    foodstmp   84-84    ///
  long    valueh     85-91    ///
  byte    rooms      92-93    ///
  byte    builtyr2   94-95    ///
  byte    bedrooms   96-97    ///
  int     pernum     98-101   ///
  double  perwt      102-111  ///
  byte    famsize    112-113  ///
  byte    nchild     114-114  ///
  byte    nchlt5     115-115  ///
  byte    eldch      116-117  ///
  byte    yngch      118-119  ///
  byte    relate     120-121  ///
  int     related    122-125  ///
  byte    sex        126-126  ///
  int     age        127-129  ///
  int     birthyr    130-133  ///
  byte    fertyr     134-134  ///
  byte    race       135-135  ///
  int     raced      136-138  ///
  byte    hispan     139-139  ///
  int     hispand    140-142  ///
  int     bpl        143-145  ///
  long    bpld       146-150  ///
  int     ancestr1   151-153  ///
  int     ancestr1d  154-157  ///
  int     ancestr2   158-160  ///
  int     ancestr2d  161-164  ///
  byte    citizen    165-165  ///
  int     yrimmig    166-169  ///
  byte    yrsusa1    170-171  ///
  byte    yrsusa2    172-172  ///
  byte    language   173-174  ///
  int     languaged  175-178  ///
  byte    speakeng   179-179  ///
  byte    school     180-180  ///
  byte    educ       181-182  ///
  int     educd      183-185  ///
  byte    empstat    186-186  ///
  byte    empstatd   187-188  ///
  byte    labforce   189-189  ///
  int     occ        190-193  ///
  int     occ1990    194-196  ///
  int     ind        197-200  ///
  int     ind1990    201-203  ///
  byte    classwkr   204-204  ///
  byte    classwkrd  205-206  ///
  str     occsoc     207-212  ///
  byte    wkswork2   213-213  ///
  byte    uhrswork   214-215  ///
  byte    wrklstwk   216-216  ///
  byte    looking    217-217  ///
  byte    availble   218-218  ///
  long    inctot     219-225  ///
  long    ftotinc    226-232  ///
  long    incwage    233-238  ///
  long    incbus00   239-244  ///
  long    incss      245-249  ///
  long    incwelfr   250-254  ///
  long    incinvst   255-260  ///
  long    incretir   261-266  ///
  long    incsupp    267-271  ///
  long    incother   272-276  ///
  long    incearn    277-283  ///
  int     poverty    284-286  ///
  byte    migrate1   287-287  ///
  byte    migrate1d  288-289  ///
  int     migplac1   290-292  ///
  byte    movedin    293-293  ///
  int     trantime   294-296  ///
  using usa_00178.dat

replace hhwt      = hhwt      / 100
replace cpi99     = cpi99     / 1000
replace perwt     = perwt     / 100

format serial    %8.0g
format hhwt      %10.2f
format cpi99     %5.3f
format perwt     %10.2f

run ${pdir}/JEPmobility/dofiles/labelvar_census.do

compress
qui summ year
saveold ${pdir}/JEPmobility/data/census_micro_`r(mean)'.dta, replace



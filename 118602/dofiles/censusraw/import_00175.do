
* Census ACS data 2005
clear
quietly infix                  ///
  int     year        1-4      ///
  long    sample      5-10     ///
  double  serial      11-18    ///
  double  cbserial    19-31    ///
  double  hhwt        32-41    ///
  double  cpi99       42-46    ///
  byte    region      47-48    ///
  byte    statefip    49-50    ///
  int     countyfip   51-53    ///
  byte    metro       54-54    ///
  long    puma        55-59    ///
  int     cpuma0010   60-63    ///
  int     cntry       64-66    ///
  byte    gq          67-67    ///
  byte    ownershp    68-68    ///
  byte    ownershpd   69-70    ///
  byte    mortgage    71-71    ///
  byte    mortgag2    72-72    ///
  long    owncost     73-77    ///
  int     rent        78-81    ///
  int     rentgrs     82-85    ///
  byte    rentmeal    86-86    ///
  int     costelec    87-90    ///
  int     costgas     91-94    ///
  int     costwatr    95-98    ///
  int     costfuel    99-102   ///
  long    hhincome    103-109  ///
  byte    foodstmp    110-110  ///
  long    valueh      111-117  ///
  byte    rooms       118-119  ///
  byte    builtyr2    120-121  ///
  byte    bedrooms    122-123  ///
  int     pernum      124-127  ///
  double  perwt       128-137  ///
  byte    famsize     138-139  ///
  byte    nchild      140-140  ///
  byte    nchlt5      141-141  ///
  byte    eldch       142-143  ///
  byte    yngch       144-145  ///
  byte    relate      146-147  ///
  int     related     148-151  ///
  byte    sex         152-152  ///
  int     age         153-155  ///
  int     birthyr     156-159  ///
  byte    fertyr      160-160  ///
  byte    race        161-161  ///
  int     raced       162-164  ///
  byte    hispan      165-165  ///
  int     hispand     166-168  ///
  int     bpl         169-171  ///
  long    bpld        172-176  ///
  int     ancestr1    177-179  ///
  int     ancestr1d   180-183  ///
  int     ancestr2    184-186  ///
  int     ancestr2d   187-190  ///
  byte    citizen     191-191  ///
  int     yrimmig     192-195  ///
  byte    yrsusa1     196-197  ///
  byte    yrsusa2     198-198  ///
  byte    language    199-200  ///
  int     languaged   201-204  ///
  byte    speakeng    205-205  ///
  byte    school      206-206  ///
  byte    educ        207-208  ///
  int     educd       209-211  ///
  byte    empstat     212-212  ///
  byte    empstatd    213-214  ///
  byte    labforce    215-215  ///
  int     occ         216-219  ///
  int     occ1990     220-222  ///
  int     ind         223-226  ///
  int     ind1990     227-229  ///
  byte    classwkr    230-230  ///
  byte    classwkrd   231-232  ///
  str     occsoc      233-238  ///
  byte    wkswork2    239-239  ///
  byte    uhrswork    240-241  ///
  byte    wrklstwk    242-242  ///
  byte    looking     243-243  ///
  byte    availble    244-244  ///
  long    inctot      245-251  ///
  long    ftotinc     252-258  ///
  long    incwage     259-264  ///
  long    incbus00    265-270  ///
  long    incss       271-275  ///
  long    incwelfr    276-280  ///
  long    incinvst    281-286  ///
  long    incretir    287-292  ///
  long    incsupp     293-297  ///
  long    incother    298-302  ///
  long    incearn     303-309  ///
  int     poverty     310-312  ///
  byte    migrate1    313-313  ///
  byte    migrate1d   314-315  ///
  int     migplac1    316-318  ///
  int     migcounty1  319-321  ///
  long    migpuma1    322-326  ///
  byte    movedin     327-327  ///
  int     trantime    328-330  ///
  using usa_00175.dat

replace hhwt      = hhwt      / 100
replace cpi99     = cpi99     / 1000
replace perwt     = perwt     / 100

format serial    %8.0g
format hhwt      %10.2f
format cpi99     %5.3f
format perwt     %10.2f

run ${pdir}/JEPmobility/dofiles/labelvar_census.do

compress
qui summ year
saveold ${pdir}/JEPmobility/data/census_micro_`r(mean)'.dta, replace



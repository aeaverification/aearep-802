
* Census decennial data 2000
clear
quietly infix                 ///
  int     year       1-4      ///
  long    sample     5-10     ///
  double  serial     11-18    ///
  double  hhwt       19-28    ///
  double  cpi99      29-33    ///
  byte    region     34-35    ///
  byte    statefip   36-37    ///
  int     countyfip  38-40    ///
  byte    metro      41-41    ///
  int     metarea    42-44    ///
  int     metaread   45-48    ///
  long    puma       49-53    ///
  long    pumasupr   54-58    ///
  int     conspuma   59-61    ///
  int     cpuma0010  62-65    ///
  int     cntry      66-68    ///
  byte    gq         69-69    ///
  byte    ownershp   70-70    ///
  byte    ownershpd  71-72    ///
  byte    mortgage   73-73    ///
  byte    mortgag2   74-74    ///
  long    owncost    75-79    ///
  int     rent       80-83    ///
  int     rentgrs    84-87    ///
  byte    rentmeal   88-88    ///
  int     costelec   89-92    ///
  int     costgas    93-96    ///
  int     costwatr   97-100   ///
  int     costfuel   101-104  ///
  long    hhincome   105-111  ///
  long    valueh     112-118  ///
  byte    rooms      119-120  ///
  byte    builtyr2   121-122  ///
  byte    bedrooms   123-124  ///
  int     pernum     125-128  ///
  double  perwt      129-138  ///
  byte    famsize    139-140  ///
  byte    nchild     141-141  ///
  byte    nchlt5     142-142  ///
  byte    eldch      143-144  ///
  byte    yngch      145-146  ///
  byte    relate     147-148  ///
  int     related    149-152  ///
  byte    sex        153-153  ///
  int     age        154-156  ///
  int     birthyr    157-160  ///
  byte    race       161-161  ///
  int     raced      162-164  ///
  byte    hispan     165-165  ///
  int     hispand    166-168  ///
  int     bpl        169-171  ///
  long    bpld       172-176  ///
  int     ancestr1   177-179  ///
  int     ancestr1d  180-183  ///
  int     ancestr2   184-186  ///
  int     ancestr2d  187-190  ///
  byte    citizen    191-191  ///
  int     yrimmig    192-195  ///
  byte    yrsusa1    196-197  ///
  byte    yrsusa2    198-198  ///
  byte    language   199-200  ///
  int     languaged  201-204  ///
  byte    speakeng   205-205  ///
  byte    school     206-206  ///
  byte    educ       207-208  ///
  int     educd      209-211  ///
  byte    empstat    212-212  ///
  byte    empstatd   213-214  ///
  byte    labforce   215-215  ///
  int     occ        216-219  ///
  int     occ1990    220-222  ///
  int     ind        223-226  ///
  int     ind1990    227-229  ///
  byte    classwkr   230-230  ///
  byte    classwkrd  231-232  ///
  str     occsoc     233-238  ///
  byte    wkswork1   239-240  ///
  byte    wkswork2   241-241  ///
  byte    uhrswork   242-243  ///
  byte    looking    244-244  ///
  byte    availble   245-245  ///
  long    inctot     246-252  ///
  long    ftotinc    253-259  ///
  long    incwage    260-265  ///
  long    incbus00   266-271  ///
  long    incss      272-276  ///
  long    incwelfr   277-281  ///
  long    incinvst   282-287  ///
  long    incretir   288-293  ///
  long    incsupp    294-298  ///
  long    incother   299-303  ///
  long    incearn    304-310  ///
  int     poverty    311-313  ///
  byte    migrate5   314-314  ///
  byte    migrate5d  315-316  ///
  int     migplac5   317-319  ///
  int     migpuma    320-322  ///
  int     migpumas   323-325  ///
  byte    movedin    326-326  ///
  int     trantime   327-329  ///
  using usa_00166.dat

replace hhwt      = hhwt      / 100
replace cpi99     = cpi99     / 1000
replace perwt     = perwt     / 100

format serial    %8.0g
format hhwt      %10.2f
format cpi99     %5.3f
format perwt     %10.2f

run ${pdir}/JEPmobility/dofiles/labelvar_census.do

compress
qui summ year
saveold ${pdir}/JEPmobility/data/census_micro_`r(mean)'.dta, replace



clear 
set more off
set scheme s1color


*******************************************************
* 1. Append data and employment-pop summary statistics
* 2. Figure 5a. 1980-2000
* 3. Figure 5b. 2000-2017
*******************************************************

********************************************************************************
*** 1. Append data and employment-pop summary statistics
********************************************************************************
use ${data}/state_directed_1980.dta, clear
foreach j of numlist 2000 2008 2009 2017 { 
	append using ${data}/state_directed_`j'.dta,
}

rename inctot inc

tempfile alldata
save `alldata'

preserve
	collapse (rawsum) totpop = pop totempl = empl, by(year)
	format tot* %16.0f
	drop if year==2008|year==2009
	sort year
	gen t=_n
	tsset t,
	gen empl_growth=d.totempl/l.totempl
	gen pop_growth=d.totpop/l.totpop
	gen empltopop_ratio=totempl/totpop
	list 
restore

********************************************************************************
*** 2. Figure 5a. 1980-2000
********************************************************************************
use `alldata', clear
keep if year==1980|year==2000
preserve
	collapse (rawsum) totpop = pop totempl = empl (mean) totinc=inc [aw=pop], by(statefip year)
	tempfile statedata19802000
	save `statedata19802000'
restore
	
collapse (rawsum) pop empl (mean) inc [aw=pop], by(fborn statefip year)

merge m:1 statefip year using `statedata19802000'

gen logtotempl = log(totempl)
gen logpop = log(pop)

gen time = 1 if year == 1980
replace time = 2 if year == 2000
egen id = group(state fborn)
xtset id time
	
	
gen ownpopgrowth = (d.pop/l.pop)/(year-1980)
gen popgrowth = (d.pop/l.totpop)/(year-1980)
gen totpopgrowth = (d.totpop/l.totpop)/(year-1980)
gen totemplgrowth = (d.totempl/l.totempl)/(year-1980)
gen x=pop if fborn == 1
bys state year: egen fbornpop = max(x)
gen fbornshare = fbornpop/totpop

xtset
reg l.fbornshare totemplgrowth if fborn==0 [aw=l.totpop], 

xtile bin_totemplgrowth = totemplgrowth, nq(10)
xtset
collapse (mean) popgr totpopgr totemplgrowth lagfbornshare = l.fbornshare (sum) weight = l.totpop, by(fborn bin_totemplgrowth)
tw scatter popgr totemplgrowth if fborn==0 [aw=weight], m(Oh) mcol(black) || ///
	lfit popgr totemplgrowth if fborn==0 [aw=weight], lcol(black) || ///
	scatter popgr totemplgrowth if fborn==1 [aw=weight], m(Th) mcol(gs12) || ///
	lfit popgr totemplgrowth if fborn==1 [aw=weight], lcol(gs12) || ///
	lfit totpopgr totemplgrowth if fborn==0 [aw=weight], lcol(black) lp(dash) ///
	ylabel(,angle(0)) ytit("Population growth 1980-2000") xtit("Employment growth 1980-2000") ///
	legend(label(1 "Natives") label(3 "Foreign born") label(5 "Total") order(1 3 5) row(1))
graph export ${graphs}/state_directed_binned_emplgrowth_1980_2000.png, as(png) replace



********************************************************************************
*** 3. Figure 5b. 2000-2017
********************************************************************************
use `alldata', clear
keep if year==2017|year==2000
preserve
	collapse (rawsum) totpop = pop totempl = empl (mean) totinc=inc [aw=pop], by(statefip year)
	tempfile statedata20002017
	save `statedata20002017'
restore
	
collapse (rawsum) pop empl (mean) inc [aw=pop], by(fborn statefip year)

merge m:1 statefip year using `statedata20002017'

gen logtotempl = log(totempl)
gen logpop = log(pop)

gen time = 1 if year == 2000
replace time = 2 if year == 2017
egen id = group(state fborn)
xtset id time
	

gen ownpopgrowth = (d.pop/l.pop)/(year-2000)
gen popgrowth = (d.pop/l.totpop)/(year-2000)
gen totpopgrowth = (d.totpop/l.totpop)/(year-2000)
gen totemplgrowth = (d.totempl/l.totempl)/(year-2000)
gen x=pop if fborn == 1
bys state year: egen fbornpop = max(x)
gen fbornshare = fbornpop/totpop

gen logtotempl_z_state = .
gen totemplgrowth_z_state = .

xtile bin_totemplgrowth = totemplgrowth, nq(10)
xtset
collapse (mean) popgr totpopgr totemplgrowth lagfbornshare = l.fbornshare (sum) weight = l.totpop, by(fborn bin_totemplgrowth)
tw scatter popgr totemplgrowth if fborn==0 [aw=weight], m(Oh) mcol(black) || ///
	lfit popgr totemplgrowth if fborn==0 [aw=weight], lcol(black) || ///
	scatter popgr totemplgrowth if fborn==1 [aw=weight], m(Th) mcol(gs12) || ///
	lfit popgr totemplgrowth if fborn==1 [aw=weight], lcol(gs12) || ///
	lfit totpopgr totemplgrowth if fborn==0 [aw=weight], lcol(black) lp(dash) ///
	ylabel(,angle(0)) ytit("Population growth 2000-2017") xtit("Employment growth 2000-2017") ///
	legend(label(1 "Natives") label(3 "Foreign born") label(5 "Total") order(1 3 5) row(1))
graph export ${graphs}/state_directed_binned_emplgrowth_2000_2017.png, as(png) replace



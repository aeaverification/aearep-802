

*********************************************************************
* Birth place aggregation dofile to aggregate bpl into 23 consistent 
* region/countries for 1970-1980-1990-2000-2010(5yr ACS 2012)
* (gbasso@ucdavis.edu, 1/12/2015; revised May 27, 2020)
*********************************************************************

gen bplgroup = .

* US
replace bplgroup = 0 if bpl<=120|bpld==90011|bpld==90021
* Canada
replace bplgroup = 1 if bpl==150
* Rest of Americas 
replace bplgroup = 2 if (bpl>150&bpl<=199)|(bpl>200&bpl<=300)
* Mexico 
replace bplgroup = 3 if bpl==200
* Western Europe 
replace bplgroup = 4 if (bpl>=400&bpl<=429)|(bpl>=431&bpl<=440)|(bpl==450)|(bpl==453)
* Eastern Europe (including East Berlin and East Germany in 1980 & 1990)
replace bplgroup = 5 if bpl==430|(bpl>=451&bpl<=465)|bpld==45303|bpld==45340
* China
replace bplgroup = 6 if bpl==500
* Japan
replace bplgroup = 7 if bpl==501
* Korea
replace bplgroup = 8 if bpl==502
* Philippines
replace bplgroup = 9 if bpl==515
* Vietnam 
replace bplgroup = 10 if bpl==518
* India
replace bplgroup = 11 if bpl==521
* Rest of Asia
replace bplgroup = 12 if (bpl>=509&bpl<=514)|(bpl>=516&bpl<=517)|(bpl>=519&bpl<=520)|(bpl>=522&bpl<=599)
* Africa
replace bplgroup = 13 if bpl==600
* Oceania
replace bplgroup = 14 if bpl>=700&bpl<=710
* Other 
replace bplgroup = 15 if bpl==499|(bpl>=800&bpl<=950)


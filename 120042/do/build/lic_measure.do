* Josh Grelewicz, updated 6/22/2020, jbgrelewicz@ucdavis.edu
*
* This file creates measures of occupational licensing stringency at the 
* state-occupation level, similar to those used by Kleiner and Soltas (2019).
* 
* Databases used: cps_00014.dta
*                 This is an IPUMS extract of the CPS basic monthly survey 
*                 2015-2019.
*
* Output:         nat_lic_est_wt_per.dta
*                 This file contains, for every state, estimates of the fraction 
*                 of workers in each occupation possessing an occupational
*                 license.
#delimit ;

set more off;

clear all;

* Change this to your project directory *

cd "$projdir";

log using "$projdir/log/build/lic_measure.smcl", replace;

do "$projdir/do/build/clean_cps.do";

program empiricalbayes;   
    * Performs an empirical bayes adjustment on the state-occupation cell means,
	* shrinking the state-occupation level estimates towards the national
	* estimates for small state-occupation cells.  Constructs new adjusted 
	* estimates for each licensing measure.;
    
    args outcome;

	* Unadjusted licensing measure;
    gen `outcome' = `outcome'_L_os/N_os;

    by occ2010: gen `outcome'_num_cells = _N;
	
	* Calculate the total number licensed in each occupation nationally;
    egen `outcome'_L_o = sum(`outcome'_L_os), by(occ2010);

	* Estimate first two moments;
    gen `outcome'_mu_o = `outcome'_L_o/N_o;
    gen `outcome'_sq_dev = (`outcome'_L_os/N_os - `outcome'_L_o/N_o)^2;
    egen `outcome'_sum_sq_dev = sum(`outcome'_sq_dev), by(occ2010);
    gen `outcome'_var_o = `outcome'_sum_sq_dev / `outcome'_num_cells;
    drop `outcome'_sq_dev `outcome'_sum_sq_dev;
	
	* For each occupation, estimate prior beta distribution parameters for the  
	* share licensed in the occupation nationally using method of moments;
    gen `outcome'_alpha_o = `outcome'_mu_o * 
	    (((`outcome'_mu_o * (1 - `outcome'_mu_o))/ `outcome'_var_o) - 1) 
	    if `outcome'_var_o < `outcome'_mu_o * (1-`outcome'_mu_o);
		 
    gen `outcome'_beta_o = (1-`outcome'_mu_o) * 
	    (((`outcome'_mu_o * (1 - `outcome'_mu_o))/ `outcome'_var_o) - 1) 
		if `outcome'_var_o < `outcome'_mu_o * (1-`outcome'_mu_o);

    * Use the "uninformative" prior alpha = beta = .5 for occupations with 
	* parameters that cannot be estimated using method of moments.  
	* Create a quality flag to note these cases.;
    gen `outcome'_flag = (`outcome'_alpha_o == .);
    replace `outcome'_alpha_o = .5 if `outcome'_alpha_o == .;
    replace `outcome'_beta_o = .5 if `outcome'_beta_o == .;

    gen `outcome'_adj = (`outcome'_alpha_o + `outcome'_L_os)
	    /(`outcome'_alpha_o + `outcome'_beta_o + N_os);
end;

program socollapse;
	* This program collapses the dataset to the state-occupation cell level;
    args outcome1 outcome2 outcome3 outcome4 weight1;
	
    gen N_os = labforce == 2;
	
    * Create indicator for being licensed & in the labor force;
    foreach outcome in `outcome1' `outcome2' `outcome3' `outcome4'{;
        gen `outcome'_L_os = labforce == 2 & `outcome' == 1;
    };
	
	* Collapse to individual level;
    collapse (mean) N_os *L_os wtfinl occ2010 everchangeocc statefip, by(cpsidp);
 
	* Keep only people that stay in the same occupation;
	keep if everchangeocc == 0;
    
	* Keep only people that answers consistently across multiple surveys;
    foreach outcome in `outcome1' `outcome2' `outcome3' `outcome4'{;
        keep if N_os == 0 | N_os == 1;
        keep if `outcome'_L_os == 0 | `outcome'_L_os == 1;
    };

    collapse (sum) N_os *L_os `weight1', by(occ2010 statefip);
	
    egen N_o = sum(N_os), by(occ2010);
	
    foreach outcome in `outcome1' `outcome2' `outcome3' `outcome4'{;
        empiricalbayes `outcome';
    };

end;


program buildlicmeasure;
    * Constructs the state-occupation cell level licensing measure for the three
	* alternative licensing variables.;
    args name weight1;

    preserve;

    socollapse certified statecert licensed foreignborn "`weight'";

    save "$projdir/dta/lic_est/`name'.dta", replace;

    restore;
end;

*** Using full sample data
** using each obs
* weighted;
buildlicmeasure "lic_est_wt_per" "[pw = wtfinl]";
* unweighted;
buildlicmeasure "lic_est_uw_per";

*** Drop imputed undocumented workers when calculating licensed share
* ;
drop if undoc;
* weighted;
buildlicmeasure "nat_lic_est_wt_per" "[pw = wtfinl]"
* unweighted;
buildlicmeasure "nat_lic_est_uw_per";

capture log close;



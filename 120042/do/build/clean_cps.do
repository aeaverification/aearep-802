* Josh Grelewicz updated 6/22/2020
*
* Cleans loads and cleans basic monthly CPS extract from IPUMS

cd "$projdir/dta/cps_bms"

* load .dat.gz into Stata
do cps_00014.do

svyset [pw=wtfinl]

gen byte female = sex == 2
drop sex

gen inlf = (empstat == 10) | (empstat == 12) | (empstat == 21) | (empstat == 22)
gen emp =  (empstat == 10) | (empstat == 12)

* Clean up the licensing variables: profcert, statecert, and jobcert
* profcert:  Do you have a license or certification?
* statecert: is your license or certification granted by state/local/fed govt?
* jobcert:   is your license required for your job?

* Recode missing as . and affirmative response as 1
replace profcert = . if profcert == 99
replace statecert = . if statecert == 99
replace jobcert = . if jobcert == 99
replace profcert = profcert - 1
replace statecert = statecert - 1
replace jobcert = jobcert - 1

* By default, the universe of respondents for jobcert and statecert are people
* that reported having a license.  I recode people without licenses to 0s
replace statecert = 0 if statecert == . & profcert == 0
replace jobcert   = 0 if jobcert   == . & profcert == 0
replace jobcert   = . if year      < 2016

rename profcert certified
rename jobcert  licensed

label define cert 0 "Uncredentialed" 1 "Credentialed"
label define statecert 0 "Not cred. by state" 1 "Cred. by state"
label define lic 0 "Unlicensed" 1 "Licensed"

label values cert cert
label values statecert statecert
label values lic lic

* Create indicator variables for whether an individual ever
* 1. Changes their reported certification status
* 2. Changes their reported occupation
* 3. Changes their reported state

sort cpsidp year month
by cpsidp: gen misp = _n
by cpsidp: egen numinterviews = max(misp)

bysort cpsidp: egen ppcert = mean(statecert)
gen evercert = ppcert > 0

by cpsidp: gen changeocc = occ2010[_n] != occ2010[_n-1] if cpsidp[_n] == cpsidp[_n-1]
replace changeocc = 0 if changeocc == .
by cpsidp: gen everchangeocc = sum(changeocc)
replace everchangeocc = everchangeocc > 0

by cpsidp: gen changestate = statefip[_n] != statefip[_n-1] if cpsidp[_n] == cpsidp[_n-1]
replace changestate = 0 if changestate == .
by cpsidp: gen everchangestate = sum(changestate)
replace everchangestate = everchangestate > 0

bysort cpsidp: egen ppfemale = mean(female)

* Education
gen educcat = .
replace educcat = 0 if educ < 73
replace educcat = 1 if educ == 73
replace educcat = 2 if educ == 81
replace educcat = 3 if educ > 90 & educ < 100
replace educcat = 4 if educ == 111
replace educcat = 5 if educ > 122 & educ < 126

label define educcat 0 "HS dropout" 1 "HS grad" 2 "Some college" 3 "Associate's degree" 4 "Bachelor's degree" 5 "Graduate degree"
label values educcat educcat

gen byte undoc = (bpl >= 20000 & bpl <= 31000) & citizen == 5 & yrimmig > 1980
gen byte undoc_mx = (bpl == 20000) & citizen == 5 & yrimmig > 1980

label define undoc1 0 "Documented" 1 "Undocumented"
label values undoc undoc1
label values undoc_mx undoc1

gen foreignborn = 1
replace foreignborn = 0 if bpl < 10000

label variable inlf `"In labor force"'
label variable emp `"Employed"'
label variable female `"Female"'


